(cl:in-package remote_control-msg)
(cl:export '(BRAKE_THROTTLE-VAL
          BRAKE_THROTTLE
          STEERING-VAL
          STEERING
          HORN-VAL
          HORN
          OPEN_BUCKET-VAL
          OPEN_BUCKET
          CLOSE_BUCKET-VAL
          CLOSE_BUCKET
          BEAMS-VAL
          BEAMS
))