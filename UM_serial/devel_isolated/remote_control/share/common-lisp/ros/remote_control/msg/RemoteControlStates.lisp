; Auto-generated. Do not edit!


(cl:in-package remote_control-msg)


;//! \htmlinclude RemoteControlStates.msg.html

(cl:defclass <RemoteControlStates> (roslisp-msg-protocol:ros-message)
  ((brake_throttle
    :reader brake_throttle
    :initarg :brake_throttle
    :type cl:float
    :initform 0.0)
   (steering
    :reader steering
    :initarg :steering
    :type cl:float
    :initform 0.0)
   (horn
    :reader horn
    :initarg :horn
    :type cl:boolean
    :initform cl:nil)
   (open_bucket
    :reader open_bucket
    :initarg :open_bucket
    :type cl:boolean
    :initform cl:nil)
   (close_bucket
    :reader close_bucket
    :initarg :close_bucket
    :type cl:boolean
    :initform cl:nil)
   (beams
    :reader beams
    :initarg :beams
    :type cl:fixnum
    :initform 0))
)

(cl:defclass RemoteControlStates (<RemoteControlStates>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <RemoteControlStates>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'RemoteControlStates)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name remote_control-msg:<RemoteControlStates> is deprecated: use remote_control-msg:RemoteControlStates instead.")))

(cl:ensure-generic-function 'brake_throttle-val :lambda-list '(m))
(cl:defmethod brake_throttle-val ((m <RemoteControlStates>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader remote_control-msg:brake_throttle-val is deprecated.  Use remote_control-msg:brake_throttle instead.")
  (brake_throttle m))

(cl:ensure-generic-function 'steering-val :lambda-list '(m))
(cl:defmethod steering-val ((m <RemoteControlStates>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader remote_control-msg:steering-val is deprecated.  Use remote_control-msg:steering instead.")
  (steering m))

(cl:ensure-generic-function 'horn-val :lambda-list '(m))
(cl:defmethod horn-val ((m <RemoteControlStates>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader remote_control-msg:horn-val is deprecated.  Use remote_control-msg:horn instead.")
  (horn m))

(cl:ensure-generic-function 'open_bucket-val :lambda-list '(m))
(cl:defmethod open_bucket-val ((m <RemoteControlStates>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader remote_control-msg:open_bucket-val is deprecated.  Use remote_control-msg:open_bucket instead.")
  (open_bucket m))

(cl:ensure-generic-function 'close_bucket-val :lambda-list '(m))
(cl:defmethod close_bucket-val ((m <RemoteControlStates>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader remote_control-msg:close_bucket-val is deprecated.  Use remote_control-msg:close_bucket instead.")
  (close_bucket m))

(cl:ensure-generic-function 'beams-val :lambda-list '(m))
(cl:defmethod beams-val ((m <RemoteControlStates>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader remote_control-msg:beams-val is deprecated.  Use remote_control-msg:beams instead.")
  (beams m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <RemoteControlStates>) ostream)
  "Serializes a message object of type '<RemoteControlStates>"
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'brake_throttle))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'steering))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'horn) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'open_bucket) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'close_bucket) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'beams)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'beams)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <RemoteControlStates>) istream)
  "Deserializes a message object of type '<RemoteControlStates>"
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'brake_throttle) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'steering) (roslisp-utils:decode-single-float-bits bits)))
    (cl:setf (cl:slot-value msg 'horn) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'open_bucket) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'close_bucket) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'beams)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'beams)) (cl:read-byte istream))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<RemoteControlStates>)))
  "Returns string type for a message object of type '<RemoteControlStates>"
  "remote_control/RemoteControlStates")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'RemoteControlStates)))
  "Returns string type for a message object of type 'RemoteControlStates"
  "remote_control/RemoteControlStates")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<RemoteControlStates>)))
  "Returns md5sum for a message object of type '<RemoteControlStates>"
  "efd37443cf846b04d46650fcb60a01f2")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'RemoteControlStates)))
  "Returns md5sum for a message object of type 'RemoteControlStates"
  "efd37443cf846b04d46650fcb60a01f2")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<RemoteControlStates>)))
  "Returns full string definition for message of type '<RemoteControlStates>"
  (cl:format cl:nil "float32 brake_throttle 		# left joystick~%float32 steering		# right joystick~%~%#### byte 2 ####~%~%bool horn			# B3~%bool open_bucket		# B4~%bool close_bucket		# B5~%uint16 beams			# CH1 (OP2 = 0 switched-off; OFF = 1 low beam; OP1 = 2 high beam)~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'RemoteControlStates)))
  "Returns full string definition for message of type 'RemoteControlStates"
  (cl:format cl:nil "float32 brake_throttle 		# left joystick~%float32 steering		# right joystick~%~%#### byte 2 ####~%~%bool horn			# B3~%bool open_bucket		# B4~%bool close_bucket		# B5~%uint16 beams			# CH1 (OP2 = 0 switched-off; OFF = 1 low beam; OP1 = 2 high beam)~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <RemoteControlStates>))
  (cl:+ 0
     4
     4
     1
     1
     1
     2
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <RemoteControlStates>))
  "Converts a ROS message object to a list"
  (cl:list 'RemoteControlStates
    (cl:cons ':brake_throttle (brake_throttle msg))
    (cl:cons ':steering (steering msg))
    (cl:cons ':horn (horn msg))
    (cl:cons ':open_bucket (open_bucket msg))
    (cl:cons ':close_bucket (close_bucket msg))
    (cl:cons ':beams (beams msg))
))
