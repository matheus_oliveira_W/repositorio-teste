;; Auto-generated. Do not edit!


(when (boundp 'remote_control::RemoteControlStates)
  (if (not (find-package "REMOTE_CONTROL"))
    (make-package "REMOTE_CONTROL"))
  (shadow 'RemoteControlStates (find-package "REMOTE_CONTROL")))
(unless (find-package "REMOTE_CONTROL::REMOTECONTROLSTATES")
  (make-package "REMOTE_CONTROL::REMOTECONTROLSTATES"))

(in-package "ROS")
;;//! \htmlinclude RemoteControlStates.msg.html


(defclass remote_control::RemoteControlStates
  :super ros::object
  :slots (_brake_throttle _steering _horn _open_bucket _close_bucket _beams ))

(defmethod remote_control::RemoteControlStates
  (:init
   (&key
    ((:brake_throttle __brake_throttle) 0.0)
    ((:steering __steering) 0.0)
    ((:horn __horn) nil)
    ((:open_bucket __open_bucket) nil)
    ((:close_bucket __close_bucket) nil)
    ((:beams __beams) 0)
    )
   (send-super :init)
   (setq _brake_throttle (float __brake_throttle))
   (setq _steering (float __steering))
   (setq _horn __horn)
   (setq _open_bucket __open_bucket)
   (setq _close_bucket __close_bucket)
   (setq _beams (round __beams))
   self)
  (:brake_throttle
   (&optional __brake_throttle)
   (if __brake_throttle (setq _brake_throttle __brake_throttle)) _brake_throttle)
  (:steering
   (&optional __steering)
   (if __steering (setq _steering __steering)) _steering)
  (:horn
   (&optional __horn)
   (if __horn (setq _horn __horn)) _horn)
  (:open_bucket
   (&optional __open_bucket)
   (if __open_bucket (setq _open_bucket __open_bucket)) _open_bucket)
  (:close_bucket
   (&optional __close_bucket)
   (if __close_bucket (setq _close_bucket __close_bucket)) _close_bucket)
  (:beams
   (&optional __beams)
   (if __beams (setq _beams __beams)) _beams)
  (:serialization-length
   ()
   (+
    ;; float32 _brake_throttle
    4
    ;; float32 _steering
    4
    ;; bool _horn
    1
    ;; bool _open_bucket
    1
    ;; bool _close_bucket
    1
    ;; uint16 _beams
    2
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float32 _brake_throttle
       (sys::poke _brake_throttle (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _steering
       (sys::poke _steering (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; bool _horn
       (if _horn (write-byte -1 s) (write-byte 0 s))
     ;; bool _open_bucket
       (if _open_bucket (write-byte -1 s) (write-byte 0 s))
     ;; bool _close_bucket
       (if _close_bucket (write-byte -1 s) (write-byte 0 s))
     ;; uint16 _beams
       (write-word _beams s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float32 _brake_throttle
     (setq _brake_throttle (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _steering
     (setq _steering (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; bool _horn
     (setq _horn (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _open_bucket
     (setq _open_bucket (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _close_bucket
     (setq _close_bucket (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; uint16 _beams
     (setq _beams (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;;
   self)
  )

(setf (get remote_control::RemoteControlStates :md5sum-) "efd37443cf846b04d46650fcb60a01f2")
(setf (get remote_control::RemoteControlStates :datatype-) "remote_control/RemoteControlStates")
(setf (get remote_control::RemoteControlStates :definition-)
      "float32 brake_throttle 		# left joystick
float32 steering		# right joystick

#### byte 2 ####

bool horn			# B3
bool open_bucket		# B4
bool close_bucket		# B5
uint16 beams			# CH1 (OP2 = 0 switched-off; OFF = 1 low beam; OP1 = 2 high beam)


")



(provide :remote_control/RemoteControlStates "efd37443cf846b04d46650fcb60a01f2")


