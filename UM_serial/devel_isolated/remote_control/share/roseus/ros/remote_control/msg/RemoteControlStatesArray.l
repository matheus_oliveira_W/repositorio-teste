;; Auto-generated. Do not edit!


(when (boundp 'remote_control::RemoteControlStatesArray)
  (if (not (find-package "REMOTE_CONTROL"))
    (make-package "REMOTE_CONTROL"))
  (shadow 'RemoteControlStatesArray (find-package "REMOTE_CONTROL")))
(unless (find-package "REMOTE_CONTROL::REMOTECONTROLSTATESARRAY")
  (make-package "REMOTE_CONTROL::REMOTECONTROLSTATESARRAY"))

(in-package "ROS")
;;//! \htmlinclude RemoteControlStatesArray.msg.html


(defclass remote_control::RemoteControlStatesArray
  :super ros::object
  :slots (_info ))

(defmethod remote_control::RemoteControlStatesArray
  (:init
   (&key
    ((:info __info) (let (r) (dotimes (i 0) (push (instance remote_control::RemoteControlStates :init) r)) r))
    )
   (send-super :init)
   (setq _info __info)
   self)
  (:info
   (&rest __info)
   (if (keywordp (car __info))
       (send* _info __info)
     (progn
       (if __info (setq _info (car __info)))
       _info)))
  (:serialization-length
   ()
   (+
    ;; remote_control/RemoteControlStates[] _info
    (apply #'+ (send-all _info :serialization-length)) 4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; remote_control/RemoteControlStates[] _info
     (write-long (length _info) s)
     (dolist (elem _info)
       (send elem :serialize s)
       )
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; remote_control/RemoteControlStates[] _info
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _info (let (r) (dotimes (i n) (push (instance remote_control::RemoteControlStates :init) r)) r))
     (dolist (elem- _info)
     (send elem- :deserialize buf ptr-) (incf ptr- (send elem- :serialization-length))
     ))
   ;;
   self)
  )

(setf (get remote_control::RemoteControlStatesArray :md5sum-) "0547a26e3f37504504fac7b1153afa62")
(setf (get remote_control::RemoteControlStatesArray :datatype-) "remote_control/RemoteControlStatesArray")
(setf (get remote_control::RemoteControlStatesArray :definition-)
      "RemoteControlStates[] info #array of Remote Control messages

================================================================================
MSG: remote_control/RemoteControlStates
float32 brake_throttle 		# left joystick
float32 steering		# right joystick

#### byte 2 ####

bool horn			# B3
bool open_bucket		# B4
bool close_bucket		# B5
uint16 beams			# CH1 (OP2 = 0 switched-off; OFF = 1 low beam; OP1 = 2 high beam)


")



(provide :remote_control/RemoteControlStatesArray "0547a26e3f37504504fac7b1153afa62")


