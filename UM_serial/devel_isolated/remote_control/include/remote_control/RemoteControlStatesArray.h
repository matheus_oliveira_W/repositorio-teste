// Generated by gencpp from file remote_control/RemoteControlStatesArray.msg
// DO NOT EDIT!


#ifndef REMOTE_CONTROL_MESSAGE_REMOTECONTROLSTATESARRAY_H
#define REMOTE_CONTROL_MESSAGE_REMOTECONTROLSTATESARRAY_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>

#include <remote_control/RemoteControlStates.h>

namespace remote_control
{
template <class ContainerAllocator>
struct RemoteControlStatesArray_
{
  typedef RemoteControlStatesArray_<ContainerAllocator> Type;

  RemoteControlStatesArray_()
    : info()  {
    }
  RemoteControlStatesArray_(const ContainerAllocator& _alloc)
    : info(_alloc)  {
  (void)_alloc;
    }



   typedef std::vector< ::remote_control::RemoteControlStates_<ContainerAllocator> , typename ContainerAllocator::template rebind< ::remote_control::RemoteControlStates_<ContainerAllocator> >::other >  _info_type;
  _info_type info;





  typedef boost::shared_ptr< ::remote_control::RemoteControlStatesArray_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::remote_control::RemoteControlStatesArray_<ContainerAllocator> const> ConstPtr;

}; // struct RemoteControlStatesArray_

typedef ::remote_control::RemoteControlStatesArray_<std::allocator<void> > RemoteControlStatesArray;

typedef boost::shared_ptr< ::remote_control::RemoteControlStatesArray > RemoteControlStatesArrayPtr;
typedef boost::shared_ptr< ::remote_control::RemoteControlStatesArray const> RemoteControlStatesArrayConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::remote_control::RemoteControlStatesArray_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::remote_control::RemoteControlStatesArray_<ContainerAllocator> >::stream(s, "", v);
return s;
}

} // namespace remote_control

namespace ros
{
namespace message_traits
{



// BOOLTRAITS {'IsFixedSize': False, 'IsMessage': True, 'HasHeader': False}
// {'remote_control': ['/home/bplus/UM_serial/src/remote_control/remote_control_node/msg'], 'std_msgs': ['/opt/ros/kinetic/share/std_msgs/cmake/../msg']}

// !!!!!!!!!!! ['__class__', '__delattr__', '__dict__', '__doc__', '__eq__', '__format__', '__getattribute__', '__hash__', '__init__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', '_parsed_fields', 'constants', 'fields', 'full_name', 'has_header', 'header_present', 'names', 'package', 'parsed_fields', 'short_name', 'text', 'types']




template <class ContainerAllocator>
struct IsFixedSize< ::remote_control::RemoteControlStatesArray_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::remote_control::RemoteControlStatesArray_<ContainerAllocator> const>
  : FalseType
  { };

template <class ContainerAllocator>
struct IsMessage< ::remote_control::RemoteControlStatesArray_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::remote_control::RemoteControlStatesArray_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::remote_control::RemoteControlStatesArray_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::remote_control::RemoteControlStatesArray_<ContainerAllocator> const>
  : FalseType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::remote_control::RemoteControlStatesArray_<ContainerAllocator> >
{
  static const char* value()
  {
    return "0547a26e3f37504504fac7b1153afa62";
  }

  static const char* value(const ::remote_control::RemoteControlStatesArray_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0x0547a26e3f375045ULL;
  static const uint64_t static_value2 = 0x04fac7b1153afa62ULL;
};

template<class ContainerAllocator>
struct DataType< ::remote_control::RemoteControlStatesArray_<ContainerAllocator> >
{
  static const char* value()
  {
    return "remote_control/RemoteControlStatesArray";
  }

  static const char* value(const ::remote_control::RemoteControlStatesArray_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::remote_control::RemoteControlStatesArray_<ContainerAllocator> >
{
  static const char* value()
  {
    return "RemoteControlStates[] info #array of Remote Control messages\n\
\n\
================================================================================\n\
MSG: remote_control/RemoteControlStates\n\
float32 brake_throttle 		# left joystick\n\
float32 steering		# right joystick\n\
\n\
#### byte 2 ####\n\
\n\
bool horn			# B3\n\
bool open_bucket		# B4\n\
bool close_bucket		# B5\n\
uint16 beams			# CH1 (OP2 = 0 switched-off; OFF = 1 low beam; OP1 = 2 high beam)\n\
\n\
";
  }

  static const char* value(const ::remote_control::RemoteControlStatesArray_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::remote_control::RemoteControlStatesArray_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.info);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct RemoteControlStatesArray_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::remote_control::RemoteControlStatesArray_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::remote_control::RemoteControlStatesArray_<ContainerAllocator>& v)
  {
    s << indent << "info[]" << std::endl;
    for (size_t i = 0; i < v.info.size(); ++i)
    {
      s << indent << "  info[" << i << "]: ";
      s << std::endl;
      s << indent;
      Printer< ::remote_control::RemoteControlStates_<ContainerAllocator> >::stream(s, indent + "    ", v.info[i]);
    }
  }
};

} // namespace message_operations
} // namespace ros

#endif // REMOTE_CONTROL_MESSAGE_REMOTECONTROLSTATESARRAY_H
