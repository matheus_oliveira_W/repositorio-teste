; Auto-generated. Do not edit!


(cl:in-package path_follower-msg)


;//! \htmlinclude VehicleState.msg.html

(cl:defclass <VehicleState> (roslisp-msg-protocol:ros-message)
  ((x
    :reader x
    :initarg :x
    :type cl:float
    :initform 0.0)
   (y
    :reader y
    :initarg :y
    :type cl:float
    :initform 0.0)
   (alt
    :reader alt
    :initarg :alt
    :type cl:float
    :initform 0.0)
   (velocity
    :reader velocity
    :initarg :velocity
    :type cl:float
    :initform 0.0)
   (yaw_angle
    :reader yaw_angle
    :initarg :yaw_angle
    :type cl:float
    :initform 0.0)
   (heading
    :reader heading
    :initarg :heading
    :type cl:float
    :initform 0.0)
   (gear
    :reader gear
    :initarg :gear
    :type cl:integer
    :initform 0)
   (gps_data_valid
    :reader gps_data_valid
    :initarg :gps_data_valid
    :type cl:boolean
    :initform cl:nil)
   (x_rel
    :reader x_rel
    :initarg :x_rel
    :type cl:float
    :initform 0.0)
   (y_rel
    :reader y_rel
    :initarg :y_rel
    :type cl:float
    :initform 0.0)
   (rel_data_valid
    :reader rel_data_valid
    :initarg :rel_data_valid
    :type cl:boolean
    :initform cl:nil)
   (steering_angle
    :reader steering_angle
    :initarg :steering_angle
    :type cl:float
    :initform 0.0)
   (steering_data_valid
    :reader steering_data_valid
    :initarg :steering_data_valid
    :type cl:boolean
    :initform cl:nil)
   (obstacle_validation
    :reader obstacle_validation
    :initarg :obstacle_validation
    :type cl:boolean
    :initform cl:nil)
   (right_lidar_segmentation
    :reader right_lidar_segmentation
    :initarg :right_lidar_segmentation
    :type cl:integer
    :initform 0)
   (left_lidar_segmentation
    :reader left_lidar_segmentation
    :initarg :left_lidar_segmentation
    :type cl:integer
    :initform 0)
   (radar_segmentation
    :reader radar_segmentation
    :initarg :radar_segmentation
    :type cl:integer
    :initform 0)
   (right_lidar_feedback_x
    :reader right_lidar_feedback_x
    :initarg :right_lidar_feedback_x
    :type cl:float
    :initform 0.0)
   (left_lidar_feedback_x
    :reader left_lidar_feedback_x
    :initarg :left_lidar_feedback_x
    :type cl:float
    :initform 0.0)
   (radar_feedback_x
    :reader radar_feedback_x
    :initarg :radar_feedback_x
    :type cl:float
    :initform 0.0)
   (right_lidar_feedback_y
    :reader right_lidar_feedback_y
    :initarg :right_lidar_feedback_y
    :type cl:float
    :initform 0.0)
   (left_lidar_feedback_y
    :reader left_lidar_feedback_y
    :initarg :left_lidar_feedback_y
    :type cl:float
    :initform 0.0)
   (radar_feedback_y
    :reader radar_feedback_y
    :initarg :radar_feedback_y
    :type cl:float
    :initform 0.0)
   (emergency_brake
    :reader emergency_brake
    :initarg :emergency_brake
    :type cl:boolean
    :initform cl:nil)
   (radar_back_feedback_x
    :reader radar_back_feedback_x
    :initarg :radar_back_feedback_x
    :type cl:float
    :initform 0.0)
   (lidar_back_feedback_x
    :reader lidar_back_feedback_x
    :initarg :lidar_back_feedback_x
    :type cl:float
    :initform 0.0)
   (radar_back_feedback_y
    :reader radar_back_feedback_y
    :initarg :radar_back_feedback_y
    :type cl:float
    :initform 0.0)
   (lidar_back_feedback_y
    :reader lidar_back_feedback_y
    :initarg :lidar_back_feedback_y
    :type cl:float
    :initform 0.0)
   (back_emergency_brake
    :reader back_emergency_brake
    :initarg :back_emergency_brake
    :type cl:boolean
    :initform cl:nil)
   (lidar_back_detection
    :reader lidar_back_detection
    :initarg :lidar_back_detection
    :type cl:integer
    :initform 0)
   (back_radar_detection
    :reader back_radar_detection
    :initarg :back_radar_detection
    :type cl:integer
    :initform 0)
   (reverse_flag
    :reader reverse_flag
    :initarg :reverse_flag
    :type cl:integer
    :initform 0))
)

(cl:defclass VehicleState (<VehicleState>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <VehicleState>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'VehicleState)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name path_follower-msg:<VehicleState> is deprecated: use path_follower-msg:VehicleState instead.")))

(cl:ensure-generic-function 'x-val :lambda-list '(m))
(cl:defmethod x-val ((m <VehicleState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:x-val is deprecated.  Use path_follower-msg:x instead.")
  (x m))

(cl:ensure-generic-function 'y-val :lambda-list '(m))
(cl:defmethod y-val ((m <VehicleState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:y-val is deprecated.  Use path_follower-msg:y instead.")
  (y m))

(cl:ensure-generic-function 'alt-val :lambda-list '(m))
(cl:defmethod alt-val ((m <VehicleState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:alt-val is deprecated.  Use path_follower-msg:alt instead.")
  (alt m))

(cl:ensure-generic-function 'velocity-val :lambda-list '(m))
(cl:defmethod velocity-val ((m <VehicleState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:velocity-val is deprecated.  Use path_follower-msg:velocity instead.")
  (velocity m))

(cl:ensure-generic-function 'yaw_angle-val :lambda-list '(m))
(cl:defmethod yaw_angle-val ((m <VehicleState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:yaw_angle-val is deprecated.  Use path_follower-msg:yaw_angle instead.")
  (yaw_angle m))

(cl:ensure-generic-function 'heading-val :lambda-list '(m))
(cl:defmethod heading-val ((m <VehicleState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:heading-val is deprecated.  Use path_follower-msg:heading instead.")
  (heading m))

(cl:ensure-generic-function 'gear-val :lambda-list '(m))
(cl:defmethod gear-val ((m <VehicleState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:gear-val is deprecated.  Use path_follower-msg:gear instead.")
  (gear m))

(cl:ensure-generic-function 'gps_data_valid-val :lambda-list '(m))
(cl:defmethod gps_data_valid-val ((m <VehicleState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:gps_data_valid-val is deprecated.  Use path_follower-msg:gps_data_valid instead.")
  (gps_data_valid m))

(cl:ensure-generic-function 'x_rel-val :lambda-list '(m))
(cl:defmethod x_rel-val ((m <VehicleState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:x_rel-val is deprecated.  Use path_follower-msg:x_rel instead.")
  (x_rel m))

(cl:ensure-generic-function 'y_rel-val :lambda-list '(m))
(cl:defmethod y_rel-val ((m <VehicleState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:y_rel-val is deprecated.  Use path_follower-msg:y_rel instead.")
  (y_rel m))

(cl:ensure-generic-function 'rel_data_valid-val :lambda-list '(m))
(cl:defmethod rel_data_valid-val ((m <VehicleState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:rel_data_valid-val is deprecated.  Use path_follower-msg:rel_data_valid instead.")
  (rel_data_valid m))

(cl:ensure-generic-function 'steering_angle-val :lambda-list '(m))
(cl:defmethod steering_angle-val ((m <VehicleState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:steering_angle-val is deprecated.  Use path_follower-msg:steering_angle instead.")
  (steering_angle m))

(cl:ensure-generic-function 'steering_data_valid-val :lambda-list '(m))
(cl:defmethod steering_data_valid-val ((m <VehicleState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:steering_data_valid-val is deprecated.  Use path_follower-msg:steering_data_valid instead.")
  (steering_data_valid m))

(cl:ensure-generic-function 'obstacle_validation-val :lambda-list '(m))
(cl:defmethod obstacle_validation-val ((m <VehicleState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:obstacle_validation-val is deprecated.  Use path_follower-msg:obstacle_validation instead.")
  (obstacle_validation m))

(cl:ensure-generic-function 'right_lidar_segmentation-val :lambda-list '(m))
(cl:defmethod right_lidar_segmentation-val ((m <VehicleState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:right_lidar_segmentation-val is deprecated.  Use path_follower-msg:right_lidar_segmentation instead.")
  (right_lidar_segmentation m))

(cl:ensure-generic-function 'left_lidar_segmentation-val :lambda-list '(m))
(cl:defmethod left_lidar_segmentation-val ((m <VehicleState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:left_lidar_segmentation-val is deprecated.  Use path_follower-msg:left_lidar_segmentation instead.")
  (left_lidar_segmentation m))

(cl:ensure-generic-function 'radar_segmentation-val :lambda-list '(m))
(cl:defmethod radar_segmentation-val ((m <VehicleState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:radar_segmentation-val is deprecated.  Use path_follower-msg:radar_segmentation instead.")
  (radar_segmentation m))

(cl:ensure-generic-function 'right_lidar_feedback_x-val :lambda-list '(m))
(cl:defmethod right_lidar_feedback_x-val ((m <VehicleState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:right_lidar_feedback_x-val is deprecated.  Use path_follower-msg:right_lidar_feedback_x instead.")
  (right_lidar_feedback_x m))

(cl:ensure-generic-function 'left_lidar_feedback_x-val :lambda-list '(m))
(cl:defmethod left_lidar_feedback_x-val ((m <VehicleState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:left_lidar_feedback_x-val is deprecated.  Use path_follower-msg:left_lidar_feedback_x instead.")
  (left_lidar_feedback_x m))

(cl:ensure-generic-function 'radar_feedback_x-val :lambda-list '(m))
(cl:defmethod radar_feedback_x-val ((m <VehicleState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:radar_feedback_x-val is deprecated.  Use path_follower-msg:radar_feedback_x instead.")
  (radar_feedback_x m))

(cl:ensure-generic-function 'right_lidar_feedback_y-val :lambda-list '(m))
(cl:defmethod right_lidar_feedback_y-val ((m <VehicleState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:right_lidar_feedback_y-val is deprecated.  Use path_follower-msg:right_lidar_feedback_y instead.")
  (right_lidar_feedback_y m))

(cl:ensure-generic-function 'left_lidar_feedback_y-val :lambda-list '(m))
(cl:defmethod left_lidar_feedback_y-val ((m <VehicleState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:left_lidar_feedback_y-val is deprecated.  Use path_follower-msg:left_lidar_feedback_y instead.")
  (left_lidar_feedback_y m))

(cl:ensure-generic-function 'radar_feedback_y-val :lambda-list '(m))
(cl:defmethod radar_feedback_y-val ((m <VehicleState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:radar_feedback_y-val is deprecated.  Use path_follower-msg:radar_feedback_y instead.")
  (radar_feedback_y m))

(cl:ensure-generic-function 'emergency_brake-val :lambda-list '(m))
(cl:defmethod emergency_brake-val ((m <VehicleState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:emergency_brake-val is deprecated.  Use path_follower-msg:emergency_brake instead.")
  (emergency_brake m))

(cl:ensure-generic-function 'radar_back_feedback_x-val :lambda-list '(m))
(cl:defmethod radar_back_feedback_x-val ((m <VehicleState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:radar_back_feedback_x-val is deprecated.  Use path_follower-msg:radar_back_feedback_x instead.")
  (radar_back_feedback_x m))

(cl:ensure-generic-function 'lidar_back_feedback_x-val :lambda-list '(m))
(cl:defmethod lidar_back_feedback_x-val ((m <VehicleState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:lidar_back_feedback_x-val is deprecated.  Use path_follower-msg:lidar_back_feedback_x instead.")
  (lidar_back_feedback_x m))

(cl:ensure-generic-function 'radar_back_feedback_y-val :lambda-list '(m))
(cl:defmethod radar_back_feedback_y-val ((m <VehicleState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:radar_back_feedback_y-val is deprecated.  Use path_follower-msg:radar_back_feedback_y instead.")
  (radar_back_feedback_y m))

(cl:ensure-generic-function 'lidar_back_feedback_y-val :lambda-list '(m))
(cl:defmethod lidar_back_feedback_y-val ((m <VehicleState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:lidar_back_feedback_y-val is deprecated.  Use path_follower-msg:lidar_back_feedback_y instead.")
  (lidar_back_feedback_y m))

(cl:ensure-generic-function 'back_emergency_brake-val :lambda-list '(m))
(cl:defmethod back_emergency_brake-val ((m <VehicleState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:back_emergency_brake-val is deprecated.  Use path_follower-msg:back_emergency_brake instead.")
  (back_emergency_brake m))

(cl:ensure-generic-function 'lidar_back_detection-val :lambda-list '(m))
(cl:defmethod lidar_back_detection-val ((m <VehicleState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:lidar_back_detection-val is deprecated.  Use path_follower-msg:lidar_back_detection instead.")
  (lidar_back_detection m))

(cl:ensure-generic-function 'back_radar_detection-val :lambda-list '(m))
(cl:defmethod back_radar_detection-val ((m <VehicleState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:back_radar_detection-val is deprecated.  Use path_follower-msg:back_radar_detection instead.")
  (back_radar_detection m))

(cl:ensure-generic-function 'reverse_flag-val :lambda-list '(m))
(cl:defmethod reverse_flag-val ((m <VehicleState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:reverse_flag-val is deprecated.  Use path_follower-msg:reverse_flag instead.")
  (reverse_flag m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <VehicleState>) ostream)
  "Serializes a message object of type '<VehicleState>"
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'x))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'y))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'alt))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'velocity))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'yaw_angle))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'heading))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let* ((signed (cl:slot-value msg 'gear)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'gps_data_valid) 1 0)) ostream)
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'x_rel))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'y_rel))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'rel_data_valid) 1 0)) ostream)
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'steering_angle))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'steering_data_valid) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'obstacle_validation) 1 0)) ostream)
  (cl:let* ((signed (cl:slot-value msg 'right_lidar_segmentation)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'left_lidar_segmentation)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'radar_segmentation)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'right_lidar_feedback_x))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'left_lidar_feedback_x))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'radar_feedback_x))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'right_lidar_feedback_y))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'left_lidar_feedback_y))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'radar_feedback_y))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'emergency_brake) 1 0)) ostream)
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'radar_back_feedback_x))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'lidar_back_feedback_x))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'radar_back_feedback_y))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'lidar_back_feedback_y))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'back_emergency_brake) 1 0)) ostream)
  (cl:let* ((signed (cl:slot-value msg 'lidar_back_detection)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'back_radar_detection)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'reverse_flag)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <VehicleState>) istream)
  "Deserializes a message object of type '<VehicleState>"
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'x) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'y) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'alt) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'velocity) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'yaw_angle) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'heading) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'gear) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:setf (cl:slot-value msg 'gps_data_valid) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'x_rel) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'y_rel) (roslisp-utils:decode-single-float-bits bits)))
    (cl:setf (cl:slot-value msg 'rel_data_valid) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'steering_angle) (roslisp-utils:decode-single-float-bits bits)))
    (cl:setf (cl:slot-value msg 'steering_data_valid) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'obstacle_validation) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'right_lidar_segmentation) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'left_lidar_segmentation) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'radar_segmentation) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'right_lidar_feedback_x) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'left_lidar_feedback_x) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'radar_feedback_x) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'right_lidar_feedback_y) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'left_lidar_feedback_y) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'radar_feedback_y) (roslisp-utils:decode-single-float-bits bits)))
    (cl:setf (cl:slot-value msg 'emergency_brake) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'radar_back_feedback_x) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'lidar_back_feedback_x) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'radar_back_feedback_y) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'lidar_back_feedback_y) (roslisp-utils:decode-single-float-bits bits)))
    (cl:setf (cl:slot-value msg 'back_emergency_brake) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'lidar_back_detection) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'back_radar_detection) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'reverse_flag) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<VehicleState>)))
  "Returns string type for a message object of type '<VehicleState>"
  "path_follower/VehicleState")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'VehicleState)))
  "Returns string type for a message object of type 'VehicleState"
  "path_follower/VehicleState")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<VehicleState>)))
  "Returns md5sum for a message object of type '<VehicleState>"
  "4f0db555610e7509196beb6dc607a0c6")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'VehicleState)))
  "Returns md5sum for a message object of type 'VehicleState"
  "4f0db555610e7509196beb6dc607a0c6")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<VehicleState>)))
  "Returns full string definition for message of type '<VehicleState>"
  (cl:format cl:nil "### vehicle pose from GPS ###~%float32 x               # [m]   UTM easting~%float32 y               # [m]   UTM northing~%float32 alt~%float32 velocity        # [m/s] ~%float32 yaw_angle       # [rad]~%float32 heading		    # [rad] ~%int32 gear              # [] current gear of the truck~%bool gps_data_valid     # []    flag if values from GPS/IMU system are valid~%~%### vehicle position relative to the beginning of the (first) path ###~%float32 x_rel           # [m]   UTM easting ~%float32 y_rel           # [m]   UTM northing ~%bool rel_data_valid     # []    flag if relative position values are valid~%~%### vehicle steering angle (based on data from paravan_node) ###~%float32 steering_angle    # [rad] steering angle from Paravan system    ~%bool steering_data_valid  # []    flag if value of steering angle is valid ~%~%### sensor distance relative to the vehicle ###~%~%bool  obstacle_validation      #	~%int32 right_lidar_segmentation #~%int32 left_lidar_segmentation  #~%int32 radar_segmentation       #~%~%float32 right_lidar_feedback_x    # [m]~%float32 left_lidar_feedback_x     # [m]~%float32 radar_feedback_x	       # [m]~%float32 right_lidar_feedback_y    # [m]~%float32 left_lidar_feedback_y     # [m]~%float32 radar_feedback_y	       # [m]~%bool emergency_brake~%~%float32 radar_back_feedback_x	# [m]~%float32 lidar_back_feedback_x	# [m]~%float32 radar_back_feedback_y	# [m]~%float32 lidar_back_feedback_y	# [m]~%~%bool back_emergency_brake ~%int32 lidar_back_detection ~%int32 back_radar_detection ~%int32 reverse_flag~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'VehicleState)))
  "Returns full string definition for message of type 'VehicleState"
  (cl:format cl:nil "### vehicle pose from GPS ###~%float32 x               # [m]   UTM easting~%float32 y               # [m]   UTM northing~%float32 alt~%float32 velocity        # [m/s] ~%float32 yaw_angle       # [rad]~%float32 heading		    # [rad] ~%int32 gear              # [] current gear of the truck~%bool gps_data_valid     # []    flag if values from GPS/IMU system are valid~%~%### vehicle position relative to the beginning of the (first) path ###~%float32 x_rel           # [m]   UTM easting ~%float32 y_rel           # [m]   UTM northing ~%bool rel_data_valid     # []    flag if relative position values are valid~%~%### vehicle steering angle (based on data from paravan_node) ###~%float32 steering_angle    # [rad] steering angle from Paravan system    ~%bool steering_data_valid  # []    flag if value of steering angle is valid ~%~%### sensor distance relative to the vehicle ###~%~%bool  obstacle_validation      #	~%int32 right_lidar_segmentation #~%int32 left_lidar_segmentation  #~%int32 radar_segmentation       #~%~%float32 right_lidar_feedback_x    # [m]~%float32 left_lidar_feedback_x     # [m]~%float32 radar_feedback_x	       # [m]~%float32 right_lidar_feedback_y    # [m]~%float32 left_lidar_feedback_y     # [m]~%float32 radar_feedback_y	       # [m]~%bool emergency_brake~%~%float32 radar_back_feedback_x	# [m]~%float32 lidar_back_feedback_x	# [m]~%float32 radar_back_feedback_y	# [m]~%float32 lidar_back_feedback_y	# [m]~%~%bool back_emergency_brake ~%int32 lidar_back_detection ~%int32 back_radar_detection ~%int32 reverse_flag~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <VehicleState>))
  (cl:+ 0
     4
     4
     4
     4
     4
     4
     4
     1
     4
     4
     1
     4
     1
     1
     4
     4
     4
     4
     4
     4
     4
     4
     4
     1
     4
     4
     4
     4
     1
     4
     4
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <VehicleState>))
  "Converts a ROS message object to a list"
  (cl:list 'VehicleState
    (cl:cons ':x (x msg))
    (cl:cons ':y (y msg))
    (cl:cons ':alt (alt msg))
    (cl:cons ':velocity (velocity msg))
    (cl:cons ':yaw_angle (yaw_angle msg))
    (cl:cons ':heading (heading msg))
    (cl:cons ':gear (gear msg))
    (cl:cons ':gps_data_valid (gps_data_valid msg))
    (cl:cons ':x_rel (x_rel msg))
    (cl:cons ':y_rel (y_rel msg))
    (cl:cons ':rel_data_valid (rel_data_valid msg))
    (cl:cons ':steering_angle (steering_angle msg))
    (cl:cons ':steering_data_valid (steering_data_valid msg))
    (cl:cons ':obstacle_validation (obstacle_validation msg))
    (cl:cons ':right_lidar_segmentation (right_lidar_segmentation msg))
    (cl:cons ':left_lidar_segmentation (left_lidar_segmentation msg))
    (cl:cons ':radar_segmentation (radar_segmentation msg))
    (cl:cons ':right_lidar_feedback_x (right_lidar_feedback_x msg))
    (cl:cons ':left_lidar_feedback_x (left_lidar_feedback_x msg))
    (cl:cons ':radar_feedback_x (radar_feedback_x msg))
    (cl:cons ':right_lidar_feedback_y (right_lidar_feedback_y msg))
    (cl:cons ':left_lidar_feedback_y (left_lidar_feedback_y msg))
    (cl:cons ':radar_feedback_y (radar_feedback_y msg))
    (cl:cons ':emergency_brake (emergency_brake msg))
    (cl:cons ':radar_back_feedback_x (radar_back_feedback_x msg))
    (cl:cons ':lidar_back_feedback_x (lidar_back_feedback_x msg))
    (cl:cons ':radar_back_feedback_y (radar_back_feedback_y msg))
    (cl:cons ':lidar_back_feedback_y (lidar_back_feedback_y msg))
    (cl:cons ':back_emergency_brake (back_emergency_brake msg))
    (cl:cons ':lidar_back_detection (lidar_back_detection msg))
    (cl:cons ':back_radar_detection (back_radar_detection msg))
    (cl:cons ':reverse_flag (reverse_flag msg))
))
