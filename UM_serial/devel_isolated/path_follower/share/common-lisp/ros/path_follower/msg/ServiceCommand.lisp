; Auto-generated. Do not edit!


(cl:in-package path_follower-msg)


;//! \htmlinclude ServiceCommand.msg.html

(cl:defclass <ServiceCommand> (roslisp-msg-protocol:ros-message)
  ()
)

(cl:defclass ServiceCommand (<ServiceCommand>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <ServiceCommand>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'ServiceCommand)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name path_follower-msg:<ServiceCommand> is deprecated: use path_follower-msg:ServiceCommand instead.")))
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql '<ServiceCommand>)))
    "Constants for message type '<ServiceCommand>"
  '((:START_RECORDING . 0)
    (:STOP_RECORDING . 1)
    (:START_FOLLOWING . 10)
    (:STOP_FOLLOWING . 11)
    (:BRAKE_IN_IDLE . 12)
    (:SAVE_PATH_TO_FILE . 20)
    (:LOAD_PATH_FROM_FILE . 21)
    (:SET_GO_ZONE_RADIUS . 30)
    (:SET_WAIT_ZONE_RADIUS . 31)
    (:POST_PROCESS_PATH . 40)
    (:SHOW_GO_ZONE . 50)
    (:SHOW_WAIT_ZONE . 51)
    (:PLOT_MAPPING_DATA . 52)
    (:PLOT_PATH_DATA . 53)
    (:ADD_PATH . 60)
    (:REMOVE_PATH . 61)
    (:RENAME_PATH . 62)
    (:SET_ACTIVE_PATH . 63)
    (:BUCKET_CLOSED . 70)
    (:BUCKET_OPENED . 71)
    (:EX_LOCATION . 80)
    (:DP_LOCATION . 81)
    (:SET_REVERSE_MISSION . 90))
)
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql 'ServiceCommand)))
    "Constants for message type 'ServiceCommand"
  '((:START_RECORDING . 0)
    (:STOP_RECORDING . 1)
    (:START_FOLLOWING . 10)
    (:STOP_FOLLOWING . 11)
    (:BRAKE_IN_IDLE . 12)
    (:SAVE_PATH_TO_FILE . 20)
    (:LOAD_PATH_FROM_FILE . 21)
    (:SET_GO_ZONE_RADIUS . 30)
    (:SET_WAIT_ZONE_RADIUS . 31)
    (:POST_PROCESS_PATH . 40)
    (:SHOW_GO_ZONE . 50)
    (:SHOW_WAIT_ZONE . 51)
    (:PLOT_MAPPING_DATA . 52)
    (:PLOT_PATH_DATA . 53)
    (:ADD_PATH . 60)
    (:REMOVE_PATH . 61)
    (:RENAME_PATH . 62)
    (:SET_ACTIVE_PATH . 63)
    (:BUCKET_CLOSED . 70)
    (:BUCKET_OPENED . 71)
    (:EX_LOCATION . 80)
    (:DP_LOCATION . 81)
    (:SET_REVERSE_MISSION . 90))
)
(cl:defmethod roslisp-msg-protocol:serialize ((msg <ServiceCommand>) ostream)
  "Serializes a message object of type '<ServiceCommand>"
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <ServiceCommand>) istream)
  "Deserializes a message object of type '<ServiceCommand>"
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<ServiceCommand>)))
  "Returns string type for a message object of type '<ServiceCommand>"
  "path_follower/ServiceCommand")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'ServiceCommand)))
  "Returns string type for a message object of type 'ServiceCommand"
  "path_follower/ServiceCommand")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<ServiceCommand>)))
  "Returns md5sum for a message object of type '<ServiceCommand>"
  "97156beaade098dab21e61954a51527c")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'ServiceCommand)))
  "Returns md5sum for a message object of type 'ServiceCommand"
  "97156beaade098dab21e61954a51527c")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<ServiceCommand>)))
  "Returns full string definition for message of type '<ServiceCommand>"
  (cl:format cl:nil "# path recording~%uint8 	START_RECORDING  = 0	~%uint8 	STOP_RECORDING 	 = 1~%~%# path following~%uint8 	START_FOLLOWING  = 10~%uint8 	STOP_FOLLOWING   = 11~%~%# idle braking~%uint8   BRAKE_IN_IDLE    = 12~%~%# file handling~%uint8 	SAVE_PATH_TO_FILE    = 20~%uint8 	LOAD_PATH_FROM_FILE  = 21~%~%# zone definition~%uint8 	SET_GO_ZONE_RADIUS   = 30~%uint8 	SET_WAIT_ZONE_RADIUS = 31~%~%# path processing~%uint8 	POST_PROCESS_PATH = 40~%~%# visualization~%uint8   SHOW_GO_ZONE      = 50~%uint8   SHOW_WAIT_ZONE    = 51~%uint8   PLOT_MAPPING_DATA = 52~%uint8   PLOT_PATH_DATA    = 53~%~%# path selection~%uint8   ADD_PATH        = 60~%uint8   REMOVE_PATH     = 61~%uint8   RENAME_PATH     = 62~%uint8   SET_ACTIVE_PATH = 63~%~%# bucket~%uint8 	BUCKET_CLOSED	= 70~%uint8 	BUCKET_OPENED	= 71~%~%#excavator~%uint8 	EX_LOCATION 	= 80~%uint8 	DP_LOCATION 	= 81~%~%#reverse mission~%uint8   SET_REVERSE_MISSION = 90~%~%~%~%~%~%~%~%~%~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'ServiceCommand)))
  "Returns full string definition for message of type 'ServiceCommand"
  (cl:format cl:nil "# path recording~%uint8 	START_RECORDING  = 0	~%uint8 	STOP_RECORDING 	 = 1~%~%# path following~%uint8 	START_FOLLOWING  = 10~%uint8 	STOP_FOLLOWING   = 11~%~%# idle braking~%uint8   BRAKE_IN_IDLE    = 12~%~%# file handling~%uint8 	SAVE_PATH_TO_FILE    = 20~%uint8 	LOAD_PATH_FROM_FILE  = 21~%~%# zone definition~%uint8 	SET_GO_ZONE_RADIUS   = 30~%uint8 	SET_WAIT_ZONE_RADIUS = 31~%~%# path processing~%uint8 	POST_PROCESS_PATH = 40~%~%# visualization~%uint8   SHOW_GO_ZONE      = 50~%uint8   SHOW_WAIT_ZONE    = 51~%uint8   PLOT_MAPPING_DATA = 52~%uint8   PLOT_PATH_DATA    = 53~%~%# path selection~%uint8   ADD_PATH        = 60~%uint8   REMOVE_PATH     = 61~%uint8   RENAME_PATH     = 62~%uint8   SET_ACTIVE_PATH = 63~%~%# bucket~%uint8 	BUCKET_CLOSED	= 70~%uint8 	BUCKET_OPENED	= 71~%~%#excavator~%uint8 	EX_LOCATION 	= 80~%uint8 	DP_LOCATION 	= 81~%~%#reverse mission~%uint8   SET_REVERSE_MISSION = 90~%~%~%~%~%~%~%~%~%~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <ServiceCommand>))
  (cl:+ 0
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <ServiceCommand>))
  "Converts a ROS message object to a list"
  (cl:list 'ServiceCommand
))
