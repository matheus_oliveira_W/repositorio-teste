; Auto-generated. Do not edit!


(cl:in-package path_follower-msg)


;//! \htmlinclude PathInfoArray.msg.html

(cl:defclass <PathInfoArray> (roslisp-msg-protocol:ros-message)
  ((info
    :reader info
    :initarg :info
    :type (cl:vector path_follower-msg:PathInfo)
   :initform (cl:make-array 0 :element-type 'path_follower-msg:PathInfo :initial-element (cl:make-instance 'path_follower-msg:PathInfo))))
)

(cl:defclass PathInfoArray (<PathInfoArray>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <PathInfoArray>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'PathInfoArray)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name path_follower-msg:<PathInfoArray> is deprecated: use path_follower-msg:PathInfoArray instead.")))

(cl:ensure-generic-function 'info-val :lambda-list '(m))
(cl:defmethod info-val ((m <PathInfoArray>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:info-val is deprecated.  Use path_follower-msg:info instead.")
  (info m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <PathInfoArray>) ostream)
  "Serializes a message object of type '<PathInfoArray>"
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'info))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'info))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <PathInfoArray>) istream)
  "Deserializes a message object of type '<PathInfoArray>"
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'info) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'info)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'path_follower-msg:PathInfo))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<PathInfoArray>)))
  "Returns string type for a message object of type '<PathInfoArray>"
  "path_follower/PathInfoArray")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'PathInfoArray)))
  "Returns string type for a message object of type 'PathInfoArray"
  "path_follower/PathInfoArray")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<PathInfoArray>)))
  "Returns md5sum for a message object of type '<PathInfoArray>"
  "e0025102d88911d653ee6a41cb463a7c")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'PathInfoArray)))
  "Returns md5sum for a message object of type 'PathInfoArray"
  "e0025102d88911d653ee6a41cb463a7c")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<PathInfoArray>)))
  "Returns full string definition for message of type '<PathInfoArray>"
  (cl:format cl:nil "PathInfo[] info # array of PathInfo messages~%================================================================================~%MSG: path_follower/PathInfo~%time 	  recording_time        # time when path recording has started~%string    name                  # name of the path~%std_msgs/ColorRGBA visualization_color   # color of path visualization in rviz~%string 	  utm_zone_string       # UTM zone of the path (string representation)~%uint16    num_points            # number of points in the path~%float32   length                # path length (sum of all segment lengths)~%float32   go_zone_radius        # [m] radius of go-zone~%float32   wait_zone_radius      # [m] radius of wait-zone~%~%================================================================================~%MSG: std_msgs/ColorRGBA~%float32 r~%float32 g~%float32 b~%float32 a~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'PathInfoArray)))
  "Returns full string definition for message of type 'PathInfoArray"
  (cl:format cl:nil "PathInfo[] info # array of PathInfo messages~%================================================================================~%MSG: path_follower/PathInfo~%time 	  recording_time        # time when path recording has started~%string    name                  # name of the path~%std_msgs/ColorRGBA visualization_color   # color of path visualization in rviz~%string 	  utm_zone_string       # UTM zone of the path (string representation)~%uint16    num_points            # number of points in the path~%float32   length                # path length (sum of all segment lengths)~%float32   go_zone_radius        # [m] radius of go-zone~%float32   wait_zone_radius      # [m] radius of wait-zone~%~%================================================================================~%MSG: std_msgs/ColorRGBA~%float32 r~%float32 g~%float32 b~%float32 a~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <PathInfoArray>))
  (cl:+ 0
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'info) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <PathInfoArray>))
  "Converts a ROS message object to a list"
  (cl:list 'PathInfoArray
    (cl:cons ':info (info msg))
))
