(cl:in-package path_follower-msg)
(cl:export '(RECORDING_TIME-VAL
          RECORDING_TIME
          NAME-VAL
          NAME
          VISUALIZATION_COLOR-VAL
          VISUALIZATION_COLOR
          UTM_ZONE_STRING-VAL
          UTM_ZONE_STRING
          NUM_POINTS-VAL
          NUM_POINTS
          LENGTH-VAL
          LENGTH
          GO_ZONE_RADIUS-VAL
          GO_ZONE_RADIUS
          WAIT_ZONE_RADIUS-VAL
          WAIT_ZONE_RADIUS
))