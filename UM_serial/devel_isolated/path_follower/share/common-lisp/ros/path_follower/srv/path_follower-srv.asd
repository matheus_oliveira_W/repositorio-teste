
(cl:in-package :asdf)

(defsystem "path_follower-srv"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "PathFollowerService" :depends-on ("_package_PathFollowerService"))
    (:file "_package_PathFollowerService" :depends-on ("_package"))
  ))