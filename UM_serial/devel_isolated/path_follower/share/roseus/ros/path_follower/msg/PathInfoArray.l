;; Auto-generated. Do not edit!


(when (boundp 'path_follower::PathInfoArray)
  (if (not (find-package "PATH_FOLLOWER"))
    (make-package "PATH_FOLLOWER"))
  (shadow 'PathInfoArray (find-package "PATH_FOLLOWER")))
(unless (find-package "PATH_FOLLOWER::PATHINFOARRAY")
  (make-package "PATH_FOLLOWER::PATHINFOARRAY"))

(in-package "ROS")
;;//! \htmlinclude PathInfoArray.msg.html


(defclass path_follower::PathInfoArray
  :super ros::object
  :slots (_info ))

(defmethod path_follower::PathInfoArray
  (:init
   (&key
    ((:info __info) (let (r) (dotimes (i 0) (push (instance path_follower::PathInfo :init) r)) r))
    )
   (send-super :init)
   (setq _info __info)
   self)
  (:info
   (&rest __info)
   (if (keywordp (car __info))
       (send* _info __info)
     (progn
       (if __info (setq _info (car __info)))
       _info)))
  (:serialization-length
   ()
   (+
    ;; path_follower/PathInfo[] _info
    (apply #'+ (send-all _info :serialization-length)) 4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; path_follower/PathInfo[] _info
     (write-long (length _info) s)
     (dolist (elem _info)
       (send elem :serialize s)
       )
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; path_follower/PathInfo[] _info
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _info (let (r) (dotimes (i n) (push (instance path_follower::PathInfo :init) r)) r))
     (dolist (elem- _info)
     (send elem- :deserialize buf ptr-) (incf ptr- (send elem- :serialization-length))
     ))
   ;;
   self)
  )

(setf (get path_follower::PathInfoArray :md5sum-) "e0025102d88911d653ee6a41cb463a7c")
(setf (get path_follower::PathInfoArray :datatype-) "path_follower/PathInfoArray")
(setf (get path_follower::PathInfoArray :definition-)
      "PathInfo[] info # array of PathInfo messages
================================================================================
MSG: path_follower/PathInfo
time 	  recording_time        # time when path recording has started
string    name                  # name of the path
std_msgs/ColorRGBA visualization_color   # color of path visualization in rviz
string 	  utm_zone_string       # UTM zone of the path (string representation)
uint16    num_points            # number of points in the path
float32   length                # path length (sum of all segment lengths)
float32   go_zone_radius        # [m] radius of go-zone
float32   wait_zone_radius      # [m] radius of wait-zone

================================================================================
MSG: std_msgs/ColorRGBA
float32 r
float32 g
float32 b
float32 a

")



(provide :path_follower/PathInfoArray "e0025102d88911d653ee6a41cb463a7c")


