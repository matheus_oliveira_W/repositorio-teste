;; Auto-generated. Do not edit!


(when (boundp 'path_follower::SensorState)
  (if (not (find-package "PATH_FOLLOWER"))
    (make-package "PATH_FOLLOWER"))
  (shadow 'SensorState (find-package "PATH_FOLLOWER")))
(unless (find-package "PATH_FOLLOWER::SENSORSTATE")
  (make-package "PATH_FOLLOWER::SENSORSTATE"))

(in-package "ROS")
;;//! \htmlinclude SensorState.msg.html


(defclass path_follower::SensorState
  :super ros::object
  :slots (_warning_flag _emergency_flag _left_lidar_state _right_lidar_state _front_radar_state _follow_with_sensors _dist_x_0 _dist_y_0 _ang_0 _dist_x_1 _dist_y_1 _ang_1 _dist_brake _dist_curve _lap_deviation ))

(defmethod path_follower::SensorState
  (:init
   (&key
    ((:warning_flag __warning_flag) nil)
    ((:emergency_flag __emergency_flag) nil)
    ((:left_lidar_state __left_lidar_state) nil)
    ((:right_lidar_state __right_lidar_state) nil)
    ((:front_radar_state __front_radar_state) nil)
    ((:follow_with_sensors __follow_with_sensors) nil)
    ((:dist_x_0 __dist_x_0) 0.0)
    ((:dist_y_0 __dist_y_0) 0.0)
    ((:ang_0 __ang_0) 0.0)
    ((:dist_x_1 __dist_x_1) 0.0)
    ((:dist_y_1 __dist_y_1) 0.0)
    ((:ang_1 __ang_1) 0.0)
    ((:dist_brake __dist_brake) 0.0)
    ((:dist_curve __dist_curve) 0.0)
    ((:lap_deviation __lap_deviation) 0.0)
    )
   (send-super :init)
   (setq _warning_flag __warning_flag)
   (setq _emergency_flag __emergency_flag)
   (setq _left_lidar_state __left_lidar_state)
   (setq _right_lidar_state __right_lidar_state)
   (setq _front_radar_state __front_radar_state)
   (setq _follow_with_sensors __follow_with_sensors)
   (setq _dist_x_0 (float __dist_x_0))
   (setq _dist_y_0 (float __dist_y_0))
   (setq _ang_0 (float __ang_0))
   (setq _dist_x_1 (float __dist_x_1))
   (setq _dist_y_1 (float __dist_y_1))
   (setq _ang_1 (float __ang_1))
   (setq _dist_brake (float __dist_brake))
   (setq _dist_curve (float __dist_curve))
   (setq _lap_deviation (float __lap_deviation))
   self)
  (:warning_flag
   (&optional __warning_flag)
   (if __warning_flag (setq _warning_flag __warning_flag)) _warning_flag)
  (:emergency_flag
   (&optional __emergency_flag)
   (if __emergency_flag (setq _emergency_flag __emergency_flag)) _emergency_flag)
  (:left_lidar_state
   (&optional __left_lidar_state)
   (if __left_lidar_state (setq _left_lidar_state __left_lidar_state)) _left_lidar_state)
  (:right_lidar_state
   (&optional __right_lidar_state)
   (if __right_lidar_state (setq _right_lidar_state __right_lidar_state)) _right_lidar_state)
  (:front_radar_state
   (&optional __front_radar_state)
   (if __front_radar_state (setq _front_radar_state __front_radar_state)) _front_radar_state)
  (:follow_with_sensors
   (&optional __follow_with_sensors)
   (if __follow_with_sensors (setq _follow_with_sensors __follow_with_sensors)) _follow_with_sensors)
  (:dist_x_0
   (&optional __dist_x_0)
   (if __dist_x_0 (setq _dist_x_0 __dist_x_0)) _dist_x_0)
  (:dist_y_0
   (&optional __dist_y_0)
   (if __dist_y_0 (setq _dist_y_0 __dist_y_0)) _dist_y_0)
  (:ang_0
   (&optional __ang_0)
   (if __ang_0 (setq _ang_0 __ang_0)) _ang_0)
  (:dist_x_1
   (&optional __dist_x_1)
   (if __dist_x_1 (setq _dist_x_1 __dist_x_1)) _dist_x_1)
  (:dist_y_1
   (&optional __dist_y_1)
   (if __dist_y_1 (setq _dist_y_1 __dist_y_1)) _dist_y_1)
  (:ang_1
   (&optional __ang_1)
   (if __ang_1 (setq _ang_1 __ang_1)) _ang_1)
  (:dist_brake
   (&optional __dist_brake)
   (if __dist_brake (setq _dist_brake __dist_brake)) _dist_brake)
  (:dist_curve
   (&optional __dist_curve)
   (if __dist_curve (setq _dist_curve __dist_curve)) _dist_curve)
  (:lap_deviation
   (&optional __lap_deviation)
   (if __lap_deviation (setq _lap_deviation __lap_deviation)) _lap_deviation)
  (:serialization-length
   ()
   (+
    ;; bool _warning_flag
    1
    ;; bool _emergency_flag
    1
    ;; bool _left_lidar_state
    1
    ;; bool _right_lidar_state
    1
    ;; bool _front_radar_state
    1
    ;; bool _follow_with_sensors
    1
    ;; float32 _dist_x_0
    4
    ;; float32 _dist_y_0
    4
    ;; float32 _ang_0
    4
    ;; float32 _dist_x_1
    4
    ;; float32 _dist_y_1
    4
    ;; float32 _ang_1
    4
    ;; float32 _dist_brake
    4
    ;; float32 _dist_curve
    4
    ;; float32 _lap_deviation
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _warning_flag
       (if _warning_flag (write-byte -1 s) (write-byte 0 s))
     ;; bool _emergency_flag
       (if _emergency_flag (write-byte -1 s) (write-byte 0 s))
     ;; bool _left_lidar_state
       (if _left_lidar_state (write-byte -1 s) (write-byte 0 s))
     ;; bool _right_lidar_state
       (if _right_lidar_state (write-byte -1 s) (write-byte 0 s))
     ;; bool _front_radar_state
       (if _front_radar_state (write-byte -1 s) (write-byte 0 s))
     ;; bool _follow_with_sensors
       (if _follow_with_sensors (write-byte -1 s) (write-byte 0 s))
     ;; float32 _dist_x_0
       (sys::poke _dist_x_0 (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _dist_y_0
       (sys::poke _dist_y_0 (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _ang_0
       (sys::poke _ang_0 (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _dist_x_1
       (sys::poke _dist_x_1 (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _dist_y_1
       (sys::poke _dist_y_1 (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _ang_1
       (sys::poke _ang_1 (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _dist_brake
       (sys::poke _dist_brake (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _dist_curve
       (sys::poke _dist_curve (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _lap_deviation
       (sys::poke _lap_deviation (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _warning_flag
     (setq _warning_flag (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _emergency_flag
     (setq _emergency_flag (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _left_lidar_state
     (setq _left_lidar_state (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _right_lidar_state
     (setq _right_lidar_state (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _front_radar_state
     (setq _front_radar_state (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _follow_with_sensors
     (setq _follow_with_sensors (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; float32 _dist_x_0
     (setq _dist_x_0 (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _dist_y_0
     (setq _dist_y_0 (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _ang_0
     (setq _ang_0 (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _dist_x_1
     (setq _dist_x_1 (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _dist_y_1
     (setq _dist_y_1 (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _ang_1
     (setq _ang_1 (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _dist_brake
     (setq _dist_brake (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _dist_curve
     (setq _dist_curve (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _lap_deviation
     (setq _lap_deviation (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get path_follower::SensorState :md5sum-) "77399bad35ed57ed01c5dee8095579a7")
(setf (get path_follower::SensorState :datatype-) "path_follower/SensorState")
(setf (get path_follower::SensorState :definition-)
      "# This message constains the state of the sensors

bool warning_flag
bool emergency_flag
bool left_lidar_state
bool right_lidar_state
bool front_radar_state
bool follow_with_sensors

float32 dist_x_0
float32 dist_y_0
float32 ang_0
float32 dist_x_1
float32 dist_y_1
float32 ang_1

float32 dist_brake
float32 dist_curve
float32 lap_deviation

")



(provide :path_follower/SensorState "77399bad35ed57ed01c5dee8095579a7")


