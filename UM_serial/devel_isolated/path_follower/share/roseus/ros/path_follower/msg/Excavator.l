;; Auto-generated. Do not edit!


(when (boundp 'path_follower::Excavator)
  (if (not (find-package "PATH_FOLLOWER"))
    (make-package "PATH_FOLLOWER"))
  (shadow 'Excavator (find-package "PATH_FOLLOWER")))
(unless (find-package "PATH_FOLLOWER::EXCAVATOR")
  (make-package "PATH_FOLLOWER::EXCAVATOR"))

(in-package "ROS")
;;//! \htmlinclude Excavator.msg.html


(defclass path_follower::Excavator
  :super ros::object
  :slots (_ex_latitude _ex_longitude ))

(defmethod path_follower::Excavator
  (:init
   (&key
    ((:ex_latitude __ex_latitude) 0.0)
    ((:ex_longitude __ex_longitude) 0.0)
    )
   (send-super :init)
   (setq _ex_latitude (float __ex_latitude))
   (setq _ex_longitude (float __ex_longitude))
   self)
  (:ex_latitude
   (&optional __ex_latitude)
   (if __ex_latitude (setq _ex_latitude __ex_latitude)) _ex_latitude)
  (:ex_longitude
   (&optional __ex_longitude)
   (if __ex_longitude (setq _ex_longitude __ex_longitude)) _ex_longitude)
  (:serialization-length
   ()
   (+
    ;; float32 _ex_latitude
    4
    ;; float32 _ex_longitude
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float32 _ex_latitude
       (sys::poke _ex_latitude (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _ex_longitude
       (sys::poke _ex_longitude (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float32 _ex_latitude
     (setq _ex_latitude (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _ex_longitude
     (setq _ex_longitude (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get path_follower::Excavator :md5sum-) "d4836f677d2197f5049d039e568a5f7c")
(setf (get path_follower::Excavator :datatype-) "path_follower/Excavator")
(setf (get path_follower::Excavator :definition-)
      "float32 ex_latitude
float32 ex_longitude

")



(provide :path_follower/Excavator "d4836f677d2197f5049d039e568a5f7c")


