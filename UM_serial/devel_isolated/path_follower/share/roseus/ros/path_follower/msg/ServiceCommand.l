;; Auto-generated. Do not edit!


(when (boundp 'path_follower::ServiceCommand)
  (if (not (find-package "PATH_FOLLOWER"))
    (make-package "PATH_FOLLOWER"))
  (shadow 'ServiceCommand (find-package "PATH_FOLLOWER")))
(unless (find-package "PATH_FOLLOWER::SERVICECOMMAND")
  (make-package "PATH_FOLLOWER::SERVICECOMMAND"))

(in-package "ROS")
;;//! \htmlinclude ServiceCommand.msg.html


(intern "*START_RECORDING*" (find-package "PATH_FOLLOWER::SERVICECOMMAND"))
(shadow '*START_RECORDING* (find-package "PATH_FOLLOWER::SERVICECOMMAND"))
(defconstant path_follower::ServiceCommand::*START_RECORDING* 0)
(intern "*STOP_RECORDING*" (find-package "PATH_FOLLOWER::SERVICECOMMAND"))
(shadow '*STOP_RECORDING* (find-package "PATH_FOLLOWER::SERVICECOMMAND"))
(defconstant path_follower::ServiceCommand::*STOP_RECORDING* 1)
(intern "*START_FOLLOWING*" (find-package "PATH_FOLLOWER::SERVICECOMMAND"))
(shadow '*START_FOLLOWING* (find-package "PATH_FOLLOWER::SERVICECOMMAND"))
(defconstant path_follower::ServiceCommand::*START_FOLLOWING* 10)
(intern "*STOP_FOLLOWING*" (find-package "PATH_FOLLOWER::SERVICECOMMAND"))
(shadow '*STOP_FOLLOWING* (find-package "PATH_FOLLOWER::SERVICECOMMAND"))
(defconstant path_follower::ServiceCommand::*STOP_FOLLOWING* 11)
(intern "*BRAKE_IN_IDLE*" (find-package "PATH_FOLLOWER::SERVICECOMMAND"))
(shadow '*BRAKE_IN_IDLE* (find-package "PATH_FOLLOWER::SERVICECOMMAND"))
(defconstant path_follower::ServiceCommand::*BRAKE_IN_IDLE* 12)
(intern "*SAVE_PATH_TO_FILE*" (find-package "PATH_FOLLOWER::SERVICECOMMAND"))
(shadow '*SAVE_PATH_TO_FILE* (find-package "PATH_FOLLOWER::SERVICECOMMAND"))
(defconstant path_follower::ServiceCommand::*SAVE_PATH_TO_FILE* 20)
(intern "*LOAD_PATH_FROM_FILE*" (find-package "PATH_FOLLOWER::SERVICECOMMAND"))
(shadow '*LOAD_PATH_FROM_FILE* (find-package "PATH_FOLLOWER::SERVICECOMMAND"))
(defconstant path_follower::ServiceCommand::*LOAD_PATH_FROM_FILE* 21)
(intern "*SET_GO_ZONE_RADIUS*" (find-package "PATH_FOLLOWER::SERVICECOMMAND"))
(shadow '*SET_GO_ZONE_RADIUS* (find-package "PATH_FOLLOWER::SERVICECOMMAND"))
(defconstant path_follower::ServiceCommand::*SET_GO_ZONE_RADIUS* 30)
(intern "*SET_WAIT_ZONE_RADIUS*" (find-package "PATH_FOLLOWER::SERVICECOMMAND"))
(shadow '*SET_WAIT_ZONE_RADIUS* (find-package "PATH_FOLLOWER::SERVICECOMMAND"))
(defconstant path_follower::ServiceCommand::*SET_WAIT_ZONE_RADIUS* 31)
(intern "*POST_PROCESS_PATH*" (find-package "PATH_FOLLOWER::SERVICECOMMAND"))
(shadow '*POST_PROCESS_PATH* (find-package "PATH_FOLLOWER::SERVICECOMMAND"))
(defconstant path_follower::ServiceCommand::*POST_PROCESS_PATH* 40)
(intern "*SHOW_GO_ZONE*" (find-package "PATH_FOLLOWER::SERVICECOMMAND"))
(shadow '*SHOW_GO_ZONE* (find-package "PATH_FOLLOWER::SERVICECOMMAND"))
(defconstant path_follower::ServiceCommand::*SHOW_GO_ZONE* 50)
(intern "*SHOW_WAIT_ZONE*" (find-package "PATH_FOLLOWER::SERVICECOMMAND"))
(shadow '*SHOW_WAIT_ZONE* (find-package "PATH_FOLLOWER::SERVICECOMMAND"))
(defconstant path_follower::ServiceCommand::*SHOW_WAIT_ZONE* 51)
(intern "*PLOT_MAPPING_DATA*" (find-package "PATH_FOLLOWER::SERVICECOMMAND"))
(shadow '*PLOT_MAPPING_DATA* (find-package "PATH_FOLLOWER::SERVICECOMMAND"))
(defconstant path_follower::ServiceCommand::*PLOT_MAPPING_DATA* 52)
(intern "*PLOT_PATH_DATA*" (find-package "PATH_FOLLOWER::SERVICECOMMAND"))
(shadow '*PLOT_PATH_DATA* (find-package "PATH_FOLLOWER::SERVICECOMMAND"))
(defconstant path_follower::ServiceCommand::*PLOT_PATH_DATA* 53)
(intern "*ADD_PATH*" (find-package "PATH_FOLLOWER::SERVICECOMMAND"))
(shadow '*ADD_PATH* (find-package "PATH_FOLLOWER::SERVICECOMMAND"))
(defconstant path_follower::ServiceCommand::*ADD_PATH* 60)
(intern "*REMOVE_PATH*" (find-package "PATH_FOLLOWER::SERVICECOMMAND"))
(shadow '*REMOVE_PATH* (find-package "PATH_FOLLOWER::SERVICECOMMAND"))
(defconstant path_follower::ServiceCommand::*REMOVE_PATH* 61)
(intern "*RENAME_PATH*" (find-package "PATH_FOLLOWER::SERVICECOMMAND"))
(shadow '*RENAME_PATH* (find-package "PATH_FOLLOWER::SERVICECOMMAND"))
(defconstant path_follower::ServiceCommand::*RENAME_PATH* 62)
(intern "*SET_ACTIVE_PATH*" (find-package "PATH_FOLLOWER::SERVICECOMMAND"))
(shadow '*SET_ACTIVE_PATH* (find-package "PATH_FOLLOWER::SERVICECOMMAND"))
(defconstant path_follower::ServiceCommand::*SET_ACTIVE_PATH* 63)
(intern "*BUCKET_CLOSED*" (find-package "PATH_FOLLOWER::SERVICECOMMAND"))
(shadow '*BUCKET_CLOSED* (find-package "PATH_FOLLOWER::SERVICECOMMAND"))
(defconstant path_follower::ServiceCommand::*BUCKET_CLOSED* 70)
(intern "*BUCKET_OPENED*" (find-package "PATH_FOLLOWER::SERVICECOMMAND"))
(shadow '*BUCKET_OPENED* (find-package "PATH_FOLLOWER::SERVICECOMMAND"))
(defconstant path_follower::ServiceCommand::*BUCKET_OPENED* 71)
(intern "*EX_LOCATION*" (find-package "PATH_FOLLOWER::SERVICECOMMAND"))
(shadow '*EX_LOCATION* (find-package "PATH_FOLLOWER::SERVICECOMMAND"))
(defconstant path_follower::ServiceCommand::*EX_LOCATION* 80)
(intern "*DP_LOCATION*" (find-package "PATH_FOLLOWER::SERVICECOMMAND"))
(shadow '*DP_LOCATION* (find-package "PATH_FOLLOWER::SERVICECOMMAND"))
(defconstant path_follower::ServiceCommand::*DP_LOCATION* 81)
(intern "*SET_REVERSE_MISSION*" (find-package "PATH_FOLLOWER::SERVICECOMMAND"))
(shadow '*SET_REVERSE_MISSION* (find-package "PATH_FOLLOWER::SERVICECOMMAND"))
(defconstant path_follower::ServiceCommand::*SET_REVERSE_MISSION* 90)
(defclass path_follower::ServiceCommand
  :super ros::object
  :slots ())

(defmethod path_follower::ServiceCommand
  (:init
   (&key
    )
   (send-super :init)
   self)
  (:serialization-length
   ()
   (+
    0
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;;
   self)
  )

(setf (get path_follower::ServiceCommand :md5sum-) "97156beaade098dab21e61954a51527c")
(setf (get path_follower::ServiceCommand :datatype-) "path_follower/ServiceCommand")
(setf (get path_follower::ServiceCommand :definition-)
      "# path recording
uint8 	START_RECORDING  = 0	
uint8 	STOP_RECORDING 	 = 1

# path following
uint8 	START_FOLLOWING  = 10
uint8 	STOP_FOLLOWING   = 11

# idle braking
uint8   BRAKE_IN_IDLE    = 12

# file handling
uint8 	SAVE_PATH_TO_FILE    = 20
uint8 	LOAD_PATH_FROM_FILE  = 21

# zone definition
uint8 	SET_GO_ZONE_RADIUS   = 30
uint8 	SET_WAIT_ZONE_RADIUS = 31

# path processing
uint8 	POST_PROCESS_PATH = 40

# visualization
uint8   SHOW_GO_ZONE      = 50
uint8   SHOW_WAIT_ZONE    = 51
uint8   PLOT_MAPPING_DATA = 52
uint8   PLOT_PATH_DATA    = 53

# path selection
uint8   ADD_PATH        = 60
uint8   REMOVE_PATH     = 61
uint8   RENAME_PATH     = 62
uint8   SET_ACTIVE_PATH = 63

# bucket
uint8 	BUCKET_CLOSED	= 70
uint8 	BUCKET_OPENED	= 71

#excavator
uint8 	EX_LOCATION 	= 80
uint8 	DP_LOCATION 	= 81

#reverse mission
uint8   SET_REVERSE_MISSION = 90











")



(provide :path_follower/ServiceCommand "97156beaade098dab21e61954a51527c")


