;; Auto-generated. Do not edit!


(when (boundp 'path_follower::VehicleState)
  (if (not (find-package "PATH_FOLLOWER"))
    (make-package "PATH_FOLLOWER"))
  (shadow 'VehicleState (find-package "PATH_FOLLOWER")))
(unless (find-package "PATH_FOLLOWER::VEHICLESTATE")
  (make-package "PATH_FOLLOWER::VEHICLESTATE"))

(in-package "ROS")
;;//! \htmlinclude VehicleState.msg.html


(defclass path_follower::VehicleState
  :super ros::object
  :slots (_x _y _alt _velocity _yaw_angle _heading _gear _gps_data_valid _x_rel _y_rel _rel_data_valid _steering_angle _steering_data_valid _obstacle_validation _right_lidar_segmentation _left_lidar_segmentation _radar_segmentation _right_lidar_feedback_x _left_lidar_feedback_x _radar_feedback_x _right_lidar_feedback_y _left_lidar_feedback_y _radar_feedback_y _emergency_brake _radar_back_feedback_x _lidar_back_feedback_x _radar_back_feedback_y _lidar_back_feedback_y _back_emergency_brake _lidar_back_detection _back_radar_detection _reverse_flag ))

(defmethod path_follower::VehicleState
  (:init
   (&key
    ((:x __x) 0.0)
    ((:y __y) 0.0)
    ((:alt __alt) 0.0)
    ((:velocity __velocity) 0.0)
    ((:yaw_angle __yaw_angle) 0.0)
    ((:heading __heading) 0.0)
    ((:gear __gear) 0)
    ((:gps_data_valid __gps_data_valid) nil)
    ((:x_rel __x_rel) 0.0)
    ((:y_rel __y_rel) 0.0)
    ((:rel_data_valid __rel_data_valid) nil)
    ((:steering_angle __steering_angle) 0.0)
    ((:steering_data_valid __steering_data_valid) nil)
    ((:obstacle_validation __obstacle_validation) nil)
    ((:right_lidar_segmentation __right_lidar_segmentation) 0)
    ((:left_lidar_segmentation __left_lidar_segmentation) 0)
    ((:radar_segmentation __radar_segmentation) 0)
    ((:right_lidar_feedback_x __right_lidar_feedback_x) 0.0)
    ((:left_lidar_feedback_x __left_lidar_feedback_x) 0.0)
    ((:radar_feedback_x __radar_feedback_x) 0.0)
    ((:right_lidar_feedback_y __right_lidar_feedback_y) 0.0)
    ((:left_lidar_feedback_y __left_lidar_feedback_y) 0.0)
    ((:radar_feedback_y __radar_feedback_y) 0.0)
    ((:emergency_brake __emergency_brake) nil)
    ((:radar_back_feedback_x __radar_back_feedback_x) 0.0)
    ((:lidar_back_feedback_x __lidar_back_feedback_x) 0.0)
    ((:radar_back_feedback_y __radar_back_feedback_y) 0.0)
    ((:lidar_back_feedback_y __lidar_back_feedback_y) 0.0)
    ((:back_emergency_brake __back_emergency_brake) nil)
    ((:lidar_back_detection __lidar_back_detection) 0)
    ((:back_radar_detection __back_radar_detection) 0)
    ((:reverse_flag __reverse_flag) 0)
    )
   (send-super :init)
   (setq _x (float __x))
   (setq _y (float __y))
   (setq _alt (float __alt))
   (setq _velocity (float __velocity))
   (setq _yaw_angle (float __yaw_angle))
   (setq _heading (float __heading))
   (setq _gear (round __gear))
   (setq _gps_data_valid __gps_data_valid)
   (setq _x_rel (float __x_rel))
   (setq _y_rel (float __y_rel))
   (setq _rel_data_valid __rel_data_valid)
   (setq _steering_angle (float __steering_angle))
   (setq _steering_data_valid __steering_data_valid)
   (setq _obstacle_validation __obstacle_validation)
   (setq _right_lidar_segmentation (round __right_lidar_segmentation))
   (setq _left_lidar_segmentation (round __left_lidar_segmentation))
   (setq _radar_segmentation (round __radar_segmentation))
   (setq _right_lidar_feedback_x (float __right_lidar_feedback_x))
   (setq _left_lidar_feedback_x (float __left_lidar_feedback_x))
   (setq _radar_feedback_x (float __radar_feedback_x))
   (setq _right_lidar_feedback_y (float __right_lidar_feedback_y))
   (setq _left_lidar_feedback_y (float __left_lidar_feedback_y))
   (setq _radar_feedback_y (float __radar_feedback_y))
   (setq _emergency_brake __emergency_brake)
   (setq _radar_back_feedback_x (float __radar_back_feedback_x))
   (setq _lidar_back_feedback_x (float __lidar_back_feedback_x))
   (setq _radar_back_feedback_y (float __radar_back_feedback_y))
   (setq _lidar_back_feedback_y (float __lidar_back_feedback_y))
   (setq _back_emergency_brake __back_emergency_brake)
   (setq _lidar_back_detection (round __lidar_back_detection))
   (setq _back_radar_detection (round __back_radar_detection))
   (setq _reverse_flag (round __reverse_flag))
   self)
  (:x
   (&optional __x)
   (if __x (setq _x __x)) _x)
  (:y
   (&optional __y)
   (if __y (setq _y __y)) _y)
  (:alt
   (&optional __alt)
   (if __alt (setq _alt __alt)) _alt)
  (:velocity
   (&optional __velocity)
   (if __velocity (setq _velocity __velocity)) _velocity)
  (:yaw_angle
   (&optional __yaw_angle)
   (if __yaw_angle (setq _yaw_angle __yaw_angle)) _yaw_angle)
  (:heading
   (&optional __heading)
   (if __heading (setq _heading __heading)) _heading)
  (:gear
   (&optional __gear)
   (if __gear (setq _gear __gear)) _gear)
  (:gps_data_valid
   (&optional __gps_data_valid)
   (if __gps_data_valid (setq _gps_data_valid __gps_data_valid)) _gps_data_valid)
  (:x_rel
   (&optional __x_rel)
   (if __x_rel (setq _x_rel __x_rel)) _x_rel)
  (:y_rel
   (&optional __y_rel)
   (if __y_rel (setq _y_rel __y_rel)) _y_rel)
  (:rel_data_valid
   (&optional __rel_data_valid)
   (if __rel_data_valid (setq _rel_data_valid __rel_data_valid)) _rel_data_valid)
  (:steering_angle
   (&optional __steering_angle)
   (if __steering_angle (setq _steering_angle __steering_angle)) _steering_angle)
  (:steering_data_valid
   (&optional __steering_data_valid)
   (if __steering_data_valid (setq _steering_data_valid __steering_data_valid)) _steering_data_valid)
  (:obstacle_validation
   (&optional __obstacle_validation)
   (if __obstacle_validation (setq _obstacle_validation __obstacle_validation)) _obstacle_validation)
  (:right_lidar_segmentation
   (&optional __right_lidar_segmentation)
   (if __right_lidar_segmentation (setq _right_lidar_segmentation __right_lidar_segmentation)) _right_lidar_segmentation)
  (:left_lidar_segmentation
   (&optional __left_lidar_segmentation)
   (if __left_lidar_segmentation (setq _left_lidar_segmentation __left_lidar_segmentation)) _left_lidar_segmentation)
  (:radar_segmentation
   (&optional __radar_segmentation)
   (if __radar_segmentation (setq _radar_segmentation __radar_segmentation)) _radar_segmentation)
  (:right_lidar_feedback_x
   (&optional __right_lidar_feedback_x)
   (if __right_lidar_feedback_x (setq _right_lidar_feedback_x __right_lidar_feedback_x)) _right_lidar_feedback_x)
  (:left_lidar_feedback_x
   (&optional __left_lidar_feedback_x)
   (if __left_lidar_feedback_x (setq _left_lidar_feedback_x __left_lidar_feedback_x)) _left_lidar_feedback_x)
  (:radar_feedback_x
   (&optional __radar_feedback_x)
   (if __radar_feedback_x (setq _radar_feedback_x __radar_feedback_x)) _radar_feedback_x)
  (:right_lidar_feedback_y
   (&optional __right_lidar_feedback_y)
   (if __right_lidar_feedback_y (setq _right_lidar_feedback_y __right_lidar_feedback_y)) _right_lidar_feedback_y)
  (:left_lidar_feedback_y
   (&optional __left_lidar_feedback_y)
   (if __left_lidar_feedback_y (setq _left_lidar_feedback_y __left_lidar_feedback_y)) _left_lidar_feedback_y)
  (:radar_feedback_y
   (&optional __radar_feedback_y)
   (if __radar_feedback_y (setq _radar_feedback_y __radar_feedback_y)) _radar_feedback_y)
  (:emergency_brake
   (&optional __emergency_brake)
   (if __emergency_brake (setq _emergency_brake __emergency_brake)) _emergency_brake)
  (:radar_back_feedback_x
   (&optional __radar_back_feedback_x)
   (if __radar_back_feedback_x (setq _radar_back_feedback_x __radar_back_feedback_x)) _radar_back_feedback_x)
  (:lidar_back_feedback_x
   (&optional __lidar_back_feedback_x)
   (if __lidar_back_feedback_x (setq _lidar_back_feedback_x __lidar_back_feedback_x)) _lidar_back_feedback_x)
  (:radar_back_feedback_y
   (&optional __radar_back_feedback_y)
   (if __radar_back_feedback_y (setq _radar_back_feedback_y __radar_back_feedback_y)) _radar_back_feedback_y)
  (:lidar_back_feedback_y
   (&optional __lidar_back_feedback_y)
   (if __lidar_back_feedback_y (setq _lidar_back_feedback_y __lidar_back_feedback_y)) _lidar_back_feedback_y)
  (:back_emergency_brake
   (&optional __back_emergency_brake)
   (if __back_emergency_brake (setq _back_emergency_brake __back_emergency_brake)) _back_emergency_brake)
  (:lidar_back_detection
   (&optional __lidar_back_detection)
   (if __lidar_back_detection (setq _lidar_back_detection __lidar_back_detection)) _lidar_back_detection)
  (:back_radar_detection
   (&optional __back_radar_detection)
   (if __back_radar_detection (setq _back_radar_detection __back_radar_detection)) _back_radar_detection)
  (:reverse_flag
   (&optional __reverse_flag)
   (if __reverse_flag (setq _reverse_flag __reverse_flag)) _reverse_flag)
  (:serialization-length
   ()
   (+
    ;; float32 _x
    4
    ;; float32 _y
    4
    ;; float32 _alt
    4
    ;; float32 _velocity
    4
    ;; float32 _yaw_angle
    4
    ;; float32 _heading
    4
    ;; int32 _gear
    4
    ;; bool _gps_data_valid
    1
    ;; float32 _x_rel
    4
    ;; float32 _y_rel
    4
    ;; bool _rel_data_valid
    1
    ;; float32 _steering_angle
    4
    ;; bool _steering_data_valid
    1
    ;; bool _obstacle_validation
    1
    ;; int32 _right_lidar_segmentation
    4
    ;; int32 _left_lidar_segmentation
    4
    ;; int32 _radar_segmentation
    4
    ;; float32 _right_lidar_feedback_x
    4
    ;; float32 _left_lidar_feedback_x
    4
    ;; float32 _radar_feedback_x
    4
    ;; float32 _right_lidar_feedback_y
    4
    ;; float32 _left_lidar_feedback_y
    4
    ;; float32 _radar_feedback_y
    4
    ;; bool _emergency_brake
    1
    ;; float32 _radar_back_feedback_x
    4
    ;; float32 _lidar_back_feedback_x
    4
    ;; float32 _radar_back_feedback_y
    4
    ;; float32 _lidar_back_feedback_y
    4
    ;; bool _back_emergency_brake
    1
    ;; int32 _lidar_back_detection
    4
    ;; int32 _back_radar_detection
    4
    ;; int32 _reverse_flag
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float32 _x
       (sys::poke _x (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _y
       (sys::poke _y (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _alt
       (sys::poke _alt (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _velocity
       (sys::poke _velocity (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _yaw_angle
       (sys::poke _yaw_angle (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _heading
       (sys::poke _heading (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; int32 _gear
       (write-long _gear s)
     ;; bool _gps_data_valid
       (if _gps_data_valid (write-byte -1 s) (write-byte 0 s))
     ;; float32 _x_rel
       (sys::poke _x_rel (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _y_rel
       (sys::poke _y_rel (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; bool _rel_data_valid
       (if _rel_data_valid (write-byte -1 s) (write-byte 0 s))
     ;; float32 _steering_angle
       (sys::poke _steering_angle (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; bool _steering_data_valid
       (if _steering_data_valid (write-byte -1 s) (write-byte 0 s))
     ;; bool _obstacle_validation
       (if _obstacle_validation (write-byte -1 s) (write-byte 0 s))
     ;; int32 _right_lidar_segmentation
       (write-long _right_lidar_segmentation s)
     ;; int32 _left_lidar_segmentation
       (write-long _left_lidar_segmentation s)
     ;; int32 _radar_segmentation
       (write-long _radar_segmentation s)
     ;; float32 _right_lidar_feedback_x
       (sys::poke _right_lidar_feedback_x (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _left_lidar_feedback_x
       (sys::poke _left_lidar_feedback_x (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _radar_feedback_x
       (sys::poke _radar_feedback_x (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _right_lidar_feedback_y
       (sys::poke _right_lidar_feedback_y (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _left_lidar_feedback_y
       (sys::poke _left_lidar_feedback_y (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _radar_feedback_y
       (sys::poke _radar_feedback_y (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; bool _emergency_brake
       (if _emergency_brake (write-byte -1 s) (write-byte 0 s))
     ;; float32 _radar_back_feedback_x
       (sys::poke _radar_back_feedback_x (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _lidar_back_feedback_x
       (sys::poke _lidar_back_feedback_x (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _radar_back_feedback_y
       (sys::poke _radar_back_feedback_y (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _lidar_back_feedback_y
       (sys::poke _lidar_back_feedback_y (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; bool _back_emergency_brake
       (if _back_emergency_brake (write-byte -1 s) (write-byte 0 s))
     ;; int32 _lidar_back_detection
       (write-long _lidar_back_detection s)
     ;; int32 _back_radar_detection
       (write-long _back_radar_detection s)
     ;; int32 _reverse_flag
       (write-long _reverse_flag s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float32 _x
     (setq _x (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _y
     (setq _y (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _alt
     (setq _alt (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _velocity
     (setq _velocity (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _yaw_angle
     (setq _yaw_angle (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _heading
     (setq _heading (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; int32 _gear
     (setq _gear (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; bool _gps_data_valid
     (setq _gps_data_valid (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; float32 _x_rel
     (setq _x_rel (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _y_rel
     (setq _y_rel (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; bool _rel_data_valid
     (setq _rel_data_valid (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; float32 _steering_angle
     (setq _steering_angle (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; bool _steering_data_valid
     (setq _steering_data_valid (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _obstacle_validation
     (setq _obstacle_validation (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; int32 _right_lidar_segmentation
     (setq _right_lidar_segmentation (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; int32 _left_lidar_segmentation
     (setq _left_lidar_segmentation (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; int32 _radar_segmentation
     (setq _radar_segmentation (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; float32 _right_lidar_feedback_x
     (setq _right_lidar_feedback_x (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _left_lidar_feedback_x
     (setq _left_lidar_feedback_x (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _radar_feedback_x
     (setq _radar_feedback_x (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _right_lidar_feedback_y
     (setq _right_lidar_feedback_y (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _left_lidar_feedback_y
     (setq _left_lidar_feedback_y (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _radar_feedback_y
     (setq _radar_feedback_y (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; bool _emergency_brake
     (setq _emergency_brake (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; float32 _radar_back_feedback_x
     (setq _radar_back_feedback_x (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _lidar_back_feedback_x
     (setq _lidar_back_feedback_x (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _radar_back_feedback_y
     (setq _radar_back_feedback_y (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _lidar_back_feedback_y
     (setq _lidar_back_feedback_y (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; bool _back_emergency_brake
     (setq _back_emergency_brake (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; int32 _lidar_back_detection
     (setq _lidar_back_detection (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; int32 _back_radar_detection
     (setq _back_radar_detection (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; int32 _reverse_flag
     (setq _reverse_flag (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get path_follower::VehicleState :md5sum-) "4f0db555610e7509196beb6dc607a0c6")
(setf (get path_follower::VehicleState :datatype-) "path_follower/VehicleState")
(setf (get path_follower::VehicleState :definition-)
      "### vehicle pose from GPS ###
float32 x               # [m]   UTM easting
float32 y               # [m]   UTM northing
float32 alt
float32 velocity        # [m/s] 
float32 yaw_angle       # [rad]
float32 heading		    # [rad] 
int32 gear              # [] current gear of the truck
bool gps_data_valid     # []    flag if values from GPS/IMU system are valid

### vehicle position relative to the beginning of the (first) path ###
float32 x_rel           # [m]   UTM easting 
float32 y_rel           # [m]   UTM northing 
bool rel_data_valid     # []    flag if relative position values are valid

### vehicle steering angle (based on data from paravan_node) ###
float32 steering_angle    # [rad] steering angle from Paravan system    
bool steering_data_valid  # []    flag if value of steering angle is valid 

### sensor distance relative to the vehicle ###

bool  obstacle_validation      #	
int32 right_lidar_segmentation #
int32 left_lidar_segmentation  #
int32 radar_segmentation       #

float32 right_lidar_feedback_x    # [m]
float32 left_lidar_feedback_x     # [m]
float32 radar_feedback_x	       # [m]
float32 right_lidar_feedback_y    # [m]
float32 left_lidar_feedback_y     # [m]
float32 radar_feedback_y	       # [m]
bool emergency_brake

float32 radar_back_feedback_x	# [m]
float32 lidar_back_feedback_x	# [m]
float32 radar_back_feedback_y	# [m]
float32 lidar_back_feedback_y	# [m]

bool back_emergency_brake 
int32 lidar_back_detection 
int32 back_radar_detection 
int32 reverse_flag

")



(provide :path_follower/VehicleState "4f0db555610e7509196beb6dc607a0c6")


