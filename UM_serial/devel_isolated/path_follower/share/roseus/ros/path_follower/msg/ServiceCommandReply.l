;; Auto-generated. Do not edit!


(when (boundp 'path_follower::ServiceCommandReply)
  (if (not (find-package "PATH_FOLLOWER"))
    (make-package "PATH_FOLLOWER"))
  (shadow 'ServiceCommandReply (find-package "PATH_FOLLOWER")))
(unless (find-package "PATH_FOLLOWER::SERVICECOMMANDREPLY")
  (make-package "PATH_FOLLOWER::SERVICECOMMANDREPLY"))

(in-package "ROS")
;;//! \htmlinclude ServiceCommandReply.msg.html


(intern "*SUCCESS*" (find-package "PATH_FOLLOWER::SERVICECOMMANDREPLY"))
(shadow '*SUCCESS* (find-package "PATH_FOLLOWER::SERVICECOMMANDREPLY"))
(defconstant path_follower::ServiceCommandReply::*SUCCESS* 0)
(intern "*FAIL_OXTS_OFFLINE*" (find-package "PATH_FOLLOWER::SERVICECOMMANDREPLY"))
(shadow '*FAIL_OXTS_OFFLINE* (find-package "PATH_FOLLOWER::SERVICECOMMANDREPLY"))
(defconstant path_follower::ServiceCommandReply::*FAIL_OXTS_OFFLINE* 10)
(intern "*FAIL_OXTS_NO_DATA*" (find-package "PATH_FOLLOWER::SERVICECOMMANDREPLY"))
(shadow '*FAIL_OXTS_NO_DATA* (find-package "PATH_FOLLOWER::SERVICECOMMANDREPLY"))
(defconstant path_follower::ServiceCommandReply::*FAIL_OXTS_NO_DATA* 11)
(intern "*FAIL_OXTS_DATA_BUT_NO_FIX*" (find-package "PATH_FOLLOWER::SERVICECOMMANDREPLY"))
(shadow '*FAIL_OXTS_DATA_BUT_NO_FIX* (find-package "PATH_FOLLOWER::SERVICECOMMANDREPLY"))
(defconstant path_follower::ServiceCommandReply::*FAIL_OXTS_DATA_BUT_NO_FIX* 12)
(intern "*FAIL_UEM_OFFLINE*" (find-package "PATH_FOLLOWER::SERVICECOMMANDREPLY"))
(shadow '*FAIL_UEM_OFFLINE* (find-package "PATH_FOLLOWER::SERVICECOMMANDREPLY"))
(defconstant path_follower::ServiceCommandReply::*FAIL_UEM_OFFLINE* 13)
(intern "*FAIL_UEM_NO_DATA*" (find-package "PATH_FOLLOWER::SERVICECOMMANDREPLY"))
(shadow '*FAIL_UEM_NO_DATA* (find-package "PATH_FOLLOWER::SERVICECOMMANDREPLY"))
(defconstant path_follower::ServiceCommandReply::*FAIL_UEM_NO_DATA* 14)
(intern "*FAIL_UEM_NO_REMOTE_CONTROL*" (find-package "PATH_FOLLOWER::SERVICECOMMANDREPLY"))
(shadow '*FAIL_UEM_NO_REMOTE_CONTROL* (find-package "PATH_FOLLOWER::SERVICECOMMANDREPLY"))
(defconstant path_follower::ServiceCommandReply::*FAIL_UEM_NO_REMOTE_CONTROL* 15)
(intern "*FAIL_STATE_LOGIC*" (find-package "PATH_FOLLOWER::SERVICECOMMANDREPLY"))
(shadow '*FAIL_STATE_LOGIC* (find-package "PATH_FOLLOWER::SERVICECOMMANDREPLY"))
(defconstant path_follower::ServiceCommandReply::*FAIL_STATE_LOGIC* 16)
(intern "*FAIL_NO_PATH_AVAILABLE*" (find-package "PATH_FOLLOWER::SERVICECOMMANDREPLY"))
(shadow '*FAIL_NO_PATH_AVAILABLE* (find-package "PATH_FOLLOWER::SERVICECOMMANDREPLY"))
(defconstant path_follower::ServiceCommandReply::*FAIL_NO_PATH_AVAILABLE* 17)
(intern "*FAIL_PATH_TOO_SHORT*" (find-package "PATH_FOLLOWER::SERVICECOMMANDREPLY"))
(shadow '*FAIL_PATH_TOO_SHORT* (find-package "PATH_FOLLOWER::SERVICECOMMANDREPLY"))
(defconstant path_follower::ServiceCommandReply::*FAIL_PATH_TOO_SHORT* 18)
(intern "*FAIL_LOADING_PATH_FROM_FILE*" (find-package "PATH_FOLLOWER::SERVICECOMMANDREPLY"))
(shadow '*FAIL_LOADING_PATH_FROM_FILE* (find-package "PATH_FOLLOWER::SERVICECOMMANDREPLY"))
(defconstant path_follower::ServiceCommandReply::*FAIL_LOADING_PATH_FROM_FILE* 19)
(intern "*FAIL_VEHCILE_NOT_IN_GO_ZONE*" (find-package "PATH_FOLLOWER::SERVICECOMMANDREPLY"))
(shadow '*FAIL_VEHCILE_NOT_IN_GO_ZONE* (find-package "PATH_FOLLOWER::SERVICECOMMANDREPLY"))
(defconstant path_follower::ServiceCommandReply::*FAIL_VEHCILE_NOT_IN_GO_ZONE* 20)
(intern "*FAIL_OLD_PLOT_STILL_ACTIVE*" (find-package "PATH_FOLLOWER::SERVICECOMMANDREPLY"))
(shadow '*FAIL_OLD_PLOT_STILL_ACTIVE* (find-package "PATH_FOLLOWER::SERVICECOMMANDREPLY"))
(defconstant path_follower::ServiceCommandReply::*FAIL_OLD_PLOT_STILL_ACTIVE* 21)
(intern "*FAIL_BUCKET_IS_OPENED*" (find-package "PATH_FOLLOWER::SERVICECOMMANDREPLY"))
(shadow '*FAIL_BUCKET_IS_OPENED* (find-package "PATH_FOLLOWER::SERVICECOMMANDREPLY"))
(defconstant path_follower::ServiceCommandReply::*FAIL_BUCKET_IS_OPENED* 22)
(intern "*WARN_UEM_OFFLINE*" (find-package "PATH_FOLLOWER::SERVICECOMMANDREPLY"))
(shadow '*WARN_UEM_OFFLINE* (find-package "PATH_FOLLOWER::SERVICECOMMANDREPLY"))
(defconstant path_follower::ServiceCommandReply::*WARN_UEM_OFFLINE* 30)
(intern "*WARN_UEM_NO_DATA*" (find-package "PATH_FOLLOWER::SERVICECOMMANDREPLY"))
(shadow '*WARN_UEM_NO_DATA* (find-package "PATH_FOLLOWER::SERVICECOMMANDREPLY"))
(defconstant path_follower::ServiceCommandReply::*WARN_UEM_NO_DATA* 31)
(intern "*WARN_UEM_NO_REMOTE_CONTROL*" (find-package "PATH_FOLLOWER::SERVICECOMMANDREPLY"))
(shadow '*WARN_UEM_NO_REMOTE_CONTROL* (find-package "PATH_FOLLOWER::SERVICECOMMANDREPLY"))
(defconstant path_follower::ServiceCommandReply::*WARN_UEM_NO_REMOTE_CONTROL* 32)
(intern "*SERVICE_ERROR*" (find-package "PATH_FOLLOWER::SERVICECOMMANDREPLY"))
(shadow '*SERVICE_ERROR* (find-package "PATH_FOLLOWER::SERVICECOMMANDREPLY"))
(defconstant path_follower::ServiceCommandReply::*SERVICE_ERROR* 66)
(defclass path_follower::ServiceCommandReply
  :super ros::object
  :slots ())

(defmethod path_follower::ServiceCommandReply
  (:init
   (&key
    )
   (send-super :init)
   self)
  (:serialization-length
   ()
   (+
    0
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;;
   self)
  )

(setf (get path_follower::ServiceCommandReply :md5sum-) "f05070682928bf756746c7dc8b2dcb1e")
(setf (get path_follower::ServiceCommandReply :datatype-) "path_follower/ServiceCommandReply")
(setf (get path_follower::ServiceCommandReply :definition-)
      "# success reply
uint8 	SUCCESS = 0

# fail replies
uint8 	FAIL_OXTS_OFFLINE              = 10
uint8 	FAIL_OXTS_NO_DATA              = 11
uint8  	FAIL_OXTS_DATA_BUT_NO_FIX      = 12
uint8 	FAIL_UEM_OFFLINE               = 13
uint8 	FAIL_UEM_NO_DATA               = 14
uint8   FAIL_UEM_NO_REMOTE_CONTROL     = 15
uint8   FAIL_STATE_LOGIC               = 16
uint8   FAIL_NO_PATH_AVAILABLE         = 17
uint8   FAIL_PATH_TOO_SHORT            = 18
uint8   FAIL_LOADING_PATH_FROM_FILE    = 19
uint8   FAIL_VEHCILE_NOT_IN_GO_ZONE    = 20
uint8   FAIL_OLD_PLOT_STILL_ACTIVE     = 21
uint8   FAIL_BUCKET_IS_OPENED          = 22

# warn replies
uint8   WARN_UEM_OFFLINE               = 30
uint8   WARN_UEM_NO_DATA               = 31
uint8   WARN_UEM_NO_REMOTE_CONTROL     = 32

# service error
uint8 SERVICE_ERROR = 66



")



(provide :path_follower/ServiceCommandReply "f05070682928bf756746c7dc8b2dcb1e")


