// Auto-generated. Do not edit!

// (in-package path_follower.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class VehicleState {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.x = null;
      this.y = null;
      this.alt = null;
      this.velocity = null;
      this.yaw_angle = null;
      this.heading = null;
      this.gear = null;
      this.gps_data_valid = null;
      this.x_rel = null;
      this.y_rel = null;
      this.rel_data_valid = null;
      this.steering_angle = null;
      this.steering_data_valid = null;
      this.obstacle_validation = null;
      this.right_lidar_segmentation = null;
      this.left_lidar_segmentation = null;
      this.radar_segmentation = null;
      this.right_lidar_feedback_x = null;
      this.left_lidar_feedback_x = null;
      this.radar_feedback_x = null;
      this.right_lidar_feedback_y = null;
      this.left_lidar_feedback_y = null;
      this.radar_feedback_y = null;
      this.emergency_brake = null;
      this.radar_back_feedback_x = null;
      this.lidar_back_feedback_x = null;
      this.radar_back_feedback_y = null;
      this.lidar_back_feedback_y = null;
      this.back_emergency_brake = null;
      this.lidar_back_detection = null;
      this.back_radar_detection = null;
      this.reverse_flag = null;
    }
    else {
      if (initObj.hasOwnProperty('x')) {
        this.x = initObj.x
      }
      else {
        this.x = 0.0;
      }
      if (initObj.hasOwnProperty('y')) {
        this.y = initObj.y
      }
      else {
        this.y = 0.0;
      }
      if (initObj.hasOwnProperty('alt')) {
        this.alt = initObj.alt
      }
      else {
        this.alt = 0.0;
      }
      if (initObj.hasOwnProperty('velocity')) {
        this.velocity = initObj.velocity
      }
      else {
        this.velocity = 0.0;
      }
      if (initObj.hasOwnProperty('yaw_angle')) {
        this.yaw_angle = initObj.yaw_angle
      }
      else {
        this.yaw_angle = 0.0;
      }
      if (initObj.hasOwnProperty('heading')) {
        this.heading = initObj.heading
      }
      else {
        this.heading = 0.0;
      }
      if (initObj.hasOwnProperty('gear')) {
        this.gear = initObj.gear
      }
      else {
        this.gear = 0;
      }
      if (initObj.hasOwnProperty('gps_data_valid')) {
        this.gps_data_valid = initObj.gps_data_valid
      }
      else {
        this.gps_data_valid = false;
      }
      if (initObj.hasOwnProperty('x_rel')) {
        this.x_rel = initObj.x_rel
      }
      else {
        this.x_rel = 0.0;
      }
      if (initObj.hasOwnProperty('y_rel')) {
        this.y_rel = initObj.y_rel
      }
      else {
        this.y_rel = 0.0;
      }
      if (initObj.hasOwnProperty('rel_data_valid')) {
        this.rel_data_valid = initObj.rel_data_valid
      }
      else {
        this.rel_data_valid = false;
      }
      if (initObj.hasOwnProperty('steering_angle')) {
        this.steering_angle = initObj.steering_angle
      }
      else {
        this.steering_angle = 0.0;
      }
      if (initObj.hasOwnProperty('steering_data_valid')) {
        this.steering_data_valid = initObj.steering_data_valid
      }
      else {
        this.steering_data_valid = false;
      }
      if (initObj.hasOwnProperty('obstacle_validation')) {
        this.obstacle_validation = initObj.obstacle_validation
      }
      else {
        this.obstacle_validation = false;
      }
      if (initObj.hasOwnProperty('right_lidar_segmentation')) {
        this.right_lidar_segmentation = initObj.right_lidar_segmentation
      }
      else {
        this.right_lidar_segmentation = 0;
      }
      if (initObj.hasOwnProperty('left_lidar_segmentation')) {
        this.left_lidar_segmentation = initObj.left_lidar_segmentation
      }
      else {
        this.left_lidar_segmentation = 0;
      }
      if (initObj.hasOwnProperty('radar_segmentation')) {
        this.radar_segmentation = initObj.radar_segmentation
      }
      else {
        this.radar_segmentation = 0;
      }
      if (initObj.hasOwnProperty('right_lidar_feedback_x')) {
        this.right_lidar_feedback_x = initObj.right_lidar_feedback_x
      }
      else {
        this.right_lidar_feedback_x = 0.0;
      }
      if (initObj.hasOwnProperty('left_lidar_feedback_x')) {
        this.left_lidar_feedback_x = initObj.left_lidar_feedback_x
      }
      else {
        this.left_lidar_feedback_x = 0.0;
      }
      if (initObj.hasOwnProperty('radar_feedback_x')) {
        this.radar_feedback_x = initObj.radar_feedback_x
      }
      else {
        this.radar_feedback_x = 0.0;
      }
      if (initObj.hasOwnProperty('right_lidar_feedback_y')) {
        this.right_lidar_feedback_y = initObj.right_lidar_feedback_y
      }
      else {
        this.right_lidar_feedback_y = 0.0;
      }
      if (initObj.hasOwnProperty('left_lidar_feedback_y')) {
        this.left_lidar_feedback_y = initObj.left_lidar_feedback_y
      }
      else {
        this.left_lidar_feedback_y = 0.0;
      }
      if (initObj.hasOwnProperty('radar_feedback_y')) {
        this.radar_feedback_y = initObj.radar_feedback_y
      }
      else {
        this.radar_feedback_y = 0.0;
      }
      if (initObj.hasOwnProperty('emergency_brake')) {
        this.emergency_brake = initObj.emergency_brake
      }
      else {
        this.emergency_brake = false;
      }
      if (initObj.hasOwnProperty('radar_back_feedback_x')) {
        this.radar_back_feedback_x = initObj.radar_back_feedback_x
      }
      else {
        this.radar_back_feedback_x = 0.0;
      }
      if (initObj.hasOwnProperty('lidar_back_feedback_x')) {
        this.lidar_back_feedback_x = initObj.lidar_back_feedback_x
      }
      else {
        this.lidar_back_feedback_x = 0.0;
      }
      if (initObj.hasOwnProperty('radar_back_feedback_y')) {
        this.radar_back_feedback_y = initObj.radar_back_feedback_y
      }
      else {
        this.radar_back_feedback_y = 0.0;
      }
      if (initObj.hasOwnProperty('lidar_back_feedback_y')) {
        this.lidar_back_feedback_y = initObj.lidar_back_feedback_y
      }
      else {
        this.lidar_back_feedback_y = 0.0;
      }
      if (initObj.hasOwnProperty('back_emergency_brake')) {
        this.back_emergency_brake = initObj.back_emergency_brake
      }
      else {
        this.back_emergency_brake = false;
      }
      if (initObj.hasOwnProperty('lidar_back_detection')) {
        this.lidar_back_detection = initObj.lidar_back_detection
      }
      else {
        this.lidar_back_detection = 0;
      }
      if (initObj.hasOwnProperty('back_radar_detection')) {
        this.back_radar_detection = initObj.back_radar_detection
      }
      else {
        this.back_radar_detection = 0;
      }
      if (initObj.hasOwnProperty('reverse_flag')) {
        this.reverse_flag = initObj.reverse_flag
      }
      else {
        this.reverse_flag = 0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type VehicleState
    // Serialize message field [x]
    bufferOffset = _serializer.float32(obj.x, buffer, bufferOffset);
    // Serialize message field [y]
    bufferOffset = _serializer.float32(obj.y, buffer, bufferOffset);
    // Serialize message field [alt]
    bufferOffset = _serializer.float32(obj.alt, buffer, bufferOffset);
    // Serialize message field [velocity]
    bufferOffset = _serializer.float32(obj.velocity, buffer, bufferOffset);
    // Serialize message field [yaw_angle]
    bufferOffset = _serializer.float32(obj.yaw_angle, buffer, bufferOffset);
    // Serialize message field [heading]
    bufferOffset = _serializer.float32(obj.heading, buffer, bufferOffset);
    // Serialize message field [gear]
    bufferOffset = _serializer.int32(obj.gear, buffer, bufferOffset);
    // Serialize message field [gps_data_valid]
    bufferOffset = _serializer.bool(obj.gps_data_valid, buffer, bufferOffset);
    // Serialize message field [x_rel]
    bufferOffset = _serializer.float32(obj.x_rel, buffer, bufferOffset);
    // Serialize message field [y_rel]
    bufferOffset = _serializer.float32(obj.y_rel, buffer, bufferOffset);
    // Serialize message field [rel_data_valid]
    bufferOffset = _serializer.bool(obj.rel_data_valid, buffer, bufferOffset);
    // Serialize message field [steering_angle]
    bufferOffset = _serializer.float32(obj.steering_angle, buffer, bufferOffset);
    // Serialize message field [steering_data_valid]
    bufferOffset = _serializer.bool(obj.steering_data_valid, buffer, bufferOffset);
    // Serialize message field [obstacle_validation]
    bufferOffset = _serializer.bool(obj.obstacle_validation, buffer, bufferOffset);
    // Serialize message field [right_lidar_segmentation]
    bufferOffset = _serializer.int32(obj.right_lidar_segmentation, buffer, bufferOffset);
    // Serialize message field [left_lidar_segmentation]
    bufferOffset = _serializer.int32(obj.left_lidar_segmentation, buffer, bufferOffset);
    // Serialize message field [radar_segmentation]
    bufferOffset = _serializer.int32(obj.radar_segmentation, buffer, bufferOffset);
    // Serialize message field [right_lidar_feedback_x]
    bufferOffset = _serializer.float32(obj.right_lidar_feedback_x, buffer, bufferOffset);
    // Serialize message field [left_lidar_feedback_x]
    bufferOffset = _serializer.float32(obj.left_lidar_feedback_x, buffer, bufferOffset);
    // Serialize message field [radar_feedback_x]
    bufferOffset = _serializer.float32(obj.radar_feedback_x, buffer, bufferOffset);
    // Serialize message field [right_lidar_feedback_y]
    bufferOffset = _serializer.float32(obj.right_lidar_feedback_y, buffer, bufferOffset);
    // Serialize message field [left_lidar_feedback_y]
    bufferOffset = _serializer.float32(obj.left_lidar_feedback_y, buffer, bufferOffset);
    // Serialize message field [radar_feedback_y]
    bufferOffset = _serializer.float32(obj.radar_feedback_y, buffer, bufferOffset);
    // Serialize message field [emergency_brake]
    bufferOffset = _serializer.bool(obj.emergency_brake, buffer, bufferOffset);
    // Serialize message field [radar_back_feedback_x]
    bufferOffset = _serializer.float32(obj.radar_back_feedback_x, buffer, bufferOffset);
    // Serialize message field [lidar_back_feedback_x]
    bufferOffset = _serializer.float32(obj.lidar_back_feedback_x, buffer, bufferOffset);
    // Serialize message field [radar_back_feedback_y]
    bufferOffset = _serializer.float32(obj.radar_back_feedback_y, buffer, bufferOffset);
    // Serialize message field [lidar_back_feedback_y]
    bufferOffset = _serializer.float32(obj.lidar_back_feedback_y, buffer, bufferOffset);
    // Serialize message field [back_emergency_brake]
    bufferOffset = _serializer.bool(obj.back_emergency_brake, buffer, bufferOffset);
    // Serialize message field [lidar_back_detection]
    bufferOffset = _serializer.int32(obj.lidar_back_detection, buffer, bufferOffset);
    // Serialize message field [back_radar_detection]
    bufferOffset = _serializer.int32(obj.back_radar_detection, buffer, bufferOffset);
    // Serialize message field [reverse_flag]
    bufferOffset = _serializer.int32(obj.reverse_flag, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type VehicleState
    let len;
    let data = new VehicleState(null);
    // Deserialize message field [x]
    data.x = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [y]
    data.y = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [alt]
    data.alt = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [velocity]
    data.velocity = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [yaw_angle]
    data.yaw_angle = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [heading]
    data.heading = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [gear]
    data.gear = _deserializer.int32(buffer, bufferOffset);
    // Deserialize message field [gps_data_valid]
    data.gps_data_valid = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [x_rel]
    data.x_rel = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [y_rel]
    data.y_rel = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [rel_data_valid]
    data.rel_data_valid = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [steering_angle]
    data.steering_angle = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [steering_data_valid]
    data.steering_data_valid = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [obstacle_validation]
    data.obstacle_validation = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [right_lidar_segmentation]
    data.right_lidar_segmentation = _deserializer.int32(buffer, bufferOffset);
    // Deserialize message field [left_lidar_segmentation]
    data.left_lidar_segmentation = _deserializer.int32(buffer, bufferOffset);
    // Deserialize message field [radar_segmentation]
    data.radar_segmentation = _deserializer.int32(buffer, bufferOffset);
    // Deserialize message field [right_lidar_feedback_x]
    data.right_lidar_feedback_x = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [left_lidar_feedback_x]
    data.left_lidar_feedback_x = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [radar_feedback_x]
    data.radar_feedback_x = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [right_lidar_feedback_y]
    data.right_lidar_feedback_y = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [left_lidar_feedback_y]
    data.left_lidar_feedback_y = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [radar_feedback_y]
    data.radar_feedback_y = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [emergency_brake]
    data.emergency_brake = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [radar_back_feedback_x]
    data.radar_back_feedback_x = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [lidar_back_feedback_x]
    data.lidar_back_feedback_x = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [radar_back_feedback_y]
    data.radar_back_feedback_y = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [lidar_back_feedback_y]
    data.lidar_back_feedback_y = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [back_emergency_brake]
    data.back_emergency_brake = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [lidar_back_detection]
    data.lidar_back_detection = _deserializer.int32(buffer, bufferOffset);
    // Deserialize message field [back_radar_detection]
    data.back_radar_detection = _deserializer.int32(buffer, bufferOffset);
    // Deserialize message field [reverse_flag]
    data.reverse_flag = _deserializer.int32(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 110;
  }

  static datatype() {
    // Returns string type for a message object
    return 'path_follower/VehicleState';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '4f0db555610e7509196beb6dc607a0c6';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    ### vehicle pose from GPS ###
    float32 x               # [m]   UTM easting
    float32 y               # [m]   UTM northing
    float32 alt
    float32 velocity        # [m/s] 
    float32 yaw_angle       # [rad]
    float32 heading		    # [rad] 
    int32 gear              # [] current gear of the truck
    bool gps_data_valid     # []    flag if values from GPS/IMU system are valid
    
    ### vehicle position relative to the beginning of the (first) path ###
    float32 x_rel           # [m]   UTM easting 
    float32 y_rel           # [m]   UTM northing 
    bool rel_data_valid     # []    flag if relative position values are valid
    
    ### vehicle steering angle (based on data from paravan_node) ###
    float32 steering_angle    # [rad] steering angle from Paravan system    
    bool steering_data_valid  # []    flag if value of steering angle is valid 
    
    ### sensor distance relative to the vehicle ###
    
    bool  obstacle_validation      #	
    int32 right_lidar_segmentation #
    int32 left_lidar_segmentation  #
    int32 radar_segmentation       #
    
    float32 right_lidar_feedback_x    # [m]
    float32 left_lidar_feedback_x     # [m]
    float32 radar_feedback_x	       # [m]
    float32 right_lidar_feedback_y    # [m]
    float32 left_lidar_feedback_y     # [m]
    float32 radar_feedback_y	       # [m]
    bool emergency_brake
    
    float32 radar_back_feedback_x	# [m]
    float32 lidar_back_feedback_x	# [m]
    float32 radar_back_feedback_y	# [m]
    float32 lidar_back_feedback_y	# [m]
    
    bool back_emergency_brake 
    int32 lidar_back_detection 
    int32 back_radar_detection 
    int32 reverse_flag
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new VehicleState(null);
    if (msg.x !== undefined) {
      resolved.x = msg.x;
    }
    else {
      resolved.x = 0.0
    }

    if (msg.y !== undefined) {
      resolved.y = msg.y;
    }
    else {
      resolved.y = 0.0
    }

    if (msg.alt !== undefined) {
      resolved.alt = msg.alt;
    }
    else {
      resolved.alt = 0.0
    }

    if (msg.velocity !== undefined) {
      resolved.velocity = msg.velocity;
    }
    else {
      resolved.velocity = 0.0
    }

    if (msg.yaw_angle !== undefined) {
      resolved.yaw_angle = msg.yaw_angle;
    }
    else {
      resolved.yaw_angle = 0.0
    }

    if (msg.heading !== undefined) {
      resolved.heading = msg.heading;
    }
    else {
      resolved.heading = 0.0
    }

    if (msg.gear !== undefined) {
      resolved.gear = msg.gear;
    }
    else {
      resolved.gear = 0
    }

    if (msg.gps_data_valid !== undefined) {
      resolved.gps_data_valid = msg.gps_data_valid;
    }
    else {
      resolved.gps_data_valid = false
    }

    if (msg.x_rel !== undefined) {
      resolved.x_rel = msg.x_rel;
    }
    else {
      resolved.x_rel = 0.0
    }

    if (msg.y_rel !== undefined) {
      resolved.y_rel = msg.y_rel;
    }
    else {
      resolved.y_rel = 0.0
    }

    if (msg.rel_data_valid !== undefined) {
      resolved.rel_data_valid = msg.rel_data_valid;
    }
    else {
      resolved.rel_data_valid = false
    }

    if (msg.steering_angle !== undefined) {
      resolved.steering_angle = msg.steering_angle;
    }
    else {
      resolved.steering_angle = 0.0
    }

    if (msg.steering_data_valid !== undefined) {
      resolved.steering_data_valid = msg.steering_data_valid;
    }
    else {
      resolved.steering_data_valid = false
    }

    if (msg.obstacle_validation !== undefined) {
      resolved.obstacle_validation = msg.obstacle_validation;
    }
    else {
      resolved.obstacle_validation = false
    }

    if (msg.right_lidar_segmentation !== undefined) {
      resolved.right_lidar_segmentation = msg.right_lidar_segmentation;
    }
    else {
      resolved.right_lidar_segmentation = 0
    }

    if (msg.left_lidar_segmentation !== undefined) {
      resolved.left_lidar_segmentation = msg.left_lidar_segmentation;
    }
    else {
      resolved.left_lidar_segmentation = 0
    }

    if (msg.radar_segmentation !== undefined) {
      resolved.radar_segmentation = msg.radar_segmentation;
    }
    else {
      resolved.radar_segmentation = 0
    }

    if (msg.right_lidar_feedback_x !== undefined) {
      resolved.right_lidar_feedback_x = msg.right_lidar_feedback_x;
    }
    else {
      resolved.right_lidar_feedback_x = 0.0
    }

    if (msg.left_lidar_feedback_x !== undefined) {
      resolved.left_lidar_feedback_x = msg.left_lidar_feedback_x;
    }
    else {
      resolved.left_lidar_feedback_x = 0.0
    }

    if (msg.radar_feedback_x !== undefined) {
      resolved.radar_feedback_x = msg.radar_feedback_x;
    }
    else {
      resolved.radar_feedback_x = 0.0
    }

    if (msg.right_lidar_feedback_y !== undefined) {
      resolved.right_lidar_feedback_y = msg.right_lidar_feedback_y;
    }
    else {
      resolved.right_lidar_feedback_y = 0.0
    }

    if (msg.left_lidar_feedback_y !== undefined) {
      resolved.left_lidar_feedback_y = msg.left_lidar_feedback_y;
    }
    else {
      resolved.left_lidar_feedback_y = 0.0
    }

    if (msg.radar_feedback_y !== undefined) {
      resolved.radar_feedback_y = msg.radar_feedback_y;
    }
    else {
      resolved.radar_feedback_y = 0.0
    }

    if (msg.emergency_brake !== undefined) {
      resolved.emergency_brake = msg.emergency_brake;
    }
    else {
      resolved.emergency_brake = false
    }

    if (msg.radar_back_feedback_x !== undefined) {
      resolved.radar_back_feedback_x = msg.radar_back_feedback_x;
    }
    else {
      resolved.radar_back_feedback_x = 0.0
    }

    if (msg.lidar_back_feedback_x !== undefined) {
      resolved.lidar_back_feedback_x = msg.lidar_back_feedback_x;
    }
    else {
      resolved.lidar_back_feedback_x = 0.0
    }

    if (msg.radar_back_feedback_y !== undefined) {
      resolved.radar_back_feedback_y = msg.radar_back_feedback_y;
    }
    else {
      resolved.radar_back_feedback_y = 0.0
    }

    if (msg.lidar_back_feedback_y !== undefined) {
      resolved.lidar_back_feedback_y = msg.lidar_back_feedback_y;
    }
    else {
      resolved.lidar_back_feedback_y = 0.0
    }

    if (msg.back_emergency_brake !== undefined) {
      resolved.back_emergency_brake = msg.back_emergency_brake;
    }
    else {
      resolved.back_emergency_brake = false
    }

    if (msg.lidar_back_detection !== undefined) {
      resolved.lidar_back_detection = msg.lidar_back_detection;
    }
    else {
      resolved.lidar_back_detection = 0
    }

    if (msg.back_radar_detection !== undefined) {
      resolved.back_radar_detection = msg.back_radar_detection;
    }
    else {
      resolved.back_radar_detection = 0
    }

    if (msg.reverse_flag !== undefined) {
      resolved.reverse_flag = msg.reverse_flag;
    }
    else {
      resolved.reverse_flag = 0
    }

    return resolved;
    }
};

module.exports = VehicleState;
