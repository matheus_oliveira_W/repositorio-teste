// Auto-generated. Do not edit!

// (in-package path_follower.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let std_msgs = _finder('std_msgs');

//-----------------------------------------------------------

class PathInfo {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.recording_time = null;
      this.name = null;
      this.visualization_color = null;
      this.utm_zone_string = null;
      this.num_points = null;
      this.length = null;
      this.go_zone_radius = null;
      this.wait_zone_radius = null;
    }
    else {
      if (initObj.hasOwnProperty('recording_time')) {
        this.recording_time = initObj.recording_time
      }
      else {
        this.recording_time = {secs: 0, nsecs: 0};
      }
      if (initObj.hasOwnProperty('name')) {
        this.name = initObj.name
      }
      else {
        this.name = '';
      }
      if (initObj.hasOwnProperty('visualization_color')) {
        this.visualization_color = initObj.visualization_color
      }
      else {
        this.visualization_color = new std_msgs.msg.ColorRGBA();
      }
      if (initObj.hasOwnProperty('utm_zone_string')) {
        this.utm_zone_string = initObj.utm_zone_string
      }
      else {
        this.utm_zone_string = '';
      }
      if (initObj.hasOwnProperty('num_points')) {
        this.num_points = initObj.num_points
      }
      else {
        this.num_points = 0;
      }
      if (initObj.hasOwnProperty('length')) {
        this.length = initObj.length
      }
      else {
        this.length = 0.0;
      }
      if (initObj.hasOwnProperty('go_zone_radius')) {
        this.go_zone_radius = initObj.go_zone_radius
      }
      else {
        this.go_zone_radius = 0.0;
      }
      if (initObj.hasOwnProperty('wait_zone_radius')) {
        this.wait_zone_radius = initObj.wait_zone_radius
      }
      else {
        this.wait_zone_radius = 0.0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type PathInfo
    // Serialize message field [recording_time]
    bufferOffset = _serializer.time(obj.recording_time, buffer, bufferOffset);
    // Serialize message field [name]
    bufferOffset = _serializer.string(obj.name, buffer, bufferOffset);
    // Serialize message field [visualization_color]
    bufferOffset = std_msgs.msg.ColorRGBA.serialize(obj.visualization_color, buffer, bufferOffset);
    // Serialize message field [utm_zone_string]
    bufferOffset = _serializer.string(obj.utm_zone_string, buffer, bufferOffset);
    // Serialize message field [num_points]
    bufferOffset = _serializer.uint16(obj.num_points, buffer, bufferOffset);
    // Serialize message field [length]
    bufferOffset = _serializer.float32(obj.length, buffer, bufferOffset);
    // Serialize message field [go_zone_radius]
    bufferOffset = _serializer.float32(obj.go_zone_radius, buffer, bufferOffset);
    // Serialize message field [wait_zone_radius]
    bufferOffset = _serializer.float32(obj.wait_zone_radius, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type PathInfo
    let len;
    let data = new PathInfo(null);
    // Deserialize message field [recording_time]
    data.recording_time = _deserializer.time(buffer, bufferOffset);
    // Deserialize message field [name]
    data.name = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [visualization_color]
    data.visualization_color = std_msgs.msg.ColorRGBA.deserialize(buffer, bufferOffset);
    // Deserialize message field [utm_zone_string]
    data.utm_zone_string = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [num_points]
    data.num_points = _deserializer.uint16(buffer, bufferOffset);
    // Deserialize message field [length]
    data.length = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [go_zone_radius]
    data.go_zone_radius = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [wait_zone_radius]
    data.wait_zone_radius = _deserializer.float32(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += object.name.length;
    length += object.utm_zone_string.length;
    return length + 46;
  }

  static datatype() {
    // Returns string type for a message object
    return 'path_follower/PathInfo';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '33a4f61a6c31b8cdccfa3cc9ec623b2d';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    time 	  recording_time        # time when path recording has started
    string    name                  # name of the path
    std_msgs/ColorRGBA visualization_color   # color of path visualization in rviz
    string 	  utm_zone_string       # UTM zone of the path (string representation)
    uint16    num_points            # number of points in the path
    float32   length                # path length (sum of all segment lengths)
    float32   go_zone_radius        # [m] radius of go-zone
    float32   wait_zone_radius      # [m] radius of wait-zone
    
    ================================================================================
    MSG: std_msgs/ColorRGBA
    float32 r
    float32 g
    float32 b
    float32 a
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new PathInfo(null);
    if (msg.recording_time !== undefined) {
      resolved.recording_time = msg.recording_time;
    }
    else {
      resolved.recording_time = {secs: 0, nsecs: 0}
    }

    if (msg.name !== undefined) {
      resolved.name = msg.name;
    }
    else {
      resolved.name = ''
    }

    if (msg.visualization_color !== undefined) {
      resolved.visualization_color = std_msgs.msg.ColorRGBA.Resolve(msg.visualization_color)
    }
    else {
      resolved.visualization_color = new std_msgs.msg.ColorRGBA()
    }

    if (msg.utm_zone_string !== undefined) {
      resolved.utm_zone_string = msg.utm_zone_string;
    }
    else {
      resolved.utm_zone_string = ''
    }

    if (msg.num_points !== undefined) {
      resolved.num_points = msg.num_points;
    }
    else {
      resolved.num_points = 0
    }

    if (msg.length !== undefined) {
      resolved.length = msg.length;
    }
    else {
      resolved.length = 0.0
    }

    if (msg.go_zone_radius !== undefined) {
      resolved.go_zone_radius = msg.go_zone_radius;
    }
    else {
      resolved.go_zone_radius = 0.0
    }

    if (msg.wait_zone_radius !== undefined) {
      resolved.wait_zone_radius = msg.wait_zone_radius;
    }
    else {
      resolved.wait_zone_radius = 0.0
    }

    return resolved;
    }
};

module.exports = PathInfo;
