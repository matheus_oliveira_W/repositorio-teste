// Auto-generated. Do not edit!

// (in-package path_follower.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class Excavator {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.ex_latitude = null;
      this.ex_longitude = null;
    }
    else {
      if (initObj.hasOwnProperty('ex_latitude')) {
        this.ex_latitude = initObj.ex_latitude
      }
      else {
        this.ex_latitude = 0.0;
      }
      if (initObj.hasOwnProperty('ex_longitude')) {
        this.ex_longitude = initObj.ex_longitude
      }
      else {
        this.ex_longitude = 0.0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type Excavator
    // Serialize message field [ex_latitude]
    bufferOffset = _serializer.float32(obj.ex_latitude, buffer, bufferOffset);
    // Serialize message field [ex_longitude]
    bufferOffset = _serializer.float32(obj.ex_longitude, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type Excavator
    let len;
    let data = new Excavator(null);
    // Deserialize message field [ex_latitude]
    data.ex_latitude = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [ex_longitude]
    data.ex_longitude = _deserializer.float32(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 8;
  }

  static datatype() {
    // Returns string type for a message object
    return 'path_follower/Excavator';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'd4836f677d2197f5049d039e568a5f7c';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    float32 ex_latitude
    float32 ex_longitude
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new Excavator(null);
    if (msg.ex_latitude !== undefined) {
      resolved.ex_latitude = msg.ex_latitude;
    }
    else {
      resolved.ex_latitude = 0.0
    }

    if (msg.ex_longitude !== undefined) {
      resolved.ex_longitude = msg.ex_longitude;
    }
    else {
      resolved.ex_longitude = 0.0
    }

    return resolved;
    }
};

module.exports = Excavator;
