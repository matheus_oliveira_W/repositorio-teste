// Auto-generated. Do not edit!

// (in-package path_follower.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class ServiceCommandReply {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
    }
    else {
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type ServiceCommandReply
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type ServiceCommandReply
    let len;
    let data = new ServiceCommandReply(null);
    return data;
  }

  static getMessageSize(object) {
    return 0;
  }

  static datatype() {
    // Returns string type for a message object
    return 'path_follower/ServiceCommandReply';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'f05070682928bf756746c7dc8b2dcb1e';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    # success reply
    uint8 	SUCCESS = 0
    
    # fail replies
    uint8 	FAIL_OXTS_OFFLINE              = 10
    uint8 	FAIL_OXTS_NO_DATA              = 11
    uint8  	FAIL_OXTS_DATA_BUT_NO_FIX      = 12
    uint8 	FAIL_UEM_OFFLINE               = 13
    uint8 	FAIL_UEM_NO_DATA               = 14
    uint8   FAIL_UEM_NO_REMOTE_CONTROL     = 15
    uint8   FAIL_STATE_LOGIC               = 16
    uint8   FAIL_NO_PATH_AVAILABLE         = 17
    uint8   FAIL_PATH_TOO_SHORT            = 18
    uint8   FAIL_LOADING_PATH_FROM_FILE    = 19
    uint8   FAIL_VEHCILE_NOT_IN_GO_ZONE    = 20
    uint8   FAIL_OLD_PLOT_STILL_ACTIVE     = 21
    uint8   FAIL_BUCKET_IS_OPENED          = 22
    
    # warn replies
    uint8   WARN_UEM_OFFLINE               = 30
    uint8   WARN_UEM_NO_DATA               = 31
    uint8   WARN_UEM_NO_REMOTE_CONTROL     = 32
    
    # service error
    uint8 SERVICE_ERROR = 66
    
    
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new ServiceCommandReply(null);
    return resolved;
    }
};

// Constants for message
ServiceCommandReply.Constants = {
  SUCCESS: 0,
  FAIL_OXTS_OFFLINE: 10,
  FAIL_OXTS_NO_DATA: 11,
  FAIL_OXTS_DATA_BUT_NO_FIX: 12,
  FAIL_UEM_OFFLINE: 13,
  FAIL_UEM_NO_DATA: 14,
  FAIL_UEM_NO_REMOTE_CONTROL: 15,
  FAIL_STATE_LOGIC: 16,
  FAIL_NO_PATH_AVAILABLE: 17,
  FAIL_PATH_TOO_SHORT: 18,
  FAIL_LOADING_PATH_FROM_FILE: 19,
  FAIL_VEHCILE_NOT_IN_GO_ZONE: 20,
  FAIL_OLD_PLOT_STILL_ACTIVE: 21,
  FAIL_BUCKET_IS_OPENED: 22,
  WARN_UEM_OFFLINE: 30,
  WARN_UEM_NO_DATA: 31,
  WARN_UEM_NO_REMOTE_CONTROL: 32,
  SERVICE_ERROR: 66,
}

module.exports = ServiceCommandReply;
