// Generated by gencpp from file path_follower/VehicleState.msg
// DO NOT EDIT!


#ifndef PATH_FOLLOWER_MESSAGE_VEHICLESTATE_H
#define PATH_FOLLOWER_MESSAGE_VEHICLESTATE_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>


namespace path_follower
{
template <class ContainerAllocator>
struct VehicleState_
{
  typedef VehicleState_<ContainerAllocator> Type;

  VehicleState_()
    : x(0.0)
    , y(0.0)
    , alt(0.0)
    , velocity(0.0)
    , yaw_angle(0.0)
    , heading(0.0)
    , gear(0)
    , gps_data_valid(false)
    , x_rel(0.0)
    , y_rel(0.0)
    , rel_data_valid(false)
    , steering_angle(0.0)
    , steering_data_valid(false)
    , obstacle_validation(false)
    , right_lidar_segmentation(0)
    , left_lidar_segmentation(0)
    , radar_segmentation(0)
    , right_lidar_feedback_x(0.0)
    , left_lidar_feedback_x(0.0)
    , radar_feedback_x(0.0)
    , right_lidar_feedback_y(0.0)
    , left_lidar_feedback_y(0.0)
    , radar_feedback_y(0.0)
    , emergency_brake(false)
    , radar_back_feedback_x(0.0)
    , lidar_back_feedback_x(0.0)
    , radar_back_feedback_y(0.0)
    , lidar_back_feedback_y(0.0)
    , back_emergency_brake(false)
    , lidar_back_detection(0)
    , back_radar_detection(0)
    , reverse_flag(0)  {
    }
  VehicleState_(const ContainerAllocator& _alloc)
    : x(0.0)
    , y(0.0)
    , alt(0.0)
    , velocity(0.0)
    , yaw_angle(0.0)
    , heading(0.0)
    , gear(0)
    , gps_data_valid(false)
    , x_rel(0.0)
    , y_rel(0.0)
    , rel_data_valid(false)
    , steering_angle(0.0)
    , steering_data_valid(false)
    , obstacle_validation(false)
    , right_lidar_segmentation(0)
    , left_lidar_segmentation(0)
    , radar_segmentation(0)
    , right_lidar_feedback_x(0.0)
    , left_lidar_feedback_x(0.0)
    , radar_feedback_x(0.0)
    , right_lidar_feedback_y(0.0)
    , left_lidar_feedback_y(0.0)
    , radar_feedback_y(0.0)
    , emergency_brake(false)
    , radar_back_feedback_x(0.0)
    , lidar_back_feedback_x(0.0)
    , radar_back_feedback_y(0.0)
    , lidar_back_feedback_y(0.0)
    , back_emergency_brake(false)
    , lidar_back_detection(0)
    , back_radar_detection(0)
    , reverse_flag(0)  {
  (void)_alloc;
    }



   typedef float _x_type;
  _x_type x;

   typedef float _y_type;
  _y_type y;

   typedef float _alt_type;
  _alt_type alt;

   typedef float _velocity_type;
  _velocity_type velocity;

   typedef float _yaw_angle_type;
  _yaw_angle_type yaw_angle;

   typedef float _heading_type;
  _heading_type heading;

   typedef int32_t _gear_type;
  _gear_type gear;

   typedef uint8_t _gps_data_valid_type;
  _gps_data_valid_type gps_data_valid;

   typedef float _x_rel_type;
  _x_rel_type x_rel;

   typedef float _y_rel_type;
  _y_rel_type y_rel;

   typedef uint8_t _rel_data_valid_type;
  _rel_data_valid_type rel_data_valid;

   typedef float _steering_angle_type;
  _steering_angle_type steering_angle;

   typedef uint8_t _steering_data_valid_type;
  _steering_data_valid_type steering_data_valid;

   typedef uint8_t _obstacle_validation_type;
  _obstacle_validation_type obstacle_validation;

   typedef int32_t _right_lidar_segmentation_type;
  _right_lidar_segmentation_type right_lidar_segmentation;

   typedef int32_t _left_lidar_segmentation_type;
  _left_lidar_segmentation_type left_lidar_segmentation;

   typedef int32_t _radar_segmentation_type;
  _radar_segmentation_type radar_segmentation;

   typedef float _right_lidar_feedback_x_type;
  _right_lidar_feedback_x_type right_lidar_feedback_x;

   typedef float _left_lidar_feedback_x_type;
  _left_lidar_feedback_x_type left_lidar_feedback_x;

   typedef float _radar_feedback_x_type;
  _radar_feedback_x_type radar_feedback_x;

   typedef float _right_lidar_feedback_y_type;
  _right_lidar_feedback_y_type right_lidar_feedback_y;

   typedef float _left_lidar_feedback_y_type;
  _left_lidar_feedback_y_type left_lidar_feedback_y;

   typedef float _radar_feedback_y_type;
  _radar_feedback_y_type radar_feedback_y;

   typedef uint8_t _emergency_brake_type;
  _emergency_brake_type emergency_brake;

   typedef float _radar_back_feedback_x_type;
  _radar_back_feedback_x_type radar_back_feedback_x;

   typedef float _lidar_back_feedback_x_type;
  _lidar_back_feedback_x_type lidar_back_feedback_x;

   typedef float _radar_back_feedback_y_type;
  _radar_back_feedback_y_type radar_back_feedback_y;

   typedef float _lidar_back_feedback_y_type;
  _lidar_back_feedback_y_type lidar_back_feedback_y;

   typedef uint8_t _back_emergency_brake_type;
  _back_emergency_brake_type back_emergency_brake;

   typedef int32_t _lidar_back_detection_type;
  _lidar_back_detection_type lidar_back_detection;

   typedef int32_t _back_radar_detection_type;
  _back_radar_detection_type back_radar_detection;

   typedef int32_t _reverse_flag_type;
  _reverse_flag_type reverse_flag;





  typedef boost::shared_ptr< ::path_follower::VehicleState_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::path_follower::VehicleState_<ContainerAllocator> const> ConstPtr;

}; // struct VehicleState_

typedef ::path_follower::VehicleState_<std::allocator<void> > VehicleState;

typedef boost::shared_ptr< ::path_follower::VehicleState > VehicleStatePtr;
typedef boost::shared_ptr< ::path_follower::VehicleState const> VehicleStateConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::path_follower::VehicleState_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::path_follower::VehicleState_<ContainerAllocator> >::stream(s, "", v);
return s;
}

} // namespace path_follower

namespace ros
{
namespace message_traits
{



// BOOLTRAITS {'IsFixedSize': True, 'IsMessage': True, 'HasHeader': False}
// {'std_msgs': ['/opt/ros/kinetic/share/std_msgs/cmake/../msg'], 'path_follower': ['/home/bplus/UM_serial/src/path_follower/path_follower_node/msg']}

// !!!!!!!!!!! ['__class__', '__delattr__', '__dict__', '__doc__', '__eq__', '__format__', '__getattribute__', '__hash__', '__init__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', '_parsed_fields', 'constants', 'fields', 'full_name', 'has_header', 'header_present', 'names', 'package', 'parsed_fields', 'short_name', 'text', 'types']




template <class ContainerAllocator>
struct IsFixedSize< ::path_follower::VehicleState_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::path_follower::VehicleState_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::path_follower::VehicleState_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::path_follower::VehicleState_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::path_follower::VehicleState_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::path_follower::VehicleState_<ContainerAllocator> const>
  : FalseType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::path_follower::VehicleState_<ContainerAllocator> >
{
  static const char* value()
  {
    return "4f0db555610e7509196beb6dc607a0c6";
  }

  static const char* value(const ::path_follower::VehicleState_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0x4f0db555610e7509ULL;
  static const uint64_t static_value2 = 0x196beb6dc607a0c6ULL;
};

template<class ContainerAllocator>
struct DataType< ::path_follower::VehicleState_<ContainerAllocator> >
{
  static const char* value()
  {
    return "path_follower/VehicleState";
  }

  static const char* value(const ::path_follower::VehicleState_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::path_follower::VehicleState_<ContainerAllocator> >
{
  static const char* value()
  {
    return "### vehicle pose from GPS ###\n\
float32 x               # [m]   UTM easting\n\
float32 y               # [m]   UTM northing\n\
float32 alt\n\
float32 velocity        # [m/s] \n\
float32 yaw_angle       # [rad]\n\
float32 heading		    # [rad] \n\
int32 gear              # [] current gear of the truck\n\
bool gps_data_valid     # []    flag if values from GPS/IMU system are valid\n\
\n\
### vehicle position relative to the beginning of the (first) path ###\n\
float32 x_rel           # [m]   UTM easting \n\
float32 y_rel           # [m]   UTM northing \n\
bool rel_data_valid     # []    flag if relative position values are valid\n\
\n\
### vehicle steering angle (based on data from paravan_node) ###\n\
float32 steering_angle    # [rad] steering angle from Paravan system    \n\
bool steering_data_valid  # []    flag if value of steering angle is valid \n\
\n\
### sensor distance relative to the vehicle ###\n\
\n\
bool  obstacle_validation      #	\n\
int32 right_lidar_segmentation #\n\
int32 left_lidar_segmentation  #\n\
int32 radar_segmentation       #\n\
\n\
float32 right_lidar_feedback_x    # [m]\n\
float32 left_lidar_feedback_x     # [m]\n\
float32 radar_feedback_x	       # [m]\n\
float32 right_lidar_feedback_y    # [m]\n\
float32 left_lidar_feedback_y     # [m]\n\
float32 radar_feedback_y	       # [m]\n\
bool emergency_brake\n\
\n\
float32 radar_back_feedback_x	# [m]\n\
float32 lidar_back_feedback_x	# [m]\n\
float32 radar_back_feedback_y	# [m]\n\
float32 lidar_back_feedback_y	# [m]\n\
\n\
bool back_emergency_brake \n\
int32 lidar_back_detection \n\
int32 back_radar_detection \n\
int32 reverse_flag\n\
";
  }

  static const char* value(const ::path_follower::VehicleState_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::path_follower::VehicleState_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.x);
      stream.next(m.y);
      stream.next(m.alt);
      stream.next(m.velocity);
      stream.next(m.yaw_angle);
      stream.next(m.heading);
      stream.next(m.gear);
      stream.next(m.gps_data_valid);
      stream.next(m.x_rel);
      stream.next(m.y_rel);
      stream.next(m.rel_data_valid);
      stream.next(m.steering_angle);
      stream.next(m.steering_data_valid);
      stream.next(m.obstacle_validation);
      stream.next(m.right_lidar_segmentation);
      stream.next(m.left_lidar_segmentation);
      stream.next(m.radar_segmentation);
      stream.next(m.right_lidar_feedback_x);
      stream.next(m.left_lidar_feedback_x);
      stream.next(m.radar_feedback_x);
      stream.next(m.right_lidar_feedback_y);
      stream.next(m.left_lidar_feedback_y);
      stream.next(m.radar_feedback_y);
      stream.next(m.emergency_brake);
      stream.next(m.radar_back_feedback_x);
      stream.next(m.lidar_back_feedback_x);
      stream.next(m.radar_back_feedback_y);
      stream.next(m.lidar_back_feedback_y);
      stream.next(m.back_emergency_brake);
      stream.next(m.lidar_back_detection);
      stream.next(m.back_radar_detection);
      stream.next(m.reverse_flag);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct VehicleState_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::path_follower::VehicleState_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::path_follower::VehicleState_<ContainerAllocator>& v)
  {
    s << indent << "x: ";
    Printer<float>::stream(s, indent + "  ", v.x);
    s << indent << "y: ";
    Printer<float>::stream(s, indent + "  ", v.y);
    s << indent << "alt: ";
    Printer<float>::stream(s, indent + "  ", v.alt);
    s << indent << "velocity: ";
    Printer<float>::stream(s, indent + "  ", v.velocity);
    s << indent << "yaw_angle: ";
    Printer<float>::stream(s, indent + "  ", v.yaw_angle);
    s << indent << "heading: ";
    Printer<float>::stream(s, indent + "  ", v.heading);
    s << indent << "gear: ";
    Printer<int32_t>::stream(s, indent + "  ", v.gear);
    s << indent << "gps_data_valid: ";
    Printer<uint8_t>::stream(s, indent + "  ", v.gps_data_valid);
    s << indent << "x_rel: ";
    Printer<float>::stream(s, indent + "  ", v.x_rel);
    s << indent << "y_rel: ";
    Printer<float>::stream(s, indent + "  ", v.y_rel);
    s << indent << "rel_data_valid: ";
    Printer<uint8_t>::stream(s, indent + "  ", v.rel_data_valid);
    s << indent << "steering_angle: ";
    Printer<float>::stream(s, indent + "  ", v.steering_angle);
    s << indent << "steering_data_valid: ";
    Printer<uint8_t>::stream(s, indent + "  ", v.steering_data_valid);
    s << indent << "obstacle_validation: ";
    Printer<uint8_t>::stream(s, indent + "  ", v.obstacle_validation);
    s << indent << "right_lidar_segmentation: ";
    Printer<int32_t>::stream(s, indent + "  ", v.right_lidar_segmentation);
    s << indent << "left_lidar_segmentation: ";
    Printer<int32_t>::stream(s, indent + "  ", v.left_lidar_segmentation);
    s << indent << "radar_segmentation: ";
    Printer<int32_t>::stream(s, indent + "  ", v.radar_segmentation);
    s << indent << "right_lidar_feedback_x: ";
    Printer<float>::stream(s, indent + "  ", v.right_lidar_feedback_x);
    s << indent << "left_lidar_feedback_x: ";
    Printer<float>::stream(s, indent + "  ", v.left_lidar_feedback_x);
    s << indent << "radar_feedback_x: ";
    Printer<float>::stream(s, indent + "  ", v.radar_feedback_x);
    s << indent << "right_lidar_feedback_y: ";
    Printer<float>::stream(s, indent + "  ", v.right_lidar_feedback_y);
    s << indent << "left_lidar_feedback_y: ";
    Printer<float>::stream(s, indent + "  ", v.left_lidar_feedback_y);
    s << indent << "radar_feedback_y: ";
    Printer<float>::stream(s, indent + "  ", v.radar_feedback_y);
    s << indent << "emergency_brake: ";
    Printer<uint8_t>::stream(s, indent + "  ", v.emergency_brake);
    s << indent << "radar_back_feedback_x: ";
    Printer<float>::stream(s, indent + "  ", v.radar_back_feedback_x);
    s << indent << "lidar_back_feedback_x: ";
    Printer<float>::stream(s, indent + "  ", v.lidar_back_feedback_x);
    s << indent << "radar_back_feedback_y: ";
    Printer<float>::stream(s, indent + "  ", v.radar_back_feedback_y);
    s << indent << "lidar_back_feedback_y: ";
    Printer<float>::stream(s, indent + "  ", v.lidar_back_feedback_y);
    s << indent << "back_emergency_brake: ";
    Printer<uint8_t>::stream(s, indent + "  ", v.back_emergency_brake);
    s << indent << "lidar_back_detection: ";
    Printer<int32_t>::stream(s, indent + "  ", v.lidar_back_detection);
    s << indent << "back_radar_detection: ";
    Printer<int32_t>::stream(s, indent + "  ", v.back_radar_detection);
    s << indent << "reverse_flag: ";
    Printer<int32_t>::stream(s, indent + "  ", v.reverse_flag);
  }
};

} // namespace message_operations
} // namespace ros

#endif // PATH_FOLLOWER_MESSAGE_VEHICLESTATE_H
