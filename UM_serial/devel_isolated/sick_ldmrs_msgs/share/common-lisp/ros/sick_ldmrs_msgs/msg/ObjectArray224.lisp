; Auto-generated. Do not edit!


(cl:in-package sick_ldmrs_msgs-msg)


;//! \htmlinclude ObjectArray224.msg.html

(cl:defclass <ObjectArray224> (roslisp-msg-protocol:ros-message)
  ((header_224
    :reader header_224
    :initarg :header_224
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (objects_224
    :reader objects_224
    :initarg :objects_224
    :type (cl:vector sick_ldmrs_msgs-msg:Object)
   :initform (cl:make-array 0 :element-type 'sick_ldmrs_msgs-msg:Object :initial-element (cl:make-instance 'sick_ldmrs_msgs-msg:Object))))
)

(cl:defclass ObjectArray224 (<ObjectArray224>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <ObjectArray224>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'ObjectArray224)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name sick_ldmrs_msgs-msg:<ObjectArray224> is deprecated: use sick_ldmrs_msgs-msg:ObjectArray224 instead.")))

(cl:ensure-generic-function 'header_224-val :lambda-list '(m))
(cl:defmethod header_224-val ((m <ObjectArray224>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader sick_ldmrs_msgs-msg:header_224-val is deprecated.  Use sick_ldmrs_msgs-msg:header_224 instead.")
  (header_224 m))

(cl:ensure-generic-function 'objects_224-val :lambda-list '(m))
(cl:defmethod objects_224-val ((m <ObjectArray224>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader sick_ldmrs_msgs-msg:objects_224-val is deprecated.  Use sick_ldmrs_msgs-msg:objects_224 instead.")
  (objects_224 m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <ObjectArray224>) ostream)
  "Serializes a message object of type '<ObjectArray224>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header_224) ostream)
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'objects_224))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'objects_224))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <ObjectArray224>) istream)
  "Deserializes a message object of type '<ObjectArray224>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header_224) istream)
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'objects_224) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'objects_224)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'sick_ldmrs_msgs-msg:Object))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<ObjectArray224>)))
  "Returns string type for a message object of type '<ObjectArray224>"
  "sick_ldmrs_msgs/ObjectArray224")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'ObjectArray224)))
  "Returns string type for a message object of type 'ObjectArray224"
  "sick_ldmrs_msgs/ObjectArray224")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<ObjectArray224>)))
  "Returns md5sum for a message object of type '<ObjectArray224>"
  "5d5683fd1fa540df61765a4fbe731729")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'ObjectArray224)))
  "Returns md5sum for a message object of type 'ObjectArray224"
  "5d5683fd1fa540df61765a4fbe731729")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<ObjectArray224>)))
  "Returns full string definition for message of type '<ObjectArray224>"
  (cl:format cl:nil "std_msgs/Header header_224~%sick_ldmrs_msgs/Object[] objects_224~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%================================================================================~%MSG: sick_ldmrs_msgs/Object~%int32 id~%~%time tracking_time                          # since when the object is tracked~%time last_seen~%~%geometry_msgs/TwistWithCovariance velocity~%~%geometry_msgs/Pose bounding_box_center~%geometry_msgs/Vector3 bounding_box_size~%~%geometry_msgs/PoseWithCovariance object_box_center~%geometry_msgs/Vector3 object_box_size~%~%geometry_msgs/Point[] contour_points~%~%================================================================================~%MSG: geometry_msgs/TwistWithCovariance~%# This expresses velocity in free space with uncertainty.~%~%Twist twist~%~%# Row-major representation of the 6x6 covariance matrix~%# The orientation parameters use a fixed-axis representation.~%# In order, the parameters are:~%# (x, y, z, rotation about X axis, rotation about Y axis, rotation about Z axis)~%float64[36] covariance~%~%================================================================================~%MSG: geometry_msgs/Twist~%# This expresses velocity in free space broken into its linear and angular parts.~%Vector3  linear~%Vector3  angular~%~%================================================================================~%MSG: geometry_msgs/Vector3~%# This represents a vector in free space. ~%# It is only meant to represent a direction. Therefore, it does not~%# make sense to apply a translation to it (e.g., when applying a ~%# generic rigid transformation to a Vector3, tf2 will only apply the~%# rotation). If you want your data to be translatable too, use the~%# geometry_msgs/Point message instead.~%~%float64 x~%float64 y~%float64 z~%================================================================================~%MSG: geometry_msgs/Pose~%# A representation of pose in free space, composed of position and orientation. ~%Point position~%Quaternion orientation~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%================================================================================~%MSG: geometry_msgs/PoseWithCovariance~%# This represents a pose in free space with uncertainty.~%~%Pose pose~%~%# Row-major representation of the 6x6 covariance matrix~%# The orientation parameters use a fixed-axis representation.~%# In order, the parameters are:~%# (x, y, z, rotation about X axis, rotation about Y axis, rotation about Z axis)~%float64[36] covariance~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'ObjectArray224)))
  "Returns full string definition for message of type 'ObjectArray224"
  (cl:format cl:nil "std_msgs/Header header_224~%sick_ldmrs_msgs/Object[] objects_224~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%================================================================================~%MSG: sick_ldmrs_msgs/Object~%int32 id~%~%time tracking_time                          # since when the object is tracked~%time last_seen~%~%geometry_msgs/TwistWithCovariance velocity~%~%geometry_msgs/Pose bounding_box_center~%geometry_msgs/Vector3 bounding_box_size~%~%geometry_msgs/PoseWithCovariance object_box_center~%geometry_msgs/Vector3 object_box_size~%~%geometry_msgs/Point[] contour_points~%~%================================================================================~%MSG: geometry_msgs/TwistWithCovariance~%# This expresses velocity in free space with uncertainty.~%~%Twist twist~%~%# Row-major representation of the 6x6 covariance matrix~%# The orientation parameters use a fixed-axis representation.~%# In order, the parameters are:~%# (x, y, z, rotation about X axis, rotation about Y axis, rotation about Z axis)~%float64[36] covariance~%~%================================================================================~%MSG: geometry_msgs/Twist~%# This expresses velocity in free space broken into its linear and angular parts.~%Vector3  linear~%Vector3  angular~%~%================================================================================~%MSG: geometry_msgs/Vector3~%# This represents a vector in free space. ~%# It is only meant to represent a direction. Therefore, it does not~%# make sense to apply a translation to it (e.g., when applying a ~%# generic rigid transformation to a Vector3, tf2 will only apply the~%# rotation). If you want your data to be translatable too, use the~%# geometry_msgs/Point message instead.~%~%float64 x~%float64 y~%float64 z~%================================================================================~%MSG: geometry_msgs/Pose~%# A representation of pose in free space, composed of position and orientation. ~%Point position~%Quaternion orientation~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%================================================================================~%MSG: geometry_msgs/PoseWithCovariance~%# This represents a pose in free space with uncertainty.~%~%Pose pose~%~%# Row-major representation of the 6x6 covariance matrix~%# The orientation parameters use a fixed-axis representation.~%# In order, the parameters are:~%# (x, y, z, rotation about X axis, rotation about Y axis, rotation about Z axis)~%float64[36] covariance~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <ObjectArray224>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header_224))
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'objects_224) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <ObjectArray224>))
  "Converts a ROS message object to a list"
  (cl:list 'ObjectArray224
    (cl:cons ':header_224 (header_224 msg))
    (cl:cons ':objects_224 (objects_224 msg))
))
