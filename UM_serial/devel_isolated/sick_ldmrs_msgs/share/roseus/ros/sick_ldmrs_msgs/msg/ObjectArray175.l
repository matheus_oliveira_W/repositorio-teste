;; Auto-generated. Do not edit!


(when (boundp 'sick_ldmrs_msgs::ObjectArray175)
  (if (not (find-package "SICK_LDMRS_MSGS"))
    (make-package "SICK_LDMRS_MSGS"))
  (shadow 'ObjectArray175 (find-package "SICK_LDMRS_MSGS")))
(unless (find-package "SICK_LDMRS_MSGS::OBJECTARRAY175")
  (make-package "SICK_LDMRS_MSGS::OBJECTARRAY175"))

(in-package "ROS")
;;//! \htmlinclude ObjectArray175.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass sick_ldmrs_msgs::ObjectArray175
  :super ros::object
  :slots (_header_175 _objects_175 ))

(defmethod sick_ldmrs_msgs::ObjectArray175
  (:init
   (&key
    ((:header_175 __header_175) (instance std_msgs::Header :init))
    ((:objects_175 __objects_175) (let (r) (dotimes (i 0) (push (instance sick_ldmrs_msgs::Object :init) r)) r))
    )
   (send-super :init)
   (setq _header_175 __header_175)
   (setq _objects_175 __objects_175)
   self)
  (:header_175
   (&rest __header_175)
   (if (keywordp (car __header_175))
       (send* _header_175 __header_175)
     (progn
       (if __header_175 (setq _header_175 (car __header_175)))
       _header_175)))
  (:objects_175
   (&rest __objects_175)
   (if (keywordp (car __objects_175))
       (send* _objects_175 __objects_175)
     (progn
       (if __objects_175 (setq _objects_175 (car __objects_175)))
       _objects_175)))
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header_175
    (send _header_175 :serialization-length)
    ;; sick_ldmrs_msgs/Object[] _objects_175
    (apply #'+ (send-all _objects_175 :serialization-length)) 4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header_175
       (send _header_175 :serialize s)
     ;; sick_ldmrs_msgs/Object[] _objects_175
     (write-long (length _objects_175) s)
     (dolist (elem _objects_175)
       (send elem :serialize s)
       )
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header_175
     (send _header_175 :deserialize buf ptr-) (incf ptr- (send _header_175 :serialization-length))
   ;; sick_ldmrs_msgs/Object[] _objects_175
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _objects_175 (let (r) (dotimes (i n) (push (instance sick_ldmrs_msgs::Object :init) r)) r))
     (dolist (elem- _objects_175)
     (send elem- :deserialize buf ptr-) (incf ptr- (send elem- :serialization-length))
     ))
   ;;
   self)
  )

(setf (get sick_ldmrs_msgs::ObjectArray175 :md5sum-) "a4365c8fdb36f56b8361098662ce741d")
(setf (get sick_ldmrs_msgs::ObjectArray175 :datatype-) "sick_ldmrs_msgs/ObjectArray175")
(setf (get sick_ldmrs_msgs::ObjectArray175 :definition-)
      "std_msgs/Header header_175
sick_ldmrs_msgs/Object[] objects_175



================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
# 0: no frame
# 1: global frame
string frame_id

================================================================================
MSG: sick_ldmrs_msgs/Object
int32 id

time tracking_time                          # since when the object is tracked
time last_seen

geometry_msgs/TwistWithCovariance velocity

geometry_msgs/Pose bounding_box_center
geometry_msgs/Vector3 bounding_box_size

geometry_msgs/PoseWithCovariance object_box_center
geometry_msgs/Vector3 object_box_size

geometry_msgs/Point[] contour_points

================================================================================
MSG: geometry_msgs/TwistWithCovariance
# This expresses velocity in free space with uncertainty.

Twist twist

# Row-major representation of the 6x6 covariance matrix
# The orientation parameters use a fixed-axis representation.
# In order, the parameters are:
# (x, y, z, rotation about X axis, rotation about Y axis, rotation about Z axis)
float64[36] covariance

================================================================================
MSG: geometry_msgs/Twist
# This expresses velocity in free space broken into its linear and angular parts.
Vector3  linear
Vector3  angular

================================================================================
MSG: geometry_msgs/Vector3
# This represents a vector in free space. 
# It is only meant to represent a direction. Therefore, it does not
# make sense to apply a translation to it (e.g., when applying a 
# generic rigid transformation to a Vector3, tf2 will only apply the
# rotation). If you want your data to be translatable too, use the
# geometry_msgs/Point message instead.

float64 x
float64 y
float64 z
================================================================================
MSG: geometry_msgs/Pose
# A representation of pose in free space, composed of position and orientation. 
Point position
Quaternion orientation

================================================================================
MSG: geometry_msgs/Point
# This contains the position of a point in free space
float64 x
float64 y
float64 z

================================================================================
MSG: geometry_msgs/Quaternion
# This represents an orientation in free space in quaternion form.

float64 x
float64 y
float64 z
float64 w

================================================================================
MSG: geometry_msgs/PoseWithCovariance
# This represents a pose in free space with uncertainty.

Pose pose

# Row-major representation of the 6x6 covariance matrix
# The orientation parameters use a fixed-axis representation.
# In order, the parameters are:
# (x, y, z, rotation about X axis, rotation about Y axis, rotation about Z axis)
float64[36] covariance

")



(provide :sick_ldmrs_msgs/ObjectArray175 "a4365c8fdb36f56b8361098662ce741d")


