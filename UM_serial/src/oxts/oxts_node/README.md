Ethernet interface to OxTS GPS receivers using the NCOM packet structure
===============================

### Parameters
- `interface` Restrict to single network interface, example: `eth0`. Default `<empty>`
- `ip_address` Restrict to single ip address. Default `<empty>`
- `port` UDP listen port. Default `3000`
- `frame_id` The frame-ID for gps position and imu. Default `gps`
- `frame_id_vel` The frame-ID for gps velocity. Default `utm`

### FAQ
I see ```Connected to Oxford GPS at <ip_address>:3000```, but no messages are published.  
Invalid data is not published. Move faster than the configured initialization speed of the sensor.