import os
import rospy
import rospkg
import rosservice


# Qt imports -----------------------------------
from qt_gui.plugin import Plugin
from python_qt_binding import loadUi
from python_qt_binding.QtWidgets import QWidget
from python_qt_binding.QtWidgets import QTreeWidgetItem, QFileDialog
from python_qt_binding.QtCore import QObject, pyqtSignal, pyqtSlot, QTimer

# Message imports -----------------------------------
from sensor_msgs.msg import NavSatFix
from sensor_msgs.msg import Imu
from geometry_msgs.msg import TwistWithCovarianceStamped


def build_gps_tree_data_dict(tree):

    tree_data_dict = {}

    tree.clear()
    tree.setColumnCount(2)

    root_gps = QTreeWidgetItem(tree, ["Position"])
    root_gps.setExpanded(True)

    item_names = ["Longitude", "Latitude", "Altitude"]
    for item_name in item_names:
        tree_data_dict[item_name] = QTreeWidgetItem(root_gps, [item_name])

    # --------------------------------------------------------

    root_velocity = QTreeWidgetItem(tree, ["Velocity"])
    root_velocity.setExpanded(True)

    root_vel_lin = QTreeWidgetItem(root_velocity, ["Linear"])
    root_vel_lin.setExpanded(True)

    root_vel_ang = QTreeWidgetItem(root_velocity, ["Angular"])
    root_vel_ang.setExpanded(True)

    item_names = ["X", "Y", "Z"]
    for item_name in item_names:
        tree_data_dict["Vel_Lin_" +
                       item_name] = QTreeWidgetItem(root_vel_lin, [item_name])

    item_names = ["X", "Y", "Z"]
    for item_name in item_names:
        tree_data_dict["Vel_Ang_" +
                       item_name] = QTreeWidgetItem(root_vel_ang, [item_name])

    # --------------------------------------------------------

    root_acc = QTreeWidgetItem(tree, ["Acceleration"])
    root_acc.setExpanded(True)

    item_names = ["X", "Y", "Z"]
    for item_name in item_names:
        tree_data_dict["Acc_" +
                       item_name] = QTreeWidgetItem(root_acc, [item_name])

    root_orientation = QTreeWidgetItem(tree, ["Orientation"])
    root_orientation.setExpanded(True)

    item_names = ["W", "X", "Y", "Z"]
    for item_name in item_names:
        tree_data_dict["Orientation_" +
                       item_name] = QTreeWidgetItem(root_orientation, [item_name])

    return tree_data_dict


class OxtsPlugin(Plugin):

    def __init__(self, context):
        super(OxtsPlugin, self).__init__(context)

        # Give QObjects reasonable names
        self.setObjectName('Path_Tools_GUI')

        # Process standalone plugin command-line arguments
        from argparse import ArgumentParser
        parser = ArgumentParser()
        # Add argument(s) to the parser.
        parser.add_argument("-q", "--quiet", action="store_true",
                            dest="quiet",
                            help="Put plugin in silent mode")
        args, unknowns = parser.parse_known_args(context.argv())
        if not args.quiet:
            print 'arguments: ', args
            print 'unknowns: ', unknowns

        # Create QWidget
        self._widget = QWidget()

        # Get path to UI file which should be in the "resource" folder of this package
        ui_file = os.path.join(rospkg.RosPack().get_path(
            'oxts'), 'resource', 'oxts_gui.ui')

        # Extend the widget with all attributes and children from UI file
        loadUi(ui_file, self._widget)

        # Give QObjects reasonable names
        self._widget.setObjectName('OxTS_GUI')

        self.current_file_name = None
        self.wait_zone_description = None
        self.go_zone_description = None

        self.gps_tree_data_dict = build_gps_tree_data_dict(
            self._widget.treeWidget)

        # subscribers ----------------------------------------------------------
        rospy.Subscriber("oxts/nav_sat_fix", NavSatFix, self.sat_data_callback)
        rospy.Subscriber("oxts/imu", Imu, self.imu_data_callback)
        rospy.Subscriber(
            "oxts/gps_vel", TwistWithCovarianceStamped, self.gps_vel_data_callback)

        self.msg_NavSatFix = None
        self.msg_Imu = None
        self.msg_TwistWithCovarianceStamped = None

        # timer ----------------------------------------------------------
        self.gui_timer = QTimer()
        self.gui_timer.timeout.connect(self.update_gui)
        self.gui_timer.start(100)  # duration in ms

        # Show _widget.windowTitle on left-top of each plugin (when
        # it's set in _widget).
        if context.serial_number() > 1:
            self._widget.setWindowTitle(
                self._widget.windowTitle() + (' (%d)' % context.serial_number()))
        # Add widget to the user interface
        context.add_widget(self._widget)

    @pyqtSlot()
    def update_gui(self):
        if self.msg_NavSatFix is not None:
            self.gps_tree_data_dict['Longitude'].setText(
                1, "{:.7f}".format(self.msg_NavSatFix.longitude))
            self.gps_tree_data_dict['Latitude'].setText(
                1, "{:.7f}".format(self.msg_NavSatFix.latitude))
            self.gps_tree_data_dict['Altitude'].setText(
                1, "{:.7f}".format(self.msg_NavSatFix.altitude))

        if self.msg_Imu is not None:
            self.gps_tree_data_dict['Acc_X'].setText(
                1, "{:.2f}".format(self.msg_Imu.linear_acceleration.x))
            self.gps_tree_data_dict['Acc_Y'].setText(
                1, "{:.2f}".format(self.msg_Imu.linear_acceleration.y))
            self.gps_tree_data_dict['Acc_Z'].setText(
                1, "{:.2f}".format(self.msg_Imu.linear_acceleration.z))
            self.gps_tree_data_dict['Orientation_X'].setText(
                1, "{:.2f}".format(self.msg_Imu.orientation.x))
            self.gps_tree_data_dict['Orientation_Y'].setText(
                1, "{:.2f}".format(self.msg_Imu.orientation.y))
            self.gps_tree_data_dict['Orientation_Z'].setText(
                1, "{:.2f}".format(self.msg_Imu.orientation.z))
            self.gps_tree_data_dict['Orientation_W'].setText(
                1, "{:.2f}".format(self.msg_Imu.orientation.w))
            self.gps_tree_data_dict['Vel_Ang_X'].setText(
                1, "{:.2f}".format(self.msg_Imu.angular_velocity.x))
            self.gps_tree_data_dict['Vel_Ang_Y'].setText(
                1, "{:.2f}".format(self.msg_Imu.angular_velocity.y))
            self.gps_tree_data_dict['Vel_Ang_Z'].setText(
                1, "{:.2f}".format(self.msg_Imu.angular_velocity.z))

        if self.msg_TwistWithCovarianceStamped is not None:
            self.gps_tree_data_dict['Vel_Lin_X'].setText(1, "{:.2f}".format(
                self.msg_TwistWithCovarianceStamped.twist.twist.linear.x))
            self.gps_tree_data_dict['Vel_Lin_Y'].setText(1, "{:.2f}".format(
                self.msg_TwistWithCovarianceStamped.twist.twist.linear.y))
            self.gps_tree_data_dict['Vel_Lin_Z'].setText(1, "{:.2f}".format(
                self.msg_TwistWithCovarianceStamped.twist.twist.linear.z))

    def sat_data_callback(self, msg_NavSatFix):
        self.msg_NavSatFix = msg_NavSatFix

    def imu_data_callback(self, msg_Imu):
        self.msg_Imu = msg_Imu

    def gps_vel_data_callback(self, msg_TwistWithCovarianceStamped):
        self.msg_TwistWithCovarianceStamped = msg_TwistWithCovarianceStamped

    def shutdown_plugin(self):
        # TODO unregister all publishers here
        pass

    def save_settings(self, plugin_settings, instance_settings):
        # TODO save intrinsic configuration, usually using:
        # instance_settings.set_value(k, v)
        pass

    def restore_settings(self, plugin_settings, instance_settings):
        # TODO restore intrinsic configuration, usually using:
        # v = instance_settings.value(k)
        pass

    # def trigger_configuration(self):
    # Comment in to signal that the plugin has a way to configure
    # This will enable a setting button (gear icon) in each dock widget title bar
    # Usually used to open a modal configuration dialog
