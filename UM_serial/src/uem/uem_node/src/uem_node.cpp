#include "ros/ros.h"
#include <ros/package.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Bool.h>
#include <std_msgs/UInt16.h>
#include <std_msgs/UInt64.h>
#include <std_msgs/Empty.h>
#include <std_srvs/SetBool.h>
#include <std_srvs/Empty.h>
#include <sensor_msgs/NavSatFix.h>

#include "path_follower/PathFollowerService.h"
#include "path_follower/ServiceCommand.h"
#include "path_follower/Excavator.h"

#include <iostream>
#include <fstream>
#include <ctime>
#include <mutex>
#include <signal.h>
#include <ros/ros.h>
#include <ros/xmlrpc_manager.h>
#include <serial/serial.h>

#include "libuemserial.h"

UMSerial um_serial;
std::mutex um_serial_mutex;
std::string node_name;
ros::NodeHandlePtr node;
ros::Timer engine_timer, horn_timer_on, horn_timer_off_1, horn_timer_off_2, bucket_timer, horn_timer;

ros::Publisher pub_steering, pub_data_signal, pub_truck_status, pub_steering_init_status, pub_gas_brake, pub_gas_brake_init_status,
        pub_rb1_status, pub_rb2_status, pub_alive, pub_rc_available, pub_general_error, pub_ex_location;

ros::ServiceClient path_follower_service_client;

double param_limit_steering, param_limit_gas, param_limit_brake,
		param_max_steering_jump, param_max_gas_brake_jump;

bool param_send_steering, param_send_gas_brake, param_send_rb_1_functions,
        param_send_rb_2_functions, param_send_rb_3_functions, param_send_truck_mode;

int magnetic_clutch_bit;

sig_atomic_t volatile g_request_shutdown = 0; // Signal-safe flag for whether shutdown is requested

// Replacement SIGINT handler
void mySigIntHandler(int sig)
{
    g_request_shutdown = 1;
}

// Replacement "shutdown" XMLRPC callback
void shutdownCallback(XmlRpc::XmlRpcValue& params, XmlRpc::XmlRpcValue& result)
{
    int num_params = 0;
    if (params.getType() == XmlRpc::XmlRpcValue::TypeArray)
        num_params = params.size();
    if (num_params > 1)
    {
        std::string reason = params[1];
        ROS_WARN("Shutdown request received. Reason: [%s]", reason.c_str());
        g_request_shutdown = 1; // Set flag
    }

    result = ros::xmlrpc::responseInt(1, "", 0);
}

void can_msg_callback(UMSerial::ParsedMsg msg)
{
    // ROS_INFO_STREAM("DENTRO DO CALLBACK"); 

    switch (msg.type)
    {
    case UMSerial::MsgType::DATA_SIGNAL:
    {
        auto ros_msg = std_msgs::Empty();
        pub_data_signal.publish(ros_msg);
        break;
    }

    case UMSerial::MsgType::GENERAL_ERROR:
    {
        auto ros_msg = std_msgs::UInt16();
        ros_msg.data = msg.data.ival;
        pub_general_error.publish(ros_msg);

        ROS_ERROR_STREAM("Received GENERAL_ERROR!");
        break;
    }

    case UMSerial::MsgType::RECEIVED_EX_LOCATION:
    {
        auto ros_msg = path_follower::Excavator();
        ros_msg.ex_latitude = std::stof(msg.data.lat);
        ros_msg.ex_longitude = std::stof(msg.data.lon);
        pub_ex_location.publish(ros_msg);
        
        if ((ros_msg.ex_latitude != 0.00) && (ros_msg.ex_longitude != 0.00))
        {
            path_follower::PathFollowerService srv;
            srv.request.command = path_follower::ServiceCommand::START_FOLLOWING;
            srv.request.int_data = 1;
            path_follower_service_client.call(srv);

            path_follower::PathFollowerService srv_ex_location;
            srv_ex_location.request.command = path_follower::ServiceCommand::EX_LOCATION;
            path_follower_service_client.call(srv_ex_location);

            ROS_INFO_STREAM("RECEIVED EXCAVATOR LOCATION!");

        }
        else
        {
            ROS_ERROR_STREAM("UNABLE TO SET!");
        }
      
        break;
    }
    case UMSerial::MsgType::RECEIVED_DP_LOCATION:
    {
        auto ros_msg = path_follower::Excavator();
        ros_msg.ex_latitude = std::stof(msg.data.lat);
        ros_msg.ex_longitude = std::stof(msg.data.lon);
        pub_ex_location.publish(ros_msg);
        
        if ((ros_msg.ex_latitude != 0.00) && (ros_msg.ex_longitude != 0.00))
        {
            path_follower::PathFollowerService srv;
            srv.request.command = path_follower::ServiceCommand::START_FOLLOWING;
            srv.request.int_data = 1;
            path_follower_service_client.call(srv);

            path_follower::PathFollowerService srv_ex_location;
            srv_ex_location.request.command = path_follower::ServiceCommand::DP_LOCATION;
            path_follower_service_client.call(srv_ex_location);

            ROS_INFO_STREAM("RECEIVED DEPOSIT LOCATION!");

        }
        else
        {
            ROS_ERROR_STREAM("UNABLE TO SET!");
        }
      
        break;
    }
    case UMSerial::MsgType::BUCKET_OPENED:
    {
        path_follower::PathFollowerService srv_open_bucket;

        srv_open_bucket.request.command = path_follower::ServiceCommand::BUCKET_OPENED;

        path_follower_service_client.call(srv_open_bucket);
               
        ROS_ERROR_STREAM("Received BUCKET_OPENED!");

        break;
    }
    case UMSerial::MsgType::BUCKET_CLOSED:
    {
        path_follower::PathFollowerService srv_close_bucket;

        srv_close_bucket.request.command = path_follower::ServiceCommand::BUCKET_CLOSED;

        path_follower_service_client.call(srv_close_bucket);
               
        ROS_ERROR_STREAM("Received BUCKET_CLOSED!");

        break;
    }

    }
}

// callback functions for updating values to be sent to paravan system
void set_actuators_control_cb(std_msgs::UInt16 msg)
{
    uint16_t actuators_control_flags = msg.data;
    ROS_DEBUG_STREAM("Received actuators control flag: " << std::bitset<16>(actuators_control_flags));

    um_serial.set_actuators_control(actuators_control_flags);
}

void set_rb_1_functions_cb(std_msgs::UInt16 msg)
{
    uint16_t relay_box_1_flags = msg.data;
    ROS_DEBUG_STREAM("Received relay box 1 flags: " << std::bitset<16>(relay_box_1_flags));

    um_serial.set_relay_box_1_flags(relay_box_1_flags);
}

void set_rb_2_functions_cb(std_msgs::UInt16 msg)
{
	uint16_t relay_box_2_flags = msg.data;
	ROS_DEBUG_STREAM("Received relay box 2 flags: " << std::bitset<16>(relay_box_2_flags));

    um_serial.set_relay_box_2_flags(relay_box_2_flags);
}

void set_rb_3_functions_cb(std_msgs::UInt16 msg)
{
	uint16_t relay_box_3_flags = msg.data;
	ROS_DEBUG_STREAM("Received relay box 3 flags: " << std::bitset<16>(relay_box_3_flags));

    um_serial.set_relay_box_3_flags(relay_box_3_flags);
}

void set_blinker_cb(std_msgs::UInt16 msg)
{
    uint16_t rb_1_flags = um_serial.get_relay_box_1_flags();
    uint16_t l_blinker = 2;
    uint16_t r_blinker = 3;

    switch(msg.data)
    {
        case 0:
        {
            rb_1_flags &= ~(1 << l_blinker);
            rb_1_flags &= ~(1 << r_blinker);
            	        
            break;
        }
        case 1:
        {
            rb_1_flags |= 1 << l_blinker;
            rb_1_flags &= ~(1 << r_blinker);
            break;
        }
        case 2:
        {
            rb_1_flags &= ~(1 << l_blinker);
            rb_1_flags |= 1 << r_blinker;
            break;
        }
    }

    um_serial.set_relay_box_1_flags(rb_1_flags);

}

void set_steering_normalized_cb(std_msgs::Float32 msg)
{
	um_serial.set_steering_value(msg.data);
}

void set_gas_brake_normalized_cb(std_msgs::Float32 msg)
{
	um_serial.set_gas_brake_value(msg.data);
}

bool handle_horn_front_srv(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res)
{
	ROS_INFO_STREAM("Requested horn front!");

    uint16_t horn_bit = 10; // see Paravan K-Matrix

    uint16_t rb_1_flags = um_serial.get_relay_box_1_flags();
    
	rb_1_flags |= 1 << horn_bit; // set horn_bit to 1
	um_serial.set_relay_box_1_flags(rb_1_flags);

	horn_timer_off_1 = node->createTimer(ros::Duration(0.2),
			[horn_bit](const ros::TimerEvent& event)
			{
				uint16_t rb_1_flags = um_serial.get_relay_box_1_flags();
				rb_1_flags &= ~(1 << horn_bit); // set horn_bit to 0
				um_serial.set_relay_box_1_flags(rb_1_flags);
			}, true); 

	horn_timer_off_1.start();

    horn_timer_on = node->createTimer(ros::Duration(0.4),
			[horn_bit](const ros::TimerEvent& event)
			{
				uint16_t rb_1_flags = um_serial.get_relay_box_1_flags();
				rb_1_flags |= 1 << horn_bit; // set horn_bit to 1
				um_serial.set_relay_box_1_flags(rb_1_flags);
			}, true);
    
    horn_timer_on.start();

    horn_timer_off_2 = node->createTimer(ros::Duration(0.6),
			[horn_bit](const ros::TimerEvent& event)
			{
				uint16_t rb_1_flags = um_serial.get_relay_box_1_flags();
				rb_1_flags &= ~(1 << horn_bit); // set horn_bit to 0
				um_serial.set_relay_box_1_flags(rb_1_flags);
			}, true); 

	horn_timer_off_2.start();

    return true;
}


bool handle_horn_warning_srv(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res)
{
	ROS_INFO_STREAM("Requested horn warning!");

    uint16_t horn_bit = 10; // see Paravan K-Matrix

    uint16_t rb_1_flags = um_serial.get_relay_box_1_flags();
    //ROS_INFO_STREAM("BEFORE OUT: " << std::bitset<16>(rb_1_flags));
	rb_1_flags |= 1 << horn_bit; // set horn_bit to 1
	um_serial.set_relay_box_1_flags(rb_1_flags);

	horn_timer = node->createTimer(ros::Duration(0.1),
			[horn_bit](const ros::TimerEvent& event)
			{
				uint16_t rb_1_flags = um_serial.get_relay_box_1_flags();
                //ROS_INFO_STREAM("BEFORE IN: " << std::bitset<16>(rb_1_flags));
				rb_1_flags &= ~(1 << horn_bit); // set horn_bit to 0
				um_serial.set_relay_box_1_flags(rb_1_flags);
			}, true);

	horn_timer.start();

    return true;
}

bool handle_manual_mode_srv(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res)
{
    ROS_INFO_STREAM("Requested Manual Mode");

    /*(uint16_t autonomous_mode = 0; //bit position
    uint16_t manual_mode = 1; //bit position

    uint16_t act_control_flags = um_serial.get_actuators_control_flags();
    ROS_INFO_STREAM("BEFORE: " << std::bitset<16>(act_control_flags));

    act_control_flags |= 1 << manual_mode;
    act_control_flags &= ~(1 << autonomous_mode);*/
    auto ros_msg = std_msgs::UInt16();
    ros_msg.data = 2;

    pub_truck_status.publish(ros_msg);
    //um_serial.set_actuators_control(act_control_flags);

    return true;
}

bool handle_autonomous_mode_srv(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res)
{
    ROS_INFO_STREAM("Requested Autonomous Mode");

    /*uint16_t autonomous_mode = 0; //bit position
    uint16_t manual_mode = 1; //bit position

    uint16_t act_control_flags = um_serial.get_actuators_control_flags();
    //ROS_INFO_STREAM("BEFORE: " << std::bitset<16>(act_control_flags));

    act_control_flags |= 1 << autonomous_mode;
    act_control_flags &= ~(1 << manual_mode);*/

    auto ros_msg = std_msgs::UInt16();
    ros_msg.data = 1;

    pub_truck_status.publish(ros_msg);
    //um_serial.set_actuators_control(act_control_flags);

    return true;
}

bool emergency_on_srv(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res)
{
    ROS_INFO_STREAM("Requested emergency!");

    uint16_t emergency = 0;
    //uint16_t horn_bit = 10;

    uint16_t rb_3_flags = um_serial.get_relay_box_3_flags();
 
    rb_3_flags &= ~(1 << emergency); //0 freado
   // rb_1_flags &= ~(1 << horn_bit);
    
    um_serial.set_relay_box_3_flags(rb_3_flags);
    

    return true;
}

bool emergency_off_srv(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res)
{
    ROS_INFO_STREAM("Requested emergency!");

    uint16_t emergency = 0;

    uint16_t rb_3_flags = um_serial.get_relay_box_3_flags();
 
    rb_3_flags |= 1 << emergency; //1 freio solto
    
    um_serial.set_relay_box_3_flags(rb_3_flags);
    

    return true;
}

bool handle_gear_back_srv(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res)
{
    ROS_INFO_STREAM("Requested gear back!");

    uint16_t gear_r = 0;
    uint16_t gear_n = 1; 
    uint16_t gear_f = 4;

    uint16_t rb_2_flags = um_serial.get_relay_box_2_flags();

    rb_2_flags |= 1 << gear_r; // 0
    rb_2_flags &= ~(1 << gear_n); //1
    rb_2_flags &= ~(1 << gear_f); //0
    
    um_serial.set_relay_box_2_flags(rb_2_flags);

    return true;
}

bool handle_gear_neutral_srv(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res)
{
    ROS_INFO_STREAM("Requested gear neutral!");

    /*
    Gear_R = 0x001  # bit position 0
    Gear_N = 0x002  # 1
    Gear_1 = 0x004  # 2
    Gear_2 = 0x008  # 3
    Gear_3 = 0x010  # 4
    Gear_4 = 0x020  # 5
    Gear_5 = 0x040  # 6
    Gear_6 = 0x080  # 7	
	*/
    uint16_t gear_r = 0;
    uint16_t gear_n = 1; 
    uint16_t gear_f = 4;

    uint16_t rb_2_flags = um_serial.get_relay_box_2_flags();

    rb_2_flags &= ~(1 << gear_r); // 0
    rb_2_flags |= 1 << gear_n; //1
    rb_2_flags &= ~(1 << gear_f); //0
    
    um_serial.set_relay_box_2_flags(rb_2_flags);
    
    return true;
}

bool handle_gear_front_srv(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res)
{
    ROS_INFO_STREAM("Requested gear front!");

    uint16_t gear_r = 0;
    uint16_t gear_n = 1;
    uint16_t gear_f = 4; 

    uint16_t rb_2_flags = um_serial.get_relay_box_2_flags();

    rb_2_flags &= ~(1 << gear_r); // 0
    rb_2_flags &= ~(1 << gear_n);
    rb_2_flags |= 1 << gear_f; //1

    um_serial.set_relay_box_2_flags(rb_2_flags);

    return true;
}

bool handle_open_bucket_srv(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res)
{
    ROS_ERROR_STREAM("Requested to open the bucket!");

    uint16_t bucket = 6;
   

    uint16_t rb_1_flags = um_serial.get_relay_box_1_flags();
    
    //rb_1_flags &= ~(1 << emergency); //0 freado
    rb_1_flags |= 1 << bucket; 

    um_serial.set_relay_box_1_flags(rb_1_flags);

    bucket_timer = node->createTimer(ros::Duration(0.9), [bucket](const ros::TimerEvent& event)
			{
				uint16_t rb_1_flags = um_serial.get_relay_box_1_flags();
				rb_1_flags &= ~(1 << bucket);
				um_serial.set_relay_box_1_flags(rb_1_flags);
			}, true);

    bucket_timer.start();

    return true;
}

bool handle_close_bucket_srv(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res)
{
    ROS_ERROR_STREAM("Requested to close the bucket!");

    uint16_t bucket = 12;

    uint16_t rb_1_flags = um_serial.get_relay_box_1_flags();

    //rb_1_flags &= ~(1 << emergency); //0 freado
    rb_1_flags |= 1 << bucket; 


    um_serial.set_relay_box_1_flags(rb_1_flags);

   bucket_timer = node->createTimer(ros::Duration(0.9), [bucket](const ros::TimerEvent& event)
			{
				uint16_t rb_1_flags = um_serial.get_relay_box_1_flags();
				rb_1_flags &= ~(1 << bucket);
				um_serial.set_relay_box_1_flags(rb_1_flags);
			}, true);

    bucket_timer.start();

    return true;
}

void stop_UMSerial()
{
    std::lock_guard < std::mutex > lock(um_serial_mutex);
    um_serial.shutdown();
}

bool start_UMSerial()
{
    std::string serial_device_name;
    node->param<std::string>("/" + node_name + "/serial_device_name", serial_device_name, "/dev/pts/20");

    if (um_serial.serial_thread_running())
    {
        stop_UMSerial();
    }

    std::lock_guard < std::mutex > lock(um_serial_mutex);

    if (um_serial.open_serial_device(serial_device_name))
    {
        um_serial.spin(); // spawn a thread for receiving and sending Paravan CAN data
        return true;
    } else
    {
        return false;
    }
}

template<typename T>
bool get_param_cached(const std::string& param_name, T& param, T default_value)
{
    T new_value;
    if (!node->getParamCached(param_name, new_value))
    {
        new_value = default_value;
    }

    if (param != new_value)
    {
        param = new_value;
        ROS_INFO_STREAM("Updated parameter " << param_name << ": " << param);
        return true;
    }

    return false;
}

void update_parameters()
{
	if (get_param_cached("/uem_node/limit_steering", param_limit_steering, 1.0)
			|| get_param_cached("/uem_node/limit_gas", param_limit_gas, 1.0)
			|| get_param_cached("/uem_node/limit_brake", param_limit_brake, -1.0))
	{
		um_serial.set_value_limits(param_limit_steering, param_limit_gas, param_limit_brake);
	}

	if (get_param_cached("/uem_node/max_steering_jump", param_max_steering_jump, 2.0)
			|| get_param_cached("/uem_node/max_gas_brake_jump", param_max_gas_brake_jump, 2.0))
	{
		um_serial.set_value_jump_limits(param_max_steering_jump, param_max_gas_brake_jump);
	}

	if (get_param_cached("/uem_node/send_steering", param_send_steering, false)
			|| get_param_cached("/uem_node/send_gas_brake", param_send_gas_brake, false)
			|| get_param_cached("/uem_node/send_relay_box_1_functions", param_send_rb_1_functions, false)
			|| get_param_cached("/uem_node/send_relay_box_2_functions", param_send_rb_2_functions, false)
            || get_param_cached("/uem_node/send_relay_box_3_functions", param_send_rb_3_functions, false)
			|| get_param_cached("/uem_node/send_truck_mode", param_send_truck_mode, false))
	{
		um_serial.set_sending_status(param_send_steering, param_send_gas_brake, param_send_rb_1_functions,
				param_send_rb_2_functions, param_send_rb_3_functions, param_send_truck_mode);
	}
}


void timer_callback(const ros::TimerEvent& timerEvent)
{
    pub_alive.publish(std_msgs::Empty());

    update_parameters();
}

int main(int argc, char **argv)
{
    node_name = "paravan_node";

    //last_rc_trigger = 0;

    // Override SIGINT handler
    ros::init(argc, argv, node_name, ros::init_options::NoSigintHandler);
    signal(SIGINT, mySigIntHandler);

    // Override XMLRPC shutdown
    ros::XMLRPCManager::instance()->unbind("shutdown");
    ros::XMLRPCManager::instance()->bind("shutdown", shutdownCallback);

    node = boost::make_shared<ros::NodeHandle>();

    /* --- PUBLISHERS --- */

    pub_alive = node->advertise<std_msgs::Empty>("paravan/alive", 10);

    pub_data_signal = node->advertise<std_msgs::Empty>("/uem/data_signal", 10);

    pub_general_error = node->advertise<std_msgs::UInt16>("/uem/general_error", 10);

    pub_ex_location = node->advertise<path_follower::Excavator>("/uem/ex_location", 10);

    pub_truck_status = node->advertise<std_msgs::UInt16>("/truck_status", 10);


    /* --- SUBSCRIBERS --- */
    std::vector<ros::Subscriber> subscribers; // vector of all subscriber objects

    // desired steering/gas/brake and relay box values
    subscribers.push_back(
            node->subscribe<std_msgs::Float32>("/steering_norm_desired", 10,
                    set_steering_normalized_cb));
    subscribers.push_back(
            node->subscribe<std_msgs::Float32>("/gas_brake_norm_desired", 10,
                    set_gas_brake_normalized_cb));
    subscribers.push_back(
            node->subscribe<std_msgs::UInt16>("/paravan/relay_box_1_desired", 10,
                    set_rb_1_functions_cb));
    subscribers.push_back(
            node->subscribe<std_msgs::UInt16>("/uem/relay_box_2_from_uem", 10,
                    set_rb_2_functions_cb));
    subscribers.push_back(
            node->subscribe<std_msgs::UInt16>("/uem/relay_box_3_from_uem", 10,
                    set_rb_3_functions_cb));
    subscribers.push_back(
            node->subscribe<std_msgs::UInt16>("/truck_status", 10,
                    set_actuators_control_cb));
    subscribers.push_back(
            node->subscribe<std_msgs::UInt16>("/path_follower/blinker", 10,
                    set_blinker_cb));

    /* --- SERVICES --- */
    path_follower_service_client = node->serviceClient<path_follower::PathFollowerService>(
            "path_follower_node/path_follower_service");

    ros::ServiceServer horn_front_service = node->advertiseService(
        		"/uem/horn_front", handle_horn_front_srv);

    ros::ServiceServer horn_warning_service = node->advertiseService(
        		"/paravan/horn_warning", handle_horn_warning_srv);

    ros::ServiceServer gear_front_service = node->advertiseService(
                "/uem/gear_front", handle_gear_front_srv);

    ros::ServiceServer gear_neutral_service = node->advertiseService(
                "/uem/gear_neutral", handle_gear_neutral_srv);

    ros::ServiceServer gear_back_service = node->advertiseService(
                "/uem/gear_back", handle_gear_back_srv);
    
    ros::ServiceServer autonomous_mode_service = node->advertiseService(
                "/uem/autonomous_mode", handle_autonomous_mode_srv);

    ros::ServiceServer manual_mode_service = node->advertiseService(
                "/uem/manual_mode", handle_manual_mode_srv);

    ros::ServiceServer open_bucket_service = node->advertiseService(
                "/uem/open_bucket", handle_open_bucket_srv);
    
    ros::ServiceServer close_bucket_service = node->advertiseService(
                "/uem/close_bucket", handle_close_bucket_srv);

    ros::ServiceServer emergency_on_service = node->advertiseService(
                "/uem/emergency_on", emergency_on_srv);

    ros::ServiceServer emergency_off_service = node->advertiseService(
                "/uem/emergency_off", emergency_off_srv);

    update_parameters();

    ros::Timer timer = node->createTimer(ros::Duration(0.1), timer_callback);

    um_serial.set_msg_callback(can_msg_callback); // set callback function for received CAN messages

    start_UMSerial(); // open CAN device and start a send/receive thread for CAN messages

    /* --- MAIN LOOP --- */
    ros::Rate loop_rate(100); // [Hz] set update frequency for ROS subscriber callback functions

    while (!g_request_shutdown) // start ROS loop
    {
        ros::spinOnce();

        loop_rate.sleep();
    }

    stop_UMSerial(); // close the CAN device connection

    ros::shutdown();

    return 0;
}
