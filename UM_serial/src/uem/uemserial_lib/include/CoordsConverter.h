#ifndef COORDS_CONVERTER_H
#define COORDS_CONVERTER_H

#include <chrono>
#include <string>
#include <thread>

class CoordsConverter
{
public:

	static void deg_to_degdec(const int loop, const char * data);

    char * lat_d;
    char * lat_m;
    char * lat_s;
    char * lon_d;
    char * lon_m;
    char * lon_s;

private:
	
};

#endif // COORDS_CONVERTER_H
