#include <cstdarg>
#include <cstring>
#include <cstdint>
#include <chrono>
#include <thread>
#include <bitset>
#include <atomic>
#include <iostream>
#include <algorithm>
#include <ctime>   
#include <iomanip> 
#include <pthread.h>
#include <string>
#include <sstream>

#include "CycleTimer.h"
#include "Logger.h"
#include "serial/serial.h"

class UMSerial
{
	typedef unsigned char byte;

public:
	enum class MsgType
	{
		RECEIVED_NEW_PATH,
		GENERAL_ERROR,
		RECEIVED_EX_LOCATION,
		RECEIVED_DP_LOCATION,
		BUCKET_OPENED,
		BUCKET_CLOSED,
		DATA_SIGNAL,
		UNKNOWN
};

	typedef struct
	{
		std::string lat, lon;
		float fval;
		uint64_t ival;
	} MsgData;

	typedef struct
	{
		MsgType type;
		MsgData data;
	} ParsedMsg; 

	/* Constructor */
	UMSerial();

	bool open_serial_device(const std::string& device_name);
	void set_msg_callback(void (*fcn_ptr)(ParsedMsg))
	{
		msg_callback = fcn_ptr;
	}
	void spin();
	void shutdown();
	void set_steering_value(const float steering_normalized);
	void set_gas_brake_value(const float gas_brake_normalized);
	void set_relay_box_1_flags(const uint16_t flag_bits);
	void set_relay_box_2_flags(const uint16_t flag_bits);
	void set_relay_box_3_flags(const uint16_t flag_bits);
	void set_actuators_control(const uint16_t flag_bits);
	void coords_converter(const int loop, char * data);
	void actual_values(const int loop, char * data);
	void set_value_limits(float max_steering_value, float max_gas_value, float max_brake_value);
	void set_value_jump_limits(float max_steering_jump, float max_gas_brake_jump);
	void set_sending_status(bool send_steering, bool send_gas_brake, bool send_relay_box_1, bool send_relay_box_2, bool send_relay_box_3, bool send_truck_mode);
	void set_keep_steering_neutral(bool keep_neutral);

	bool serial_thread_running()
	{
		return serial_thread_alive;
	}

	float get_steering_value() { return steering_value_actual; }
	float get_gas_brake_value() { return gas_brake_value_actual; }

	long double lat_degree(const char * degree, const char * minutes, float multiplier)
	{
		long double degree_i = atof(degree);
		long double minutes_i = atof(minutes);

		long double lat = degree_i + (minutes_i/ 60.0);

		lat = multiplier * lat;
		
		return lat;
	}
	double lon_degree(const char * degree, const char * minutes, float multiplier)
	{
		long double degree_i = atof(degree);
		long double minutes_i = atof(minutes);

		long double lon = degree_i + (minutes_i/ 60.0);

		lon = multiplier * lon;
		
		return lon;
	}

	uint16_t get_relay_box_1_flags() { return relay_box_1_flags_desired; }
	uint16_t get_relay_box_2_flags() { return relay_box_2_flags_desired; }
	uint16_t get_relay_box_3_flags() { return relay_box_3_flags_desired; }
	uint16_t get_actuators_control_flags() { return actuators_control_flags_desired; }
	
	/**
		Foi construida uma mensagem do tipo ParsedMsg que, originalmente, era uma lista de mensagens 
		recebidas do sistema Paravan. Essa mensagem ainda é dividida em MsgType e MsgData.
		A MsgData ainda é dividida em fval e ival.
		Quando envia-se, no caso p_msg, ou seja, a mensagem inteira, obviamente envia-se todos os dados,
		sendo eles: 
		p_msg.type e p_msg.type.ival e p_msg.type.fval  
	**/
	uint16_t processing_dispatch_data(char * const& data);
	uint16_t processing_excavator_data(char * const& data);
	uint16_t processing_data_signal(const std::string& data, const uint16_t description);

	const std::string currentDateTime();

private: 
	bool first_cycle, first_cycle_rb1, persist, send_bucket, gas_flag;
	
	int serial_handle_; // handle to the Peak CAN device
	std::string new_data_d, 
	new_data_a, 
	new_data_f, 
	new_data_prc, 
	new_data_pos, 
	new_data_se, 
	new_data_sd,
	new_data_fb,
	new_data_tr,
	new_data_fe,
	new_data_fa,
	new_data_b,
	new_data_st,
	new_data_ba,
	new_data_pa,
	end_string = ",?", 
	error_msg,
	new_data_default;
	int shutdown_sequence_counter, cont, last_send;
	uint16_t old_relay_box_1_flags_desired, old_relay_box_2_flags_desired, old_relay_box_3_flags_desired, gears;
	void (*msg_callback)(ParsedMsg) = NULL; // function pointer to the callback for received CAN messages
	void spin_thread(); // contains the infinite loop for CAN send/receive
	void send_msg(std::string const& data, const uint16_t description);
	char * value;
	char * lat_d;
    char * lat_m;
    char * lon_d;
    char * lon_m;

	char * actual_gas;
	char * actual_brake;
	char * actual_steering;
	char * actual_gear;
	char * actual_status;
	char * actual_button;
	char * actual_petrol;
    
	float lat_multiplier, lon_multiplier;

	float get_final_steering_value();
	float get_final_gas_brake_value();

	template<typename Enumeration> // helper function for converting "enum class" to int
	static auto as_integer(
			Enumeration const value) -> typename std::underlying_type<Enumeration>::type;

	std::atomic<float> // steering and gas/brake values
	steering_value_desired, gas_brake_value_desired, steering_value_actual, gas_brake_value_actual;

	std::atomic<uint16_t> relay_box_1_flags_desired, relay_box_2_flags_desired, relay_box_3_flags_desired,
	relay_box_1_flags_actual, relay_box_2_flags_actual, relay_box_3_flags_actual, actuators_control_flags_desired, actuators_control_flags_actual;


	std::atomic<bool> // variables for communication with thread
	serial_thread_alive;

	std::atomic<bool> // which CAN messages should be sent out
		send_shutdown_sequence, send_steering, send_gas_brake, send_relay_box_1,
		send_relay_box_2, send_relay_box_3, send_relay_box_keep_alive, keep_steering_neutral, send_truck_mode;

	float max_steering_jump, max_gas_brake_jump, max_steering_value, max_gas_value, max_brake_value;

	/*pcanfd_msg msg_send_steering, msg_send_gas_brake, msg_send_relay_box_1,
			msg_send_relay_box_2, msg_send_relay_alive, msg_send_shutdown;*/

	//pcanfd_msgs* msgs_received;

	std::thread serial_thread_; // thread for CAN send/receive
};
