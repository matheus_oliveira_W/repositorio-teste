#include "CycleTimer.h"

using std::chrono::milliseconds;
using std::chrono::steady_clock;

CycleTimer::CycleTimer(const milliseconds cycle_time)
{
	desired_cycle_time_ = cycle_time;
	time_point_last_cycle_ = steady_clock::now();
	cycle_millis_ = 0;
}

bool CycleTimer::check_new_cycle()
{
	if ((steady_clock::now() - time_point_last_cycle_) > desired_cycle_time_)
	{
		std::chrono::duration<double, std::milli> cycle_time =
				(steady_clock::now() - time_point_last_cycle_);
		cycle_millis_ = cycle_time.count();
		time_point_last_cycle_ = steady_clock::now();
		return true;
	}
	else
	{
		return false;
	}
}

void CycleTimer::sleep_until_next_cycle()
{
	auto time_left = desired_cycle_time_
			- (steady_clock::now() - time_point_last_cycle_);

	if (time_left > std::chrono::microseconds(0))
	{
		std::this_thread::sleep_for(time_left);
	}
	time_point_last_cycle_ = steady_clock::now();
}

double CycleTimer::last_cycle_millis()
{
	return cycle_millis_;
}
