#include "libuemserial.h"

using std::endl;
using std::chrono::milliseconds;

using namespace serial;

Serial actuators, excavator; //actuators; 

Serial aux; 


UMSerial::UMSerial()
{
	serial_handle_ = 0;
	steering_value_desired = 0;
	gas_brake_value_desired = 0;
	serial_thread_alive = false;
	relay_box_1_flags_desired = 0;
	relay_box_2_flags_desired = 0;
	relay_box_3_flags_desired = 0;
	actuators_control_flags_desired = 0;
	relay_box_1_flags_actual = 0;
	relay_box_3_flags_actual = 0;
	cont = 0;
	
	msg_callback = NULL;

	max_steering_jump = 2.0; // default value does not limit
	max_gas_brake_jump = 2.0; // default value does not limit

	send_steering = false;
	send_shutdown_sequence = false;
	send_gas_brake = false;
	send_relay_box_1 = false;
	send_relay_box_2 = false;
	send_relay_box_3 = false;
	send_relay_box_keep_alive = false;
	send_truck_mode = false;
	first_cycle = false;
	persist = false;
	gas_flag;

}

void UMSerial::set_value_jump_limits(float max_steering_jump, float max_gas_brake_jump)
{
	this->max_steering_jump = max_steering_jump;
	this->max_gas_brake_jump = max_gas_brake_jump;
}

void UMSerial::set_value_limits(float max_steering_value, float max_gas_value, float max_brake_value)
{
	this->max_steering_value = max_steering_value;
	this->max_gas_value = max_gas_value;
	this->max_brake_value = max_brake_value;
}

void UMSerial::set_sending_status(bool send_steering, bool send_gas_brake, bool send_relay_box_1, bool send_relay_box_2, bool send_relay_box_3, bool send_truck_mode)
{
	this->send_steering = send_steering;
	this->send_gas_brake = send_gas_brake;
	this->send_relay_box_1 = send_relay_box_1;
	this->send_relay_box_2 = send_relay_box_2;
	this->send_relay_box_3 = send_relay_box_3;
	this->send_truck_mode = send_truck_mode;
}

template<typename Enumeration>
auto UMSerial::as_integer(
		Enumeration const value) -> typename std::underlying_type<Enumeration>::type
{
	return static_cast<typename std::underlying_type<Enumeration>::type>(value);
}

void UMSerial::set_steering_value(const float steering_value)
{
	//UEM_LOG_INFO("Received steering value " << steering_value);
	steering_value_desired = steering_value;
}

void UMSerial::set_gas_brake_value(const float gas_brake_value)
{
	//UEM_LOG_ERROR("Received gas/brake value " << gas_brake_value);
	gas_brake_value_desired = gas_brake_value;
}

/*float UMSerial::get_final_steering_value()
{
	float steering_value = steering_value_desired;

	if (steering_value > max_steering_value)
	{
		steering_value = max_steering_value;
	}
	else if (steering_value < -max_steering_value)
	{
		steering_value = -max_steering_value;
	}

	double delta_steering = steering_value - steering_value_actual;

	if (std::fabs(delta_steering) > max_steering_jump)
	{
		//UEM_LOG_INFO("Limiting steering rate: desired = " << steering_value << ", actual = " << steering_value_actual);

		if (delta_steering > 0)
		{
			return steering_value_actual + max_steering_jump;
			//UEM_LOG_INFO("DELTA_STEER > 0: " << steering_value_actual + max_steering_jump);
		}
		else
		{
			return steering_value_actual - max_steering_jump;
			//UEM_LOG_INFO("ELSE: " << steering_value_actual - max_steering_jump);
		}
	}
	else
	{
		//UEM_LOG_INFO("GET_FINAL_STEERING: " << steering_value);
		return steering_value;
	}
}

float UMSerial::get_final_gas_brake_value()
{
	float gas_brake_value = gas_brake_value_desired;

	if (gas_brake_value > max_gas_value)
	{
		gas_brake_value = max_gas_value;
	}
	else if (gas_brake_value < max_brake_value)
	{
		gas_brake_value = max_brake_value;
	}

	double delta_gas_brake = gas_brake_value - gas_brake_value_actual;

	if (std::fabs(delta_gas_brake) > max_gas_brake_jump) // if actual gas/brake always < 0, change this condition
	{
		//UEM_LOG_INFO("Limiting gas/brake rate: desired = " << gas_brake_value << ", actual = " << gas_brake_value_actual);

		if (delta_gas_brake > 0)
		{
			return gas_brake_value_actual + max_gas_brake_jump;
			//UEM_LOG_INFO("GAS BRAKE > 0: " << gas_brake_value_actual + max_gas_brake_jump);
		}
		else
		{
			return gas_brake_value_actual - max_gas_brake_jump;
			//UEM_LOG_INFO("ELSE: " << gas_brake_value_actual - max_gas_brake_jump);
		}
	}
	else
	{
		//UEM_LOG_INFO("GET FINAL GAS BRAKE: " << gas_brake_value);
		return gas_brake_value;
	}
}*/

void UMSerial::set_relay_box_1_flags(const uint16_t flag_bits)
{
	relay_box_1_flags_desired = flag_bits;

	UEM_LOG_ERROR("Received relay box 1 flags: " << relay_box_1_flags_desired);
}

void UMSerial::set_relay_box_2_flags(const uint16_t flag_bits)
{
	relay_box_2_flags_desired = flag_bits;
	UEM_LOG_ERROR("Received relay box 2 flags: " << flag_bits);
}

void UMSerial::set_relay_box_3_flags(const uint16_t flag_bits)
{
	relay_box_3_flags_desired = flag_bits;
	UEM_LOG_ERROR("Received relay box 3 flags: " << flag_bits);
}

void UMSerial::set_actuators_control(const uint16_t flag_bits)
{
	actuators_control_flags_desired = flag_bits;
	//UEM_LOG_ERROR("ACT CONTROL: " << //actuators_control_flags_desired);
	//UEM_LOG_ERROR("Received //actuators control flags: " << std::bitset<16>(flag_bits));
}

bool UMSerial::open_serial_device(const std::string& device_name)
{

	try
	{
	
		//char * clock[100] = std::chrono::system_clock::now();

		//UEM_LOG_ERROR("Date and time: " << clock);

		actuators.setPort("/dev/ttyUSB0"); //porta real: "/dev/ttyUSB0"
		actuators.setBaudrate(9600);
		serial::Timeout to = serial::Timeout::simpleTimeout(50); //DO NOT MODIFY TIMEOUT VALUE. SET TO 50 BY LUIZA.
		actuators.setTimeout(to);
		actuators.open();  

		/*//aux.setPort("/dev/pts/20"); //porta real: "/dev/ttyUSB0"
		//aux.setBaudrate(9600);
		serial::Timeout toa = serial::Timeout::simpleTimeout(10);
		//aux.setTimeout(toa);
		//aux.open();*/
		

		/*excavator.setPort("/dev/ttyUSB1"); //porta real: "/dev/ttyUSB0"
		excavator.setBaudrate(9600);
		serial::Timeout toex = serial::Timeout::simpleTimeout(50);
		excavator.setTimeout(toex);
		excavator.open();
		serial_handle_ = excavator.isOpen();*/

		/*UEM_LOG_INFO("aqui2");
		//excavator.isOpen();*/

		serial_handle_ = actuators.isOpen(); 
		UEM_LOG_DEBUG("opened serial port: ");

		return true;   
    }
    catch (SerialException e) 
	{
        UEM_LOG_INFO("Failed to connect: " << e.what());
     
        return 0;
    }
}

void UMSerial::send_msg(std::string const& data, const uint16_t description)
{
	
	switch (description)
	{
		case 101:
		{
			/*if (data != new_data_a)
			{
				actuators.write("A," + data + end_string);
				//aux.write("A," + data + end_string);
				new_data_a = data;
				std::string a = "A," + data + end_string; 
				processing_data_signal(a, 101);
			}*/
			actuators.write("A," + data + end_string);
				////aux.write("A," + data + end_string);
				//new_data_a = data;
			std::string a = "A," + data + end_string; 
			processing_data_signal(a, 101);
			break;
		}
		case 102:
		{
			if (data != new_data_d)
			{
				actuators.write("D," + data + end_string);
				//aux.write("D," + data + end_string);
				new_data_d = data;

				std::string a = "D," + data + end_string; 
				processing_data_signal(a, 102);
			}
			/*std::string a = "D," + data + end_string; 
			processing_data_signal(a, 102);*/
			break;
		}
		case 103:
		{
			/*if (data != new_data_f)
			{
				actuators.write("F," + data + end_string);
				//aux.write("F," + data + end_string);
				new_data_f = data;

				std::string a = "F," + data + end_string; 
				processing_data_signal(a, 103);
				
			}*/
			actuators.write("F," + data + end_string);
			////aux.write("F," + data + end_string);
				//new_data_f = data;

			std::string a = "F," + data + end_string; 
			processing_data_signal(a, 103);
			break;
		}
		case 105:
		{
			if (data != new_data_fb)
			{
				actuators.write("FB," + data + end_string);
				//aux.write("FB," + data + end_string);
				new_data_fb = data;
			}
			break;
		}
		case 107:
		{
			if (data != new_data_prc)
			{
				actuators.write("PRC," + data + end_string);
				//aux.write("PRC," + data + end_string);
				new_data_prc = data;
			}
			break;
		}	
		case 108:
		{
			if (data != new_data_pos)
			{
				actuators.write("POS," + data + end_string);
				//aux.write("POS," + data + end_string);
				new_data_pos = data;
			}
			
			break;
		}
		case 111:
		{
			if (data != new_data_se)
			{
				actuators.write("SE," + data + end_string);
				//aux.write("SE," + data + end_string);
				new_data_se = data;
			}
			
			break;
		}	
		case 112:
		{
			if (data != new_data_sd)
			{
				actuators.write("SD," + data + end_string);
				//aux.write("SD," + data + end_string);
				new_data_sd = data;
			}
			
			break;
		}
		case 104:
		{
			if (data != new_data_tr)
			{
				actuators.write("T," + data + end_string);
				//aux.write("T," + data + end_string);
				new_data_tr = data;
				std::string a = "T," + data + end_string; 
				processing_data_signal(a, 104);
			}
			/*std::string a = "T," + data + end_string; 
			processing_data_signal(a, 104);*/
			break;
		}
		case 110:
		{
			if (data != new_data_fe)
			{
				actuators.write("FE," + data + end_string);
				//aux.write("FE," + data + end_string);
				new_data_fe = data;
				std::string a = "FE," + data + end_string; 
				processing_data_signal(a, 110);
			}
			/*std::string a = "FE," + data + end_string; 
			processing_data_signal(a, 110);*/
			break;
		}
		case 113:
		{
			if (data != new_data_fa)
			{
				actuators.write("FA," + data + end_string);
				//aux.write("FA," + data + end_string);
				new_data_fa = data;
			}
			
			break;
		}
		case 106:
		{
			if (data != new_data_b)
			{
				actuators.write("B," + data + end_string);
				//aux.write("B," + data + end_string);
				new_data_b = data;
			}
			
			break;
		}
		case 114:
		{
			if (data != new_data_st)
			{
				actuators.write("ST," + data + end_string);
				//aux.write("ST," + data + end_string);
				new_data_st = data;
			
			}
			
			break;
		}
		case 115:
		{
			actuators.write("BA," + data + end_string);
			//aux.write("BA," + data + end_string);
			persist = true;
			send_bucket = true;
						
			break;
		}
		case 116:
		{
			if (data != new_data_pa)
			{
				actuators.write("PA," + data + end_string);
				//aux.write("PA," + data + end_string);
				new_data_pa = data;
			}
			
			break;
		}

	}
		
}

void UMSerial::spin_thread()
{
	CycleTimer timer_send_steering(milliseconds(40)); // spec: 40 ms
	CycleTimer timer_send_gas_brake(milliseconds(40)); // spec: 40 ms
	CycleTimer timer_receive_from_dispatch(milliseconds(50));
	//CycleTimer timer_receive_from_excavator(milliseconds(50));
	CycleTimer timer_send_relay_alive(milliseconds(100));
	CycleTimer timer_send_relay_functions(milliseconds(10));
	CycleTimer timer_send_actuators_control(milliseconds(10));
	CycleTimer timer_shutdown_sequence(milliseconds(40));
	CycleTimer timer_thread_cycle(milliseconds(1));
	CycleTimer timer_data_signal(milliseconds(50));

	while (serial_thread_alive)
	{
	
		if (timer_send_actuators_control.check_new_cycle() && send_truck_mode)
		{
			if (!first_cycle)
			{
				first_cycle = true;
				send_msg(std::to_string(2), 114);
			}
			
			//UEM_LOG_ERROR("//actuators_control_flags_desired: " << //actuators_control_flags_desired);
			switch(actuators_control_flags_desired)
			{
				case (1): //autonomous mode
				{
					send_msg(std::to_string(0), 114);
					break;
				}
				case (2): //manual mode
				{
					send_msg(std::to_string(2), 114);
					break;
				}
				case (4): //radio controled mode
				{
					send_msg(std::to_string(1), 114);
					break;
				}
				case (8): //radio controled mode
				{
					send_msg(std::to_string(1), 114);
					break;
				}
			}
	
		}

		/*
		 * relay box function bit flags
		 * ##### DO NOT MODIFY ##### Tested 19.02.2018 against Paravan Simulator
		 */
		if (timer_send_relay_functions.check_new_cycle())
		{
			if (send_relay_box_1) // relay box 1 flags
			{
				//ROS_INFO_STREAM("old_relay_box_1_flags_desired: " << old_relay_box_1_flags_desired);
				//ROS_INFO_STREAM("relay_box_1_flags_desired: " << relay_box_1_flags_desired);

				/*PRÉ-CHAVE*/
				if ((old_relay_box_1_flags_desired + 1) == relay_box_1_flags_desired)
				{
					send_msg(std::to_string(1), 107);
					old_relay_box_1_flags_desired = relay_box_1_flags_desired;
										
				}
				else
				{
					if ((old_relay_box_1_flags_desired - 1) == relay_box_1_flags_desired)
					{
						send_msg(std::to_string(0), 107);
						old_relay_box_1_flags_desired = relay_box_1_flags_desired;
					}
											
				}
				
				/*PÓS-CHAVE*/
				if ((old_relay_box_1_flags_desired + 2) == relay_box_1_flags_desired)
				{
					send_msg(std::to_string(1), 108);
					old_relay_box_1_flags_desired = relay_box_1_flags_desired;
									
				}
				else
				{
					if ((old_relay_box_1_flags_desired - 2) == relay_box_1_flags_desired)
					{
						send_msg(std::to_string(0), 108);
						old_relay_box_1_flags_desired = relay_box_1_flags_desired;
					}
											
				}
				
				/*SETA ESQUERDA*/
				if ((old_relay_box_1_flags_desired + 4) == relay_box_1_flags_desired)
				{
					send_msg(std::to_string(1), 111);
					old_relay_box_1_flags_desired = relay_box_1_flags_desired;
										
				}
				else
				{
					if ((old_relay_box_1_flags_desired - 4) == relay_box_1_flags_desired)
					{
						send_msg(std::to_string(0), 111);
						old_relay_box_1_flags_desired = relay_box_1_flags_desired;
					}
											
				}
				
				/*SETA DIREITA*/
				if ((old_relay_box_1_flags_desired + 8) == relay_box_1_flags_desired)
				{
					send_msg(std::to_string(1), 112);
					old_relay_box_1_flags_desired = relay_box_1_flags_desired;
										
				}
				else
				{
					if ((old_relay_box_1_flags_desired - 8) == relay_box_1_flags_desired)
					{
						send_msg(std::to_string(0), 112);
						old_relay_box_1_flags_desired = relay_box_1_flags_desired;
					}
											
				}

				/*FAROL BAIXO*/
				if ((old_relay_box_1_flags_desired + 128) == relay_box_1_flags_desired)
				{	
					send_msg(std::to_string(1), 105);
					old_relay_box_1_flags_desired = relay_box_1_flags_desired;
									
				}
				else
				{
					if ((old_relay_box_1_flags_desired - 128) == relay_box_1_flags_desired)
					{
						send_msg(std::to_string(0), 105);
						old_relay_box_1_flags_desired = relay_box_1_flags_desired;
					}
											
				}
				/*FAROL ALTO*/
				if ((old_relay_box_1_flags_desired + 256) == relay_box_1_flags_desired)
				{
					send_msg(std::to_string(1), 113);
					old_relay_box_1_flags_desired = relay_box_1_flags_desired;
										
				}
				else
				{
					if ((old_relay_box_1_flags_desired - 256) == relay_box_1_flags_desired)
					{
						send_msg(std::to_string(0), 113);
						old_relay_box_1_flags_desired = relay_box_1_flags_desired;
					}
											
				}

				/*BUZINA*/	
				if ((old_relay_box_1_flags_desired + 1024) == relay_box_1_flags_desired)
				{
					send_msg(std::to_string(1), 106);
					old_relay_box_1_flags_desired = relay_box_1_flags_desired;
										
				}
				else
				{
					if ((old_relay_box_1_flags_desired - 1024) == relay_box_1_flags_desired)
					{
						send_msg(std::to_string(0), 106);
						old_relay_box_1_flags_desired = relay_box_1_flags_desired;
					}
											
				}
				
				/*PISCA ALERTA*/
				if ((old_relay_box_1_flags_desired + 16) == relay_box_1_flags_desired)
				{
					//ROS_INFO_STREAM("pisca alerta on");
					send_msg(std::to_string(1), 116);
					old_relay_box_1_flags_desired = relay_box_1_flags_desired;
										
				}
				else
				{
					if ((old_relay_box_1_flags_desired - 16) == relay_box_1_flags_desired)
					{
						//ROS_INFO_STREAM("pisca alerta off");
						send_msg(std::to_string(0), 116);
						old_relay_box_1_flags_desired = relay_box_1_flags_desired;
					}
											
				}

				/*ABRIR BÁSCULA*/	
				if ((old_relay_box_1_flags_desired + 64) == relay_box_1_flags_desired)
				{
					//UEM_LOG_INFO("SOBE 1");
					send_msg(std::to_string(1), 115);
					old_relay_box_1_flags_desired = relay_box_1_flags_desired;
					last_send = 1;
										
				}
				else
				{
					if ((old_relay_box_1_flags_desired - 64) == relay_box_1_flags_desired)
					{
					//UEM_LOG_INFO("SOBE 2");
						send_msg(std::to_string(1), 115);
						old_relay_box_1_flags_desired = relay_box_1_flags_desired;
						last_send = 1;
										
					}
				}
				
				/*FECHAR BÁSCULA*/
				if ((old_relay_box_1_flags_desired + 4096) == relay_box_1_flags_desired)
				{
					//UEM_LOG_INFO("DESCE 1");
					send_msg(std::to_string(0), 115);
					old_relay_box_1_flags_desired = relay_box_1_flags_desired;
					last_send = 0;
				}
				else
				{
					if ((old_relay_box_1_flags_desired - 4096) == relay_box_1_flags_desired)
					{
					//UEM_LOG_INFO("DESCE 2");
						send_msg(std::to_string(0), 115);
						old_relay_box_1_flags_desired = relay_box_1_flags_desired;
						last_send = 0;
										
					}
				}
				
								
			}

			if (send_relay_box_2)
			{				
				switch(relay_box_2_flags_desired)
				{
					case 1:
					{
						send_msg(std::to_string(7), 104);
						break;
					}
					case 2:
					{
						send_msg(std::to_string(0), 104);
						break;
					}
					case 4:
					{
						send_msg(std::to_string(1), 104);
						break;
					}
					case 8:
					{
						send_msg(std::to_string(2), 104);
						break;
					}
					case 16:
					{
						send_msg(std::to_string(3), 104);
						break;
					}
					case 32:
					{
						send_msg(std::to_string(4), 104);
						break;
					}
					case 64:
					{
						send_msg(std::to_string(5), 104);
						break;
					}
					case 128:
					{
						send_msg(std::to_string(6), 104);
						break;
					}
				}

			}

			if(send_relay_box_3)
			{
				/*FREIO DE EMERGÊNCIA*/	
				if ((old_relay_box_3_flags_desired + 1) == relay_box_3_flags_desired)
				{
					send_msg(std::to_string(1), 110);
					old_relay_box_3_flags_desired = relay_box_3_flags_desired;
										
				}
				else
				{
					if ((old_relay_box_3_flags_desired - 1) == relay_box_3_flags_desired)
					{
						send_msg(std::to_string(0), 110);
						old_relay_box_3_flags_desired = relay_box_3_flags_desired;
					}
											
				}
			}
		}
		/*
		 * steering position
		 * 
		 */
		if (timer_send_steering.check_new_cycle() && send_steering)
		{
			//Para garantir que o valor certo sempre sera usado
			
			float val = steering_value_desired; //std::max(-1.0, std::min(1.0, (double) get_final_steering_value()));
			uint64_t steer_pos = val * 50 + 50; // range: 0 to 100
			
			send_msg(std::to_string(steer_pos), 102); //102 is the steering code
			
			//UEM_LOG_ERROR("steer_pos: " << steer_pos << " val: " << get_final_steering_value());
		}

		/*
		 * gas/brake position;
		 * 
		 */
		if (timer_send_gas_brake.check_new_cycle() && send_gas_brake)
		{
			float val = gas_brake_value_desired; //std::max(-1.0, std::min(1.0, (double) get_final_gas_brake_value()));
			
			if (val >= 0)
				{
					if (gas_flag == true)
					{
						uint64_t gas_brake_pos = val * 100; // range: 0 to 100
						send_msg(std::to_string(gas_brake_pos), 101); 
					}
					else
					{
						uint64_t gas_brake_pos = val * 100; // range: 0 to 100
					
						send_msg(std::to_string(gas_brake_pos), 101); 
						send_msg(std::to_string(0), 103); //101 is the brake code

						gas_flag = true;
					}
				}
			else if(val < 0)
				{
					if(gas_flag == false)
					{
						uint64_t gas_brake_pos = -val * 100; // range: 0 to 100
						send_msg(std::to_string(gas_brake_pos), 103); //103 is the brake code
					}
					else
					{
						uint64_t gas_brake_pos = -val * 100; // range: 0 to 100

						send_msg(std::to_string(0), 101); //101 is the gas code
						send_msg(std::to_string(gas_brake_pos), 103); //103 is the brake code

						gas_flag = false;
					}
					
					

				}
				
		}

		/*
		 * receive messages
		 * ##### DO NOT MODIFY ##### Tested 10.04.2019
		 */
		if (timer_receive_from_dispatch.check_new_cycle() && send_bucket)
		{
			std::string buffer = actuators.readline('\n');

			//UEM_LOG_INFO("ret: " << buffer.size());
			
			char char_buffer[buffer.size()];

			strcpy(char_buffer, buffer.c_str());

			actuators.flush();

			buffer = '\0';

			if (strlen(char_buffer) > 3)
			{
				UEM_LOG_INFO("Received buffer: " << buffer);
				UEM_LOG_INFO("Received: " << char_buffer);
				
				processing_dispatch_data(char_buffer);

				memset(char_buffer,'\0',strlen(char_buffer));
				
			}
			else if(strlen(char_buffer) >= 0 && strlen(char_buffer) <= 3)
			{
				//UEM_LOG_INFO("Received buffer: " << buffer);
				//UEM_LOG_INFO("Received: " << char_buffer);

				if(persist == true && last_send == 1)
				{
					actuators.write("BA," + std::to_string(1) + end_string);
				}
				else if (persist == true && last_send == 0)
				{
					actuators.write("BA," + std::to_string(0) + end_string);
				}

				memset(char_buffer,'\0',strlen(char_buffer));
			}
			

		}

		/*if (timer_receive_from_excavator.check_new_cycle())
		{
			std::string buffer = excavator.readline('?');
			
			char char_buffer[buffer.size()];

			strcpy(char_buffer, buffer.c_str());

			excavator.flush();

			UEM_LOG_INFO("Received buffer: " << buffer);

			buffer = '\0';

			if (strlen(char_buffer) > 30)
			{
				UEM_LOG_INFO("Received buffer: " << buffer);
				UEM_LOG_INFO("Received: " << char_buffer);
				
				processing_excavator_data(char_buffer);

				memset(char_buffer,'\0',strlen(char_buffer));
				
			}
			else if(strlen(char_buffer) > 0 && strlen(char_buffer) <= 30)
			{
				UEM_LOG_INFO("Received buffer: " << buffer);
				UEM_LOG_INFO("Received: " << char_buffer);

				memset(char_buffer,'\0',strlen(char_buffer));
			}

		}*/

		if (timer_data_signal.check_new_cycle())
		{
			std::string buffer = actuators.readline('?');
			
			char char_buffer[buffer.size()];

			strcpy(char_buffer, buffer.c_str());

			actuators.flush();

			buffer = '\0';

			if (strlen(char_buffer) > 2)
			{
				//processing_data_signal(char_buffer);
				memset(char_buffer,'\0',strlen(char_buffer));
			}

		}

		timer_thread_cycle.sleep_until_next_cycle();
	}
}

/*void UMSerial::coords_converter(const int loop, char * data)
{
	
	switch (loop)
    {
        case 0:
        {
            lat_d = data;
            break;
        }
        case 1:
        {
            lat_m = data;
            break;
        }
        case 2:
        {
            if (std::string(data) == "N")
			{
				lat_multiplier = 1.0;
			}
			else
			{
				lat_multiplier = -1.0;
			}
			
            break;
        }
        case 3:
        {
            lon_d = data;
            break;
        }
        case 4:
        {
            lon_m = data;
            break;
        }
        case 5:
        {
            if (std::string(data)== "E")
			{
				lon_multiplier = 1.0;
			}
			else
			{
				lon_multiplier = -1.0;
			}
			
            break;
        }
    }
}

void UMSerial::actual_values(const int loop, char * data)
{
	switch (loop)
    {
        case 0:
        {
            actual_gas = data;

            break;
        }
        case 2:
        {
            actual_brake = data;

            break;
        }
        case 4:
        {
			actual_steering = data;

            break;
        }
        case 6:
        {
            actual_gear = data;

            break;
        }
        case 8:
        {
			actual_status = data;

            break;
        }
        case 10:
        {
            actual_button = data;

            break;
        }
		case 12:
        {
            actual_petrol = data;

            break;
        }
    }
}*/

uint16_t UMSerial::processing_data_signal(const std::string& data, const uint16_t description)
{
	std::string buffer = actuators.readline(',?');

	char snt_buffer[data.size()];
	strcpy(snt_buffer, data.c_str());

	

	if (buffer.size() > 2)
	{
		ParsedMsg p_msg;
		p_msg.type = MsgType::DATA_SIGNAL;
		msg_callback(p_msg);

		char rcvd_buffer[buffer.size()];
		strcpy(rcvd_buffer, buffer.c_str());
		actuators.flush();
		buffer = '\0';
		
		/*char * snt_bucket;
		snt_bucket = strtok (snt_buffer, ",");

		if((strcmp(snt_bucket, "BA") == 0) || (strcmp(snt_bucket, "BF") == 0)) return 0;
		
		else */if ((strcmp(snt_buffer, rcvd_buffer) != 0))
		{
			UEM_LOG_INFO("******* DIF *********");
			UEM_LOG_ERROR("snt_buffer: " << snt_buffer);
			
			UEM_LOG_ERROR("rcvd_buffer: " << rcvd_buffer);

			/*char * repeat_data;
			repeat_data = strtok (snt_buffer, ",");
			UEM_LOG_ERROR("aqui");
			if (repeat_data != NULL)
			{
				value = strtok (NULL, ",");
			}
			
			send_msg(value, description);
			UEM_LOG_ERROR("value: " << value);
			UEM_LOG_ERROR("description: " << description);*/
			actuators.write(data);
		}
	
	memset(rcvd_buffer,'\0',strlen(rcvd_buffer));
	}
}


/*uint16_t UMSerial::processing_excavator_data(char * const& data)
{
	
	ParsedMsg p_msg;

	char new_data[strlen(data)];
	strcpy(new_data, data);
	
	char * new_data_split;
	
	new_data_split = strtok (new_data, ",");
	
	if ((strcmp(new_data_split, "EX") == 0))
	{
		for (int i = 0; i < 7; i++)
			{
				char * data;
				data = strtok(NULL,",");
				coords_converter(i, data);

			}

		std::stringstream stream_lat;
		stream_lat << std::fixed << std::setprecision(7) << lat_degree(lat_d,lat_m,lat_multiplier);
		std::string result_lat = stream_lat.str();

		std::stringstream stream_lon;
		stream_lon << std::fixed << std::setprecision(7) << lon_degree(lon_d,lon_m,lat_multiplier);
		std::string result_lon = stream_lon.str();

		p_msg.type = MsgType::RECEIVED_EX_LOCATION;
		p_msg.data.lat = result_lat;
		p_msg.data.lon = result_lon;

		msg_callback(p_msg);

		stream_lon << '\0';
		stream_lat << '\0';

		result_lat = '\0';
		result_lon = '\0';


	}
	if ((strcmp(new_data_split, "DP") == 0))
	{
		for (int i = 0; i < 7; i++)
			{
				char * data;
				data = strtok(NULL,",");
				coords_converter(i, data);

			}

		std::stringstream stream_lat;
		stream_lat << std::fixed << std::setprecision(7) << lat_degree(lat_d,lat_m,lat_multiplier);
		std::string result_lat = stream_lat.str();

		std::stringstream stream_lon;
		stream_lon << std::fixed << std::setprecision(7) << lon_degree(lon_d,lon_m,lat_multiplier);
		std::string result_lon = stream_lon.str();

		p_msg.type = MsgType::RECEIVED_DP_LOCATION;
		p_msg.data.lat = result_lat;
		p_msg.data.lon = result_lon;

		msg_callback(p_msg);

		stream_lon << '\0';
		stream_lat << '\0';

		result_lat = '\0';
		result_lon = '\0';


	}
	if (strcmp(new_data_split, "A") == 0)
	{
		for (int i = 0; i < 14; i++)
			{
				new_data_split = strtok(NULL,",");
				actual_values(i, new_data_split);

			}
		

		memset(new_data,'\0',strlen(new_data));
		memset(new_data_split,'\0',strlen(new_data_split));

		UEM_LOG_ERROR("actual_gas: " << actual_gas <<
					  ", actual_brake: " << actual_brake <<
					  ", actual_steering: " << actual_steering <<
					  ", actual_gear: " << actual_gear <<
					  ", actual_status: " << actual_status <<
					  ", actual_button: " << actual_button <<
					  ", actual_petrol: " << actual_petrol);
	}		

}*/

uint16_t UMSerial::processing_dispatch_data(char * const& data)
{	
	ParsedMsg p_msg;

	char new_data[strlen(data)];
	strcpy(new_data, data);
	
	char * new_data_split;
	
	new_data_split = strtok (new_data, ",");
		
	/*	if (new_data_split != NULL)
	{
		value = strtok (NULL, ",");
	}*/

	if (strcmp(new_data_split,"BA") == 0)
	{
		persist = false;
		send_bucket = false;
		p_msg.type = MsgType::BUCKET_OPENED;
		
	}
	if (strcmp(new_data_split,"BF") == 0)
	{
		persist = false;
		send_bucket = false;
		p_msg.type = MsgType::BUCKET_CLOSED;

		
	}

	/*switch (value[0])
	{
	case '0':
	{
		p_msg.data.ival = 0;
		
		break;
	}
	case '1':
	{
		//UEM_LOG_ERROR("Insp 2");
		p_msg.data.ival = 1;
		
		break;
	}
	case '2':
	{
		//UEM_LOG_ERROR("Insp 2");
		p_msg.data.ival = 2;
		
		break;
	}
		
	}*/

	memset(new_data,'\0',strlen(new_data));
	memset(new_data_split,'\0',strlen(new_data_split));

	msg_callback(p_msg);
}

void UMSerial::spin()
{
	if (serial_handle_ > 0) // SERIAL device has been opened
	{
		serial_thread_alive = true;
		serial_thread_ = std::thread(&UMSerial::spin_thread, this); // spawn SERIAL sender/receiver thread
	}
	else
	{
		UEM_LOG_ERROR("error starting SERIAL thread: SERIAL device not opened");
	}
}

void UMSerial::shutdown()
{
	if (serial_thread_alive)
	{
		serial_thread_alive = false; // request SERIAL thread to stop
		serial_thread_.join();
	}

	if (serial_handle_ > 0) // CAN device has been opened previously
	{
		actuators.flush();
		
		actuators.close();
		UEM_LOG_INFO("closed SERIAL connection, libuemserial shutdown complete!");
	}
} 

const std::string currentDateTime() 
{
    time_t     now = time(0);
    struct tm  tstruct;
    char       buf[80];
    tstruct = *localtime(&now);
    // Visit http://en.cppreference.com/w/cpp/chrono/c/strftime
    // for more information about date/time format
    strftime(buf, sizeof(buf), "%Y-%m-%d.%X", &tstruct);

    return buf;
}
