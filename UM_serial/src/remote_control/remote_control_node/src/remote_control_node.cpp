#include "ros/ros.h"
#include <ros/package.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Bool.h>
#include <std_msgs/UInt16.h>
#include <std_msgs/UInt64.h>
#include <std_msgs/Empty.h>
#include <std_srvs/SetBool.h>
#include <std_srvs/Empty.h>
#include <sensor_msgs/NavSatFix.h>

#include "path_follower/PathFollowerService.h"
#include "path_follower/ServiceCommand.h"

#include <iostream>
#include <fstream>
#include <ctime>
#include <mutex>
#include <signal.h>
#include <ros/ros.h>
#include <ros/xmlrpc_manager.h>
#include <serial/serial.h>

#include "libcontrol.h"

RemoteControl remote_control_lib;

std::mutex remote_control_lib_mutex;
std::string node_name;
ros::NodeHandlePtr node;
ros::Timer engine_timer;

ros::Publisher pub_remote_control_states,
                pub_rc_available, 
                pub_relay_box_1,
                pub_relay_box_2,
                pub_relay_box_3, 
                pub_gas_brake_desired, 
                pub_steering_desired, 
                pub_rc_on, 
                pub_alive,
                pub_truck_status;

ros::ServiceClient path_follower_service_client;

double param_limit_steering, param_limit_gas, param_limit_brake,
		param_max_steering_jump, param_max_gas_brake_jump;

bool param_allow_rc_publishers, param_send_steering, param_send_gas_brake, param_send_rb_1_functions,
        param_send_rb_2_functions, param_send_rb_3_functions, param_send_truck_mode, param_allow_steering_throttle;

sig_atomic_t volatile g_request_shutdown = 0; // Signal-safe flag for whether shutdown is requested

// Replacement SIGINT handler
void mySigIntHandler(int sig)
{
    g_request_shutdown = 1;
}

// Replacement "shutdown" XMLRPC callback
void shutdownCallback(XmlRpc::XmlRpcValue& params, XmlRpc::XmlRpcValue& result)
{
    int num_params = 0;
    if (params.getType() == XmlRpc::XmlRpcValue::TypeArray)
        num_params = params.size();
    if (num_params > 1)
    {
        std::string reason = params[1];
        ROS_WARN("Shutdown request received. Reason: [%s]", reason.c_str());
        g_request_shutdown = 1; // Set flag
    }

    result = ros::xmlrpc::responseInt(1, "", 0);
}

void truck_status_cb(std_msgs::UInt16 msg)
{
    if(msg.data == 8) param_allow_steering_throttle = true; //rc_gui || rc_rc || manual

	else param_allow_steering_throttle = false;
}

void serial_msg_callback(RemoteControl::ParsedMsg msg)
{
    switch (msg.type)
    {
  
    case RemoteControl::MsgType::HORN_FROM_RC:
    {
        auto ros_msg = std_msgs::UInt16();
        ros_msg.data = msg.data.ival;
        
        if(param_allow_rc_publishers) pub_relay_box_1.publish(ros_msg);

        break;
    }
    case RemoteControl::MsgType::BEAMS_FROM_RC:
    {
        auto ros_msg = std_msgs::UInt16();
        ros_msg.data = msg.data.ival;
        
        if(param_allow_rc_publishers) pub_relay_box_1.publish(ros_msg);
        
        break;
    }
    case RemoteControl::MsgType::OPEN_BUCKET_FROM_RC:
    {
        auto ros_msg = std_msgs::UInt16();
        ros_msg.data = msg.data.ival;

         if(param_allow_rc_publishers) pub_relay_box_1.publish(ros_msg);
        
        break;
    }
    case RemoteControl::MsgType::CLOSE_BUCKET_FROM_RC:
    {
        auto ros_msg = std_msgs::UInt16();
        ros_msg.data = msg.data.ival;

        if(param_allow_rc_publishers) pub_relay_box_1.publish(ros_msg);
        
        break;
    }   
    case RemoteControl::MsgType::RC_ON:
    {
        auto ros_msg = std_msgs::Bool();
        
        if (msg.data.ival == 1)
        { 
            param_allow_rc_publishers = true;
        }
        else 
        {
            param_allow_rc_publishers = false;
        }

        ros_msg.data = param_allow_rc_publishers;
        pub_rc_on.publish(ros_msg);

        break;
    } 
    case RemoteControl::MsgType::MISSION_ALLOW:
    {
        if (msg.data.ival == 1 && param_allow_rc_publishers)
        {
            ROS_INFO_STREAM("Received START_FOLLOWING trigger from remote control!");
            
            path_follower::PathFollowerService srv;
            srv.request.command = path_follower::ServiceCommand::START_FOLLOWING;

            srv.request.int_data = 1; // comment in to activate short warning horn when starting path following

            if (!path_follower_service_client.call(srv))
            {
                ROS_WARN_STREAM("Failed to call PathFollowerService, START_FOLLOWING not possible!");
            }
        }
        else if (msg.data.ival == 0 && param_allow_rc_publishers)
        {
            ROS_INFO_STREAM("Received STOP_FOLLOWING trigger from remote control!");
            
            path_follower::PathFollowerService srv;
            srv.request.command = path_follower::ServiceCommand::STOP_FOLLOWING;

            srv.request.int_data = 1; // comment in to activate short warning horn when starting path following

            if (!path_follower_service_client.call(srv))
            {
                ROS_WARN_STREAM("Failed to call PathFollowerService, START_FOLLOWING not possible!");
            }
        }

        break;
    }
    case RemoteControl::MsgType::EMERGENCY_BRAKE_FROM_RC:
    {
        auto ros_msg = std_msgs::UInt16();
        ros_msg.data = msg.data.ival;

        if(param_allow_rc_publishers) pub_relay_box_3.publish(ros_msg);
        
        break;
    } 
    case RemoteControl::MsgType::BLINKER_FROM_RC:
    {
        auto ros_msg = std_msgs::UInt16();
        ros_msg.data = msg.data.ival;

        if(param_allow_rc_publishers) pub_relay_box_1.publish(ros_msg);
        
        break;
    } 
    case RemoteControl::MsgType::RECEPTOR_STATUS:
    {
        auto ros_msg = std_msgs::Empty();
        pub_rc_available.publish(ros_msg);
        
        break;
    } 
    case RemoteControl::MsgType::MODE_FROM_RC:
    {
        auto ros_msg = std_msgs::UInt16();
        ros_msg.data = msg.data.ival;

        /*if(msg.data.ival == 8)
        {
            param_allow_steering_throttle = true;
        }
        else
        {
            param_allow_steering_throttle = false;
        }
        */

        if(param_allow_rc_publishers) pub_truck_status.publish(ros_msg);
        
        break;
    } 
    case RemoteControl::MsgType::GEAR_FROM_RC:
    {
        auto ros_msg = std_msgs::UInt16();
        ros_msg.data = msg.data.ival;

        if(param_allow_rc_publishers) pub_relay_box_2.publish(ros_msg);
        
        break;
    } 
    case RemoteControl::MsgType::BRAKE_FROM_RC:
    {
        auto ros_msg = std_msgs::Float32();
 
        ros_msg.data = -(static_cast<float> (msg.data.ival) / 100.00);
        
        if(param_allow_rc_publishers && param_allow_steering_throttle) pub_gas_brake_desired.publish(ros_msg);

        break;
    } 
    case RemoteControl::MsgType::THROTTLE_FROM_RC:
    {
        auto ros_msg = std_msgs::Float32();

        ros_msg.data = static_cast<float> (msg.data.ival) / 100.00;
        if(param_allow_rc_publishers && param_allow_steering_throttle) pub_gas_brake_desired.publish(ros_msg);
        
        break;
    } 
    case RemoteControl::MsgType::STEER_LEFT_FROM_RC:
    {
        auto ros_msg = std_msgs::Float32();
 
        ros_msg.data = -(static_cast<float> (msg.data.ival) / 100.00);
        if(param_allow_rc_publishers) pub_steering_desired.publish(ros_msg);

        break;
    } 
    case RemoteControl::MsgType::STEER_RIGHT_FROM_RC:
    {
        auto ros_msg = std_msgs::Float32();

        ros_msg.data = static_cast<float> (msg.data.ival) / 100.00;
        if(param_allow_rc_publishers) pub_steering_desired.publish(ros_msg);
        
        break;
    }
    }
}

void following_from_rc_cb(std_msgs::UInt16 msg)
{
    if (msg.data == 1)
    {
        ROS_INFO_STREAM("Received START_FOLLOWING trigger from remote control!");
        
        path_follower::PathFollowerService srv;
        srv.request.command = path_follower::ServiceCommand::START_FOLLOWING;

        srv.request.int_data = 1; // comment in to activate short warning horn when starting path following

        if (!path_follower_service_client.call(srv))
        {
            ROS_WARN_STREAM("Failed to call PathFollowerService, START_FOLLOWING not possible!");
        }
    }
}


void stop_RemoteControl()
{
    std::lock_guard < std::mutex > lock(remote_control_lib_mutex);
    remote_control_lib.shutdown();
}

bool start_RemoteControl()
{
    std::string serial_device_name;
    node->param<std::string>("/" + node_name + "/serial_device_name", serial_device_name, "/dev/pts/20");

    if (remote_control_lib.serial_thread_running())
    {
        stop_RemoteControl();
    }

    std::lock_guard < std::mutex > lock(remote_control_lib_mutex);

    if (remote_control_lib.open_serial_device(serial_device_name))
    {
        remote_control_lib.spin(); // spawn a thread for receiving and sending Paravan CAN data
        return true;
    } else
    {
        return false;
    }
}

template<typename T>
bool get_param_cached(const std::string& param_name, T& param, T default_value)
{
    T new_value;
    if (!node->getParamCached(param_name, new_value))
    {
        new_value = default_value;
    }

    if (param != new_value)
    {
        param = new_value;
        ROS_INFO_STREAM("Updated parameter " << param_name << ": " << param);
        return true;
    }

    return false;
}


void timer_callback(const ros::TimerEvent& timerEvent)
{
    pub_alive.publish(std_msgs::Empty());

}

int main(int argc, char **argv)
{
    node_name = "paravan_node";

    //last_rc_trigger = 0;

    // Override SIGINT handler
    ros::init(argc, argv, node_name, ros::init_options::NoSigintHandler);
    signal(SIGINT, mySigIntHandler);

    // Override XMLRPC shutdown
    ros::XMLRPCManager::instance()->unbind("shutdown");
    ros::XMLRPCManager::instance()->bind("shutdown", shutdownCallback);

    node = boost::make_shared<ros::NodeHandle>();

    /* --- PUBLISHERS --- */

    pub_alive = node->advertise<std_msgs::Empty>("paravan/alive", 10);
    pub_relay_box_1 = node->advertise<std_msgs::UInt16>("/paravan/relay_box_1_desired", 10);
    pub_relay_box_2 = node->advertise<std_msgs::UInt16>("/uem/relay_box_2_from_uem", 10);
    pub_relay_box_3 = node->advertise<std_msgs::UInt16>("/uem/relay_box_3_from_uem", 10);
    pub_gas_brake_desired = node->advertise<std_msgs::Float32>("/gas_brake_norm_desired", 10);
    pub_steering_desired = node->advertise<std_msgs::Float32>("/steering_norm_desired", 10);
    pub_truck_status = node->advertise<std_msgs::UInt16>("/truck_status", 10);

    // remote control availability status
    pub_rc_available = node->advertise<std_msgs::Empty>("/paravan/remote_control_available", 10);
    pub_rc_on = node->advertise<std_msgs::Bool>("/uem/rc_on", 10);
    

    /* --- SUBSCRIBERS --- */
    std::vector<ros::Subscriber> subscribers; // vector of all subscriber objects

    subscribers.push_back(node->subscribe<std_msgs::UInt16>("/truck_status", 10,truck_status_cb));


    /* --- SERVICES --- */
    path_follower_service_client = node->serviceClient<path_follower::PathFollowerService>(
            "path_follower_node/path_follower_service");

    ros::Timer timer = node->createTimer(ros::Duration(0.1), timer_callback);

    remote_control_lib.set_msg_callback(serial_msg_callback); // set callback function for received CAN messages

    start_RemoteControl(); // open CAN device and start a send/receive thread for CAN messages

    /* --- MAIN LOOP --- */
    ros::Rate loop_rate(1000); // [Hz] set update frequency for ROS subscriber callback functions

    while (!g_request_shutdown) // start ROS loop
    {
        ros::spinOnce();

        loop_rate.sleep();
    }

    stop_RemoteControl(); // close the CAN device connection

    ros::shutdown();

    return 0;
}
