#ifndef CYCLE_TIMER_H
#define CYCLE_TIMER_H

#include <chrono>
#include <string>
#include <thread>

class CycleTimer
{
public:
	CycleTimer(const std::chrono::milliseconds cycle_time);
	bool check_new_cycle(void);
	void sleep_until_next_cycle(void);
	double last_cycle_millis();

private:
	std::chrono::time_point<std::chrono::steady_clock> time_point_last_cycle_;
	std::chrono::milliseconds desired_cycle_time_;
	double cycle_millis_;
};

#endif // CYCLE_TIMER_H
