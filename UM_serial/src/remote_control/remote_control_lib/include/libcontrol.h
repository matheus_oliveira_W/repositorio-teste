#include <cstdarg>
#include <cstring>
#include <cstdint>
#include <chrono>
#include <thread>
#include <bitset>
#include <atomic>
#include <iostream>
#include <iterator>
#include <algorithm>
#include <ctime>   
#include <iomanip> 
#include <pthread.h>
#include <string>
#include <sstream>
#include <stdio.h>


#include "CycleTimer.h"
#include "Logger.h"
#include "serial/serial.h"

class RemoteControl
{
	typedef unsigned char byte;

public:
	enum class MsgType
	{
		/* Byte 2 */
		HORN_FROM_RC,
		OPEN_BUCKET_FROM_RC,
		CLOSE_BUCKET_FROM_RC,
		BEAMS_FROM_RC,
		/* Byte 3 */
		RC_ON,
		MISSION_ALLOW,
		EMERGENCY_BRAKE_FROM_RC,
		BLINKER_FROM_RC,
		ALERT_BLINKER_FROM_RC,
		RECEPTOR_STATUS,
		/* Byte 4 */
		MODE_FROM_RC,
		GEAR_FROM_RC,
		/* Left joy */
		BRAKE_FROM_RC,
		THROTTLE_FROM_RC,
		/* Right joy */
		STEER_LEFT_FROM_RC,
		STEER_RIGHT_FROM_RC,
	};

	typedef struct
	{
		std::string lat, lon;
		float fval;
		uint64_t ival;
	} MsgData;

	typedef struct
	{
		MsgType type;
		MsgData data;
	} ParsedMsg; 

	/* Constructor */
	RemoteControl();

	bool open_serial_device(const std::string& device_name);
	void set_msg_callback(void (*fcn_ptr)(ParsedMsg))
	{
		msg_callback = fcn_ptr;
	}
	void spin();
	void shutdown();

	bool serial_thread_running()
	{
		return serial_thread_alive;
	}

	std::string string_to_hex(const std::string& input);

	void hex_split(std::string hex_string);
	
	char * hex_to_bin(char * hexdec);

	void buffer_split(const int loop, std::string data);

	uint16_t processing_byte2(int byte2_buffer);
	uint16_t processing_byte3(int byte3_buffer);
	uint16_t processing_byte4(int byte4_buffer);
	uint16_t processing_joystick_left(int joystick_left_buffer);
	uint16_t processing_joystick_right(int joystick_right_buffer);
		
	/**
		Foi construida uma mensagem do tipo ParsedMsg que, originalmente, era uma lista de mensagens 
		recebidas do sistema Paravan. Essa mensagem ainda é dividida em MsgType e MsgData.
		A MsgData ainda é dividida em fval e ival.
		Quando envia-se, no caso p_msg, ou seja, a mensagem inteira, obviamente envia-se todos os dados,
		sendo eles: 
		p_msg.type e p_msg.type.ival e p_msg.type.fval  
	**/
	uint16_t processing_dispatch_data(char * const& data);
	uint16_t processing_excavator_data(char * const& data);

	std::string exec_shell_command(const std::string &cmd);

private: 
	bool first_cycle, first_cycle_rb1, persist;
	ParsedMsg p_msg;	
	char * char_bin;
	char * bin_buffer;
	int horn_flag, 
		beam_flag, 
		open_bucket_flag, 
		close_bucket_flag, 
		remote_control_on_flag, 
		receptor_status_flag,
		emergency_brake_flag,
		left_blinker_flag,
		right_blinker_flag,
		mission_allow_flag,
		mode_flag,
		gear_flag;
	int serial_handle_, byte2_sum = 0; // handle to the Peak CAN device
	void (*msg_callback)(ParsedMsg) = NULL; // function pointer to the callback for received CAN messages
	void spin_thread(); // contains the infinite loop for CAN send/receive

	std::string header, byte2, byte3, byte4, joystick_left, joystick_right, crc, buffer;

	template<typename Enumeration> // helper function for converting "enum class" to int
	static auto as_integer(
			Enumeration const value) -> typename std::underlying_type<Enumeration>::type;

	std::atomic<bool> // variables for communication with thread
	serial_thread_alive;

	std::thread serial_thread_; // thread for CAN send/receive
};
