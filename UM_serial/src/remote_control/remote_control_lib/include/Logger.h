#ifndef LOGGER_H
#define LOGGER_H

#include <iostream>
#include "ros/console.h"

#define USE_ROS_LOGGING // comment out to use custom logging functions

#ifdef USE_ROS_LOGGING

#define RC_LOG_INFO(...)   ROS_INFO_STREAM("[RC] "<< __VA_ARGS__)
#define RC_LOG_DEBUG(...)  ROS_DEBUG_STREAM("[RC] "<< __VA_ARGS__)
#define RC_LOG_WARN(...)   ROS_WARN_STREAM("[RC] "<< __VA_ARGS__)
#define RC_LOG_ERROR(...)  ROS_ERROR_STREAM("[RC] "<< __VA_ARGS__)
#define RC_LOG_FATAL(...)  ROS_FATAL_STREAM("[RC] "<< __VA_ARGS__)

#else

#define RC_LOG_INFO(...) \
  std::cout << "[INFO] " __VA_ARGS__ ; // very crude implementation
#define RC_LOG_DEBUG(...) \
  std::cout << "[DEBUG] " __VA_ARGS__ ;
#define RC_LOG_WARN(...)  \
  std::cerr << "[WARN] " __VA_ARGS__ ;
#define RC_LOG_ERROR(...) \
  std::cerr << "[ERROR] " __VA_ARGS__ ;
#define RC_LOG_FATAL(...) \
  std::cerr << "[FATAL] " __VA_ARGS__ ;

#endif // USE_ROS_LOGGING

#endif // LOGGER_H
