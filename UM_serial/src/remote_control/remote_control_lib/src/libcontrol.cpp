#include "libcontrol.h"

using std::endl;
using std::chrono::milliseconds;

using namespace serial;

Serial control; 


RemoteControl::RemoteControl()
{
	serial_handle_ = 0;

	serial_thread_alive = false;

	msg_callback = NULL;

}

bool RemoteControl::open_serial_device(const std::string& device_name)
{

	try
	{
		control.setPort("/dev/ttyUSB1"); //porta real: "/dev/ttyUSB0"
		control.setBaudrate(9600);
		serial::Timeout to = serial::Timeout::simpleTimeout(1000); //DO NOT MODIFY TIMEOUT VALUE. SET TO 50 BY LUIZA.
		control.setTimeout(to);
		//control.setFlowcontrol(flowcontrol_software);
		//control.setParity(parity_odd);
		
		control.open();  

	
		serial_handle_ = control.isOpen();
		RC_LOG_INFO("opened serial port: ");

		return true;   
    }
    catch (SerialException e) 
	{
        RC_LOG_INFO("Failed to connect: " << e.what());
     
        return 0;
    }
}

void RemoteControl::spin_thread()
{
	CycleTimer timer_receptor_status(milliseconds(100));
	CycleTimer timer_receive_from_control(milliseconds(1));
	CycleTimer timer_thread_cycle(milliseconds(1));

	while (serial_thread_alive)
	{
		buffer = '\0';

		control.flush();
		
		buffer = control.read(9);
		//RC_LOG_INFO("S2H Compl2: " << string_to_hex(buffer));
		//RC_LOG_INFO("size: " << buffer.size());

		std::string header;
		std::string byte2;
		std::string byte3;
		std::string byte4;
		std::string joystick_left;
		std::string joystick_right;
		std::string crc;

		if(buffer.size() == 9)
		{
			//RC_LOG_INFO("S2H Compl: " << string_to_hex(buffer).size());
			for (int i = 0; i < 18; i++)
			{

				switch (i)
				{
					//RC_LOG_INFO("switch");
					case 0:
					{
						header.push_back(string_to_hex(buffer)[i]);
						break;
					}
					case 1:
					{
						header.push_back(string_to_hex(buffer)[i]);
						break;
					}
					case 2:
					{
						header.push_back(string_to_hex(buffer)[i]);
						break;
					}
					case 3:
					{
						header.push_back(string_to_hex(buffer)[i]);
						break;
					}
					case 4:
					{
						byte2.push_back(string_to_hex(buffer)[i]);
						break;
					}
					case 5:
					{
						byte2.push_back(string_to_hex(buffer)[i]);
						break;
					}
					case 6:
					{
						byte3.push_back(string_to_hex(buffer)[i]);
						break;
					}
					case 7:
					{
						byte3.push_back(string_to_hex(buffer)[i]);
						break;
					}
					case 8:
					{
						byte4.push_back(string_to_hex(buffer)[i]);
						break;
					}
					case 9:
					{
						byte4.push_back(string_to_hex(buffer)[i]);
						break;
					}
					case 10:
					{
						joystick_left.push_back(string_to_hex(buffer)[i]);
						break;
					}
					case 11:
					{
						joystick_left.push_back(string_to_hex(buffer)[i]);
						break;
					}
					case 12:
					{
						joystick_right.push_back(string_to_hex(buffer)[i]);
						break;
					}
					case 13:
					{
						joystick_right.push_back(string_to_hex(buffer)[i]);
						break;
					}
					case 14:
					{
						crc.push_back(string_to_hex(buffer)[i]);
						break;
					}
					case 15:
					{
						crc.push_back(string_to_hex(buffer)[i]);
						break;
					}
					case 16:
					{
						crc.push_back(string_to_hex(buffer)[i]);
						break;
					}
					case 17:
					{
						crc.push_back(string_to_hex(buffer)[i]);
						break;
					}

				}
			
			}
		}
		
		char new_header[header.size()];
		strcpy(new_header, header.c_str());

		char new_byte2[byte2.size()];
		strcpy(new_byte2, byte2.c_str());

		char new_byte3[byte3.size()];
		strcpy(new_byte3, byte3.c_str());

		char new_byte4[byte4.size()];
		strcpy(new_byte4, byte4.c_str());

		char new_joystick_left[joystick_left.size()];
		strcpy(new_joystick_left, joystick_left.c_str());

		char new_joystick_right[joystick_right.size()];
		strcpy(new_joystick_right, joystick_right.c_str());

		if ((new_header != NULL) && (strcmp(new_header, "3A3A") == 0))
		{

			if (new_byte2 != NULL) processing_byte2(std::stoi(new_byte2, 0, 16));
			
			if (new_byte3 != NULL) processing_byte3(std::stoi(new_byte3, 0, 16));

			if (new_byte4 != NULL) processing_byte4(std::stoi(new_byte4, 0, 16));

			if (new_joystick_left != NULL) processing_joystick_left(std::stoi(new_joystick_left, 0, 16));
		
			if (new_joystick_right != NULL) processing_joystick_right(std::stoi(new_joystick_right, 0, 16));
				
		}
		else
		{
			control.close();
			
			control.open();  
			
			RC_LOG_ERROR("Re-opened serial port!");
		}

	if (timer_receptor_status.check_new_cycle() && receptor_status_flag == 1)
	{
		p_msg.type = MsgType::RECEPTOR_STATUS;
		msg_callback(p_msg);
		//RC_LOG_INFO("Receiving");
	}
	buffer = '\0';
	timer_thread_cycle.sleep_until_next_cycle();
	}
}



std::string RemoteControl::string_to_hex(const std::string& input)
{
	static const char* const lut = "0123456789ABCDEF";
	size_t len = input.length();

	std::string output;
	output.reserve(2 * len);

	for (size_t i = 0; i < len; ++i)
	{
		const unsigned char c = input[i];
		output.push_back(lut[c >> 4]);
		output.push_back(lut[c & 15]);
	}

	return output;
}

uint16_t RemoteControl::processing_byte2(int byte2_buffer)
{
	int horn = 3;
	int open_bucket = 4;
	int close_bucket = 5;
	int ch1_o1 = 7; //farois
	int ch1_o2 = 8; //farois


	if ((byte2_buffer & (1 << (horn - 1))) && horn_flag == 0)
	{	
		byte2_sum = byte2_sum + 1024;
		p_msg.type = MsgType::HORN_FROM_RC;
		p_msg.data.ival = byte2_sum;
		msg_callback(p_msg);
		horn_flag = 1;
		RC_LOG_INFO("HORN FROM RC [ON]");
	}
	else
	{
		if ((byte2_buffer & (1 << (horn - 1))) && horn_flag == 1) horn_flag = 1;
		
		else if (horn_flag == 1)
		{
			byte2_sum = byte2_sum - 1024;
			p_msg.type = MsgType::HORN_FROM_RC;
			p_msg.data.ival = byte2_sum;
			msg_callback(p_msg);
			horn_flag = 0;
			RC_LOG_INFO("HORN FROM RC [OFF]");
		}
	}

	if ((byte2_buffer & (1 << (open_bucket - 1))) && open_bucket_flag == 0)
	{	
		byte2_sum = byte2_sum + 64;
		p_msg.type = MsgType::OPEN_BUCKET_FROM_RC;
		p_msg.data.ival = byte2_sum;
		msg_callback(p_msg);
		open_bucket_flag = 1;
		RC_LOG_INFO("BUCKET FROM RC [ON]");
	}
	else
	{
		if ((byte2_buffer & (1 << (open_bucket - 1))) && open_bucket_flag == 1) open_bucket_flag = 1;
		else if (open_bucket_flag == 1)
		{
			byte2_sum = byte2_sum - 64;
			p_msg.type = MsgType::OPEN_BUCKET_FROM_RC;
			p_msg.data.ival = byte2_sum;
			msg_callback(p_msg);	
			open_bucket_flag = 0;
			RC_LOG_INFO("BUCKET FROM RC [ON]");
		}
		
	}

	if ((byte2_buffer & (1 << (close_bucket - 1))) && close_bucket_flag == 0)
	{	
		byte2_sum = byte2_sum + 4096;
		p_msg.type = MsgType::CLOSE_BUCKET_FROM_RC;
		p_msg.data.ival = byte2_sum;
		msg_callback(p_msg);
		close_bucket_flag = 1;
		RC_LOG_INFO("BUCKET FROM RC [OFF]");
	}
	else
	{
		if ((byte2_buffer & (1 << (close_bucket - 1))) && close_bucket_flag == 1) close_bucket_flag = 1;
		else if (close_bucket_flag == 1)
		{
			byte2_sum = byte2_sum - 4096;
			p_msg.type = MsgType::CLOSE_BUCKET_FROM_RC;
			p_msg.data.ival = byte2_sum;
			msg_callback(p_msg);
			close_bucket_flag = 0;
			RC_LOG_INFO("BUCKET FROM RC [OFF]");
		}
	}

	if ((byte2_buffer & (1 << (ch1_o2 - 1)))) 
	{	
		if(beam_flag == 1)
		{	
			byte2_sum = byte2_sum - 128;
			p_msg.type = MsgType::BEAMS_FROM_RC;
			p_msg.data.ival = byte2_sum;
			msg_callback(p_msg);
			beam_flag = 0;
			RC_LOG_INFO("BEAM FROM RC [OFF]");
		}
		else beam_flag = beam_flag;

	}
	else
	{
		if (byte2_buffer & (1 << (ch1_o1 - 1))) 
		{	
			if(beam_flag == 1)
			{	
				byte2_sum = byte2_sum + 256;
				p_msg.type = MsgType::BEAMS_FROM_RC;
				p_msg.data.ival = byte2_sum;
				msg_callback(p_msg);
				beam_flag = 2;
				RC_LOG_INFO("HIGH BEAM FROM RC [ON]");
			}
			else beam_flag = beam_flag;
		}
		else
		{
			if(beam_flag == 0)
			{
				byte2_sum = byte2_sum + 128;		
				p_msg.type = MsgType::BEAMS_FROM_RC;
				p_msg.data.ival = byte2_sum;
				msg_callback(p_msg);
				beam_flag = 1;
				RC_LOG_INFO("LOW BEAM FROM RC [ON]");
			}
			else beam_flag = beam_flag;

			if(beam_flag == 2)
			{
				byte2_sum = byte2_sum - 256;
				p_msg.type = MsgType::BEAMS_FROM_RC;
				p_msg.data.ival = byte2_sum;
				msg_callback(p_msg);
				beam_flag = 1;
				RC_LOG_INFO("HIGH BEAM FROM RC [OFF]");
			}
			else beam_flag = beam_flag;
		}
	}
	
}

uint16_t RemoteControl::processing_byte3(int byte3_buffer)
{
	int remote_control_on = 1;
	int mission_allow = 2;
	int emergency_brake = 3;
	int right_blinker = 6;
	int left_blinker = 5;
	int control_status = 7;
	int receptor_status = 8;

	if ((byte3_buffer & (1 << (remote_control_on - 1))) && remote_control_on_flag == 0) 
	{
		remote_control_on_flag = 1;
		p_msg.type = MsgType::RC_ON;
		p_msg.data.ival = remote_control_on_flag;
		msg_callback(p_msg);
		RC_LOG_INFO("ALLOW RC FROM RC [ON]");
	}
	
	else
	{
		if ((byte3_buffer & (1 << (remote_control_on - 1))) && remote_control_on_flag == 1) remote_control_on_flag = 1;
		
		else if (remote_control_on_flag == 1) 
		{
			remote_control_on_flag = 0;
			p_msg.type = MsgType::RC_ON;
			p_msg.data.ival = remote_control_on_flag;
			msg_callback(p_msg);
			RC_LOG_INFO("ALLOW RC FROM RC [OFF]");
		}
	}

	if ((byte3_buffer & (1 << (mission_allow - 1))) && mission_allow_flag == 0) 
	{
		mission_allow_flag = 1;
		p_msg.type = MsgType::MISSION_ALLOW;
		p_msg.data.ival = mission_allow_flag;
		msg_callback(p_msg);
		RC_LOG_INFO("MISSION ALLOW FROM RC [ON]");
	}
	
	else
	{
		if ((byte3_buffer & (1 << (mission_allow - 1))) && mission_allow_flag == 1) mission_allow_flag = 1;
		
		else if (mission_allow_flag == 1) 
		{
			mission_allow_flag = 0;
			p_msg.type = MsgType::MISSION_ALLOW;
			p_msg.data.ival = mission_allow_flag;
			msg_callback(p_msg);
			RC_LOG_INFO("MISSION ALLOW FROM RC [OFF]");
		}
	}
/*
	if ((byte3_buffer & (1 << (emergency_brake - 1))) && emergency_brake_flag == 0) 
	{
		emergency_brake_flag = 1;

		p_msg.type = MsgType::EMERGENCY_BRAKE_FROM_RC;
		p_msg.data.ival = emergency_brake_flag;
	
		msg_callback(p_msg);

		RC_LOG_DEBUG("EMERGENCY BRAKE OFF");
	}
	
	else
	{
		if ((byte3_buffer & (1 << (emergency_brake - 1))) && emergency_brake_flag == 1) emergency_brake_flag = 1;
		
		else if (emergency_brake_flag == 1) 
		{
			emergency_brake_flag = 0;

			p_msg.type = MsgType::EMERGENCY_BRAKE_FROM_RC;
			p_msg.data.ival = emergency_brake_flag;
		
			msg_callback(p_msg);

			RC_LOG_DEBUG("EMERGENCY BRAKE OFF");
		}
	}
*/
	if ((byte3_buffer & (1 << (left_blinker - 1))) && left_blinker_flag == 0) 
	{
		left_blinker_flag = 1;
		byte2_sum = byte2_sum + 4;
		p_msg.type = MsgType::BLINKER_FROM_RC;
		p_msg.data.ival = byte2_sum;
		msg_callback(p_msg);
		RC_LOG_INFO("LEFT BLINKER FROM RC [ON]");
	}
	
	else
	{
		if ((byte3_buffer & (1 << (left_blinker - 1))) && left_blinker_flag == 1) left_blinker_flag = 1;
		
		else if (left_blinker_flag == 1) 
		{
			left_blinker_flag = 0;
			byte2_sum = byte2_sum - 4;
			p_msg.type = MsgType::BLINKER_FROM_RC;
			p_msg.data.ival = byte2_sum;
			msg_callback(p_msg);
			RC_LOG_INFO("LEFT BLINKER FROM RC [OFF]");
		}
	}

	if ((byte3_buffer & (1 << (right_blinker - 1))) && right_blinker_flag == 0) 
	{
		right_blinker_flag = 1;
		byte2_sum = byte2_sum + 8;
		p_msg.type = MsgType::BLINKER_FROM_RC;
		p_msg.data.ival = byte2_sum;
		msg_callback(p_msg);
		RC_LOG_INFO("RIGHT BLINKER FROM RC [ON]");
	}
	
	else
	{
		if ((byte3_buffer & (1 << (right_blinker - 1))) && right_blinker_flag == 1) right_blinker_flag = 1;
		
		else if (right_blinker_flag == 1) 
		{
			right_blinker_flag = 0;
			byte2_sum = byte2_sum - 8;
			p_msg.type = MsgType::BLINKER_FROM_RC;
			p_msg.data.ival = byte2_sum;
			msg_callback(p_msg);
			RC_LOG_INFO("RIGHT BLINKER FROM RC [OFF]");
		}
		
	}
	if (byte3_buffer & (1 << (receptor_status - 1))) receptor_status_flag = 1;
	else receptor_status_flag = 0;
}

uint16_t RemoteControl::processing_byte4(int byte4_buffer)
{
	int che_o1 = 1;
	int che_o2 = 2;
	int chd_o1 = 3;
	int chd_o2 = 4;
	int emergency_brake = 8;

	if ((byte4_buffer & (1 << (che_o1 - 1)))) 
	{	
		if(mode_flag == 1)
		{	
			p_msg.type = MsgType::MODE_FROM_RC;
			p_msg.data.ival = 1;
			msg_callback(p_msg);
			mode_flag = 0;
			RC_LOG_INFO("AUTONOMOUS FROM RC [ON]");
		}
		else mode_flag = mode_flag;
	}
	else
	{
		if (byte4_buffer & (1 << (che_o2 - 1))) 
		{	
			if(mode_flag == 1)
			{	
				p_msg.type = MsgType::MODE_FROM_RC;
				p_msg.data.ival = 2;
				msg_callback(p_msg);
				mode_flag = 2;
				RC_LOG_INFO("MANUAL FROM RC [ON]");
			}
			else mode_flag = mode_flag;
		}
		else
		{
			if(mode_flag == 0)
			{
				p_msg.type = MsgType::MODE_FROM_RC;
				p_msg.data.ival = 8;
				msg_callback(p_msg);
				mode_flag = 1;
				RC_LOG_INFO("RC FROM RC [ON]");
			}
			else mode_flag = mode_flag;

			if(mode_flag == 2)
			{
				p_msg.type = MsgType::MODE_FROM_RC;
				p_msg.data.ival = 8;
				msg_callback(p_msg);
				mode_flag = 1;
				RC_LOG_INFO("RC FROM RC [ON]");
			}
			else mode_flag = mode_flag;
		}
	}

	if ((byte4_buffer & (1 << (chd_o1 - 1)))) 
	{	
		if(gear_flag == 1)
		{	
			p_msg.type = MsgType::GEAR_FROM_RC;
			p_msg.data.ival = 1;
			msg_callback(p_msg);
			gear_flag = 0;
			RC_LOG_INFO("GEAR R FROM RC [ON]");
		}
		else gear_flag = gear_flag;

	}
	else
	{
		if (byte4_buffer & (1 << (chd_o2 - 1))) 
		{	
			if(gear_flag == 1)
			{	
				p_msg.type = MsgType::GEAR_FROM_RC;
				p_msg.data.ival = 16;
				msg_callback(p_msg);
				gear_flag = 2;
				RC_LOG_INFO("GEAR D FROM RC [ON]");
			}
			else gear_flag = gear_flag;
		}
		else
		{
			if(gear_flag == 0)
			{
				p_msg.type = MsgType::GEAR_FROM_RC;
				p_msg.data.ival = 4;
				msg_callback(p_msg);
				gear_flag = 1;
				RC_LOG_INFO("GEAR N FROM RC [ON]");
			}
			else gear_flag = gear_flag;

			if(gear_flag == 2)
			{
				p_msg.type = MsgType::GEAR_FROM_RC;
				p_msg.data.ival = 4;
				msg_callback(p_msg);
				gear_flag = 1;
				RC_LOG_INFO("GEAR N FROM RC [ON]");
			}
			else gear_flag = gear_flag;
		}
	}

	if ((byte4_buffer & (1 << (emergency_brake - 1))) && emergency_brake_flag == 0) 
	{
		emergency_brake_flag = 1;
		p_msg.type = MsgType::EMERGENCY_BRAKE_FROM_RC;
		p_msg.data.ival = emergency_brake_flag;
		msg_callback(p_msg);
		RC_LOG_INFO("EMERGENCY BRAKE FROM RC [ON]");
	}
	
	else
	{
		if ((byte4_buffer & (1 << (emergency_brake - 1))) && emergency_brake_flag == 1) emergency_brake_flag = 1;
		
		else if (emergency_brake_flag == 1) 
		{
			emergency_brake_flag = 0;
			p_msg.type = MsgType::EMERGENCY_BRAKE_FROM_RC;
			p_msg.data.ival = emergency_brake_flag;
			msg_callback(p_msg);
			RC_LOG_INFO("EMERGENCY BRAKE FROM RC [OFF]");
		}
	}
}

uint16_t RemoteControl::processing_joystick_left(int joystick_left_buffer)
{
	int brake = 8;
	
	if (joystick_left_buffer & (1 << (brake - 1))) 
	{	
		p_msg.type = MsgType::BRAKE_FROM_RC;
		p_msg.data.ival = 250 - joystick_left_buffer;
		msg_callback(p_msg);
		//RC_LOG_INFO("BRAKE FROM RC [ON]" << p_msg.data.ival);
	}
	else
	{
		p_msg.type = MsgType::THROTTLE_FROM_RC;
		p_msg.data.ival = joystick_left_buffer;
		msg_callback(p_msg);
		//RC_LOG_INFO("THROTTLE FROM RC [ON]" << p_msg.data.ival);
	}
}

uint16_t RemoteControl::processing_joystick_right(int joystick_right_buffer)
{
	int steering_left = 8;
	
	if (joystick_right_buffer & (1 << (steering_left - 1))) 
	{	
		p_msg.type = MsgType::STEER_LEFT_FROM_RC;
		p_msg.data.ival = 256 - joystick_right_buffer;
		msg_callback(p_msg);
		//RC_LOG_INFO("STEERING FROM RC [ON]" << p_msg.data.ival);
	}
	else
	{
		p_msg.type = MsgType::STEER_RIGHT_FROM_RC;
		p_msg.data.ival = joystick_right_buffer;
		msg_callback(p_msg);
		//RC_LOG_INFO("STEERING FROM RC [ON]" << p_msg.data.ival);
	}
}


void RemoteControl::spin()
{
	if (serial_handle_ > 0) // SERIAL device has been opened
	{
		serial_thread_alive = true;
		serial_thread_ = std::thread(&RemoteControl::spin_thread, this); // spawn SERIAL sender/receiver thread
	}
	else
	{
		RC_LOG_ERROR("error starting SERIAL thread: SERIAL device not opened");
	}
}

void RemoteControl::shutdown()
{
	if (serial_thread_alive)
	{
		serial_thread_alive = false; // request SERIAL thread to stop
		serial_thread_.join();
	}

	if (serial_handle_ > 0) // CAN device has been opened previously
	{
		control.flush();
		control.close();
		RC_LOG_INFO("closed SERIAL connection, libuemserial shutdown complete!");
	}
} 

char * RemoteControl::hex_to_bin(char * hexdec) 
{ 
    int i = 0; 
	
	char_bin = (char*) calloc (1, sizeof(int));
	bin_buffer = (char*) calloc (1, sizeof(int));

    while (hexdec[i]) { 
  
        switch (hexdec[i]) 
		{ 
			case '0': //"0000"
				strcpy(char_bin, "0000");
				break; 
			case '1': //"0001"
				strcpy(char_bin, "0001");
				break; 
			case '2': //"0010"
				strcpy(char_bin, "0010");
				break; 
			case '3': //"0011"
				strcpy(char_bin, "0011");
				break; 
			case '4': //"0100"
				strcpy(char_bin, "0100");
				break; 
			case '5': //"0101"
				strcpy(char_bin, "0101");
				break; 
			case '6': //"0110"
				strcpy(char_bin, "0110");
				break; 
			case '7': //"0111"
				strcpy(char_bin, "0111");
				break; 
			case '8': //"1000"
				strcpy(char_bin, "1000");
				break; 
			case '9': //"1001
				strcpy(char_bin, "1001");
				break; 
			case 'A': //"1010"
				strcpy(char_bin, "1010");
				break; 
			case 'B': //"1011"
				strcpy(char_bin, "1011");
				break; 
			case 'C':  //"1100"
				strcpy(char_bin, "1100");
				break; 
			case 'D':  //"1101"
				strcpy(char_bin, "1101");
				break; 
			case 'E':  //"1110"
				strcpy(char_bin, "1110");
				break; 
			case 'F': //"1111"
				strcpy(char_bin, "1111");
				break; 
        } 

		strcat(bin_buffer, char_bin);
		i++; 
    } 

	RC_LOG_ERROR("BIN: " << bin_buffer);
	return bin_buffer;

	char_bin = '\0';
	bin_buffer = '\0';
} 
