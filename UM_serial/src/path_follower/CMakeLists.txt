cmake_minimum_required(VERSION 2.8.3)
project(path_follower)

add_compile_options(-std=c++11)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -g")

set(NODE_NAME path_follower_node)


find_package(catkin REQUIRED COMPONENTS
  roscpp
  roslib 
  rospy
  rqt_gui
  rqt_gui_py
  message_generation 
  std_msgs
  sensor_msgs
  geometry_msgs
  tf
  dynamic_reconfigure
  sick_ldmrs_msgs
)

find_package(PythonLibs REQUIRED)

# ------------------------
# messages and services
# ------------------------

add_message_files(
DIRECTORY 
  path_follower_node/msg 
FILES
  ServiceCommand.msg
  ServiceCommandReply.msg
  PathInfo.msg
  PathInfoArray.msg
  StateEvents.msg
  VehicleState.msg
  ControlTargetState.msg
  SensorState.msg
  Excavator.msg
)

add_service_files(
DIRECTORY 
  path_follower_node/srv 
FILES
  PathFollowerService.srv
)

generate_messages(DEPENDENCIES std_msgs)

# ------------------------
# path_follower_node
# ------------------------

find_library(${NODE_NAME} Geographic)

set(NODE_SRC_DIR path_follower_node/src)

set(NODE_SRC_FILES
  ${NODE_SRC_DIR}/PathFollowerNode.cpp	
  ${NODE_SRC_DIR}/Timeout.cpp
  ${NODE_SRC_DIR}/GpsPath.cpp
  ${NODE_SRC_DIR}/PathManager.cpp
  ${NODE_SRC_DIR}/Wgs2UtmConverter.cpp
  ${NODE_SRC_DIR}/Visualization.cpp
  ${NODE_SRC_DIR}/SteeringController.cpp
  ${NODE_SRC_DIR}/VelocityController.cpp
  ${NODE_SRC_DIR}/DataPlotter.cpp
  ${NODE_SRC_DIR}/HelperFunctions.cpp
  ${NODE_SRC_DIR}/Oxts.cpp
  ${NODE_SRC_DIR}/Uem.cpp
  ${NODE_SRC_DIR}/PathFollower.cpp
  ${NODE_SRC_DIR}/CollisionAvoidance.cpp

)

include_directories( 
  ${NODE_NAME}/include
  ${NODE_NAME}/include/thirdparty
  /usr/local/include 
  ${PYTHON_INCLUDE_DIRS}
  ${catkin_INCLUDE_DIRS}
)


add_executable(${NODE_NAME}
  ${NODE_SRC_FILES}
)

generate_dynamic_reconfigure_options(
  path_follower_node/cfg/PathFollower.cfg
)

add_dependencies(path_follower_node 
  path_follower_generate_messages_cpp
  path_follower_gencfg
)

link_directories(/usr/local/lib)

target_link_libraries(${NODE_NAME}
  Geographic 
  ${PYTHON_LIBRARIES}
  ${catkin_LIBRARIES}
)

install(TARGETS ${NODE_NAME}  
  RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
  ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}        
)

catkin_package(
  CATKIN_DEPENDS message_runtime std_msgs
  DEPENDS Geographic
)


install(DIRECTORY launch
  DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}
)

install(DIRECTORY config
  DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}
)


# ------------------------
# watchdog node
# ------------------------

add_executable(watchdog_node
	watchdog/src/WatchdogNode.cpp
	path_follower_node/src/Timeout.cpp
	
)

target_link_libraries(watchdog_node
  ${catkin_LIBRARIES}
)

install(TARGETS watchdog_node
  RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
  ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}        
)

# ------------------------
# install rqt gui-plugin 
# ------------------------

set(GUI_DIR path_follower_gui)

install(PROGRAMS ${GUI_DIR}/scripts/path_follower
  DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)

install(DIRECTORY
  ${GUI_DIR}/src
  ${GUI_DIR}/resource
  DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}
)

install(FILES
  ${GUI_DIR}/plugin.xml
  DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}
)








