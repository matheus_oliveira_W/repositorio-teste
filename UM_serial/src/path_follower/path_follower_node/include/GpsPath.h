/** @file DataPlotter.cpp
 @brief Definition of GpsPath class
 @author Tobias Brunner
 */

#pragma once

#include <cmath>
#include <vector>
#include <string>

#include <ros/ros.h>
#include "json.hpp"
#include "thirdparty/PathTools.hpp"
#include "HelperFunctions.h"

#include <boost/geometry.hpp>
#include <boost/geometry/geometries/linestring.hpp>
#include <boost/geometry/geometries/point_xy.hpp>
#include <boost/assign.hpp>

/**
 * @brief GpsPath class
 */
class GpsPath: public PathTools::Path<double>
{
public:
	/** Simple struct for storing visualization color of a GpsPath*/
	typedef struct VisualizationColor
	{
		double a, r, g, b;
	} VisualizationColor;

	/** Perform post-processing of a recorded GPS path
	 *
	 * @param max_seg_length maximum distance between two path points after post-processing
	 * @param epsilon Distance criterion for path simplification (see
	 * https://en.wikipedia.org/wiki/Ramer%E2%80%93Douglas%E2%80%93Peucker_algorithm)
	 * @param curvature_smoothing_factor Value between 0.0 ... 1.0 to determine how strongly to smoothen curvature values
	 */
	void post_process(const double max_seg_length, const double epsilon,
			const double curvature_smoothing_factor);

	/** Add a single way point to the GpsPath
	 *
	 * @param x UTM easting coordinate [m]
	 * @param y UTM northing coordinate [m]
	 * @param gps_lat WGS84 latitude value
	 * @param gps_lon WGS84 longitude value
	 * @param velocity Recorded velocity at the point [m/s]
	 */
	void add_point(const double x, const double y, const double gps_lat,
			const double gps_lon, const double gps_alt, const double velocity);

	/** Remove all points from the path */
	void clear();

	/** Determine the total length of the path in [m] */
	double length() const;

	/** Check if the path is empty or contains at least one point */
	bool has_points() const;

	const std::vector<double>& gps_lat() const
	{
		return m_gps_lat;
	}
	const std::vector<double>& gps_lon() const
	{
		return m_gps_lon;
	}
	const std::vector<double>& gps_alt() const
	{
		return m_gps_alt;
	}
	const std::vector<double>& velocity() const
	{
		return m_velocity;
	}
	const std::vector<double>& heading() const
	{
		return m_heading;
	}
	const std::vector<double>& curvature() const
	{
		return m_curvature;
	}
	const std::vector<double>& s() const
	{
		return m_p_length;
	}

	/** Calculate altitude at a given path point
	 * @param pp path point
	 */
	double altitude(PathTools::PathPoint<double> const& pp) const;

	/** Calculate velocity at a given path point
	 * @param pp path point
	 */
	double velocity(PathTools::PathPoint<double> const& pp) const;

	/** Calculate heading at a given path point
	 * @param pp path point
	 */
	double heading(PathTools::PathPoint<double> const& pp) const;

	/** Calculate curvature at a given path point
	 * @param pp path point
	 */
	double curvature(PathTools::PathPoint<double> const& pp) const;

	/** Calculate distance along path from beginning to path point pp
	 * @param pp path point
	 */
	double s(PathTools::PathPoint<double> const& pp) const;

	void set_name(const std::string& name);
	void set_color(const double a, const double r, const double g,
			const double b);

	const std::string& name();
	const VisualizationColor& color();

	void init(const double go_zone_radius, const double wait_zone_radius)
	{
		this->go_zone_radius = go_zone_radius;
		this->wait_zone_radius = wait_zone_radius;

		this->rec_sec = 0;
		this->rec_nsec = 0;
	}

	// befriend external functions for exporting to / reading from JSON format
	friend void to_json(nlohmann::json& j, const GpsPath& path);
	friend void from_json(const nlohmann::json& j, GpsPath& path);

	double go_zone_radius, wait_zone_radius; // size of go / wait zone
	int rec_sec, rec_nsec, reverse; // time that recording started

protected:
	std::vector<double> m_gps_lat, m_gps_lon, m_gps_alt, m_velocity, m_heading,
			m_curvature;
	std::string m_name;
	VisualizationColor m_color;

};

