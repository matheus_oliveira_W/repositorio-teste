#pragma once

#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <exception>
#include <boost/filesystem.hpp>
#include <iostream>
#include <iomanip>
#include <ctime>

#include "GpsPath.h"

class PathManager
{
public:
	PathManager();

	GpsPath& active_path(); // returns reference to the currently active path
	GpsPath& reference_path(); // returns reference to the first path

	double x_pos_ref() const;
	double y_pos_ref() const;
	unsigned int active_path_id() const;

	void reset_paths();

	void set_color_pallet(const std::vector<std::vector<double>>& path_colors);

	void post_process_path(const double max_seg_length, const double epsilon,
			const double curvature_smoothing_factor);

	// handle multiple paths
	void add_path();
	void add_path(const std::string& path_name);
	void add_path(const std::string& path_name, const int reverse, const double go_zone_radius,
			const double wait_zone_radius);

	void rename_path(const unsigned int index, const std::string& new_name);

	const std::vector<GpsPath>& paths() const;

	void set_active_path_index(const unsigned int index);

	void remove_path(const unsigned int index);

	void load_from_file(const std::string& file_name);
	void save_to_file(const std::string& file_name);

	std::string save_to_temp_file();
	bool load_last_path();

private:
	void set_path_color();
	void set_last_path_file(const std::string& path);
	std::string get_last_path_file();

	std::vector<GpsPath> path_vector;
	unsigned int active_path_index;
	std::vector<std::vector<double>> color_pallet;
	bool valid_color_pallet;
	std::string temp_dir;

	const double go_zone_radius_default = 5;
	const double wait_zone_radius_default = 5;

	int reverse = 0;
};
