#pragma once

#include <ros/ros.h>
#include <std_msgs/Empty.h>
#include <std_msgs/Float32.h>
#include <std_msgs/UInt16.h>
#include "path_follower/Excavator.h"

#include "Timeout.h"

class UeM
{
public:

	/**
	 * States of the paravan_node
	 */
	enum class State
	{
		OFFLINE, /**< paravan_node is not running */
		NO_DATA, /**< paravan_node is running, but no messages are being received from paravan_node */
		NO_REMOTE_CONTROL, /**< paravan_node is running and sending CAN Data, but remote control is not available */
		ONLINE /**< space drive is online, sending CAN data and remote control is available */
	};

	struct Data
	{
		//std_msgs::Float32 gas_brake;
		//std_msgs::Float32 steering;
		path_follower::Excavator ex_location;
		int data_signal = 0;
		std_msgs::UInt16 gear_from_uem;
	};

	void init(const double timeout_alive, const double timeout_data);

	void callback_alive(const std_msgs::Empty::ConstPtr& msg);
	//void callback_remote_control_available(const std_msgs::Empty::ConstPtr& msg);
	void callback_data_signal(const std_msgs::Empty::ConstPtr& msg);
	void callback_relay_box_2(const std_msgs::UInt16::ConstPtr& msg);
	void callback_ex_location(const path_follower::Excavator::ConstPtr& msg);

	const UeM::Data& data();
	const UeM::State& state();
	//const bool magnetic_clutch_closed();

	void update_state();

private:
	bool magnetic_clutch_closed_;
	State state_;
	Data data_;
	Timeout data_timeout_, alive_timeout_, remote_control_timeout_;
};


class ParavanActuatorCommand
{
public:
	enum ControlType
	{
		Control,
		KeepNeutral
	};

	ParavanActuatorCommand(ControlType control_type, double value = 0)
				: control_type_(control_type), value_(value) {}

	double value() const { return value_; }
	ControlType control_type() const { return control_type_; }

protected:
	ControlType control_type_;
	double value_;
};


class SteeringCommand : public ParavanActuatorCommand
{
public:
	SteeringCommand(ControlType control_type, double value = 0) : ParavanActuatorCommand(control_type, value) {}
};


class GasBrakeCommand : public ParavanActuatorCommand
{
public:
	GasBrakeCommand(ControlType control_type, double value = 0) : ParavanActuatorCommand(control_type, value) {}
};
