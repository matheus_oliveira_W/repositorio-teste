#pragma once

#include <sensor_msgs/NavSatFix.h>
#include <sensor_msgs/NavSatStatus.h>
#include <sensor_msgs/Imu.h>
#include <geometry_msgs/TwistWithCovarianceStamped.h>
#include <std_msgs/Empty.h>

#include "Timeout.h"

class Oxts
{
public:

	/**
	 * States of the oxts system
	 */
	enum class State
	{
		OFFLINE, /**< oxts node is not running */
		NO_DATA, /**< oxts node is running, but no satellite data is being received (no UDP data from RT1003) */
		DATA_BUT_NO_FIX, /**< 'NavSatFix' messages are being received, but no satellite fix */
		FIX /**< satellite fix is available */
	};

	struct Data
	{
		sensor_msgs::NavSatFix nav_sat_fix;
		sensor_msgs::Imu imu;
		geometry_msgs::TwistWithCovarianceStamped twist;
	};

	void init(const double timeout_alive, const double timeout_data);

	void callback_nav_sat_fix(const sensor_msgs::NavSatFix::ConstPtr& msg);
	void callback_imu(const sensor_msgs::Imu::ConstPtr& msg);
	void callback_twist(
			const geometry_msgs::TwistWithCovarianceStamped::ConstPtr& msg);
	void callback_alive(const std_msgs::Empty::ConstPtr& msg);

	const Oxts::Data& data();
	const Oxts::State& state();

	void update_state();

private:
	State state_;
	Data data_;
	Timeout data_timeout_, alive_timeout_;
};
