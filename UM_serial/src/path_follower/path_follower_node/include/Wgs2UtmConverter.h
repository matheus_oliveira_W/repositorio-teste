#pragma once

#include <string>
#include <GeographicLib/UTMUPS.hpp>

class Wgs2UtmConverter
{
public:
	Wgs2UtmConverter() :
			m_reference_zone_set(false)
	{
	}
	void convert(const double gps_lat, const double gps_lon, double& utm_x,
			double& utm_y);
	void reset()
	{
		m_reference_zone_set = false;
	}
	void init_zone(const double gps_lat, const double gps_lon);

	std::string get_zone_string() const
	{
		return m_zonestr;
	}

private:
	std::string m_zonestr;
	int m_zone;
	bool m_reference_zone_set;

};
