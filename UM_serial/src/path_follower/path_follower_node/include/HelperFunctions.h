#pragma once

#include <vector>

#include "GpsPath.h"

namespace HelperFunctions
{

template<typename T>
double interpolate(const std::vector<T>& x_vec, const std::vector<T>& y_vec,
		const T x_val)
{
	if (x_val <= x_vec.front())
		return y_vec.front();
	if (x_val >= x_vec.back())
		return y_vec.back();

	for (int k = 1; k < x_vec.size(); ++k)
	{
		if (x_val < x_vec[k])
		{
			T dx = x_vec[k] - x_vec[k - 1];
			T dy = y_vec[k] - y_vec[k - 1];

			return y_vec[k - 1] + dy / dx * (x_val - x_vec[k - 1]);
		}
	}

	return 0;
}

template<typename T>
double distance(const T x0, const T y0, const T x1, const T y1)
{
	return std::sqrt((x1 - x0) * (x1 - x0) + (y1 - y0) * (y1 - y0));
}

}
