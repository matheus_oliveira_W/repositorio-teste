/** @file SteeringController.h
 @brief Class definition of Timeout
 @author Tobias Brunner
 */

#pragma once

#include <chrono>

/*
 * @brief: Check if a recurrent signal has been sent during a fixed time frame.
 *
 * A Timeout object is typically assigned to a specific signal source (e.g. a recurrent ros topic).
 * Timeout provides functions to check if the signal has been received during a specified time frame.
 */
class Timeout
{
public:
	Timeout();

	/*
	 * @param seconds time out duration, after which the signal source is assumed to be timed-out
	 */
	Timeout(double seconds);

	/*
	 * Check if the signal source has timed out, i.e. the last signal has been received too long ago
	 *
	 * @return true, if the signal source has timed out
	 */
	bool timed_out();

	/*
	 * This function should be called, whenever a signal from the signal source is registered/observed.
	 */
	void got_signal();

	/*
	 * Set the time out value
	 *
	 * @param seconds time out duration, after which the signal source is assumed to be timed-out
	 */
	void set_timeout(double seconds);

private:
	std::chrono::time_point<std::chrono::steady_clock> m_last_signal;
	std::chrono::milliseconds m_timeout = std::chrono::milliseconds(1000);

};
