#pragma once

class PathFollower
{
public:

	/**
	 * States of the path follower
	 */
	enum class State
	{
		RECORDING, /**< gps path is being recorded */
		FOLLOWING, /**< gps path is being followed */
		IDLE, /**< neither recording nor following a path */
		HANDOVER /**< handing over control back to driver */
	};

	void init();

	const PathFollower::State& state();
	void set_state(const PathFollower::State state);

private:
	State state_;
};
