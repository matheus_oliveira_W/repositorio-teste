#pragma once

#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/Quaternion.h>
#include <std_msgs/ColorRGBA.h>
#include <tf/tf.h>

#include <vector>

#include "States.h"
#include "GpsPath.h"

namespace Visualization
{
void init(const VehicleStaticState& vehicle_static_state);

void quaternion_from_angle(geometry_msgs::Quaternion& q,
		const double orientation);

void color_from_argb(std_msgs::ColorRGBA& color, const double a, const double r,
		const double g, const double b);

const visualization_msgs::MarkerArray& get_path_marker_array(
		const std::vector<GpsPath>& paths);

const visualization_msgs::MarkerArray& get_trajectory_marker(
		const std::vector<double>& traj_x, const std::vector<double>& traj_y,
		const double trajectory_target_x, const double trajectory_target_y,
		const GpsPath& path);

const visualization_msgs::MarkerArray& get_zone_marker_array(
		const std::vector<GpsPath>& paths, bool display_go_zone,
		bool display_wait_zone);

const visualization_msgs::MarkerArray& get_vehicle_marker(const double pos_x,
		const double pos_y, const double yaw_angle,
		const VehicleStaticState& vehicle_static_state,
		const double current_steering_angle);

const visualization_msgs::Marker& get_steering_desired_marker(
		const double pos_x, const double pos_y, const double yaw_angle,
		const double steering_angle,
		const VehicleStaticState& vehicle_static_state);

const visualization_msgs::MarkerArray& get_control_point_marker(
		const double pos_x, const double pos_y, const double lap_x,
		const double lap_y);

const visualization_msgs::MarkerArray& get_control_obstacle_point_marker(
		const double pos_x, const double pos_y, const double lap_x,
		const double lap_y);

}



