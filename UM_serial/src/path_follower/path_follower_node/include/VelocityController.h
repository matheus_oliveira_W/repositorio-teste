#pragma once

#include <vector>
#include <fstream>

#include "HelperFunctions.h"

class VelocityController
{
public:
	typedef struct DynamicParameters
	{
		double p_max, p_min, p_idle, delta_v_brake, a_gas_neg, a_gas_pos,
				a_brake, filter_coeff, v_max, v_min, acc_lat_max;
	} DynamicParameters;

	void init(DynamicParameters dynamic_params,
			const std::vector<double> p_stat_map_v,
			const std::vector<double> p_stat_map_p);

	void update_parameters(DynamicParameters dynamic_params);

	double get_target_velocity(const double recorded_velocity,
			const double curvature);

	double get_gas_brake_value(const double current_velocity,
			const double target_velocity);

	void reset(double initial_gas_brake_val = 0);

	void export_map();

	double get_obstacle_velocity(double& velocity, const bool& emergency);


	friend class DataPlotter;

private:
	DynamicParameters params;
	double last_gas_brake_value, v_max_safe, target_velocity;
	std::vector<double> p_stat_map_v, p_stat_map_p;

	/*
	 * Get steady state gas-value for a given target velocity
	 */
	double get_p_stat(const double target_velocity);

};
