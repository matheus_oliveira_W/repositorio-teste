/** @file SteeringController.h
 @brief Class definition of the steering controller
 @author Tobias Brunner
 */

#pragma once

#include <ros/ros.h>
#include <cmath>
#include <vector>
#include <boost/math/tools/rational.hpp>

#include "HelperFunctions.h"
#include "States.h"

/**
 @brief The class SteeringController implements two methods for calculating a desired steering angle to follow a given path.

 Two control methods are implemented:
 1. Pure Pursuit Controller:
 2. Flatness Controller
 */
class SteeringController
{
public:
	/**
	 @brief Declare two methods of calculating the steering angle
	 */
	enum ControllerType
	{
		PurePursuitController, /**< classical Pure-Pursuit Controller */
		FlatnessController /**< advanced "Flatness" based controller */
	};

	/**
	 @brief Parameters of steering controller, that can be reconfigured at runtime

	 For explanation of look-ahead-distance, see SteeringController::get_look_ahead_distance
	 For explanation of eta_x, dt and poly_dist_min, see SteeringController::get_flatness_steering_angle
	 */
	typedef struct DynamicParameters
	{
		double look_ahead_dist_max, /**< maximum look-ahead distance */
		look_ahead_dist_min, /**< minimum look-ahead distance */
		look_ahead_dist_factor; /**< velocity scaling factor for look-ahead-distance */
		double eta_1, /**< polynomial tuning factor ("velocity" parameter) for FlatnessController */
		eta_2, /**< polynomial tuning factor ("velocity" parameter) for FlatnessController */
		eta_3, /**< polynomial tuning factor ("twist" parameter) for FlatnessController */
		eta_4, /**< polynomial tuning factor ("twist" parameter) for FlatnessController */
		dt, /**< look-ahead time (along polynomial trajectory) */
		poly_dist_min; /**< minimal look-ahead distance (along polynomial trajectory) */
	} DynamicParameters;

	/**
	 Initialization of SteeringController

	 @param[in] dynamic_params Initial values of dynamic parameters (see SteeringController::DynamicParameters)
	 @param[in] wheelbase Vehicle wheel base (German: "Radstand")
	 @param[in] max_steering_angle Maximum allowed absolute value of steering angle
	 @param[in] steering_map_norm Normalized steering values (range -1...1)
	 @param[in] steering_map_rad Steering values [rad], having point correspondence to the normalized steering values,
	 i.e. steering_map_rad.size() == steering_map_norm.size()
	 */
	void init(DynamicParameters dynamic_params, const double wheelbase, const double max_steering_angle,
			const std::vector<double> steering_map_norm,
			const std::vector<double> steering_map_rad);

	/**
	 Update dynamic parameters of SteeringController

	 @param[in] dynamic_params Updated values of dynamic parameters (see SteeringController::DynamicParameters)
	 */
	void update_parameters(const DynamicParameters& dynamic_params);

	/**
	 Calculate desired steering angle based on Pure-Pursuit Controller

	 @param[in] vehicle_state Dynamic vehicle state (see ::VehicleDynamicState)
	 @param[in] target_x x coordinate (UTM easting) of the look-ahead point
	 @param[in] target_y y coordinate (UTM northing) of the look-ahead point
	 @return desired steering angle
	 */
	double get_pure_pursuit_steering_angle(
			const VehicleDynamicState& vehicle_state, const double target_x,
			const double target_y) const;

	/**
	 Calculate desired steering angle based on Flatness Controller

	 @param[in] vehicle_state Dynamic vehicle state (see ::VehicleDynamicState)
	 @param[in] target_x x coordinate (UTM easting) of the look-ahead point
	 @param[in] target_y y coordinate (UTM northing) of the look-ahead point
	 @param[in] target_orientation path orientation (heading) at look-ahead point
	 @param[in] target_curvature path curvature at look-ahead point
	 @param[out] trajectory_x x coordinate of planned polynomial path evaluated at discrete points (for visualization)
	 @param[out] trajectory_y y coordinate of planned polynomial path evaluated at discrete points (for visualization)
	 @param[out] trajectory_target_x x coordinate of point on planned trajectory used for steering angle calculation (for visualization)
	 @param[out] trajectory_target_y y coordinate of point on planned trajectory used for steering angle calculation (for visualization)
	 @return desired steering angle
	 */
	double get_flatness_steering_angle(const VehicleDynamicState& vehicle_state,
			const double target_x, const double target_y,
			const double target_orientation, const double target_curvature,
			std::vector<double>& trajectory_x,
			std::vector<double>& trajectory_y,
			double& trajectory_target_x,
			double& trajectory_target_y) const;

	/**
	 Calculate look-ahead distance, i.e. the distance along the path, that the vehicle should look ahead for
	 steering/velocity control.

	 @param[in] velocity current vehicle velocity
	 @return look-ahead distance (in [m])
	 */
	double get_look_ahead_distance(const double velocity);

	/**
	 Calculate steering angle in [rad] based on normalized steering angle (range -1...1)

	 @param[in] steering_angle_normalized normalized steering angle (range -1...1)
	 @return steering angle in [rad]
	 */
	double norm2rad(const double steering_angle_normalized) const;

	/**
	 Calculate normalized steering angle (range -1...1) based on steering angle in [rad]

	 @param[in] steering_angle_rad steering angle in [rad]
	 @return normalized steering angle (range -1...1)
	 */
	double rad2norm(const double steering_angle_rad) const;

	friend class DataPlotter; // grant class DataPlotter access to private member variables

	double curvature_from_steering_angle(const double steering_angle) const;

	double steering_angle_from_curvature(const double curvature) const;

private:
	DynamicParameters params;
	double wheelbase, max_steering_angle;
	std::vector<double> steering_map_norm, steering_map_rad;
	

	double walk_along_poly_path(const double desired_dist,
			const boost::array<double, 6>& poly_x,
			const boost::array<double, 6>& poly_y) const;

};
