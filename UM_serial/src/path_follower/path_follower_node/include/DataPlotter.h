/** @file DataPlotter.cpp
 @brief Definition of DataPlotter class
 @author Tobias Brunner
 */

#pragma once

#include <memory>
#include <thread>
#include <string>
#include <sstream>
#include <vector>
#include <iostream>
#include <iomanip>
#include <boost/filesystem.hpp>

#include "GpsPath.h"
#include "VelocityController.h"
#include "SteeringController.h"

class DataPlotter
{
public:
	void init();

	bool plot_mappings(const VelocityController& velocity_controller,
			const SteeringController& steering_controller);

	bool plot_path_data(const GpsPath& gps_path,
			VelocityController& velocity_controller);

private:
	template<typename T>
	std::string list_from_vector(const std::vector<T>& vec);
	bool execute_python_command(const std::string& cmd);
	std::string exec_shell_command(const std::string &cmd);

	std::unique_ptr<std::thread> python_thread;
	bool python_thread_running;
	bool waiting_for_join;
	std::string temp_dir;

};

template<typename T>
std::string DataPlotter::list_from_vector(const std::vector<T>& vec)
{
	std::stringstream ss;

	ss << std::setprecision(9) << "[";

	for (int k = 0; k < vec.size(); ++k)
	{
		ss << vec[k];
		if (k < vec.size() - 1)
			ss << ", ";
	}

	ss << "]";

	return ss.str();
}
