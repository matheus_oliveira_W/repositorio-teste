/** @file States.h
 @brief State definitions of PathFollower system
 @author Tobias Brunner
 */

#pragma once

/**
 * Dynamic state of the vehicle
 */
struct VehicleDynamicState
{
	double x, /**< UTM easting [m] */
	y, /**< UTM northing [m] */
	alt,
	velocity, /**< velocity [m/s] */
	yaw_angle, /**< yaw angle [rad], relative to UTM easting axis */
	steering_angle, /**< steering angle [rad] */
	steering_angle_norm, /**< steering angle normalized to interval (-1.0, 1.0); -1.0 is fully left, +1.0 is fully right */
	gas_brake_pos, /**< currently actuated gas/brake position; -1.0 is full braking, +1.0 is full gas*/
	gear;
};

/**
 * Static state of the vehicle
 */
struct VehicleStaticState
{
	double wheelbase, /**< "Radstand": distance between front and rear axle */
	max_steering_angle, /**< maximum steering angle (reduced to single track model) */
	length, /**< length of vehicle body */
	width, /**< width of vehicle body */
	dist_mid_to_rear_axle, /**< distance from vehicle mid point to rear axle */
	wheel_width, /**< width of wheels */
	wheel_diameter, /**< diameter of wheels */
	wheel_offset, /**< lateral offset of wheels from middle of the vehicle */
	front_wheel_offset,
	rearD_wheel_offset,
	rearF_wheel_offset;
};

/**
 * Control target of the vehicle (desired values and intermediate quantities)
 */
struct ControlTargetState
{
	double closest_point_x, /**< UTM easting [m] of projection of vehicle position onto path (closest distance) */
	closest_point_y, /**< UTM northing [m] of projection of vehicle position onto path (closest distance) */
	look_ahead_point_x, /**< UTM easting [m] of look-ahead-point on path */
	look_ahead_point_y, /**< UTM northing [m] of look-ahead-point on path */
	
	look_ahead_obstacle_point_x, /**< UTM easting [m] of look-ahead-point on path */
	look_ahead_obstacle_point_y, /**< UTM northing [m] of look-ahead-point on path */	
	
	look_ahead_distance, /**< look-ahead distance [m] along the path from closest_point to look_ahead_point */
	velocity, /**< desired velocity [m/s] */
	steering_angle, /**< desired steering angle [rad] */
	steering_angle_norm, /**< desired steering angle normalized to interval (-1.0, 1.0) */
	gas_brake_norm; /**< desired gas/brake position normalized to interval (-1.0, 1.0) */
	
};

struct SensorState
{
	double 
		obstacle_range_base,
		obstacle_range_min,
		obstacle_range_factor,
		obstacle_curve_range_factor;

	bool
		follow_with_sensor;
	
};

/**
 * Global parameters
 */
struct GlobalParameters
{
	double
		min_path_length,					/**< minimal path length [m], so that the path is not deemed too short */
		post_process_max_seg_length,		/**< maximum distance [m] between path-points after post processing */
		post_process_epsilon,				/**< [m], deviation parameter for Douglas-Peucker-Algorithm */
		post_process_curvature_smoothing,	/**< smoothing factor between 0.0 and 1.0; bigger values lead to more smoothing of curvature values */
		idle_brake_value,					/**< brake value (0.0, 1.0) to keep vehicle still in state IDLE */
		max_steering_jump,					/**< maximum difference between actual and desired steering value */
		max_gas_brake_jump,					/**< maximum difference between actual and desired gas/brake value */
		max_dist_from_path,					/**< maximum allowed distance from the path */
		max_heading_deviation,				/**< maximum allowed difference in heading between the path and the vehicle */
		max_velocity_for_idle_braking,		/**< maximum velocity at which idle braking can be applied */
		max_velocity;						/**< maximum allowed velocity */
	bool
		display_go_zone,					/**< true: Go-zones will be displayed in the GUI */
		display_wait_zone,					/**< true: Wait-zones will be displayed in the GUI */
		following_from_outside_go_zone,		/**< true: Path following can be activated even when the vehicle is not inside a go-zone */
		following_without_paravan,          /**< true: Path following can be activated without Paravan being available */
		idle_braking,						/**< true: Brake is applied in state IDLE */
		control_gas_brake,					/**< true: Gas/brake is controlled during FOLLOWING; false: only steering is controleld */
		keep_steering_neutral;				/**< true: Paravan node is keeping the desired value = actual value */
	int cycles_before_start_following;      /**< Number of timer cycles before FOLLLOWING is actually actuated, i.e. a short delay */
};

