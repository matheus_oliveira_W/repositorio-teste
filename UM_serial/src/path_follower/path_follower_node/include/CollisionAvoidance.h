#pragma once

#include <ros/ros.h>
#include <math.h>
#include <vector>

//ROS messages provided by sensors nodes
#include "sick_ldmrs_msgs/ObjectArray222.h"
#include "sick_ldmrs_msgs/ObjectArray224.h"
#include "sick_ldmrs_msgs/ObjectArray175.h"
#include <radar_msgs/RadarDetectionArray.h>
#include <derived_object_msgs/ObjectWithCovarianceArray.h>

#include "States.h"

#define PI 3.14159265
#define TRUCK_WIDTH 5.5

class CollisionAvoidance
{
public:

	
	/**
	 * Callback from the ROS topics provided by the sensor nodes
	 */

	void callback_sick_front_left(sick_ldmrs_msgs::ObjectArray175 msg);
	void callback_sick_front_right(sick_ldmrs_msgs::ObjectArray224 msg);
	void callback_radar_front(derived_object_msgs::ObjectWithCovarianceArray msg);

	//void callback_sick_back(sick_ldmrs_msgs::ObjectArray222 msg);
	//void callback_radar_back(derived_object_msgs::ObjectWithCovarianceArray msg);

	void lidar_fusion();

	double get_sensor_range_distance(double velocity, double base_distance, double dist_factor);

	double get_sensor_range_curve_distance(double velocity, double base_distance, double dist_factor, double curve_value);

	void nearest_obstacle();

	std::vector<double>  left_lidar_x,
						 right_lidar_x,
						 front_radar_x,
						 sensor_fused_h,
						 sensor_fused_x,
						 sensor_fused_y,
						 sensor_fused_ang;

	double obj0_x = 100,
		   obj1_x,
		   obj0_y,
		   obj1_y,
		   ang1,
		   ang0;
	
private:

	std::vector<double> left_lidar_y,
						left_lidar_ang,
						left_lidar_size_x,
						left_lidar_size_y,
						right_lidar_y,
						right_lidar_size_x,
						right_lidar_size_y,
						right_lidar_ang,
						front_radar_y,
						front_radar_ang;
	
	int k;

	float new_left_lidar_y,
		  new_right_lidar_y,
		  new_lidar_x,
		  new_lidar_y;
	
};	
