#include "Oxts.h"

void Oxts::init(const double alive_timeout, const double data_timeout)
{
	alive_timeout_.set_timeout(alive_timeout);
	data_timeout_.set_timeout(data_timeout);

	state_ = State::OFFLINE;
}

const Oxts::Data& Oxts::data()
{
	return data_;
}

const Oxts::State& Oxts::state()
{
	return state_;
}

void Oxts::update_state()
{
	/** If oxts node timed out (see ::Timeout), set state to Oxts::State::OFFLINE */
	if (alive_timeout_.timed_out())
	{
		state_ = Oxts::State::OFFLINE;
	}
	else /** If oxts node is alive ... */
	{
		if (data_timeout_.timed_out())
		{
			/** - ... but no navigation data is being received, set state to Oxts::State::NO_DATA */
			state_ = Oxts::State::NO_DATA;
		}
		else // oxts node is sending data
		{
			if (data_.nav_sat_fix.status.status
					== sensor_msgs::NavSatStatus::STATUS_NO_FIX) // no gps fix
			{
				/** - ... and navigation data is being received, but no GPS fix is available,
				 set state to Oxts::State::DATA_BUT_NO_FIX */
				state_ = Oxts::State::DATA_BUT_NO_FIX;
			}
			else
			{
				/** - ... and navigation data is being received and a GPS fix is available,
				 set state to Oxts::State::FIX */
				state_ = Oxts::State::FIX;
			}
		}
	}
}

void Oxts::callback_nav_sat_fix(const sensor_msgs::NavSatFix::ConstPtr& msg)
{
	data_timeout_.got_signal();
	data_.nav_sat_fix = *msg;
}

void Oxts::callback_imu(const sensor_msgs::Imu::ConstPtr& msg)
{
	data_.imu = *msg;
}

void Oxts::callback_twist(
		const geometry_msgs::TwistWithCovarianceStamped::ConstPtr& msg)
{
	data_.twist = *msg;
}

void Oxts::callback_alive(const std_msgs::Empty::ConstPtr& msg)
{
	alive_timeout_.got_signal();
}

