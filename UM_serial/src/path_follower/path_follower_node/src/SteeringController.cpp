/** @file SteeringController.cpp
 @brief Implementation of class SteeringController
 @author Tobias Brunner
 */

#include "SteeringController.h"
#include <iostream>

void SteeringController::init(DynamicParameters dynamic_params,	const double wheelbase,
		const double max_steering_angle,
		const std::vector<double> steering_map_norm,
		const std::vector<double> steering_map_rad)
{
	params = dynamic_params;
	this->wheelbase = wheelbase;
	this->max_steering_angle = max_steering_angle;
	this->steering_map_norm = steering_map_norm;
	this->steering_map_rad = steering_map_rad;
}

void SteeringController::update_parameters(
		const DynamicParameters& dynamic_params)
{
	params = dynamic_params;
}

double SteeringController::get_look_ahead_distance(const double velocity)
{
	double look_ahead_dist = params.look_ahead_dist_min + velocity * params.look_ahead_dist_factor;

	if (look_ahead_dist > params.look_ahead_dist_max)
		return params.look_ahead_dist_max;

	return look_ahead_dist;
}

double SteeringController::get_pure_pursuit_steering_angle(
		const VehicleDynamicState& vehicle_state, const double target_x,
		const double target_y) const
{
	/**
	 (for detailed theoretical background see 'Implementation of the Pure Pursuit
	 Path Tracking Algorithm' (1992) by R. Craig Coulter)
	 */

	/** Algorithm steps: */

	/** - Transform the look-ahead point to the vehicle coordinate system (translate, then rotate). */
	double look_x = target_x - vehicle_state.x;
	double look_y = target_y - vehicle_state.y;

	double x = std::cos(vehicle_state.yaw_angle) * look_x
			+ std::sin(vehicle_state.yaw_angle) * look_y;
	double y = -std::sin(vehicle_state.yaw_angle) * look_x
			+ std::cos(vehicle_state.yaw_angle) * look_y;

	/** - calculate the distance between vehicle and look ahead point  */
	double l_squared = x * x + y * y;

	if (l_squared < 0.01)
		return 0;

	/** - calculate the desired curvature to reach the look ahead point (this curvature is the inverse of the
	 radius of the circle, that passes through the look-ahead point and is tangential to the current vehicle heading)
	 */
	double gamma = 2.0 * y / l_squared;

	/** - based on this curvature, calculate the desired steering_angle */
	double steering_angle = steering_angle_from_curvature(gamma);

	/** - if the desired steering angle is too large, limit it to the allowed range */
	if (steering_angle < -max_steering_angle)
		return -max_steering_angle;
	if (steering_angle > max_steering_angle)
		return max_steering_angle;

	return steering_angle;
}

double SteeringController::get_flatness_steering_angle(
		const VehicleDynamicState& vehicle_state, const double target_x,
		const double target_y, const double target_orientation,
		const double target_curvature,
		std::vector<double>& trajectory_x,
		std::vector<double>& trajectory_y,
		double& trajectory_target_x,
		double& trajectory_target_y) const
{
	/**
	 (for detailed theoretical background see 'THE ARGO AUTONOMOUS VEHICLE'S VISION
	 AND CONTROL SYSTEMS' (1999) by Massimo Bertozzi et. al.)
	 */

	/**
	 The path-tracking flatness controller is based on creating a polynomial description of the trajectory
	 that the vehicle should follow.	Flatness, in the context of control theory, means that given a
	 desired trajectory for a "flat" output of a dynamic system (in this case: dynamic system = vehicle motion model,
	 flat output = position of the vehicle), the corresponding input trajectory can be calculated.
	 */

	/** Algorithm steps: */

	/** - calculate current curvature (based on current steering angle) */
	double kappa_A = 0.0; //curvature_from_steering_angle(vehicle_state.steering_angle);

	// helper references, for better readability
	const double& x_A = vehicle_state.x;
	const double& y_A = vehicle_state.y;
	const double& theta_A = vehicle_state.yaw_angle;

	const double& x_B = target_x;
	const double& y_B = target_y;
	const double& theta_B = target_orientation;
	const double& kappa_B = target_curvature;

	/** - calculate coefficients of fifth degree polynomial (both in x- and y direction), that satisfies
	 the boundary conditions (curvature, heading and position) given by the current vehicle state and the target state */
	double x0 = x_A;
	double x1 = params.eta_1 * std::cos(theta_A);
	double x2 =
			0.5
					* (params.eta_3 * std::cos(theta_A)
							- params.eta_1 * params.eta_1 * kappa_A
									* std::sin(theta_A));

	double x3 = 10.0 * (x_B - x_A)
			- (6.0 * params.eta_1 + 1.5 * params.eta_3) * std::cos(theta_A)
			- (4.0 * params.eta_2 - 0.5 * params.eta_4) * std::cos(theta_B)
			+ 1.5 * params.eta_1 * params.eta_1 * kappa_A * std::sin(theta_A)
			- 0.5 * params.eta_2 * params.eta_2 * kappa_B * std::sin(theta_B);

	double x4 = -15.0 * (x_B - x_A)
			+ (8.0 * params.eta_1 + 1.5 * params.eta_3) * std::cos(theta_A)
			+ (7.0 * params.eta_2 - params.eta_4) * std::cos(theta_B)
			- 1.5 * params.eta_1 * params.eta_1 * kappa_A * std::sin(theta_A)
			+ params.eta_2 * params.eta_2 * kappa_B * std::sin(theta_B);

	double x5 = 6.0 * (x_B - x_A)
			- (3.0 * params.eta_1 + 0.5 * params.eta_3) * std::cos(theta_A)
			- (3.0 * params.eta_2 - 0.5 * params.eta_4) * std::cos(theta_B)
			+ 0.5 * params.eta_1 * params.eta_1 * kappa_A * std::sin(theta_A)
			- 0.5 * params.eta_2 * params.eta_2 * kappa_B * std::sin(theta_B);

	double y0 = y_A;
	double y1 = params.eta_1 * std::sin(theta_A);
	double y2 =
			0.5
					* (params.eta_3 * std::sin(theta_A)
							+ params.eta_1 * params.eta_1 * kappa_A
									* std::cos(theta_A));

	double y3 = 10.0 * (y_B - y_A)
			- (6.0 * params.eta_1 + 1.5 * params.eta_3) * std::sin(theta_A)
			- (4.0 * params.eta_2 - 0.5 * params.eta_4) * std::sin(theta_B)
			- 1.5 * params.eta_1 * params.eta_1 * kappa_A * std::cos(theta_A)
			+ 0.5 * params.eta_2 * params.eta_2 * kappa_B * std::cos(theta_B);

	double y4 = -15.0 * (y_B - y_A)
			+ (8.0 * params.eta_1 + 1.5 * params.eta_3) * std::sin(theta_A)
			+ (7.0 * params.eta_2 - params.eta_4) * std::sin(theta_B)
			+ 1.5 * params.eta_1 * params.eta_1 * kappa_A * std::cos(theta_A)
			- params.eta_2 * params.eta_2 * kappa_B * std::cos(theta_B);

	double y5 = 6.0 * (y_B - y_A)
			- (3.0 * params.eta_1 + 0.5 * params.eta_3) * std::sin(theta_A)
			- (3.0 * params.eta_2 - 0.5 * params.eta_4) * std::sin(theta_B)
			- 0.5 * params.eta_1 * params.eta_1 * kappa_A * std::cos(theta_A)
			+ 0.5 * params.eta_2 * params.eta_2 * kappa_B * std::cos(theta_B);

	/** - based on the coefficients, create the polynomial path and its first/second derivatives */
	boost::array<double, 6> poly_x =
	{ x0, x1, x2, x3, x4, x5 };
	boost::array<double, 5> poly_xp =
	{ x1, 2.0 * x2, 3.0 * x3, 4.0 * x4, 5.0 * x5 };
	boost::array<double, 4> poly_xpp =
	{ 2.0 * x2, 6.0 * x3, 12.0 * x4, 20.0 * x5 };

	boost::array<double, 6> poly_y =
	{ y0, y1, y2, y3, y4, y5 };
	boost::array<double, 5> poly_yp =
	{ y1, 2.0 * y2, 3.0 * y3, 4.0 * y4, 5.0 * y5 };
	boost::array<double, 4> poly_ypp =
	{ 2.0 * y2, 6.0 * y3, 12.0 * y4, 20.0 * y5 };

	/** - determine how far ahead of the vehicle (but on the polynomial path) the desired curvature should be calculated and
	 find the corresponding value of the polynomial argument (inverse path length function) */
	double path_dist = std::max(params.poly_dist_min,
			params.dt * vehicle_state.velocity);
	double u = walk_along_poly_path(path_dist, poly_x, poly_y);

	/** - calculate desired curvature using the polynomial path's derivatives  */
	double xp = boost::math::tools::evaluate_polynomial(poly_xp, u);
	double yp = boost::math::tools::evaluate_polynomial(poly_yp, u);

	double xpp = boost::math::tools::evaluate_polynomial(poly_xpp, u);
	double ypp = boost::math::tools::evaluate_polynomial(poly_ypp, u);

	double kappa = (xp * ypp - xpp * yp) / std::pow((xp * xp + yp * yp), 1.5);

	ROS_DEBUG_STREAM("[Steering Controller]: u = " << u << ", curvature =" << kappa);

	/** - based on this curvature, calculate the desired steering_angle */
	double steering_angle = steering_angle_from_curvature(kappa);

	// calculate coordinates of trajectory look-ahead point (given by u)
	trajectory_target_x = boost::math::tools::evaluate_polynomial(poly_x, u);
	trajectory_target_y = boost::math::tools::evaluate_polynomial(poly_y, u);

	// evaluate polynomial path at discrete points (only for visualization purposes)
	for (u = 0.0; u < 1.0; u += 0.02)
	{
		trajectory_x.push_back(
				boost::math::tools::evaluate_polynomial(poly_x, u));
		trajectory_y.push_back(
				boost::math::tools::evaluate_polynomial(poly_y, u));
	}

	/** - if the desired steering angle is too large, limit it to the allowed range */
	if (steering_angle < -max_steering_angle)
		return -max_steering_angle;
	if (steering_angle > max_steering_angle)
		return max_steering_angle;

	return steering_angle;
}

double SteeringController::walk_along_poly_path(const double desired_dist,
		const boost::array<double, 6>& poly_x,
		const boost::array<double, 6>& poly_y) const
{
	const double du = 0.02; //

	double arc_length = 0;
	double u = 0;
	double dist = 0;

	while (arc_length < desired_dist)
	{
		double x0 = boost::math::tools::evaluate_polynomial(poly_x, u);
		double y0 = boost::math::tools::evaluate_polynomial(poly_y, u);

		double x1 = boost::math::tools::evaluate_polynomial(poly_x, u + du);
		double y1 = boost::math::tools::evaluate_polynomial(poly_y, u + du);

		dist = std::sqrt((x1 - x0) * (x1 - x0) + (y1 - y0) * (y1 - y0));

		arc_length += dist;
		u += du;

		if (u >= 1.0)
			return 1.0;
	}

	if (dist > 0)
	{
		u -= du * (arc_length - desired_dist) / dist; // interpolate
	}

	return u;
}

double SteeringController::steering_angle_from_curvature(
		const double curvature) const
{
	return std::atan(wheelbase * curvature);
}

double SteeringController::curvature_from_steering_angle(
		const double steering_angle) const
{
	return std::tan(steering_angle) / wheelbase;
}

double SteeringController::norm2rad(
		const double steering_angle_normalized) const
{
	double abs_val = std::fabs(steering_angle_normalized);
	double sign = steering_angle_normalized < 0 ? -1 : 1;

	sign *= -1; // ATTENTION: This is done because Paravan defines negative values as steering left

	return sign
			* HelperFunctions::interpolate(steering_map_norm, steering_map_rad,
					abs_val);
}

double SteeringController::rad2norm(const double steering_angle_rad) const
{
	double abs_val = std::fabs(steering_angle_rad);
	double sign = steering_angle_rad < 0 ? -1 : 1;

	sign *= -1; // ATTENTION: This is done because Paravan defines negative values as steering left

	return sign
			* HelperFunctions::interpolate(steering_map_rad, steering_map_norm,
					abs_val);
}

