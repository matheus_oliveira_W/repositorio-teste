#include "Wgs2UtmConverter.h"

void Wgs2UtmConverter::convert(const double gps_lat, const double gps_lon,
		double& utm_x, double& utm_y)
{
	int zone;
	bool northp;

	if (m_reference_zone_set)
	{
		GeographicLib::UTMUPS::Forward(gps_lat, gps_lon, zone, northp, utm_x,
				utm_y, m_zone);
	}
	else
	{
		GeographicLib::UTMUPS::Forward(gps_lat, gps_lon, zone, northp, utm_x,
				utm_y);
		m_zone = zone;
		m_zonestr = GeographicLib::UTMUPS::EncodeZone(zone, northp);
		m_reference_zone_set = true;
	}
}

void Wgs2UtmConverter::init_zone(const double gps_lat, const double gps_lon)
{
	double x, y;
	convert(gps_lat, gps_lon, x, y);
}

