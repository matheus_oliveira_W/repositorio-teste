/** @file DataPlotter.cpp
 @brief Implementation of GpsPath methods
 @author Tobias Brunner
 */

#include "GpsPath.h"

using HelperFunctions::distance;

void to_json(nlohmann::json& j, const GpsPath& path)
{
	j = nlohmann::json
	{
	{ "name", path.m_name },
	{ "reverse", path.reverse},
	{ "color_a", path.m_color.a },
	{ "color_r", path.m_color.r },
	{ "color_g", path.m_color.g },
	{ "color_b", path.m_color.b },
	{ "rec_start_seconds", path.rec_sec },
	{ "rec_start_nanoseconds", path.rec_nsec },
	{ "go_zone_radius", path.go_zone_radius },
	{ "wait_zone_radius", path.wait_zone_radius },
	{ "gps_lat", path.m_gps_lat },
	{ "gps_lon", path.m_gps_lon },
	{ "gps_alt", path.m_gps_alt },
	{ "utm_easting", path.m_x },
	{ "utm_northing", path.m_y },
	{ "velocity", path.m_velocity } };
}

void from_json(const nlohmann::json& j, GpsPath& path)
{
	path.m_name = j.at("name").get<std::string>();
	path.reverse = j.at("reverse").get<int>();
	path.m_color.a = j.at("color_a").get<double>();
	path.m_color.r = j.at("color_r").get<double>();
	path.m_color.g = j.at("color_g").get<double>();
	path.m_color.b = j.at("color_b").get<double>();
	path.rec_sec = j.at("rec_start_seconds").get<int>();
	path.rec_nsec = j.at("rec_start_nanoseconds").get<int>();
	path.go_zone_radius = j.at("go_zone_radius").get<double>();
	path.wait_zone_radius = j.at("wait_zone_radius").get<double>();

	std::vector<double> gps_lat = j.at("gps_lat").get<std::vector<double>>();
	std::vector<double> gps_lon = j.at("gps_lon").get<std::vector<double>>();
	std::vector<double> gps_alt = j.at("gps_alt").get<std::vector<double>>();
	std::vector<double> px = j.at("utm_easting").get<std::vector<double>>();
	std::vector<double> py = j.at("utm_northing").get<std::vector<double>>();
	std::vector<double> velocity = j.at("velocity").get<std::vector<double>>();

	for (size_t k = 0; k < px.size(); ++k)
	{
		path.add_point(px[k], py[k], gps_lat[k], gps_lon[k], gps_alt[k], velocity[k]);
	}

}

double GpsPath::length() const
{
	if (this->size() > 0)
	{
		return m_p_length.back();
	}

	return 0;
}

void GpsPath::set_name(const std::string& name)
{
	m_name = name;
}

const std::string& GpsPath::name()
{
	return m_name;
}

void GpsPath::set_color(const double a, const double r, const double g,
		const double b)
{
	m_color.a = a;
	m_color.r = r;
	m_color.g = g;
	m_color.b = b;
}

const GpsPath::VisualizationColor& GpsPath::color()
{
	return m_color;
}

void GpsPath::add_point(const double x, const double y, const double gps_lat,
		const double gps_lon, const double gps_alt, const double velocity)
{
	Path<double>::add_point(x, y);
	m_gps_lat.push_back(gps_lat);
	m_gps_lon.push_back(gps_lon);
	m_gps_alt.push_back(gps_alt);
	m_velocity.push_back(velocity);

	// heading (from 2 points)
	if (size() > 1)
	{
		double dx = m_x[size() - 1] - m_x[size() - 2];
		double dy = m_y[size() - 1] - m_y[size() - 2];
		double heading = std::atan2(dy, dx);

		m_heading.push_back(heading);

		if (size() == 2)
		{
			m_heading[0] = heading;
		}
	}
	else
	{
		m_heading.push_back(0.0);
	}

	// curvature (from 3 points)
	size_t ind = size() - 1;
	if (size() > 2)
	{
		// A, B and C are side lengths of a triangle
		double A = distance(m_x[ind - 1], m_y[ind - 1], m_x[ind - 2],
				m_y[ind - 2]);
		double B = distance(m_x[ind], m_y[ind], m_x[ind - 2], m_y[ind - 2]);
		double C = distance(m_x[ind], m_y[ind], m_x[ind - 1], m_y[ind - 1]);

		double s = 0.5 * (A + B + C);
		double triangle_area = std::sqrt(s * (s - A) * (s - B) * (s - C));

		double curvature = 0;
		if (triangle_area > 0.0001)
		{
			double radius = A * B * C / (4 * triangle_area);
			curvature = 1 / radius;

			double x1 = m_x[ind - 1] - m_x[ind - 2];
			double y1 = m_y[ind - 1] - m_y[ind - 2];
			double x2 = m_x[ind] - m_x[ind - 2];
			double y2 = m_y[ind] - m_y[ind - 2];

			if (x1 * y2 - x2 * y1 < 0) // cross product
			{
				curvature *= -1;
			}
		}

		m_curvature.push_back(curvature);

		if (size() == 3)
		{
			m_curvature[0] = curvature;
			m_curvature[1] = curvature;
		}
	}
	else
	{
		m_curvature.push_back(0.0);
	}
}

void GpsPath::clear()
{
	Path<double>::clear();
	m_gps_lat.clear();
	m_gps_lon.clear();
	m_gps_alt.clear();
	m_velocity.clear();
	m_heading.clear();
	m_curvature.clear();
}

double GpsPath::s(PathTools::PathPoint<double> const& pp) const
{
	if (size() == 0 || pp.ind >= size())
	{
		return 0;
	}

	if (size() == 1)
	{
		return m_p_length[0];
	}

	if (size() - 1 == pp.ind)
	{
		return m_p_length[pp.ind];
	}

	return m_p_length[pp.ind]
			+ pp.u * (m_p_length[pp.ind + 1] - m_p_length[pp.ind]);
}

double GpsPath::velocity(PathTools::PathPoint<double> const& pp) const
{
	if (size() == 0 || pp.ind >= size())
	{
		return 0;
	}

	if (size() == 1)
	{
		return m_velocity[0];
	}

	if (size() - 1 == pp.ind)
	{
		return m_velocity[pp.ind];
	}

	return m_velocity[pp.ind]
			+ pp.u * (m_velocity[pp.ind + 1] - m_velocity[pp.ind]);
}

double GpsPath::altitude(PathTools::PathPoint<double> const& pp) const
{
	if (size() == 0 || pp.ind >= size())
	{
		return 0;
	}

	if (size() == 1)
	{
		return m_gps_alt[0];
	}

	if (size() - 1 == pp.ind)
	{
		return m_gps_alt[pp.ind];
	}

	return m_gps_alt[pp.ind]
			+ pp.u * (m_gps_alt[pp.ind + 1] - m_gps_alt[pp.ind]);
}

double GpsPath::heading(PathTools::PathPoint<double> const& pp) const
{
	if (size() == 0 || pp.ind >= size())
	{
		return 0;
	}

	if (size() == 1)
	{
		return m_heading[0];
	}

	if (size() - 1 == pp.ind)
	{
		return m_heading[pp.ind];
	}

	double h0 = m_heading[pp.ind];
	double h1 = m_heading[pp.ind + 1];

	// interpolate using vector notation
	double h0_x = std::cos(h0);
	double h0_y = std::sin(h0);
	double h1_x = std::cos(h1);
	double h1_y = std::sin(h1);

	double h_x_interp = h0_x + (h1_x - h0_x) * pp.u;
	double h_y_interp = h0_y + (h1_y - h0_y) * pp.u;

	double heading_interp = std::atan2(h_y_interp, h_x_interp);

	return heading_interp;
}

double GpsPath::curvature(PathTools::PathPoint<double> const& pp) const
{
	if (size() == 0 || pp.ind >= size())
	{
		return 0;
	}

	if (size() == 1)
	{
		return m_curvature[0];
	}

	if (size() - 1 == pp.ind)
	{
		return m_curvature[pp.ind];
	}

	return m_curvature[pp.ind]
			+ pp.u * (m_curvature[pp.ind + 1] - m_curvature[pp.ind]);
}

bool GpsPath::has_points() const
{
	return (size() > 0);
}

void GpsPath::post_process(const double max_seg_length, const double epsilon,
		const double curvature_smoothing_factor)
{
	using namespace boost::assign;
	typedef boost::geometry::model::d2::point_xy<double> xy;

	boost::geometry::model::linestring<xy> line;

	for (size_t k = 0; k < size(); ++k)
	{
		line += xy(m_x[k], m_y[k]);
	}

	// simplify the line-string using distance of epsilon
	boost::geometry::model::linestring<xy> simplified;
	boost::geometry::simplify(line, simplified, epsilon);

	std::vector<double> x, y, lat, lon, alt, vel, head;

	unsigned int point_count = 0;
	double last_x, last_y;

	for (auto const& xy_point : simplified)
	{
		// find closest point of original path to the simplified point
		PathTools::Point<double> p(xy_point.x(), xy_point.y());
		auto closest_path_point = this->closest_point(p, false);
		int ind =
				closest_path_point.u < 0.5 ?
						closest_path_point.ind : closest_path_point.ind + 1;

		if (point_count > 0)
		{
			double dx = xy_point.x() - last_x;
			double dy = xy_point.y() - last_y;
			double seg_length = std::sqrt(dx * dx + dy * dy);

			// if the current segment is longer than max_seg_length, add evenly spaced points
			int num_added_points = seg_length / max_seg_length;

			for (int k = 0; k < num_added_points; ++k)
			{
				double factor = static_cast<double>(k + 1)
						/ (num_added_points + 1);

				x.push_back(last_x + (xy_point.x() - last_x) * factor);
				y.push_back(last_y + (xy_point.y() - last_y) * factor);

				lat.push_back(
						m_gps_lat[ind - 1] + (m_gps_lat[ind] - m_gps_lat[ind - 1]) * factor);
				lon.push_back(
						m_gps_lon[ind - 1] + (m_gps_lon[ind] - m_gps_lon[ind - 1]) * factor);
				alt.push_back(
						m_gps_alt[ind - 1] + (m_gps_alt[ind] - m_gps_alt[ind - 1]) * factor);

				// for velocity values, again find closest point on original path
				// (alternatively, this could also be done for x, y, lat, lon)
				PathTools::Point<double> p_inner(x.back(), y.back());
				auto closest_path_point_inner = this->closest_point(p_inner,
						false);
				int ind_inner =
						closest_path_point_inner.u < 0.5 ?
								closest_path_point_inner.ind :
								closest_path_point_inner.ind + 1;

				vel.push_back(m_velocity[ind_inner]);
			}
		}

		x.push_back(xy_point.x());
		y.push_back(xy_point.y());
		lat.push_back(m_gps_lat[ind]);
		lon.push_back(m_gps_lon[ind]);
		alt.push_back(m_gps_alt[ind]);
		vel.push_back(m_velocity[ind]);

		last_x = xy_point.x();
		last_y = xy_point.y();
		++point_count;
	}

	this->clear();

	double ignore_radius = 1.0; // only allow 1 point in area around first/last path-point respectively

	for (int k = 0; k < x.size(); ++k)
	{
		double dist_to_start = sqrt(
				(x[k] - x.front()) * (x[k] - x.front())
						+ (y[k] - y.front()) * (y[k] - y.front()));
		double dist_to_end = sqrt(
				(x[k] - x.back()) * (x[k] - x.back())
						+ (y[k] - y.back()) * (y[k] - y.back()));

		if ((dist_to_end > ignore_radius || x.size() - 1 == k)
				&& (dist_to_start > ignore_radius || 0 == k))
		{
			this->add_point(x[k], y[k], lat[k], lon[k], alt[k], vel[k]);
		}
	}

	// remove artifacts where heading difference is unreasonably big (probably from a standstill period during recording)
	for (int it = 0; it < 3; ++it) // perform 3 iterations
	{
		x = this->m_x;
		y = this->m_y;
		lat = this->m_gps_lat;
		lon = this->m_gps_lon;
		alt = this->m_gps_alt;
		vel = this->m_velocity;
		head = this->m_heading;

		this->clear();

		this->add_point(x[0], y[0], lat[0], lon[0], alt[0], vel[0]); // add first point

		for (int k = 1; k < head.size() - 1; ++k)
		{
			if (std::acos(std::cos(head[k] - head[k - 1])) < 1.0)
			{
				this->add_point(x[k], y[k], lat[k], lon[k], alt[k], vel[k]);
			}
		}

		this->add_point(x.back(), y.back(), lat.back(), lon.back(), alt.back(), vel.back()); // add last point
	}

	// smooth curvature values
	for (int k = 1; k < m_curvature.size() - 1; ++k)
	{
		double c0 = m_curvature[k - 1];
		double c1 = m_curvature[k];
		double c2 = m_curvature[k + 1];

		m_curvature[k] = 0.5 * curvature_smoothing_factor * c0
				+ (1.0 - curvature_smoothing_factor) * c1
				+ 0.5 * curvature_smoothing_factor * c2;
	}
}

