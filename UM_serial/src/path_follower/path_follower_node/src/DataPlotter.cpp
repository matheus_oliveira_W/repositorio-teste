/** @file DataPlotter.cpp
 @brief Implementation of DataPlotter methods
 @author Tobias Brunner
 */

#include "DataPlotter.h"

void DataPlotter::init()
{
	python_thread_running = false;
	waiting_for_join = false;

	std::string home_dir = boost::filesystem::path(getenv("HOME")).c_str();
	temp_dir = home_dir + "/.tmp";
	boost::filesystem::create_directories(temp_dir);
}

bool DataPlotter::plot_path_data(const GpsPath& gps_path,
		VelocityController& velocity_controller)
{
	std::vector<double> x_rel, y_rel, v_target, alt_rel;
	for (const auto val : gps_path.x())
		x_rel.push_back(val - gps_path.x().front());
	for (const auto val : gps_path.y())
		y_rel.push_back(val - gps_path.y().front());
	for (const auto val : gps_path.gps_alt())
		alt_rel.push_back(val - gps_path.gps_alt().front());
	for (int k = 0; k < gps_path.curvature().size(); ++k)
	{
		v_target.push_back(
				velocity_controller.get_target_velocity(gps_path.velocity()[k],
						gps_path.curvature()[k]));
	}

	std::stringstream cmd;

	cmd << "import matplotlib.pyplot as plt \n";
	cmd << "import matplotlib.gridspec as gridspec \n";

	cmd << "plt.figure(figsize=(14,8)) \n";

	cmd << "s = " + list_from_vector(gps_path.s()) + "\n";
	cmd << "v = " + list_from_vector(gps_path.velocity()) + "\n";
	cmd << "v_target = " + list_from_vector(v_target) + "\n";
	cmd << "heading = " + list_from_vector(gps_path.heading()) + "\n";
	cmd << "curvature = " + list_from_vector(gps_path.curvature()) + "\n";
	cmd << "x = " + list_from_vector(x_rel) + "\n";
	cmd << "y = " + list_from_vector(y_rel) + "\n";
	cmd << "altitude = " + list_from_vector(alt_rel) + "\n";

	cmd << "gridspec.GridSpec(3,4) \n";
	cmd << "plt.subplot2grid((3,4), (0,2), colspan=2, rowspan=3)  \n";
	cmd << "plt.xlabel('x [m]') \n";
	cmd << "plt.ylabel('y [m]') \n";
	cmd << "plt.plot(x, y) \n";
	cmd << "if len(x) > 0: \n";
	cmd << "	plt.plot([0], [0], 'kx') \n";
	cmd << "	plt.plot(x[-1], y[-1], 'ko') \n";
	cmd << "plt.axis('equal') \n";
	//cmd << "plt.grid(True) \n";
	cmd << "plt.gca().grid(which='major', axis='both', linestyle='--') \n";
	cmd << "plt.title('Path', fontsize=10) \n";

	cmd << "plt.subplot2grid((3,4), (0,0), colspan=2, rowspan=1) \n";
	cmd << "line1, = plt.plot(s, v_target, '-r', label='target') \n";
	cmd << "line2, = plt.plot(s, v, label='recorded') \n";
	cmd << "plt.gca().legend() \n";
	cmd << "plt.ylabel('velocity [m/s]') \n";
	cmd << "plt.xlabel('path length [m]') \n";
	cmd << "if len(s) > 0: \n";
	cmd << "	plt.plot([0], [0], 'kx') \n";
	cmd << "	plt.plot(s[-1], v[-1], 'ko') \n";
	//cmd << "plt.title('velocity', fontsize=10) \n";
	cmd << "plt.grid(True) \n";

	cmd << "plt.subplot2grid((3,4), (1,0), colspan=2, rowspan=1) \n";
	cmd << "plt.plot(s, heading) \n";
	cmd << "plt.xlabel('path length [m]') \n";
	cmd << "plt.ylabel('heading [rad]') \n";
	//cmd << "plt.title('heading', fontsize=10) \n";
	cmd << "plt.grid(True) \n";

	cmd << "plt.subplot2grid((3,4), (2,0), colspan=2, rowspan=1) \n";
	cmd << "plt.plot(s, curvature) \n";
	cmd << "plt.xlabel('path length [m]') \n";
	cmd << "plt.ylabel('curvature [1/m]') \n";
	//cmd << "plt.title('curvature', fontsize=10) \n";
	cmd << "plt.grid(True) \n";

	cmd	<< "plt.subplots_adjust(left=0.08, bottom=0.08, right=0.92, top=0.92, wspace=0.5, hspace=0.5) \n";

	cmd << " \n";
	cmd << " \n";
	cmd << "plt.show() \n";

	// write to temporary file, as command might get too big for being used as command line string
	std::string script_file = temp_dir + "/path_data.py";
	std::ofstream out_file(script_file);
	out_file << cmd.str();
	out_file.close();

	return execute_python_command("python " + script_file);
}

bool DataPlotter::plot_mappings(const VelocityController& velocity_controller,
		const SteeringController& steering_controller)
{
	std::stringstream cmd;

	cmd << "import matplotlib.pyplot as plt \n";
	cmd << "import matplotlib.gridspec as gridspec \n";

	cmd << "plt.figure(figsize=(8, 4)) \n";

	cmd
			<< "steering_map_norm = "
					+ list_from_vector(steering_controller.steering_map_norm)
					+ "\n";
	cmd
			<< "steering_map_rad = "
					+ list_from_vector(steering_controller.steering_map_rad)
					+ "\n";

	cmd
			<< "p_stat_map_v = "
					+ list_from_vector(velocity_controller.p_stat_map_v) + "\n";
	cmd
			<< "p_stat_map_p = "
					+ list_from_vector(velocity_controller.p_stat_map_p) + "\n";

	cmd << "plt.subplot2grid((1,2), (0,0), colspan=1, rowspan=1) \n";
	cmd << "plt.plot(steering_map_norm, steering_map_rad, '-o') \n";
	cmd << "plt.xlabel('steering angle (normalized) [-]') \n";
	cmd << "plt.ylabel('steering angle [rad]') \n";
	cmd << "plt.title('steering mapping', fontsize=10) \n";
	cmd << "plt.grid(True) \n";

	cmd << "plt.subplot2grid((1,2), (0,1), colspan=1, rowspan=1) \n";
	cmd << "plt.plot(p_stat_map_v, p_stat_map_p, '-o') \n";
	cmd << "plt.xlabel('velocity [m/s]') \n";
	cmd << "plt.ylabel('p_stat [-]') \n";
	cmd << "plt.title('p_stat mapping', fontsize=10) \n";
	cmd << "plt.grid(True) \n";

	cmd
			<< "plt.subplots_adjust(left=0.1, bottom=None, right=None, top=None, wspace=0.4, hspace=0.5) \n";

	cmd << " \n";
	cmd << " \n";
	cmd << "plt.show() \n";

	return execute_python_command("python -c \"" + cmd.str() + "\"");
}

bool DataPlotter::execute_python_command(const std::string& command_string)
{
	if (waiting_for_join)
	{
		python_thread->join();
		this->python_thread_running = false;
	}

	if (python_thread_running)
	{
		return false; // python thread still running, probably some open plotting windows
	}
	else
	{
		this->python_thread_running = true;
		this->waiting_for_join = false;

		python_thread.reset(new std::thread([command_string, this]()
		{
			this -> exec_shell_command(command_string);
			this->waiting_for_join = true;
		}));
	}
	return true;
}

std::string DataPlotter::exec_shell_command(const std::string &cmd)
{
	std::array<char, 128> buffer;
	std::string result;
	std::shared_ptr<FILE> pipe(popen(cmd.c_str(), "r"), pclose);
	if (!pipe)
		throw std::runtime_error("popen() failed!");
	while (!feof(pipe.get()))
	{
		if (fgets(buffer.data(), 128, pipe.get()) != nullptr)
			result += buffer.data();
	}
	return result;
}
