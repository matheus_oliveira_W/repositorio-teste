#include "PathFollower.h"

void PathFollower::init()
{
	state_ = State::IDLE;
}

const PathFollower::State& PathFollower::state()
{
	return state_;
}

void PathFollower::set_state(const PathFollower::State state)
{
	state_ = state;
}

