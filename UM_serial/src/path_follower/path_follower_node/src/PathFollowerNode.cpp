/** @file PathFollowerNode.cpp
 @brief Main Node for bringing together all path following functionality.
 @author Tobias Brunner
 */

#include <ctime>
#include <iostream>
#include <map>
#include <signal.h>
#include <iostream>
#include <cmath>

// ROS system includes
#include <ros/ros.h>
#include <ros/package.h>
#include <ros/xmlrpc_manager.h>
#include <tf/tf.h>
#include <tf/transform_broadcaster.h>
#include <dynamic_reconfigure/server.h>

// ROS standard messages
#include <sensor_msgs/NavSatFix.h>
#include <sensor_msgs/NavSatStatus.h>
#include <sensor_msgs/Imu.h>
#include <geometry_msgs/TwistWithCovarianceStamped.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Empty.h>
#include <std_msgs/Float32.h>
#include <std_msgs/UInt8.h>
#include <std_msgs/UInt16.h>
#include <std_msgs/Float32MultiArray.h>
#include <std_srvs/SetBool.h>
#include <std_srvs/Empty.h>

// Custom ROS messages
#include "path_follower/PathFollowerService.h"
#include "path_follower/ServiceCommandReply.h"
#include "path_follower/ServiceCommand.h"
#include "path_follower/PathInfoArray.h"
#include "path_follower/StateEvents.h"
#include "path_follower/VehicleState.h"
#include "path_follower/ControlTargetState.h"
#include "path_follower/PathFollowerConfig.h"
#include "path_follower/Excavator.h"
#include "path_follower/SensorState.h"

// Custom classes and helper functions
#include "PathManager.h"
#include "Wgs2UtmConverter.h"
#include "Visualization.h"
#include "SteeringController.h"
#include "VelocityController.h"
#include "States.h"
#include "DataPlotter.h"
#include "Oxts.h"
#include "Uem.h"
#include "PathFollower.h"
#include "CollisionAvoidance.h"

/*****************************
 Global variable declarations
 *****************************/

GlobalParameters params;
PathManager path_manager;
Wgs2UtmConverter utm_converter;
SteeringController steering_controller;
VelocityController velocity_controller;
VehicleDynamicState vehicle_state;
VehicleStaticState vehicle_static_state;
ControlTargetState control_target;
DataPlotter data_plotter;
SensorState sensor_state;

Oxts oxts;
UeM um;
PathFollower follower;
CollisionAvoidance collision;

SteeringController::DynamicParameters steering_params;

std::map<const char*, ros::Publisher> publishers; // map for storing all publishers
std::map<const char*, ros::Subscriber> subscribers; // map for storing all subscribers

ros::NodeHandlePtr node_handle, private_node_handle;
bool bypass_actuators, truck_status_flag, reverse_state = true,vehicle_reached_wait_zone_flag, look_ahead_point_reached_wait_zone_flag,
wait_zone_values_calculated, alert_started, obstacle_values_calculated, back_obstacle_values_calculated,
parameters_change,allow_autonomous = false, bucket_check, horn_front_flag, idle_flag, obstacle_warning, obstacle_emergency;
int following_timer_cycles, active_path_index, wait_parameters_change, wait_drive_handover, count_warning_flag, count_emergency_flag;
double look_ahead_point_deviation, dist_to_obstacle_to_start_braking, dist_to_obstacle_curve, v0_wait_zone, d0_wait_zone, v_obstacle, heading_deviation, path_heading, target_curvature, _path_heading, _target_heading;
PathTools::PathPoint<double> last_path_point;


using ServiceCommand = path_follower::ServiceCommand;
using ServiceReply = path_follower::ServiceCommandReply;

/*****************************
 Node shutdown handling
 *****************************/

sig_atomic_t volatile g_request_shutdown = 0; // Signal-safe flag for whether shutdown is requested

/**
 *  Function to handle shutdown signal
 */
void mySigIntHandler(int sig) // Replacement SIGINT handler
{
	g_request_shutdown = 1;
}

/**
 *  Callback function for shutdown request
 */
// Replacement "shutdown" XMLRPC callback
void shutdownCallback(XmlRpc::XmlRpcValue& params, XmlRpc::XmlRpcValue& result)
{
	int num_params = 0;
	if (params.getType() == XmlRpc::XmlRpcValue::TypeArray)
		num_params = params.size();
	if (num_params > 1)
	{
		std::string reason = params[1];
		ROS_WARN("Shutdown request received. Reason: [%s]", reason.c_str());
		g_request_shutdown = 1; // Set flag
	}
	result = ros::xmlrpc::responseInt(1, "", 0);
}


void collision_avoidance(size_t objects_size, double curve_flag, double lap_deviation, 
						double dist_start_braking, double dist_to_emergency, double dist_in_curve)
{
	count_warning_flag = 0;
	count_emergency_flag = 0;

	ROS_INFO_STREAM("###");

	//curva para direita
	if (curve_flag < -0.01)
	{
		ROS_INFO_STREAM("curva para direita");
		ROS_INFO_STREAM(".........");
		ROS_INFO_STREAM("dist curve: " << dist_to_obstacle_curve);
		ROS_INFO_STREAM("dist brake: " << dist_to_obstacle_to_start_braking);
		ROS_INFO_STREAM("lap deviation: " << lap_deviation);
		ROS_INFO_STREAM(".........");
		for (size_t j = 0; j < objects_size; ++j)
		{
					
			/* Se o obstáculo estiver entre o angulo da rota e zero,
			 * compara com a distancia gerada para começar a freiar.
			 * Se for menor que a distancia pra começar a freiar e 
			 * a de emergencia, aciona a emergência.
			 */
			if ((-collision.sensor_fused_ang[j] <= lap_deviation + 1.0)
				&& -collision.sensor_fused_ang[j] > 0) 
			{	
				if (collision.sensor_fused_h[j] < dist_start_braking) 
				{
					ROS_ERROR_STREAM("SENSOR H: " << collision.sensor_fused_h[j]);
					ROS_ERROR_STREAM("SENSOR X-----------: " << collision.sensor_fused_x[j]);
					ROS_ERROR_STREAM("SENSOR Y: " << collision.sensor_fused_y[j]);
					ROS_ERROR_STREAM("SENSOR ANG: " << collision.sensor_fused_ang[j]);
					obstacle_warning = true;
					++count_warning_flag;
				}
				if (collision.sensor_fused_h[j] < dist_to_emergency) 
				{
					ROS_ERROR_STREAM("SENSOR H: " << collision.sensor_fused_h[j]);
					ROS_ERROR_STREAM("SENSOR X-----------: " << collision.sensor_fused_x[j]);
					ROS_ERROR_STREAM("SENSOR Y: " << collision.sensor_fused_y[j]);
					ROS_ERROR_STREAM("SENSOR ANG: " << collision.sensor_fused_ang[j]);
					obstacle_emergency = true;
					++count_emergency_flag;
				}
			}

			/* Se o obstáculo estiver entre o angulo da rota e 80,
			 * compara com a distancia de emergencia.
			 * Se for menor, já freia total.
			*/
			if ((-collision.sensor_fused_y[j] <= (vehicle_static_state.width + 1.0) / 2)
				&& -collision.sensor_fused_ang[j] > lap_deviation + 1.0) 
			{	
				if (collision.sensor_fused_h[j] < dist_in_curve) 
				{
					ROS_ERROR_STREAM("SENSOR H: " << collision.sensor_fused_h[j]);
					ROS_ERROR_STREAM("SENSOR X----------: " << collision.sensor_fused_x[j]);
					ROS_ERROR_STREAM("SENSOR Y: " << collision.sensor_fused_y[j]);
					ROS_ERROR_STREAM("SENSOR ANG: " << collision.sensor_fused_ang[j]);
					obstacle_warning = true;
					++count_warning_flag;

					obstacle_emergency = true;
					++count_emergency_flag;
				}
			}

			/* Se o obstáculo estiver entre o meio do caminhao e o
			 * lado esquerdo, compara com a distancia de curva,
			 * que é praticamente metade da distancia para começar 
			 * a freiar. Se for menor, já freia total.
			 */
			if ((collision.sensor_fused_y[j] > 0) && 
				(collision.sensor_fused_y[j] <= (vehicle_static_state.width + 1.0) / 2))
			{
				if (collision.sensor_fused_h[j] < dist_in_curve) 
				{
					ROS_ERROR_STREAM("SENSOR H: " << collision.sensor_fused_h[j]);
					ROS_ERROR_STREAM("SENSOR X-----------: " << collision.sensor_fused_x[j]);
					ROS_ERROR_STREAM("SENSOR Y: " << collision.sensor_fused_y[j]);
					ROS_ERROR_STREAM("SENSOR ANG: " << collision.sensor_fused_ang[j]);
					obstacle_warning = true;
					++count_warning_flag;

					obstacle_emergency = true;
					++count_emergency_flag;
				}	
			}
			if (count_warning_flag > 0) obstacle_warning = true;
			else obstacle_warning = false;

			if (count_emergency_flag > 0) obstacle_emergency = true;
			else obstacle_emergency = false;
		}
	}
	//curva para esquerda
	if (curve_flag > 0.01)
	{
		ROS_INFO_STREAM("curva para esquerda");
		ROS_INFO_STREAM(".........");
		ROS_INFO_STREAM("dist curve: " << dist_to_obstacle_curve);
		ROS_INFO_STREAM("dist brake: " << dist_to_obstacle_to_start_braking);
		ROS_INFO_STREAM("lap deviation: " << lap_deviation);
		ROS_INFO_STREAM(".........");
		for (size_t j = 0; j < objects_size; ++j)
		{

			if ((collision.sensor_fused_ang[j] <= lap_deviation + 1.0)
				&& collision.sensor_fused_ang[j] > 0)
			{
				if (collision.sensor_fused_h[j] < dist_start_braking) 
				{
					ROS_ERROR_STREAM("SENSOR H: " << collision.sensor_fused_h[j]);
					ROS_ERROR_STREAM("SENSOR X-----------: " << collision.sensor_fused_x[j]);
					ROS_ERROR_STREAM("SENSOR Y: " << collision.sensor_fused_y[j]);
					ROS_ERROR_STREAM("SENSOR ANG: " << collision.sensor_fused_ang[j]);
					obstacle_warning = true;
					++count_warning_flag;
				}
				if (collision.sensor_fused_h[j] < dist_in_curve) 
				{
					ROS_ERROR_STREAM("SENSOR H: " << collision.sensor_fused_h[j]);
					ROS_ERROR_STREAM("SENSOR X-----------: " << collision.sensor_fused_x[j]);
					ROS_ERROR_STREAM("SENSOR Y: " << collision.sensor_fused_y[j]);
					ROS_ERROR_STREAM("SENSOR ANG: " << collision.sensor_fused_ang[j]);
					obstacle_emergency = true;
					++count_emergency_flag;
				}
			}
			if ((collision.sensor_fused_y[j] <= (vehicle_static_state.width + 1.0) / 2)
				&& collision.sensor_fused_ang[j] > lap_deviation + 1.0) 
			{	
				if (collision.sensor_fused_h[j] < dist_in_curve) 
				{
					ROS_ERROR_STREAM("SENSOR H: " << collision.sensor_fused_h[j]);
					ROS_ERROR_STREAM("SENSOR X-----------: " << collision.sensor_fused_x[j]);
					ROS_ERROR_STREAM("SENSOR Y: " << collision.sensor_fused_y[j]);
					ROS_ERROR_STREAM("SENSOR ANG: " << collision.sensor_fused_ang[j]);
					obstacle_warning = true;
					++count_warning_flag;

					obstacle_emergency = true;
					++count_emergency_flag;
				}
			}
			if ((-collision.sensor_fused_y[j] > 0) && 
				(-collision.sensor_fused_y[j] <= (vehicle_static_state.width + 1.0) / 2))
			{
				if (collision.sensor_fused_h[j] < dist_in_curve) 
				{
					ROS_ERROR_STREAM("SENSOR H: " << collision.sensor_fused_h[j]);
					ROS_ERROR_STREAM("SENSOR X-----------: " << collision.sensor_fused_x[j]);
					ROS_ERROR_STREAM("SENSOR Y: " << collision.sensor_fused_y[j]);
					ROS_ERROR_STREAM("SENSOR ANG: " << collision.sensor_fused_ang[j]);
					obstacle_warning = true;
					++count_warning_flag;
					
					obstacle_emergency = true;
					++count_emergency_flag;
				}	
			}

			if (count_warning_flag > 0) obstacle_warning = true;
			else obstacle_warning = false;

			if (count_emergency_flag > 0) obstacle_emergency = true;
			else obstacle_emergency = false;
		}
	}
	//reta
	if (curve_flag > -0.01 && curve_flag < 0.01)
	{
		ROS_INFO_STREAM("reta");
		ROS_INFO_STREAM(".........");
		ROS_INFO_STREAM("dist curve: " << dist_to_obstacle_curve);
		ROS_INFO_STREAM("dist brake: " << dist_to_obstacle_to_start_braking);
		
		for (size_t j = 0; j < objects_size; ++j)
		{
						
			if ((collision.sensor_fused_y[j] >= (-vehicle_static_state.width - 1.0) / 2) &&
				(collision.sensor_fused_y[j] <= (vehicle_static_state.width + 1.0) / 2))
				{
					if (collision.sensor_fused_h[j] <= dist_start_braking)
					{
						ROS_ERROR_STREAM("SENSOR H: " << collision.sensor_fused_h[j]);
						ROS_ERROR_STREAM("SENSOR X-----------: " << collision.sensor_fused_x[j]);
						ROS_ERROR_STREAM("SENSOR Y: " << collision.sensor_fused_y[j]);
						ROS_ERROR_STREAM("SENSOR ANG: " << collision.sensor_fused_ang[j]);
						obstacle_warning = true;
						++count_warning_flag;

						if (collision.sensor_fused_h[j] <= dist_to_emergency) 
						{
							ROS_ERROR_STREAM("SENSOR H: " << collision.sensor_fused_h[j]);
							ROS_ERROR_STREAM("SENSOR X-----------: " << collision.sensor_fused_x[j]);
							ROS_ERROR_STREAM("SENSOR Y: " << collision.sensor_fused_y[j]);
							ROS_ERROR_STREAM("SENSOR ANG: " << collision.sensor_fused_ang[j]);
							obstacle_emergency = true;
							++count_emergency_flag;
						}
					}
					else
					{
						if (count_warning_flag > 0) obstacle_warning = true;
						else obstacle_warning = false;

						if (count_emergency_flag > 0) obstacle_emergency = true;
						else obstacle_emergency = false;
					}	 
				}
		}
	}
	
}

/**
 Check if the vehicle is currently inside the wait zone of the active path

 @return true, if the vehicle is currently inside the wait zone of the active path
 */
bool inside_wait_zone(double px, double py)
{
	if (path_manager.active_path().has_points())
	{
		double dx = px - path_manager.active_path().x().back();
		double dy = py - path_manager.active_path().y().back();

		double dist_to_end_of_path = std::sqrt(dx * dx + dy * dy);

		if ((path_manager.active_path().reverse == 1) && (dist_to_end_of_path < 1))
		{
			return true;
		}

		if ((path_manager.active_path().reverse == 0) && (dist_to_end_of_path < path_manager.active_path().wait_zone_radius))
		{
			return true;
		}
	}

	return false;
}

/**
 Check if the vehicle has reached the wait zone of the active path

 @return true, if the vehicle has reached the wait zone (also returns true, if the vehicle has
 entered and again left the wait zone since path following was started)
 */
bool vehicle_reached_wait_zone()
{
	if (inside_wait_zone(vehicle_state.x, vehicle_state.y) || vehicle_reached_wait_zone_flag)
	{
		vehicle_reached_wait_zone_flag = true;
		return true;
	}
	else
	{
		return false;
	}
}

/**
 Check if the look ahead point has reached the wait zone of the active path

 @return true, if the look ahead point has reached the wait zone
 */
bool look_ahead_point_reached_wait_zone()
{
	if (inside_wait_zone(control_target.look_ahead_point_x,	control_target.look_ahead_point_y)
			|| look_ahead_point_reached_wait_zone_flag)
	{
		look_ahead_point_reached_wait_zone_flag = true;
		return true;
	}
	else
	{
		return false;
	}
}


/**
 Check if the vehicle is allowed to start following a path based on a set of rules.

 @return true, if the vehicle is allowed to start following a path.

 The following rules apply:
 - if the vehicle is currently inside the go-zone of path p, following is allowed and p is set as active path
 - if the go-zones of multiple paths are overlapping, the path with the lowest id is set as active path
 - if the vehicle is currently not inside any go-zone and the parameter "following_from_outside_go_zone" is set to false,
 following is not allowed
 - if the vehicle is currently not inside any go-zone and the parameter "following_from_outside_go_zone" is set to true,
 following is allowed and the path closest to the current vehicle position is set as active path
 */
bool following_allowed()
{
	int n_path = 0;
	for (auto path : path_manager.paths())
	{
		if (path.has_points())
		{
			
			// calculate distance from beginning of the path to current vehicle position
			double dx = vehicle_state.x - path.x().front();
			double dy = vehicle_state.y - path.y().front();

			double dist_to_path_start = std::sqrt(dx * dx + dy * dy);

			if (dist_to_path_start < path.go_zone_radius)
			{
				path_manager.set_active_path_index(n_path);
				return true;
			}
		}
		++n_path;
	}

	if (params.following_from_outside_go_zone)
	{
		n_path = 0;
		int n_closest_path = 0;
		double dist_min = 999999.0;
		bool valid_path_available = false;
		PathTools::Point<double> p(vehicle_state.x, vehicle_state.y);

		// iterate over all paths and find the path with lowest distance to current vehicle position
		for (auto path : path_manager.paths())
		{
			if (path.has_points())
			{
				valid_path_available = true;

				auto closest_path_point = path.closest_point(p, false);
				auto closest_point = path.coordinates(closest_path_point);

				double dist = HelperFunctions::distance(vehicle_state.x,
						vehicle_state.y, closest_point.x, closest_point.y);

				if (dist < dist_min)
				{
					dist_min = dist;
					n_closest_path = n_path;
				}
			}
			++n_path;
		}

		path_manager.set_active_path_index(n_closest_path);

		if (valid_path_available)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	return false;
}

/**
 Publish all paths as visualization_msgs::MarkerArray
 */
void publish_path_marker()
{
	publishers["path_marker_array"].publish(
			Visualization::get_path_marker_array(path_manager.paths()));
}

/**
 Publish all go-zones and wait-zones as visualization_msgs::MarkerArray
 */
void publish_zone_marker()
{
	publishers["zone_marker_array"].publish(
			Visualization::get_zone_marker_array(path_manager.paths(),
					params.display_go_zone, params.display_wait_zone));
}

/**
 Publish the planned polynomial trajectory as visualization_msgs::Marker

 @param[in] trajectory_x vector of x-coordinates of the polynomial trajectory
 @param[in] trajectory_y vector of y-coordinates of the polynomial trajectory
 @param[out] trajectory_target_x x coordinate of point on planned trajectory used for steering angle calculation
 @param[out] trajectory_target_y y coordinate of point on planned trajectory used for steering angle calculation
 */
void publish_trajectory_marker(const std::vector<double>& trajectory_x,
		const std::vector<double>& trajectory_y,
		const double trajectory_target_x, const double trajectory_target_y)
{
	publishers["trajectory_marker_array"].publish(
			Visualization::get_trajectory_marker(trajectory_x, trajectory_y,
					trajectory_target_x, trajectory_target_y,
					path_manager.reference_path()));
}


/**
 Publish the vehicle position and heading (visualized as rectangle) as visualization_msgs::MarkerArray
 */
void publish_vehicle_marker()
{
	if (!(oxts.state() == Oxts::State::FIX))
	{
		publishers["vehicle_marker"].publish(
				Visualization::get_vehicle_marker(0, 0, vehicle_state.yaw_angle,
						vehicle_static_state, control_target.steering_angle));
	}
	else
	{
		publishers["vehicle_marker"].publish(
				Visualization::get_vehicle_marker(
						vehicle_state.x - path_manager.x_pos_ref(),
						vehicle_state.y - path_manager.y_pos_ref(),
						vehicle_state.yaw_angle, vehicle_static_state,
						control_target.steering_angle));
	}
}

/**
 Publish the transformation between vehicle position and global map position as tf::StampedTransform
 */
void publish_vehicle_transform()
{
	double x = 0, y = 0;

	if (oxts.state() == Oxts::State::FIX)
	{
		x = vehicle_state.x - path_manager.x_pos_ref();
		y = vehicle_state.y - path_manager.y_pos_ref();
	}

	static tf::TransformBroadcaster br;
	tf::Transform transform;
	transform.setOrigin(tf::Vector3(x, y, 0.0));
	tf::Quaternion q;
	q.setRPY(0, 0, 0);
	transform.setRotation(q);
	br.sendTransform(
			tf::StampedTransform(transform, ros::Time::now(), "map",
					"vehicle"));
}

/**
 Publish arrows indicating the desired and actual steering angle of the vehicle as visualization_msgs::Marker
 */
void publish_steering_desired_marker()
{
	publishers["steering_marker"].publish(
			Visualization::get_steering_desired_marker(
					vehicle_state.x - path_manager.x_pos_ref(),
					vehicle_state.y - path_manager.y_pos_ref(),
					vehicle_state.yaw_angle, control_target.steering_angle,
					vehicle_static_state));
}

/**
 Publish circles (spheres) to indicate the projection of the vehicle onto the path and the look-ahead point
 */
void publish_control_points_marker()
{
	publishers["control_points_marker"].publish(
			Visualization::get_control_point_marker(
					control_target.closest_point_x - path_manager.x_pos_ref(),
					control_target.closest_point_y - path_manager.y_pos_ref(),
					control_target.look_ahead_point_x
							- path_manager.x_pos_ref(),
					control_target.look_ahead_point_y
							- path_manager.y_pos_ref()));
}


/**
 Publish information about all available paths
 */
void publish_path_info()
{
	path_follower::PathInfoArray path_info_array;

	for (auto path : path_manager.paths())
	{
		path_follower::PathInfo path_info;

		path_info.num_points = path.size();
		path_info.name = path.name();
		path_info.length = path.length();
		path_info.utm_zone_string = utm_converter.get_zone_string();
		path_info.recording_time.sec = path.rec_sec;
		path_info.recording_time.nsec = path.rec_nsec;
		path_info.go_zone_radius = path.go_zone_radius;
		path_info.wait_zone_radius = path.wait_zone_radius;

		GpsPath::VisualizationColor path_color = path.color();

		path_info.visualization_color.a = path_color.a;
		path_info.visualization_color.r = path_color.r;
		path_info.visualization_color.g = path_color.g;
		path_info.visualization_color.b = path_color.b;

		path_info_array.info.push_back(path_info);
	}

	publishers["path_info_array"].publish(path_info_array);
}

/**
 Publish the ID (index) of the currently active path
 */
void publish_active_path_id()
{
	std_msgs::UInt8 msg;
	msg.data = path_manager.active_path_id();

	active_path_index = msg.data;

	publishers["active_path_id"].publish(msg);
}

/**
 Publish state of the path follower node (state constants are defined in the message path_follower::StateEvents)
 */
void publish_state_event(const unsigned int state_event)
{
	std_msgs::UInt8 msg;
	msg.data = state_event;
	publishers["state_event"].publish(msg);
}

/**
 Publish an empty message to indicate that the path follower node is still alive
 */
void publish_node_alive_signal()
{
	publishers["node_alive"].publish(std_msgs::Empty());
}

/**
 Publish the current dynamic state (see ::VehicleDynamicState) of the vehicle
 */
void publish_vehicle_state()
{
	path_follower::VehicleState veh_state_msg;

	if ((oxts.state() == Oxts::State::FIX)
			|| (oxts.state() == Oxts::State::DATA_BUT_NO_FIX))
	{
		veh_state_msg.x = vehicle_state.x;
		veh_state_msg.y = vehicle_state.y;
		veh_state_msg.velocity = vehicle_state.velocity;
		veh_state_msg.yaw_angle = vehicle_state.yaw_angle;
		veh_state_msg.gps_data_valid = true;
		veh_state_msg.heading = heading_deviation;
	}

	if (path_manager.reference_path().has_points())
	{
		veh_state_msg.x_rel = vehicle_state.x - path_manager.x_pos_ref();
		veh_state_msg.y_rel = vehicle_state.y - path_manager.y_pos_ref();
		veh_state_msg.rel_data_valid = true;
	}

	if (um.state() == UeM::State::ONLINE
			|| um.state() == UeM::State::NO_REMOTE_CONTROL)
	{
		veh_state_msg.steering_angle = vehicle_state.steering_angle;
		veh_state_msg.steering_data_valid = true;
	}

	publishers["vehicle_state"].publish(veh_state_msg);
}

/**
 Publish the current control target (see ::ControlTargetState) of the vehicle,
 e.g. desired steering angle and desired gas/brake position
 */
void publish_control_target_state()
{
	path_follower::ControlTargetState target_state_msg;
	target_state_msg.closest_point_x = control_target.closest_point_x;
	target_state_msg.closest_point_y = control_target.closest_point_y;
	target_state_msg.look_ahead_point_x = control_target.look_ahead_point_x;
	target_state_msg.look_ahead_point_y = control_target.look_ahead_point_y;
	target_state_msg.look_ahead_distance = control_target.look_ahead_distance;
	target_state_msg.velocity = control_target.velocity;
	target_state_msg.steering_angle = control_target.steering_angle;
	target_state_msg.steering_angle_norm = control_target.steering_angle_norm;
	target_state_msg.gas_brake_norm = control_target.gas_brake_norm;

	publishers["control_target_state"].publish(target_state_msg);
}

void publish_sensor_state()
{
	path_follower::SensorState sensor_state_msg;

	sensor_state_msg.left_lidar_state = collision.left_lidar_x.empty();
	sensor_state_msg.right_lidar_state = collision.right_lidar_x.empty();
	sensor_state_msg.front_radar_state = collision.front_radar_x.empty();

	sensor_state_msg.warning_flag = obstacle_warning;
	sensor_state_msg.emergency_flag = obstacle_emergency;

	sensor_state_msg.dist_x_0 = collision.obj0_x;
	sensor_state_msg.dist_y_0 = collision.obj0_y;
	sensor_state_msg.ang_0 = collision.ang0;

	sensor_state_msg.dist_x_1 = collision.obj1_x;
	sensor_state_msg.dist_y_1 = collision.obj1_y;
	sensor_state_msg.ang_1 = collision.ang1;

	sensor_state_msg.dist_brake = dist_to_obstacle_to_start_braking;
	sensor_state_msg.dist_curve = dist_to_obstacle_curve;
	sensor_state_msg.lap_deviation = look_ahead_point_deviation;

	sensor_state_msg.follow_with_sensors = sensor_state.follow_with_sensor;
	

	publishers["sensor_state"].publish(sensor_state_msg);
}

/**
 Publish desired steering/gas/brake positions to paravan node
 */
void publish_actuators_commands(const SteeringCommand& steering_command, const GasBrakeCommand& gas_brake_command)
{
	std_msgs::Float32 steering_msg, gas_brake_msg;
	std_msgs::UInt16 blinker_msg;

	std::stringstream log_string;

	/* Desired steering value */
	switch (steering_command.control_type())
	{
	case SteeringCommand::Control:
	{
		if (params.keep_steering_neutral)
		{
			node_handle->setParam("/paravan_node/keep_steering_neutral", false);
			log_string << "set 'keep_steering_neutral' to false; ";
		}

		steering_msg.data = steering_command.value();
		log_string << "SteeringCommand::Control, value = " << steering_command.value() << "; ";
		break;
	}
	case SteeringCommand::KeepNeutral:
	{
		if (!params.keep_steering_neutral)
		{
			node_handle->setParam("/paravan_node/keep_steering_neutral", true);
			log_string << "set 'keep_steering_neutral' to true; ";
		}

		steering_msg.data = vehicle_state.steering_angle_norm;
		log_string << "SteeringCommand::KeepNeutral; ";
		break;
	}
	}

	/* Desired gas/brake value */
	if (params.control_gas_brake)
	{
		switch (gas_brake_command.control_type())
		{
		case GasBrakeCommand::Control:
		{
			gas_brake_msg.data = gas_brake_command.value();
			log_string << "GasBrakeCommand::Control, value = " << gas_brake_command.value();
			break;
		}
		case GasBrakeCommand::KeepNeutral:
		{
			gas_brake_msg.data = 0.0;
			log_string << "GasBrakeCommand::KeepNeutral";
			break;
		}
		}
	}
	else
	{
		gas_brake_msg.data = 0;
		log_string << "GasBrakeCommand::KeepNeutral (control_gas_brake = false)";
	}

	ROS_DEBUG_STREAM(log_string.str());

	if (steering_msg.data < -0.218)
	{
		blinker_msg.data = 1; //left
	}
	else
	{
		if(steering_msg.data > 0.183)
		{
			blinker_msg.data = 2; //right
		}
		else
		{
			blinker_msg.data = 0; //center
		}
		
	}

	if(truck_status_flag == true)
	{
		//publishers["blinker"].publish(blinker_msg);
		publishers["steering"].publish(steering_msg);
		publishers["gas_brake"].publish(gas_brake_msg);
	}
	else return;
	
}

/**
 Publish suitable value to start/stop alert signal
 */
void publish_alert(bool alert)
{
	std_msgs::Bool msg;
	msg.data = alert;
	publishers["alert"].publish(msg);
}

/**
 Start driver alert (Take Over Request)
 */
void alert()
{
	if (!alert_started)
	{
		alert_started = true;
		publish_alert(true);
	}
}

/**
 Stop driver alert
 */
void stop_alert()
{
	alert_started = false;
	publish_alert(false);
}

/**
 Reset velocity controller and control-logic variables
 */
void prepare_following()
{
	velocity_controller.reset(vehicle_state.gas_brake_pos);

	steering_params.look_ahead_dist_max = 12.0;
	steering_params.look_ahead_dist_min = 8.0;
	steering_params.look_ahead_dist_factor = 1.0;
	steering_params.eta_1 = 10.0;
	steering_params.eta_2 = 10.0;
	steering_params.eta_3 = 30.0;
	steering_params.eta_4 = 30.0;
	steering_params.dt = 0.5;
	steering_params.poly_dist_min = 1.0;

	steering_controller.update_parameters(steering_params);

	vehicle_reached_wait_zone_flag = false;
	look_ahead_point_reached_wait_zone_flag = false;
	wait_zone_values_calculated = false;
	following_timer_cycles = 0;
	wait_parameters_change = 0;
	parameters_change = false;
	bucket_check = false;
	horn_front_flag = false;
	idle_flag = false;

	last_path_point = path_manager.active_path().closest_point({vehicle_state.x, vehicle_state.y});

	ros::ServiceClient client_emergency = node_handle->serviceClient<std_srvs::Empty>("/uem/emergency_off");
				    std_srvs::Empty srv_emergency;
				    client_emergency.call(srv_emergency);
}


/**
 Update the dynamic vehicle state (see ::VehicleDynamicState) based on data from OxTS and Paravan system.

 Position, yaw angle and velocity are updated from OxTS data.
 If no oxts data is available, values are set to 0.

 Steering angle and gas/brake position are updated from Paravan data.
 */
void update_vehicle_dynamic_state()
{
	if (um.state() == UeM::State::ONLINE
			|| um.state() == UeM::State::NO_REMOTE_CONTROL)
	{
		//vehicle_state.steering_angle_norm = um.data().steering.data;
		vehicle_state.steering_angle = steering_controller.norm2rad(
				vehicle_state.steering_angle_norm);
		//vehicle_state.gas_brake_pos = um.data().gas_brake.data;
	}
	else
	{
		vehicle_state.steering_angle_norm = 0;
		vehicle_state.steering_angle = 0;
		vehicle_state.gas_brake_pos = 0;
	}

	if (oxts.state() == Oxts::State::FIX)
	{
		double utm_x, utm_y;

		// convert current gps point to utm point
		utm_converter.convert(oxts.data().nav_sat_fix.latitude,
				oxts.data().nav_sat_fix.longitude, utm_x, utm_y);

		vehicle_state.x = utm_x;
		vehicle_state.y = utm_y;
		vehicle_state.alt = oxts.data().nav_sat_fix.altitude;

		double vx = oxts.data().twist.twist.twist.linear.x;
		double vy = oxts.data().twist.twist.twist.linear.y;

		vehicle_state.velocity = std::sqrt(vx * vx + vy * vy);

		tf::Quaternion q(oxts.data().imu.orientation.x,
				oxts.data().imu.orientation.y, oxts.data().imu.orientation.z,
				oxts.data().imu.orientation.w);

		tf::Matrix3x3 m(q);

		double roll, pitch, yaw;
		m.getRPY(roll, pitch, yaw);

		yaw += 0.5 * M_PI; // add 90° (correction for different coordinate system definitions)

		vehicle_state.yaw_angle = yaw;
	}
	else
	{
		vehicle_state.x = 0;
		vehicle_state.y = 0;
		vehicle_state.alt = 0;
		vehicle_state.velocity = 0;
		vehicle_state.yaw_angle = 0;
	}

	ROS_DEBUG_STREAM("vehicle_state: "
			<< "x: " << vehicle_state.x << ", "
			<< "y: " << vehicle_state.y << ", "
			<< "vel: " << vehicle_state.velocity << ", "
			<< "yaw: " << vehicle_state.yaw_angle << ", "
			<< "steer_ang: " << vehicle_state.steering_angle << ", "
			<< "steer_ang_norm: " << vehicle_state.steering_angle_norm << ", "
			<< "gas_brake_pos: " << vehicle_state.gas_brake_pos);
}

/**
 Check if a driver handover is necessary.

 @return true, if vehicle exceeds distance or heading thresholds and driver needs to take over
 */
bool driver_handover_necessary()
{

	/* Check if vehicle distance from path exceeds a certain threshold. */
	auto closest_path_point = last_path_point;
	if (closest_path_point.u > 1.0) closest_path_point.u = 1.0; // 'removes' potential extension after path end

	auto closest_point = path_manager.active_path().coordinates(closest_path_point);
	double dist_from_path = PathTools::dist_2d({vehicle_state.x, vehicle_state.y}, closest_point);

	/* Check if the difference between the vehicle heading and path heading exceeds a certain threshold */
	path_heading = path_manager.active_path().heading(closest_path_point);
	heading_deviation = acos(cos(path_heading - vehicle_state.yaw_angle));
	double heading = path_heading - vehicle_state.yaw_angle;

	if (dist_from_path > params.max_dist_from_path)
	{
		ROS_ERROR_STREAM("Driver handover necessary - too far from path! (" << dist_from_path << " m)");
		return true;
	}
	if (heading_deviation > params.max_heading_deviation)
	{
		ROS_ERROR_STREAM("Driver handover necessary - heading deviation too high! (" << heading_deviation << " rad)");
		return true;
	}
	if (vehicle_state.velocity > params.max_velocity)
	{
		ROS_ERROR_STREAM("Driver handover necessary - velocity too high! (" << vehicle_state.velocity << " m/s)");
		return true;
	}
	if (um.data().data_signal == 0 && !params.following_without_paravan)
	{
		
		/** AQUI BYPASSA A PLACA. Referencia: https://www.embarcados.com.br/usando-gpio-e-pwm-no-linux-embarcado/
		https://www.emcraft.com/stm32f429discovery/controlling-gpio-from-linux-user-space
		cd /usr/src/linux-headers...
		sudo apt-get install libncurses5-dev libncursesw5-dev

		FILE *fun, *fex, *fd, *fv;        
        fun = fopen("/sys/class/gpio/unexport","w");
        fputs("8",fun);                              
        fclose(fun);   
   		fex = fopen("/sys/class/gpio/export","w");
        fputs("8",fex);                            
        fclose(fex);                               
                       
        //GPIO como saida                                           
        fd = fopen("/sys/class/gpio/gpio8/direction", "w");
        fputs("out",fd);
        fclose(fd);           
                   
        //GPIO nível alto                        
		fv = fopen("/sys/class/gpio/gpio8/value", "w");
		fputs("1", fv);                                
		fclose(fv); */

		ROS_ERROR_STREAM("Driver handover necessary - lost communication with actuators!");
		return true;
	}
	
	/*if(sensor_state.follow_with_sensor == true)
	{
		if((collision.left_lidar_x.empty() || collision.right_lidar_x.empty() || collision.front_radar_x.empty()))
		{
			ROS_ERROR_STREAM("Driver handover necessary - sensors offline!	");
			return true;
		}
	}*/
	

	return false;
}

/**
 Update the control target (see ::ControlTargetState) of the vehicle (desired steering/gas/brake values)
 */
void update_vehicle_control_target()
{
	/** On the currently active path, find the point which is closest to the current vehicle position.
	 * This search is restricted to a search window around the last determined point. This is done in
	 * order to be able to follow paths that cross through themselves. */

	// find start and end indices of the path points corresponding to the search window size
	int ind_start = 0;
	int ind_end = path_manager.active_path().size() - 1;

	double search_window = 20.0; // constant window size in [m]
	double s_last = path_manager.active_path().s(last_path_point);

	double s_start = s_last - search_window;
	double s_end = s_last + search_window;


	for (int k = 0; k < path_manager.active_path().size(); ++k)
	{
		if (path_manager.active_path().s()[k] < s_start)
		{
			ind_start = k;
		}
		if (path_manager.active_path().s()[k] > s_end)
		{
			ind_end = k;
			break;
		}
	}

	// determine closest path point to current vehicle position
	auto closest_path_point = path_manager.active_path().closest_point(
			{vehicle_state.x, vehicle_state.y}, true, ind_start, ind_end);

	// store path point for next iteration
	last_path_point = closest_path_point;

	// calculate x/y coordinates of path point
	auto closest_point = path_manager.active_path().coordinates(closest_path_point);

	auto closest_altitude = path_manager.active_path().altitude(closest_path_point);

	/** Determine the look-ahead distance, i.e. how far the controller should look ahead along the path */
	control_target.look_ahead_distance = steering_controller.get_look_ahead_distance(vehicle_state.velocity);

	// augment look ahead distance by distance from path
	control_target.look_ahead_distance += PathTools::dist_2d({vehicle_state.x, vehicle_state.y}, closest_point);

	/** Calculate the look-ahead point by walking along the path, starting at the closest path point*/
	auto look_ahead_path_point = path_manager.active_path().move_along(
			closest_path_point, control_target.look_ahead_distance, true);
	

	auto look_ahead_point = path_manager.active_path().coordinates(look_ahead_path_point);

	
	auto look_ahead_altitude = path_manager.active_path().altitude(look_ahead_path_point);


	ROS_INFO_STREAM("ALT DIF: " << (look_ahead_altitude - closest_altitude));
	
	
	/** #### Steering control: #### */

	/** Calculate curvature and orientation of the path at the look-ahead point, then determine
	 the desired steering angle */
	double target_heading = path_manager.active_path().heading(look_ahead_path_point);
	target_curvature = path_manager.active_path().curvature(look_ahead_path_point);
	look_ahead_point_deviation = acos(cos(target_heading - path_heading)) * 180/3.1416;
	
	std::vector<double> trajectory_x, trajectory_y; // planned trajectory/path of the vehicle; for visualization
	double trajectory_target_x, trajectory_target_y; // target point on trajectory

	control_target.steering_angle =
			steering_controller.get_flatness_steering_angle(vehicle_state,
					look_ahead_point.x, look_ahead_point.y, target_heading,
					target_curvature, trajectory_x, trajectory_y,
					trajectory_target_x, trajectory_target_y);

	publish_trajectory_marker(trajectory_x, trajectory_y, trajectory_target_x, trajectory_target_y);

	/** Convert the steering angle to its normalized value (see ::SteeringController::rad2norm) and update
	 * the vehicle control target (see ::ControlTargetState) */
	control_target.steering_angle_norm = steering_controller.rad2norm(control_target.steering_angle);

	control_target.closest_point_x = closest_point.x;
	control_target.closest_point_y = closest_point.y;

	control_target.look_ahead_point_x = look_ahead_point.x;
	control_target.look_ahead_point_y = look_ahead_point.y;

	/* #### Sensors detection: #### */

	if(sensor_state.follow_with_sensor == true)
	{
		int n_obj = collision.sensor_fused_x.size();

		dist_to_obstacle_to_start_braking = collision.get_sensor_range_distance(control_target.velocity, 
													sensor_state.obstacle_range_base, sensor_state.obstacle_range_factor);
		
		dist_to_obstacle_curve = collision.get_sensor_range_curve_distance(control_target.velocity, 
										sensor_state.obstacle_range_base, sensor_state.obstacle_curve_range_factor, 
										target_curvature);

		collision_avoidance(n_obj, target_curvature, look_ahead_point_deviation, dist_to_obstacle_to_start_braking,
							sensor_state.obstacle_range_min, dist_to_obstacle_curve);
	}
		
	/** #### Velocity control: #### */

	/** Determine the target velocity based on the recorded velocity, the path's curvature and the
	 * position of the vehicle relative to the end of the path */

	double recorded_velocity = path_manager.active_path().velocity(look_ahead_path_point);
	double path_curvature = path_manager.active_path().curvature(look_ahead_path_point);

	/** If the look ahead point has reached into the wait zone, start slowing down the vehicle
	 * in proportion to its distance from the end of the path */
	if (look_ahead_point_reached_wait_zone())
	{
			double dist_to_path_end = HelperFunctions::distance(closest_point.x, closest_point.y,
						path_manager.active_path().x().back(), path_manager.active_path().y().back());

			if (!wait_zone_values_calculated)
			{
				ROS_INFO_STREAM("WAIT ZONE VALUES CALCULATED");

				wait_zone_values_calculated = true;

				// target velocity when look ahead point enters wait zone
				v0_wait_zone = velocity_controller.get_target_velocity(recorded_velocity, path_curvature);

				// distance from closest path point to end of path when look ahead point enters wait zone
				d0_wait_zone = dist_to_path_end;
			}

			if (obstacle_warning) 
			{			
				if(!obstacle_values_calculated) 
				{
					v_obstacle = velocity_controller.get_target_velocity(recorded_velocity, path_curvature);
					control_target.gas_brake_norm = velocity_controller.get_obstacle_velocity(v_obstacle, obstacle_emergency);
					obstacle_values_calculated = true;
					ROS_INFO_STREAM("WAIT ZONE - FRONT CALCULATED");
				}
				control_target.gas_brake_norm = velocity_controller.get_obstacle_velocity(control_target.gas_brake_norm, obstacle_emergency);
				ROS_INFO_STREAM("WAIT ZONE - FRONT OBSTACLE");
			}
			else
			{
				obstacle_values_calculated = false;
				back_obstacle_values_calculated = false;
				control_target.velocity = v0_wait_zone * (dist_to_path_end / d0_wait_zone);
				control_target.gas_brake_norm = velocity_controller.get_gas_brake_value(vehicle_state.velocity, control_target.velocity);
				ROS_INFO_STREAM("WAIT ZONE - NO OBSTACLE");
			}
	}
	else 
	{
		if (obstacle_warning) 
		{		
			if(!obstacle_values_calculated)
			{
				v_obstacle = velocity_controller.get_target_velocity(recorded_velocity, path_curvature);
				control_target.gas_brake_norm = velocity_controller.get_obstacle_velocity(control_target.gas_brake_norm, obstacle_emergency);
				obstacle_values_calculated = true;
				ROS_INFO_STREAM("OBS CALCULATED");
			}
			control_target.gas_brake_norm = velocity_controller.get_obstacle_velocity(control_target.gas_brake_norm, obstacle_emergency);
			ROS_INFO_STREAM("OBS");
		}
		else
		{
			obstacle_values_calculated = false;
			back_obstacle_values_calculated = false;
			control_target.velocity = velocity_controller.get_target_velocity(recorded_velocity, path_curvature);
			control_target.gas_brake_norm = velocity_controller.get_gas_brake_value(vehicle_state.velocity, control_target.velocity);
			ROS_INFO_STREAM("NO OBS");
		}
	}

	/** Use the velocity controller (see ::VelocityController) to determine the gas/brake value which
	 * will be used to reach the target velocity */

	/*ROS_INFO_STREAM("control_target: "
			<< "steer_ang: " << control_target.steering_angle << ", "
			<< "steer_ang_norm: " << control_target.steering_angle_norm << ", "
			<< "vel: " << control_target.velocity << ", "
			<< "gas_brake_norm: " << control_target.gas_brake_norm);*/
}


/**
 Callback function to update parameters from dynamic_reconfigure
 */
void parameter_callback(path_follower::PathFollowerConfig &config,
		uint32_t level)
{
	VelocityController::DynamicParameters velocity_params;

	velocity_params.p_max = config.p_max;
	velocity_params.p_min = config.p_min;
	velocity_params.p_idle = config.idle_brake_value;
	velocity_params.a_gas_neg = config.a_gas_neg;
	velocity_params.a_gas_pos = config.a_gas_pos;
	velocity_params.a_brake = config.a_brake;
	velocity_params.filter_coeff = config.filter_coeff;
	velocity_params.v_max = config.v_target_max;
	velocity_params.v_min = config.v_target_min;
	velocity_params.acc_lat_max = config.acc_lat_max;

	velocity_controller.update_parameters(velocity_params);

	params.post_process_max_seg_length = config.post_process_max_seg_length;
	params.post_process_epsilon = config.post_process_epsilon;
	params.post_process_curvature_smoothing = config.post_process_curvature_smoothing;
	params.idle_brake_value = config.idle_brake_value;
	params.control_gas_brake = config.control_gas_brake;

	ROS_INFO("Parameters reconfigured");
}
/**
 Callback function for the Truck Status
 */

void truck_status_cb(std_msgs::UInt16 msg)
{
	ROS_ERROR_STREAM("status: " << msg.data);

	if(msg.data == 4 || msg.data == 8 || msg.data == 2) truck_status_flag = false; //rc_gui || rc_rc || manual

	else truck_status_flag = true;
}

void rc_on_cb(std_msgs::Bool msg)
{
	if (msg.data == false) allow_autonomous = true;
	else if (msg.data == true) allow_autonomous = false;
}


/**
 Callback function for the PathFollowerService

 The PathFollowerService is used to communicate between the GUI and the path follower node.
 This callback function contains the logic for deciding if state transitions or other
 actions requested by the GUI are valid and therefore can/will be performed.
 */
bool path_follower_service(path_follower::PathFollowerService::Request& req,
		path_follower::PathFollowerService::Response& res)
{
	/** All commands for the PathFollowerService are defined in the message path_follower::ServiceCommand. */
	switch (req.command)
	{
	case ServiceCommand::START_RECORDING: /** ### ServiceCommand::START_RECORDING: ### */
	{
		/** A state transition to PathFollower::State::RECORDING will be performed, if: */
		/** - the path follower is currently in state PathFollower::State::IDLE and */
		if (!(follower.state() == PathFollower::State::IDLE))
		{
			res.reply = ServiceReply::FAIL_STATE_LOGIC;
			return true;
		}

		switch (oxts.state())
		{
		case Oxts::State::FIX: /** - the state of oxts is Oxts::State::FIX */
		{
			follower.set_state(PathFollower::State::RECORDING);

			/** The active path will be cleared and its recording time set to the current time. */
			path_manager.active_path().clear();

			ros::Time t = ros::Time::now(); // initiate path recording time
			path_manager.active_path().rec_sec = t.sec;
			path_manager.active_path().rec_nsec = t.nsec;

			/** If the active path is the reference path (id of active path is 0), the UTM converter will be reset. */
			if (path_manager.active_path_id() == 0)
			{
				utm_converter.reset();
			}

			res.reply = ServiceReply::SUCCESS;
			return true;
		}
			/** A state transition to PathFollowerState::RECORDING will NOT be performed,
			 if the state of oxts is either: */
		case Oxts::State::OFFLINE: /** - Oxts::State::OFFLINE (OxTS node not running) */
		{
			res.reply = ServiceReply::FAIL_OXTS_OFFLINE;
			return true;
		}
		case Oxts::State::NO_DATA: /** - Oxts::State::NO_DATA (OxTS node is running, but no data is being received) */
		{
			res.reply = ServiceReply::FAIL_OXTS_NO_DATA;
			return true;
		}
		case Oxts::State::DATA_BUT_NO_FIX:
			/** - Oxts::State::DATA_BUT_NO_FIX (OxTS data is being received, but satellite fix is not available) */
		{
			res.reply = ServiceReply::FAIL_OXTS_DATA_BUT_NO_FIX;
			return true;
		}
		}

		break;
	}

	case ServiceCommand::STOP_RECORDING: /** ### ServiceCommand::STOP_RECORDING: ### */
	{
		/** A state transition to PathFollower::State::IDLE will be performed, if the path follower
		 is currently in state PathFollower::State::RECORDING */
		if (!(follower.state() == PathFollower::State::RECORDING))
		{
			res.reply = ServiceReply::FAIL_STATE_LOGIC;
			return true;
		}

		follower.set_state(PathFollower::State::IDLE); // stop recording and go back to IDLE

		path_manager.save_to_temp_file();

		res.reply = ServiceReply::SUCCESS;
		return true;
	}

	case ServiceCommand::START_FOLLOWING: /** ### ServiceCommand::START_FOLLOWING: ### */
	{
		/** A state transition to PathFollower::State::FOLLOWING will be performed, if: */
		/** - the path follower is currently in state PathFollower::State::IDLE and */
		if (!(follower.state() == PathFollower::State::IDLE))
		{
			res.reply = ServiceReply::FAIL_STATE_LOGIC;
			return true;
		}

		switch (oxts.state())
		{
		case Oxts::State::FIX: /** - the state of oxts is Oxts::State::FIX and */
		{
			/*if (!bucket_check)
			{
				//ROS_INFO_STREAM("bucket check");
				res.reply = ServiceReply::FAIL_BUCKET_IS_OPENED;
				return true;
			}*/
			if (!following_allowed()) /** - following is allowed (see following_allowed()) */
			{
				res.reply = ServiceReply::FAIL_VEHCILE_NOT_IN_GO_ZONE;
				return true;
			}

			/** - the active path has points (see GpsPath::has_points()) and */
			if (!path_manager.active_path().has_points())
			{
				res.reply = ServiceReply::FAIL_NO_PATH_AVAILABLE;
				return true;
			}

			/** - the length of the active path is greater than GlobalParameters::min_path_length */
			if (path_manager.active_path().length()	< params.min_path_length)
			{
				res.reply = ServiceReply::FAIL_PATH_TOO_SHORT;
				return true;
			}

			/** If the state of paravan is not Paravan::State::ONLINE, following will be activated with a warning.
			 *  In that case, the calculated control targets for steering/gas/brake will be calculated,
			 *  but cannot be actuated by the paravan system. */
			switch (um.state())
			{
			case (UeM::State::ONLINE):
			{
				res.reply = ServiceReply::SUCCESS;
				break;
			}
			case (UeM::State::NO_REMOTE_CONTROL):
			{
				if (params.following_without_paravan)
				{
					res.reply = ServiceReply::WARN_UEM_NO_REMOTE_CONTROL;
				}
				else
				{
					res.reply = ServiceReply::FAIL_UEM_NO_REMOTE_CONTROL;
					return true;
				}
				break;
			}
			case (UeM::State::OFFLINE):
			{
				if (params.following_without_paravan)
				{
					res.reply = ServiceReply::WARN_UEM_OFFLINE;
				}
				else
				{
					res.reply = ServiceReply::FAIL_UEM_OFFLINE;
					return true;
				}
				break;
			}
			case (UeM::State::NO_DATA):
			{
				if (params.following_without_paravan)
				{
					res.reply = ServiceReply::SUCCESS;
				}
				else
				{
					res.reply = ServiceReply::FAIL_UEM_NO_DATA;
					return true;
				}
				break;
			}
			}

			if (req.int_data == 1)
			{
				/*ros::ServiceClient client = node_handle->serviceClient<std_srvs::Empty>("/paravan/horn_warning");
				std_srvs::Empty srv;
				client.call(srv);*/
			}

			follower.set_state(PathFollower::State::FOLLOWING);

			/*ROS_ERROR_STREAM("req.int_data: " << req.int_data);
			ROS_INFO_STREAM("####");*/

			prepare_following();
			return true;
		}
		/** A state transition to PathFollower::State::FOLLOWING will NOT be performed,
		 if the state of oxts is either: */
		case Oxts::State::OFFLINE: /** - Oxts::State::OFFLINE (OxTS node not running) */
		{
			res.reply = ServiceReply::FAIL_OXTS_OFFLINE;
			return true;
		}
		case Oxts::State::NO_DATA: /** - Oxts::State::NO_DATA (OxTS node is running, but no data is being received) */
		{
			res.reply = ServiceReply::FAIL_OXTS_NO_DATA;
			return true;
		}
		case Oxts::State::DATA_BUT_NO_FIX:
		{
			/** - Oxts::State::DATA_BUT_NO_FIX (OxTS data is being received, but satellite fix is not available) */
			res.reply = ServiceReply::FAIL_OXTS_DATA_BUT_NO_FIX;
			return true;
		}
		}

		break;
	}

	case ServiceCommand::STOP_FOLLOWING: /** ### ServiceCommand::STOP_FOLLOWING: ### */
	{

		publish_actuators_commands(
					{SteeringCommand::KeepNeutral},
					{GasBrakeCommand::Control, params.idle_brake_value});
		
		/** A state transition to PathFollower::State::IDLE will be performed, if the path follower is currently in state:
		 *	-  PathFollower::State::FOLLOWING or
		 *	-  PathFollower::State::HANDOVER */
		if (!(follower.state() == PathFollower::State::FOLLOWING
				|| follower.state() == PathFollower::State::HANDOVER))
		{
			res.reply = ServiceReply::FAIL_STATE_LOGIC;
			return true;
		}

		/** If the path follower is currently in state PathFollower::State::HANDOVER,
		 the driver alert is stopped (ServiceCommand::STOP_FOLLOWING is considered as alert acknowledged by the driver) */
		if (follower.state() == PathFollower::State::HANDOVER)
		{
			stop_alert();
		}

		follower.set_state(PathFollower::State::IDLE);
		
		
		//allow_autonomous = false;

		res.reply = ServiceReply::SUCCESS;
		return true;
	}

	/*case ServiceCommand::EX_LOCATION:
	{
		double utm_ex_x, utm_ex_y;

		// convert current gps point to utm point
		utm_converter.convert(um.data().ex_location.ex_latitude,
				um.data().ex_location.ex_longitude, utm_ex_x, utm_ex_y);

		ROS_INFO_STREAM("utm_ex_x: " << utm_ex_x
						<< ", utm_ex_y: " << utm_ex_y
						<< ", um.data().ex_location.ex_latitude: " << um.data().ex_location.ex_latitude
						<< ", um.data().ex_location.ex_longitude: " << um.data().ex_location.ex_longitude);

		if (active_path_index == 1)
		{
			path_manager.active_path().add_point(utm_ex_x, 
											utm_ex_y,
											um.data().ex_location.ex_latitude,
											um.data().ex_location.ex_longitude,
											0.0);

			path_manager.post_process_path(params.post_process_max_seg_length,
										params.post_process_epsilon,
										params.post_process_curvature_smoothing);

			//path_manager.save_to_temp_file();
		}
		else
		{
			ROS_ERROR_STREAM("UNABLE TO SET EXCAVATOR SPOT!");
		}
		

		res.reply = ServiceReply::SUCCESS;
		return true;
	}

	case ServiceCommand::DP_LOCATION:
	{
		double utm_ex_x, utm_ex_y;

		// convert current gps point to utm point
		utm_converter.convert(um.data().ex_location.ex_latitude,
				um.data().ex_location.ex_longitude, utm_ex_x, utm_ex_y);

		ROS_INFO_STREAM("utm_ex_x: " << utm_ex_x
						<< ", utm_ex_y: " << utm_ex_y
						<< ", um.data().ex_location.ex_latitude: " << um.data().ex_location.ex_latitude
						<< ", um.data().ex_location.ex_longitude: " << um.data().ex_location.ex_longitude);

		if (active_path_index == 99999)
		{
			path_manager.active_path().add_point(utm_ex_x, 
											utm_ex_y,
											um.data().ex_location.ex_latitude,
											um.data().ex_location.ex_longitude,
											0.0);

			path_manager.post_process_path(params.post_process_max_seg_length,
										params.post_process_epsilon,
										params.post_process_curvature_smoothing);

			//path_manager.save_to_temp_file();
		}
		else
		{
			ROS_ERROR_STREAM("UNABLE TO SET DP SPOT!");
		}
		

		res.reply = ServiceReply::SUCCESS;
		return true;
	}*/

	case ServiceCommand::POST_PROCESS_PATH: /** ### ServiceCommand::POST_PROCESS_PATH: ### */
	{
		/** Post processing (see GpsPath::post_process()) will be applied to the active path, if */
		/** - the active path has points (see GpsPath::has_points()) and */
		if (!path_manager.active_path().has_points())
		{
			res.reply = ServiceReply::FAIL_NO_PATH_AVAILABLE;
			return true;
		}
		/** - the length of the active path is greater than GlobalParameters::min_path_length */
		if (path_manager.active_path().length() < params.min_path_length)
		{
			res.reply = ServiceReply::FAIL_PATH_TOO_SHORT;
			return true;
		}

		path_manager.post_process_path(params.post_process_max_seg_length,
				params.post_process_epsilon,
				params.post_process_curvature_smoothing);

		path_manager.save_to_temp_file();

		res.reply = ServiceReply::SUCCESS;
		return true;
	}

	case ServiceCommand::SAVE_PATH_TO_FILE: /** ### ServiceCommand::SAVE_PATH_TO_FILE: ### */
	{
		//Check path_manager.active_path().has_points() before saving, if no points, display suitable message!!!
		/** All paths will be saved to a single file (see PathManager::save_to_file). */
		path_manager.save_to_file(req.str_data);

		res.reply = ServiceReply::SUCCESS;
		return true;
	}

	case ServiceCommand::LOAD_PATH_FROM_FILE: /** ### ServiceCommand::LOAD_PATH_FROM_FILE: ### */
	{
		/** Paths will be loaded from a file (see PathManager::load_from_file). */
		try
		{
			path_manager.load_from_file(req.str_data);
			res.reply = ServiceReply::SUCCESS;

			utm_converter.reset();
			if (path_manager.active_path().has_points())
			{
				utm_converter.init_zone(
						path_manager.active_path().gps_lat().front(),
						path_manager.active_path().gps_lon().front());
			}
		} catch (const std::exception& e)
		{
			ROS_ERROR_STREAM(e.what());
			res.reply = ServiceReply::FAIL_LOADING_PATH_FROM_FILE;
		}

		return true;
	}

	case ServiceCommand::SHOW_GO_ZONE: /** ### ServiceCommand::SHOW_GO_ZONE: ### */
	{
		/** Go-zones will be displayed in rviz or not, depending on the PathFollowerService's request content. */
		params.display_go_zone = static_cast<bool>(req.int_data);

		res.reply = ServiceReply::SUCCESS;
		return true;
	}

	case ServiceCommand::SHOW_WAIT_ZONE: /** ### ServiceCommand::SHOW_WAIT_ZONE: ### */
	{
		/** Wait-zones will be displayed in rviz or not, depending on the PathFollowerService's request content. */
		params.display_wait_zone = static_cast<bool>(req.int_data);

		res.reply = ServiceReply::SUCCESS;
		return true;
	}

	case ServiceCommand::SET_GO_ZONE_RADIUS: /** ### ServiceCommand::SET_GO_ZONE_RADIUS: ### */
	{
		/** Go-zone radius of the active path will be set to the value from the PathFollowerService's request. */
		path_manager.active_path().go_zone_radius = req.float_data;

		res.reply = ServiceReply::SUCCESS;
		return true;
	}

	case ServiceCommand::SET_WAIT_ZONE_RADIUS: /** ### ServiceCommand::SET_WAIT_ZONE_RADIUS: ### */
	{
		/** Wait-zone radius of the active path will be set to the value from the PathFollowerService's request. */
		path_manager.active_path().wait_zone_radius = req.float_data;

		res.reply = ServiceReply::SUCCESS;
		return true;
	}

	case ServiceCommand::PLOT_MAPPING_DATA: /** ### ServiceCommand::PLOT_MAPPING_DATA: ### */
	{
		/** Mappings will be plotted (see DataPlotter::plot_mappings) for:
		 * - steering angle rad vs. norm
		 * - steady state velocity vs. gas/brake value */
		bool plot_successful = data_plotter.plot_mappings(velocity_controller,
				steering_controller);

		if (plot_successful)
		{
			res.reply = ServiceReply::SUCCESS;
		}
		else /** If a previous plotting window is still open, plotting will fail. */
		{
			//res.reply = ServiceReply::FAIL_OLD_PLOT_STILL_ACTIVE;
		}

		return true;
	}

	case ServiceCommand::PLOT_PATH_DATA: /** ### ServiceCommand::PLOT_PATH_DATA: ### */
	{
		/** Path data will be plotted (see DataPlotter::plot_path_data):
		 * - recorded and target velocity
		 * - path heading
		 * - path curvature
		 * - 2D path view */
		bool plot_successful = data_plotter.plot_path_data(
				path_manager.active_path(), velocity_controller);

		if (plot_successful)
		{
			res.reply = ServiceReply::SUCCESS;
		}
		else /** If a previous plotting window is still open, plotting will fail. */
		{
			//res.reply = ServiceReply::FAIL_OLD_PLOT_STILL_ACTIVE;
		}

		return true;
	}

	case ServiceCommand::BRAKE_IN_IDLE: /** ### ServiceCommand::BRAKE_IN_IDLE: ### */
	{
		/** GlobalParameters::idle_braking will be set to the requested value. */
		params.idle_braking = static_cast<bool>(req.int_data);

		res.reply = ServiceReply::SUCCESS;
		return true;
	}

	case ServiceCommand::ADD_PATH: /** ### ServiceCommand::ADD_PATH: ### */
	{
		/** A new (empty) path will be added and set as active path. */
		if (follower.state() == PathFollower::State::FOLLOWING)
		{
			res.reply = ServiceReply::SERVICE_ERROR; // Prohibits adding new path while following the current path.
			return true;
		}

		const std::string& path_name = req.str_data;
		path_manager.add_path(path_name);
		path_manager.set_active_path_index(path_manager.paths().size() - 1);
		res.reply = ServiceReply::SUCCESS;

		ROS_INFO_STREAM("Added path: \"" << path_name <<"\"");
		return true;
	}
	case ServiceCommand::REMOVE_PATH: /** ### ServiceCommand::REMOVE_PATH: ### */
	{
		/** The path with ID according to the PathFollowerService's request will be removed . */
		if (follower.state() == PathFollower::State::FOLLOWING)
		{
			res.reply = ServiceReply::SERVICE_ERROR; // Prohibits removing active path while following the current path.
			return true;
		}

		try
		{
			int index = req.int_data;
			path_manager.remove_path(index);
			res.reply = ServiceReply::SUCCESS;
		} catch (std::exception& e)
		{
			ROS_ERROR_STREAM(e.what());
			res.reply = ServiceReply::SERVICE_ERROR;
		}

		return true;
	}
	case ServiceCommand::RENAME_PATH: /** ### ServiceCommand::RENAME_PATH: ### */
	{
		/** The path with ID according to the PathFollowerService's request will be renamed . */
		if (follower.state() == PathFollower::State::FOLLOWING)
		{
			res.reply = ServiceReply::SERVICE_ERROR; // Prohibits renaming active path while following the current path.
			return true;
		}

		try
		{
			unsigned int index = static_cast<unsigned int>(req.int_data);
			const std::string& new_name = req.str_data;
			path_manager.rename_path(index, new_name);
			res.reply = ServiceReply::SUCCESS;
		} catch (std::exception& e)
		{
			ROS_ERROR_STREAM(e.what());
			res.reply = ServiceReply::SERVICE_ERROR;
		}

		return true;
	}
	case ServiceCommand::SET_REVERSE_MISSION: /** ### ServiceCommand::SET_REVERSE_MISSION: ### */
	{
		if (follower.state() == PathFollower::State::FOLLOWING)
		{
			res.reply = ServiceReply::SERVICE_ERROR; 
			return true;
		}

		else
		{
			if (req.int_data == 1)
			{
				path_manager.active_path().reverse = req.int_data;
				ROS_INFO_STREAM("SET REVERSE MISSION");
			}
			else
			{
				path_manager.active_path().reverse = req.int_data;
				ROS_INFO_STREAM("SET FORWARD MISSION");
			}

			res.reply = ServiceReply::SUCCESS;
		}

		return true;
	}
	case ServiceCommand::SET_ACTIVE_PATH: /** ### ServiceCommand::SET_ACTIVE_PATH: ### */
	{
		/** The path with ID according to the PathFollowerService's request will be set as active path . */
		if (follower.state() == PathFollower::State::FOLLOWING)
		{
			res.reply = ServiceReply::SERVICE_ERROR; // Safety feature: Prohibits changing active path while following the current path.
			return true;
		}

		try
		{
			unsigned int index = static_cast<unsigned int>(req.int_data);
			path_manager.set_active_path_index(index);
			res.reply = ServiceReply::SUCCESS;
		} catch (std::exception& e)
		{
			ROS_ERROR_STREAM(e.what());
			res.reply = ServiceReply::SERVICE_ERROR;
		}

		return true;
	}
	case ServiceCommand::BUCKET_OPENED:
	{
		ROS_ERROR_STREAM("BUCKET_OPENED");

		bucket_check = true;


		return true;
	}
	case ServiceCommand::BUCKET_CLOSED:
	{
		ROS_ERROR_STREAM("BUCKET_CLOSED");
		
		//bucket_check = true;
		return true;
	}
	}

	return true;
}

/**
 * Check if vehicle velocity is valid (OxTs is in state FIX) and low
 * (smaller than GlobalParameters::max_velocity_for_idle_braking)
 */
bool velocity_valid_and_low()
{
    if (oxts.state() == Oxts::State::FIX // velocity value is only valid, if OxTS is sending valid data
        && vehicle_state.velocity < params.max_velocity_for_idle_braking)
    {
        return true;
    }
    else
    {
        return false;
    }
}

/**
 Cyclic timer callback function of the path follower node
 */
void timer_callback(const ros::TimerEvent& timerEvent)
{
	/** Independent of the state of the path follower, the following update and publisher functions are called: */
	oxts.update_state(); /** - Oxts::update_state() */
	um.update_state(); /** - U&M::update_state() */
	update_vehicle_dynamic_state(); /** - update_vehicle_dynamic_state() */

	publish_node_alive_signal(); /** - publish_node_alive_signal() */
	publish_vehicle_state(); /** - publish_vehicle_state() */
	publish_path_info(); /** - publish_path_info() */
	publish_active_path_id(); /** - publish_active_path_id() */
	publish_vehicle_marker(); /** - publish_vehicle_marker() */
	publish_vehicle_transform(); /** - publish_vehicle_transform() */


	if (path_manager.reference_path().has_points())
	{
		publish_path_marker(); /** - publish_path_marker() */
		publish_zone_marker(); /** - publish_zone_marker() */
	}

	/** Depending on the state of the path follower, specific actions are performed: */
	switch (follower.state())
	{
	case PathFollower::State::IDLE: /** If the path follower is in state PathFollower::State::IDLE: */
	{
		publish_state_event(path_follower::StateEvents::IDLE);

		/**If velocity is valid and low (see ::velocity_valid_and_low()) and
		 * GlobalParameters::idle_braking is true, apply brake using GlobalParameters::idle_brake_value. */
		
		/** Otherwise keep a neutral steering/gas/brake position */
		
			publish_actuators_commands(
					{SteeringCommand::KeepNeutral},
					{GasBrakeCommand::Control, params.idle_brake_value});

		if(sensor_state.follow_with_sensor == true)
		{
			collision.lidar_fusion();
			collision.nearest_obstacle();

			publish_sensor_state();
		}
		
		return;
		//ROS_INFO_STREAM("1");
		/*if (active_path_index == 1)
		{
			if (idle_flag = true)
			{
			//ROS_INFO_STREAM("2");
			publish_actuators_commands({SteeringCommand::KeepNeutral}, {GasBrakeCommand::Control, 0.14});

			}
			
			else
			{
				if (bucket_check == true)
				{
					//ROS_INFO_STREAM("3");
					publish_actuators_commands(
						{SteeringCommand::KeepNeutral},
						{GasBrakeCommand::Control, params.idle_brake_value});
				}
				else
				{
					return;
				}
			}
			
					
		}
		
		else
		{	

			ROS_DEBUG_STREAM("4 - Teste");
			publish_actuators_commands(
					{SteeringCommand::KeepNeutral},
					{GasBrakeCommand::Control, params.idle_brake_value});

		} */

		
	}

	case PathFollower::State::RECORDING: /** If the path follower is in state PathFollower::State::RECORDING: */
	{
		publish_state_event(path_follower::StateEvents::RECORDING);

		switch (oxts.state())
		{
		case Oxts::State::FIX: /** - and oxts state is Oxts::State::FIX, add a point to the active path */
		{
			path_manager.active_path().add_point(vehicle_state.x,
					vehicle_state.y, oxts.data().nav_sat_fix.latitude,
					oxts.data().nav_sat_fix.longitude, oxts.data().nav_sat_fix.altitude,
					vehicle_state.velocity);

			return;
		}
		case Oxts::State::OFFLINE: /** - and oxts state is Oxts::State::OFFLINE, change to PathFollower::State::IDLE */
		{
			publish_state_event(path_follower::StateEvents::RECORDING_FAILED_OXTS_NODE_DEAD);
			follower.set_state(PathFollower::State::IDLE);
			return;
		}
		case Oxts::State::NO_DATA: /** - and oxts state is Oxts::State::NO_DATA, change to PathFollower::State::IDLE */
		{
			publish_state_event(path_follower::StateEvents::RECORDING_FAILED_OXTS_NO_DATA);
			follower.set_state(PathFollower::State::IDLE);
			return;
		}
		case Oxts::State::DATA_BUT_NO_FIX: /** - and oxts state is Oxts::State::DATA_BUT_NO_FIX, change to PathFollower::State::IDLE */
		{
			publish_state_event(path_follower::StateEvents::RECORDING_FAILED_OXTS_NO_FIX);
			follower.set_state(PathFollower::State::IDLE);
			return;
		}
		}

		break;
	}

	case PathFollower::State::FOLLOWING: /** If the path follower is in state PathFollower::State::FOLLOWING */
	{
		publish_state_event(path_follower::StateEvents::FOLLOWING);

		switch (oxts.state())
		{
		case Oxts::State::FIX:
		{
			
			/** - and oxts state is Oxts::State::FIX:
			 *  	- update desired steering angle and gas/brake position (update_vehicle_control_target())
			 *  	- if driver_handover_necessary(), change path follower state to PathFollower::State::HANDOVER
			 * 	- if oxts state is Oxts::State::OFFLINE or Oxts::State::NO_DATA or Oxts::State::DATA_BUT_NO_FIX,
			 * 	change path follower state to PathFollower::State::HANDOVER
			 */


			if (driver_handover_necessary())
			{
				follower.set_state(PathFollower::State::HANDOVER);
				break;
			}
			
			if(sensor_state.follow_with_sensor == true)
			{

				collision.lidar_fusion();
				collision.nearest_obstacle();

				publish_sensor_state();
			}

			update_vehicle_control_target();
			 
			if (vehicle_reached_wait_zone())
			{
				ROS_INFO_STREAM("vehicle has reached wait zone");

				publish_state_event(
						path_follower::StateEvents::WAIT_ZONE_REACHED); //  publish state event that wait zone has been reached

				if (velocity_valid_and_low())
				{
                    			
					publish_actuators_commands(
					{SteeringCommand::KeepNeutral},
					{GasBrakeCommand::Control, -0.4});

					
					ros::ServiceClient client_emergency_on = node_handle->serviceClient<std_srvs::Empty>("/uem/emergency_on");
				    	std_srvs::Empty srv_emergency_on;
				    	client_emergency_on.call(srv_emergency_on);
		

					ros::ServiceClient client_gear_neutral = node_handle->serviceClient<std_srvs::Empty>("/uem/gear_neutral");
				    	std_srvs::Empty srv_gear_neutral;
					client_gear_neutral.call(srv_gear_neutral);

							


					ROS_ERROR_STREAM("Final de rota");

					follower.set_state(PathFollower::State::IDLE);

					/*if (path_manager.active_path().reverse == 1 && active_path_index == 1)
					{
						ROS_INFO_STREAM("RE 1");
						idle_flag = true;
						ros::ServiceClient client_bucket = node_handle->serviceClient<std_srvs::Empty>("/uem/open_bucket");
				    	std_srvs::Empty srv_bucket;
				    	client_bucket.call(srv_bucket);
					}
					if (path_manager.active_path().reverse == 0 && active_path_index == 2)
					{
						ROS_INFO_STREAM("RE 2");
						ros::ServiceClient client_bucket = node_handle->serviceClient<std_srvs::Empty>("/uem/close_bucket");
						std_srvs::Empty srv_bucket;
						client_bucket.call(srv_bucket);
					
					}*/


				}
			}

			publish_control_target_state();

			publish_steering_desired_marker();
			publish_control_points_marker();
					

			if (following_timer_cycles >= params.cycles_before_start_following)
			{
				if (following_timer_cycles == params.cycles_before_start_following)
				{
					ROS_INFO_STREAM("Started FOLLOWING");

					control_target.gas_brake_norm = 0;

					publish_actuators_commands(
						{SteeringCommand::Control, control_target.steering_angle_norm},
						{GasBrakeCommand::Control, control_target.gas_brake_norm});

				}

				publish_actuators_commands(
						{SteeringCommand::Control, control_target.steering_angle_norm},
						{GasBrakeCommand::Control, control_target.gas_brake_norm});

			}
			else /* stay in IDLE behavior */
			{
				ROS_INFO_STREAM("Wait cycle " << (following_timer_cycles + 1) << "/"
						<< params.cycles_before_start_following << " before FOLLOWING");

				if (path_manager.active_path().reverse == 1)
				{
					//collision.reverse_flag = true;

					if (wait_parameters_change < 20)
					{

						if(!parameters_change)
						{
							steering_params.look_ahead_dist_min = 4.0;
							steering_params.eta_1 = 0.0;
							steering_params.eta_2 = 10.0;
							
							steering_controller.update_parameters(steering_params);

							parameters_change = true;
						}

						ROS_ERROR_STREAM("Requesting gear R...");

						ros::ServiceClient client_gear_back = node_handle->serviceClient<std_srvs::Empty>("/uem/gear_back");
				        std_srvs::Empty srv_gear_back;
				        client_gear_back.call(srv_gear_back);

						ros::ServiceClient client_autonomous_mode = node_handle->serviceClient<std_srvs::Empty>("/uem/autonomous_mode");
				    	std_srvs::Empty srv_autonomous_mode;
				    	client_autonomous_mode.call(srv_autonomous_mode);

						/*ros::ServiceClient client_emergency = node_handle->serviceClient<std_srvs::Empty>("/uem/emergency_off");
				    	std_srvs::Empty srv_emergency;
				    	client_emergency.call(srv_emergency);*/
						
						
						following_timer_cycles = 0;

						++wait_parameters_change;
					}
					else
					{		
						if(steering_params.eta_1 == 0.0
							&& steering_params.look_ahead_dist_min == 4.0
							&& steering_params.eta_2 == 10.0)
						{
							ROS_ERROR_STREAM("Following back SUCCESS!");
						}
						else
						{
							following_timer_cycles = 0;

							wait_parameters_change = 0;
						}
						
					}
				}
				else
				{	
					//collision.reverse_flag = false;

					if(!horn_front_flag)
					{
						ros::ServiceClient client_horn_front = node_handle->serviceClient<std_srvs::Empty>("/uem/horn_front");
						std_srvs::Empty srv_horn_front;
						client_horn_front.call(srv_horn_front);

						horn_front_flag = true;
					}

					else
					{
						ROS_INFO_STREAM("Requesting gear D...");

						ros::ServiceClient client_gear_front = node_handle->serviceClient<std_srvs::Empty>("/uem/gear_front");
						std_srvs::Empty srv_gear_front;
						client_gear_front.call(srv_gear_front);

						ros::ServiceClient client_autonomous_mode = node_handle->serviceClient<std_srvs::Empty>("/uem/autonomous_mode");
				    	std_srvs::Empty srv_autonomous_mode;
				    	client_autonomous_mode.call(srv_autonomous_mode);
					}

				}

				if (velocity_valid_and_low() && params.idle_braking)
				{
					publish_actuators_commands(
							{SteeringCommand::KeepNeutral},
							{GasBrakeCommand::Control, params.idle_brake_value});
				}
				else
				{
					publish_actuators_commands(
							{SteeringCommand::KeepNeutral},
							{GasBrakeCommand::Control, params.idle_brake_value});
				}
			}

			++following_timer_cycles;

			if (!params.following_without_paravan)
			{
				switch (um.state())
				{
				case UeM::State::OFFLINE:
				{
					ROS_ERROR_STREAM("Path following failed - UeM node is dead!");

					publish_state_event(path_follower::StateEvents::FOLLOWING_FAILED_PARAVAN_NODE_DEAD);

					follower.set_state(PathFollower::State::HANDOVER);
					break;
				}
				case UeM::State::NO_DATA:
				{
					ROS_ERROR_STREAM("Path following failed - UeM does not provide data!");

					publish_state_event(path_follower::StateEvents::FOLLOWING_FAILED_PARAVAN_NO_DATA);

					follower.set_state(PathFollower::State::HANDOVER);
					break;
				}
				case UeM::State::NO_REMOTE_CONTROL:
				{
					ROS_ERROR_STREAM("Path following failed - Remote control not available!");

					publish_state_event(path_follower::StateEvents::FOLLOWING_FAILED_PARAVAN_NO_REMOTE_CONTROL);

					follower.set_state(PathFollower::State::HANDOVER);
					break;
				}
				}
			}

			break;
		}
		case Oxts::State::OFFLINE:
		{
			ROS_ERROR_STREAM("Path following failed - OxTS is OFFLINE!");

			publish_state_event(path_follower::StateEvents::FOLLOWING_FAILED_OXTS_NODE_DEAD);

			follower.set_state(PathFollower::State::HANDOVER);
			return;
		}
		case Oxts::State::NO_DATA:
		{
			ROS_ERROR_STREAM("Path following failed - OxTS is providing NO_DATA!");

			publish_state_event(path_follower::StateEvents::FOLLOWING_FAILED_OXTS_NO_DATA);

			follower.set_state(PathFollower::State::HANDOVER);
			return;
		}
		case Oxts::State::DATA_BUT_NO_FIX:
		{
			ROS_ERROR_STREAM("Path following failed - OxTS has DATA_BUT_NO_FIX!");

			publish_state_event(path_follower::StateEvents::FOLLOWING_FAILED_OXTS_NO_FIX);

			follower.set_state(PathFollower::State::HANDOVER);
			return;
		}
		}

		break;
	}
	case PathFollower::State::HANDOVER: /** If the path follower is in state PathFollower::State::HANDOVER */
	{
		publish_state_event(path_follower::StateEvents::HANDOVER);

		publish_actuators_commands(
					{SteeringCommand::KeepNeutral},
					{GasBrakeCommand::Control, params.idle_brake_value});

		if (velocity_valid_and_low()) 
		{
			stop_alert();
			follower.set_state(PathFollower::State::IDLE);
		}
		else
		{
			alert();
		}
	}
	}
}

/**
 Get parameter value from ROS parameter server

 @param param_name Name of the parameter
 @param default_value Default value of the parameter
 @param global If true, param_name refers to the global parameter space
 */
template<typename T>
T get_param(const std::string& param_name, T default_value, bool global = false)
{
	T value;
	if (global)
	{
		node_handle->param<T>(param_name, value, default_value);
	}
	else
	{
		private_node_handle->param<T>(param_name, value, default_value);
	}

	return value;
}

/**
 Get parameter value from ROS parameter server (cached version with less communication overhead)

 @param param_name Name of the parameter
 @param param Reference to the parameter storage variable
 @param global If true, param_name refers to the global parameter space
 */
template<typename T>
void get_param_cached(const std::string& param_name, T& param, bool global = false)
{
	T copy = param;

	bool changed_value = false;
	if (global)
	{
		changed_value = node_handle->getParamCached(param_name, param);
	}
	else
	{
		changed_value = private_node_handle->getParamCached(param_name, param);
	}

	if (changed_value)
	{
		if (copy != param)
		{
			ROS_INFO_STREAM("Updated parameter " << param_name << ": " << param);
		}
	}
}

/**
 Cyclic update of some parameters, that can change frequently
 */
void update_parameters()
{
	get_param_cached("display_go_zone", params.display_go_zone);
	get_param_cached("display_wait_zone", params.display_wait_zone);
	get_param_cached("idle_braking", params.idle_braking);
	get_param_cached("/paravan_node/keep_steering_neutral", params.keep_steering_neutral, true); // global parameter
}

template <typename T>
void add_publisher(const char* publisher_name, const std::string& topic_name, int queue_size = 10)
{
	publishers[publisher_name] = node_handle->advertise<T>(topic_name, queue_size);
}

/**
 Main function
 */
int main(int argc, char** argv)
{
	// Override SIGINT handler
	ros::init(argc, argv, "path_follower_node",
			ros::init_options::NoSigintHandler);

	// Override XMLRPC shutdown
	ros::XMLRPCManager::instance()->unbind("shutdown");
	ros::XMLRPCManager::instance()->bind("shutdown", shutdownCallback);

	node_handle = boost::make_shared<ros::NodeHandle>();
	private_node_handle = boost::make_shared<ros::NodeHandle>("~");

	// publishers
	//add_publisher<std_msgs::UInt16>("relay_box_2_from_path", "path_follower_node/relay_box_2_desired");
	add_publisher<std_msgs::Float32>("steering", "/steering_norm_desired");
	add_publisher<std_msgs::Float32>("gas_brake", "/gas_brake_norm_desired");
	add_publisher<std_msgs::Empty>("node_alive", "path_follower_node/alive");
	add_publisher<path_follower::PathInfoArray>("path_info_array", "path_follower_node/path_info_array");
	add_publisher<std_msgs::UInt8>("active_path_id", "path_follower_node/active_path_id");
	add_publisher<std_msgs::UInt8>("state_event", "path_follower_node/state_event");
	add_publisher<std_msgs::Bool>("alert", "/alert");
	add_publisher<visualization_msgs::MarkerArray>("path_marker_array", "path_follower_node/path_marker_array");
	add_publisher<visualization_msgs::MarkerArray>("zone_marker_array", "path_follower_node/zone_marker_array");
	add_publisher<visualization_msgs::MarkerArray>("trajectory_marker_array", "path_follower_node/trajectory");
	add_publisher<visualization_msgs::MarkerArray>("vehicle_marker", "path_follower_node/vehicle_marker");
	add_publisher<visualization_msgs::Marker>("steering_marker", "path_follower_node/steering_marker");
	add_publisher<visualization_msgs::MarkerArray>("control_points_marker", "path_follower_node/control_points_marker");
	add_publisher<path_follower::ControlTargetState>("control_target_state", "path_follower_node/control_target_state");
	add_publisher<path_follower::SensorState>("sensor_state", "path_follower_node/sensor_state");
	add_publisher<path_follower::VehicleState>("vehicle_state", "path_follower_node/vehicle_state");
	add_publisher<std_msgs::UInt16>("blinker", "path_follower/blinker");

	// uem subscribers

	subscribers["truck_status"] = node_handle->subscribe<std_msgs::UInt16>(
			"/truck_status", 10, truck_status_cb);
	subscribers["rc_on"] = node_handle->subscribe<std_msgs::Bool>(
			"/uem/rc_on", 10, rc_on_cb);
	subscribers["relay_box_2_from_uem"] = node_handle->subscribe<std_msgs::UInt16>(
			"uem/relay_box_2_from_uem", 10, &UeM::callback_relay_box_2, &um);
	subscribers["uem_data_signal"] = node_handle->subscribe<std_msgs::Empty>(
			"/uem/data_signal", 10, &UeM::callback_data_signal, &um);
	subscribers["ex_location"] = node_handle->subscribe<path_follower::Excavator>(
			"uem/ex_location", 10, &UeM::callback_ex_location, &um);	
	subscribers["paravan_alive"] = node_handle->subscribe<std_msgs::Empty>(
			"paravan/alive", 10, &UeM::callback_alive, &um);


	// oxts subscribers
	subscribers["nav_sat_fix"] = node_handle->subscribe<sensor_msgs::NavSatFix>(
			"oxts/nav_sat_fix", 10, &Oxts::callback_nav_sat_fix, &oxts);
	subscribers["imu"] = node_handle->subscribe<sensor_msgs::Imu>("oxts/imu",
			10, &Oxts::callback_imu, &oxts);
	subscribers["gps_vel"] = node_handle->subscribe<
			geometry_msgs::TwistWithCovarianceStamped>("oxts/gps_vel", 10,
			&Oxts::callback_twist, &oxts);
	subscribers["oxts_alive"] = node_handle->subscribe<std_msgs::Empty>(
			"oxts/alive", 10, &Oxts::callback_alive, &oxts);

	// sensor
	subscribers["sick_left"] = node_handle->subscribe<sick_ldmrs_msgs::ObjectArray175>(
			"sick_175/objects_175", 10, &CollisionAvoidance::callback_sick_front_left, &collision);
	subscribers["sick_right"] = node_handle->subscribe<sick_ldmrs_msgs::ObjectArray224>(
			"sick_224/objects_224", 10, &CollisionAvoidance::callback_sick_front_right, &collision);
	subscribers["radar_front"] = node_handle->subscribe<derived_object_msgs::ObjectWithCovarianceArray>(
				"/as_tx/objects", 10, &CollisionAvoidance::callback_radar_front, &collision);
	
	// services
	ros::ServiceServer service = node_handle->advertiseService(
			"path_follower_node/path_follower_service", path_follower_service);


	// initialization of sub-components
	alert_started = false;
	following_timer_cycles = 0;
	const double alive_timeout = 0.1;
	const double data_timeout = 3.0;

	oxts.init(alive_timeout, get_param("gps_data_timeout", 1));
	um.init(alive_timeout, data_timeout);
	follower.init();

	data_plotter.init();

	std::vector<std::vector<double>> path_colors;
	path_colors.push_back(get_param("path_colors_a", std::vector<double>
	{ 1.0, 1.0 }));
	path_colors.push_back(get_param("path_colors_r", std::vector<double>
	{ 0.0, 0.9 }));
	path_colors.push_back(get_param("path_colors_g", std::vector<double>
	{ 0.9, 0.0 }));
	path_colors.push_back(get_param("path_colors_b", std::vector<double>
	{ 0.0, 0.0 }));

	path_manager.set_color_pallet(path_colors);
	path_manager.reset_paths();
	if (path_manager.load_last_path())
	{
		if (path_manager.active_path().has_points())
		{
			utm_converter.init_zone(
					path_manager.active_path().gps_lat().front(),
					path_manager.active_path().gps_lon().front());
		}
	}

	VelocityController::DynamicParameters velocity_params;

	velocity_params.p_max = get_param("p_max", 0.5);
	velocity_params.p_min = get_param("p_min", -0.4);
	velocity_params.p_idle = get_param("idle_brake_value", -0.3);
	velocity_params.delta_v_brake = get_param("delta_v_brake", -5.0);
	velocity_params.a_gas_neg = get_param("a_gas_neg", 0.5);
	velocity_params.a_gas_pos = get_param("a_gas_pos", 0.2);
	velocity_params.a_brake = get_param("a_brake", 0.2);
	velocity_params.filter_coeff = get_param("filter_coeff", 0.8);
	velocity_params.v_max = get_param("v_target_max", 15.0);
	velocity_params.v_min = get_param("v_target_min", 3.0);
	velocity_params.acc_lat_max = get_param("acc_lat_max", 1.0);

	velocity_controller.init(velocity_params,
			get_param("p_stat_map_v", std::vector<double>
			{ 0.0, 1.0 }), get_param("p_stat_map_p", std::vector<double>
			{ 0.0, 1.0 }));

	

	steering_params.look_ahead_dist_max = get_param("look_ahead_dist_max",
			10.0);
	steering_params.look_ahead_dist_min = get_param("look_ahead_dist_min", 4.0);
	steering_params.look_ahead_dist_factor = get_param("look_ahead_dist_factor",
			1.5);
	steering_params.eta_1 = get_param("eta_1", 10.0);
	steering_params.eta_2 = get_param("eta_2", 10.0);
	steering_params.eta_3 = get_param("eta_3", 10.0);
	steering_params.eta_4 = get_param("eta_4", 50.0);
	steering_params.dt = get_param("dt", 0.2);
	steering_params.poly_dist_min = get_param("poly_dist_min", 0.1);

    steering_controller.init(steering_params,
		get_param("vehicle_info_wheel_base", 4.57),
		get_param("steering_angle_max", 1.0),
		get_param("steering_map_norm", std::vector<double>{0.0, 1.0}),
		get_param("steering_map_rad",  std::vector<double>{0.0, 1.0}));


	vehicle_static_state.wheelbase 				= get_param("vehicle_info_wheel_base", 2.6),
	vehicle_static_state.length 				= get_param("vehicle_info_length", 4.3);
	vehicle_static_state.width 					= get_param("vehicle_info_width", 1.8);
	vehicle_static_state.dist_mid_to_rear_axle 	= get_param("vehicle_info_dist_mid_to_rear_axle", 1.0);
	vehicle_static_state.wheel_width 			= get_param("vehicle_info_wheel_width", 0.215);
	vehicle_static_state.wheel_diameter 		= get_param("vehicle_info_wheel_diameter", 0.6);
	vehicle_static_state.wheel_offset 			= get_param("vehicle_info_wheel_offset", 0.8);
	
	vehicle_static_state.front_wheel_offset 			= get_param("vehicle_info_front_wheel_offset", 0.8);
	vehicle_static_state.rearD_wheel_offset 			= get_param("vehicle_info_rearD_wheel_offset", 0.8);
	vehicle_static_state.rearF_wheel_offset 			= get_param("vehicle_info_rearF_wheel_offset", 0.8);

    params.min_path_length 					= get_param("min_path_length", 2.0);
    params.post_process_max_seg_length 		= get_param("post_process_max_seg_length", 10.0);
    params.post_process_curvature_smoothing = get_param("post_process_curvature_smoothing", 0.5);
    params.post_process_epsilon 			= get_param("post_process_epsilon", 0.5);
    params.display_go_zone 					= get_param("display_go_zone", true);
    params.display_wait_zone 				= get_param("display_wait_zone", true);
    params.following_from_outside_go_zone 	= get_param("following_from_outside_go_zone", false);
    params.following_without_paravan        = get_param("following_without_paravan", true);
    params.idle_braking 					= get_param("idle_braking", false);
    params.idle_brake_value 				= get_param("idle_brake_value", -0.3);
    params.max_steering_jump				= get_param("max_steering_jump", 0.2);
	params.max_gas_brake_jump				= get_param("max_gas_brake_jump", 0.2);
    params.control_gas_brake				= get_param("control_gas_brake", false);
    params.max_dist_from_path				= get_param("max_dist_from_path", 2.0);
    params.max_heading_deviation			= get_param("max_heading_deviation", 1.4);
    params.max_velocity_for_idle_braking	= get_param("max_velocity_for_idle_braking", 0.5);
    params.max_velocity                     = get_param("max_velocity", 12.0);
    params.keep_steering_neutral			= get_param("/paravan_node/keep_steering_neutral", false, true);
    params.cycles_before_start_following    = get_param("cycles_before_start_following", 10);

	sensor_state.obstacle_range_base		= get_param("obstacle_range_base", 15.0, true);
	sensor_state.obstacle_range_min			= get_param("obstacle_range_min", 7.0, true);
	sensor_state.obstacle_range_factor		= get_param("obstacle_range_factor", 1.0, true);
	sensor_state.obstacle_curve_range_factor= get_param("obstacle_curve_range_factor", 0.4, true);
	sensor_state.follow_with_sensor			= get_param("follow_with_sensor", false);

    Visualization::init(vehicle_static_state);

    /*** dynamic reconfiguration of parameters ***/

    dynamic_reconfigure::Server<path_follower::PathFollowerConfig> dynamic_reconfigure_server;
    dynamic_reconfigure_server.setCallback(boost::bind(&parameter_callback, _1, _2));
    
    ros::Timer timer = node_handle->createTimer(ros::Duration(0.1), timer_callback); // main execution loop
    vehicle_reached_wait_zone_flag = false;
    look_ahead_point_reached_wait_zone_flag = false;

    ros::Rate loop_rate(50); // [Hz] update frequency for ROS subscriber callback functions

    signal(SIGINT, mySigIntHandler);
    while(!g_request_shutdown) // start ROS loop
    {
        ros::spinOnce();
	
		update_parameters();

		loop_rate.sleep();
	}

	ros::shutdown();

	return 0;
}
 
