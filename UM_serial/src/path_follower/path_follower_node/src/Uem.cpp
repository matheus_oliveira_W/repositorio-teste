#include "Uem.h"

void UeM::init(const double alive_timeout, const double data_timeout)
{
	alive_timeout_.set_timeout(alive_timeout);
	data_timeout_.set_timeout(data_timeout);
	remote_control_timeout_.set_timeout(data_timeout);

	state_ = State::OFFLINE;
	magnetic_clutch_closed_ = false;
}

const UeM::Data& UeM::data()
{
	return data_;
}

const UeM::State& UeM::state()
{
	return state_;
}

void UeM::update_state()
{
	/** If paravan node timed out (see ::Timeout), set state to Paravan::State::OFFLINE */
	if (alive_timeout_.timed_out())
	{
		state_ = UeM::State::OFFLINE;
		data_.data_signal = 0;

	}
	else /** If paravan node is alive ... */
	{
		if (data_timeout_.timed_out())
		{
			/** - ... but no data is being received, set state to Paravan::State::NO_DATA */
			state_ = UeM::State::NO_DATA;
			data_.data_signal = 0;
		}
		else /** If data is being received ... */
		{
			if (remote_control_timeout_.timed_out())
			{
				/** - ... but remote control is not available, set state to Paravan::State::NO_REMOTE_CONTROL */
				state_ = UeM::State::ONLINE;
				data_.data_signal = 1;
			}
			else
			{
				/** - ... and remote control is available, set state to Paravan::State::ONLINE */
				state_ = UeM::State::ONLINE;
				data_.data_signal = 1;
			}
		}
	}
}

void UeM::callback_data_signal(const std_msgs::Empty::ConstPtr& msg)
{
	data_timeout_.got_signal();
}

void UeM::callback_alive(const std_msgs::Empty::ConstPtr& msg)
{
	alive_timeout_.got_signal();
}

void UeM::callback_relay_box_2(const std_msgs::UInt16::ConstPtr& msg)
{
	data_.gear_from_uem = *msg;
}

void UeM::callback_ex_location(const path_follower::Excavator::ConstPtr& msg)
{
	data_.ex_location = *msg;
}




/*void UeM::callback_remote_control_available(const std_msgs::Empty::ConstPtr& msg)
{
	remote_control_timeout_.got_signal();
}

const bool UeM::magnetic_clutch_closed()
{
	return magnetic_clutch_closed_;
}*/

