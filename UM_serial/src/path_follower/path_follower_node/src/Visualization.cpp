#include "Visualization.h"

namespace Visualization
{

visualization_msgs::Marker path_marker, trajectory_marker,
		trajectory_target_point_marker, veh_body_marker,
		veh_orientation_marker, veh_rear_wheel_left_marker, 
		veh_rearF_wheel_left_marker, veh_rearF_wheel_right_marker,
		veh_rear_wheel_right_marker, veh_front_wheel_left_marker,
		veh_front_wheel_right_marker, veh_steering_desired_marker,
		veh_steering_actual_marker, closest_path_point_marker,
		look_ahead_point_marker, obstacle_path_point_marker,
		look_ahead_obstacle_point_marker;

visualization_msgs::MarkerArray path_marker_array, veh_marker_array,
		control_points_marker_array, zone_marker_array, trajectory_marker_array,
		control_obstacle_points_marker_array;

void quaternion_from_angle(geometry_msgs::Quaternion& quat,
		const double orientation)
{
	tf::Quaternion q = tf::createQuaternionFromRPY(0, 0, orientation);

	quat.w = q.w();
	quat.x = q.x();
	quat.y = q.y();
	quat.z = q.z();
}

void color_from_argb(std_msgs::ColorRGBA& color, const double a, const double r,
		const double g, const double b)
{
	color.a = a;
	color.r = r;
	color.g = g;
	color.b = b;
}

const visualization_msgs::MarkerArray& get_zone_marker_array(
		const std::vector<GpsPath>& paths, bool display_go_zone,
		bool display_wait_zone)
{
	auto marker_lifetime = ros::Duration(0.3);

	zone_marker_array.markers.clear();

	int marker_count = 0;

	for (auto path : paths)
	{
		if (path.size() > 0 && paths[0].size() > 0)
		{
			if (display_go_zone)
			{
				visualization_msgs::Marker go_zone_marker;

				go_zone_marker.header.frame_id = "map";
				go_zone_marker.id = marker_count++;
				go_zone_marker.type = visualization_msgs::Marker::CYLINDER;
				go_zone_marker.action = visualization_msgs::Marker::ADD;

				go_zone_marker.scale.x = 2 * path.go_zone_radius;
				go_zone_marker.scale.y = 2 * path.go_zone_radius;
				go_zone_marker.scale.z = 0.1;

				go_zone_marker.pose.position.x = path.x().front()
						- paths[0].x().front();
				go_zone_marker.pose.position.y = path.y().front()
						- paths[0].y().front();
				go_zone_marker.pose.position.z = -1.0;

				color_from_argb(go_zone_marker.color, 1.0, 0.8, 0.8, 0.8);

				go_zone_marker.lifetime = marker_lifetime;

				zone_marker_array.markers.push_back(go_zone_marker);
			}

			if (display_wait_zone)
			{
				visualization_msgs::Marker wait_zone_marker;

				wait_zone_marker.header.frame_id = "map";
				wait_zone_marker.id = marker_count++;
				wait_zone_marker.type = visualization_msgs::Marker::CYLINDER;
				wait_zone_marker.action = visualization_msgs::Marker::ADD;

				wait_zone_marker.scale.x = 2 * path.wait_zone_radius;
				wait_zone_marker.scale.y = 2 * path.wait_zone_radius;
				wait_zone_marker.scale.z = 0.1;

				wait_zone_marker.pose.position.x = path.x().back()
						- paths[0].x().front();
				wait_zone_marker.pose.position.y = path.y().back()
						- paths[0].y().front();
				wait_zone_marker.pose.position.z = -1.0;

				color_from_argb(wait_zone_marker.color, 1.0, 0.0, 0.5, 0.75);

				wait_zone_marker.lifetime = marker_lifetime;

				zone_marker_array.markers.push_back(wait_zone_marker);
			}
		}
	}

	return zone_marker_array;
}

const visualization_msgs::MarkerArray& get_control_point_marker(
		const double pos_x, const double pos_y, const double lap_x,
		const double lap_y)
{
	control_points_marker_array.markers.clear();

	closest_path_point_marker.pose.position.x = pos_x;
	closest_path_point_marker.pose.position.y = pos_y;

	look_ahead_point_marker.pose.position.x = lap_x;
	look_ahead_point_marker.pose.position.y = lap_y;

	control_points_marker_array.markers.push_back(closest_path_point_marker);
	control_points_marker_array.markers.push_back(look_ahead_point_marker);

	return control_points_marker_array;
}

const visualization_msgs::MarkerArray& get_control_obstacle_point_marker(
		const double pos_x, const double pos_y, const double lap_x,
		const double lap_y)
{
	control_obstacle_points_marker_array.markers.clear();

	obstacle_path_point_marker.pose.position.x = pos_x;
	obstacle_path_point_marker.pose.position.y = pos_y;

	look_ahead_obstacle_point_marker.pose.position.x = lap_x;
	look_ahead_obstacle_point_marker.pose.position.y = lap_y;

	control_obstacle_points_marker_array.markers.push_back(obstacle_path_point_marker);
	control_obstacle_points_marker_array.markers.push_back(look_ahead_obstacle_point_marker);

	return control_obstacle_points_marker_array;
}

const visualization_msgs::Marker& get_steering_desired_marker(
		const double pos_x, const double pos_y, const double yaw_angle,
		const double steering_angle,
		const VehicleStaticState& vehicle_static_state)
{
	// marker visualizing the steering_angle of the vehicle
	quaternion_from_angle(veh_steering_desired_marker.pose.orientation,
			yaw_angle + steering_angle);

	veh_steering_desired_marker.pose.position.x = pos_x
			+ std::cos(yaw_angle) * vehicle_static_state.wheelbase;
	veh_steering_desired_marker.pose.position.y = pos_y
			+ std::sin(yaw_angle) * vehicle_static_state.wheelbase;

	return veh_steering_desired_marker;
}

const visualization_msgs::MarkerArray& get_vehicle_marker(const double pos_x,
		const double pos_y, const double yaw_angle,
		const VehicleStaticState& vehicle_static_state,
		const double current_steering_angle)
{
	veh_marker_array.markers.clear();

	quaternion_from_angle(veh_body_marker.pose.orientation, yaw_angle);

	veh_body_marker.pose.position.x = pos_x
			+ std::cos(yaw_angle) * vehicle_static_state.dist_mid_to_rear_axle;
	veh_body_marker.pose.position.y = pos_y
			+ std::sin(yaw_angle) * vehicle_static_state.dist_mid_to_rear_axle;
	veh_body_marker.pose.position.z = -0.2;

	// marker visualizing the orientation of the vehicle (along the body x-axis)
	quaternion_from_angle(veh_orientation_marker.pose.orientation, yaw_angle);

	veh_orientation_marker.scale.x = vehicle_static_state.wheelbase;

	veh_orientation_marker.pose.position.x = pos_x; // + std::cos(yaw_angle) * vehicle_static_state.wheelbase;
	veh_orientation_marker.pose.position.y = pos_y; // + std::sin(yaw_angle) * vehicle_static_state.wheelbase;

	// rear INSIDE wheel markers
	quaternion_from_angle(veh_rear_wheel_left_marker.pose.orientation,
			yaw_angle);

	veh_rear_wheel_left_marker.pose.position.x = pos_x
			- std::sin(yaw_angle) * vehicle_static_state.rearD_wheel_offset;
	veh_rear_wheel_left_marker.pose.position.y = pos_y
			+ std::cos(yaw_angle) * vehicle_static_state.rearD_wheel_offset;

	quaternion_from_angle(veh_rear_wheel_right_marker.pose.orientation,
			yaw_angle);

	veh_rear_wheel_right_marker.pose.position.x = pos_x
			+ std::sin(yaw_angle) * vehicle_static_state.rearD_wheel_offset;
	veh_rear_wheel_right_marker.pose.position.y = pos_y
			- std::cos(yaw_angle) * vehicle_static_state.rearD_wheel_offset;

	// rear OUTSIDE wheel markers
	quaternion_from_angle(veh_rearF_wheel_left_marker.pose.orientation,
			yaw_angle);

	veh_rearF_wheel_left_marker.pose.position.x = pos_x
			- std::sin(yaw_angle) * vehicle_static_state.rearF_wheel_offset;
	veh_rearF_wheel_left_marker.pose.position.y = pos_y
			+ std::cos(yaw_angle) * vehicle_static_state.rearF_wheel_offset;

	quaternion_from_angle(veh_rearF_wheel_right_marker.pose.orientation,
			yaw_angle);

	veh_rearF_wheel_right_marker.pose.position.x = pos_x
			+ std::sin(yaw_angle) * vehicle_static_state.rearF_wheel_offset;
	veh_rearF_wheel_right_marker.pose.position.y = pos_y
			- std::cos(yaw_angle) * vehicle_static_state.rearF_wheel_offset;

	// front wheel markers
	quaternion_from_angle(veh_front_wheel_left_marker.pose.orientation,
			yaw_angle + current_steering_angle);

	veh_front_wheel_left_marker.pose.position.x = pos_x
			+ std::cos(yaw_angle) * vehicle_static_state.wheelbase
			- std::sin(yaw_angle) * vehicle_static_state.front_wheel_offset;

	veh_front_wheel_left_marker.pose.position.y = pos_y
			+ std::sin(yaw_angle) * vehicle_static_state.wheelbase
			+ std::cos(yaw_angle) * vehicle_static_state.front_wheel_offset;

	quaternion_from_angle(veh_front_wheel_right_marker.pose.orientation,
			yaw_angle + current_steering_angle);

	veh_front_wheel_right_marker.pose.position.x = pos_x
			+ std::cos(yaw_angle) * vehicle_static_state.wheelbase
			+ std::sin(yaw_angle) * vehicle_static_state.front_wheel_offset;

	veh_front_wheel_right_marker.pose.position.y = pos_y
			+ std::sin(yaw_angle) * vehicle_static_state.wheelbase
			- std::cos(yaw_angle) * vehicle_static_state.front_wheel_offset;

	veh_marker_array.markers.push_back(veh_body_marker);
	veh_marker_array.markers.push_back(veh_orientation_marker);

	veh_marker_array.markers.push_back(veh_rear_wheel_left_marker);
	veh_marker_array.markers.push_back(veh_rear_wheel_right_marker);

	veh_marker_array.markers.push_back(veh_rearF_wheel_left_marker);
	veh_marker_array.markers.push_back(veh_rearF_wheel_right_marker);

	veh_marker_array.markers.push_back(veh_front_wheel_left_marker);
	veh_marker_array.markers.push_back(veh_front_wheel_right_marker);

	return veh_marker_array;
}

const visualization_msgs::MarkerArray& get_path_marker_array(
		const std::vector<GpsPath>& paths)
{
	auto marker_lifetime = ros::Duration(2.0);

	path_marker_array.markers.clear();

	int n_path = 0;
	for (auto path : paths)
	{
		visualization_msgs::Marker path_marker;

		path_marker.header.frame_id = "map";
		path_marker.id = n_path;

		path_marker.type = visualization_msgs::Marker::LINE_STRIP;
		path_marker.action = visualization_msgs::Marker::ADD;

		path_marker.scale.x = 0.2;

		GpsPath::VisualizationColor color = path.color();

		color_from_argb(path_marker.color, color.a, color.r, color.g, color.b);

		path_marker.lifetime = marker_lifetime;

		for (size_t k = 0; k < path.size(); ++k)
		{
			geometry_msgs::Point p;
			p.x = path.x()[k] - paths[0].x().front();
			p.y = path.y()[k] - paths[0].y().front();
			p.z = 0;

			path_marker.points.push_back(p);

		}
		
		path_marker_array.markers.push_back(path_marker);

		++n_path;
	}

	return path_marker_array;
}

const visualization_msgs::MarkerArray& get_trajectory_marker(
		const std::vector<double>& traj_x, const std::vector<double>& traj_y,
		const double trajectory_target_x, const double trajectory_target_y,
		const GpsPath& path)
{
	trajectory_marker.points.clear();

	trajectory_marker.action = visualization_msgs::Marker::ADD;

	if (traj_x.size() == 0)
	{
		geometry_msgs::Point p;
		p.x = 0;
		p.y = 0;
		p.z = 0;

		trajectory_marker.points.push_back(p);
	}
	else
	{
		for (size_t k = 0; k < traj_x.size(); ++k)
		{
			geometry_msgs::Point p;
			p.x = traj_x[k] - path.x().front();
			p.y = traj_y[k] - path.y().front();
			p.z = 0.7;

			trajectory_marker.points.push_back(p);
		}
	}

	trajectory_target_point_marker.pose.position.x = trajectory_target_x - path.x().front();
	trajectory_target_point_marker.pose.position.y = trajectory_target_y - path.y().front();

	trajectory_marker_array.markers.clear();
	trajectory_marker_array.markers.push_back(trajectory_marker);
	trajectory_marker_array.markers.push_back(trajectory_target_point_marker);

	return trajectory_marker_array;
}

void init(const VehicleStaticState& vehicle_static_state)
{
	auto marker_lifetime = ros::Duration(0.5);

	// path marker
	path_marker.header.frame_id = "map";
	path_marker.id = 0;
	path_marker.type = visualization_msgs::Marker::LINE_STRIP;
	path_marker.action = visualization_msgs::Marker::ADD;

	path_marker.scale.x = 0.2;
	color_from_argb(path_marker.color, 1.0, 0, 0.9, 0);
	path_marker.lifetime = marker_lifetime;

	// trajectory marker
	trajectory_marker.header.frame_id = "map";
	trajectory_marker.id = 0;
	trajectory_marker.type = visualization_msgs::Marker::LINE_STRIP;
	trajectory_marker.action = visualization_msgs::Marker::ADD;

	trajectory_marker.scale.x = 0.1;
	color_from_argb(trajectory_marker.color, 1.0, 0, 0.2, 1.0);
	trajectory_marker.lifetime = marker_lifetime;

	trajectory_target_point_marker.header.frame_id = "map";
	trajectory_target_point_marker.id = 1;
	trajectory_target_point_marker.type = visualization_msgs::Marker::CYLINDER;
	trajectory_target_point_marker.action = visualization_msgs::Marker::ADD;

	trajectory_target_point_marker.scale.x = 0.5;
	trajectory_target_point_marker.scale.y = 0.5;
	trajectory_target_point_marker.scale.z = 2.0;

	color_from_argb(trajectory_target_point_marker.color, 1.0, 0, 0, 0);
	trajectory_target_point_marker.lifetime = marker_lifetime;

	// vehicle body marker
	veh_body_marker.header.frame_id = "map";
	veh_body_marker.id = 0;
	veh_body_marker.type = visualization_msgs::Marker::CUBE;
	veh_body_marker.action = visualization_msgs::Marker::ADD;

	veh_body_marker.scale.x = vehicle_static_state.length;
	veh_body_marker.scale.y = vehicle_static_state.width;
	veh_body_marker.scale.z = 0.1;

	color_from_argb(veh_body_marker.color, 1.0, 254 / 255.0 * 0.8,
			196 / 255.0 * 0.8, 14 / 255.0 * 0.8);

	veh_body_marker.lifetime = marker_lifetime;

	// vehicle orientation marker
	veh_orientation_marker.header.frame_id = "map";
	veh_orientation_marker.id = 1;
	veh_orientation_marker.type = visualization_msgs::Marker::ARROW;
	veh_orientation_marker.action = visualization_msgs::Marker::ADD;

	veh_orientation_marker.scale.y = 0.8;
	veh_orientation_marker.scale.z = 0.1;
	veh_orientation_marker.pose.position.z = 0.5;

	color_from_argb(veh_orientation_marker.color, 1.0, 254 / 255.0 * 0.5,
			196 / 255.0 * 0.5, 14 / 255.0 * 0.5);

	veh_orientation_marker.lifetime = marker_lifetime;

	// vehicle steering desired marker
	veh_steering_desired_marker.header.frame_id = "map";
	veh_steering_desired_marker.id = 2;
	veh_steering_desired_marker.type = visualization_msgs::Marker::ARROW;
	veh_steering_desired_marker.action = visualization_msgs::Marker::ADD;

	veh_steering_desired_marker.scale.x = 2.0;
	veh_steering_desired_marker.scale.y = 0.1;
	veh_steering_desired_marker.scale.z = 0.1;

	veh_steering_desired_marker.pose.position.z = 1.5;
	color_from_argb(veh_steering_desired_marker.color, 1.0, 0.1, 0.1, 0.1);
	veh_steering_desired_marker.lifetime = marker_lifetime;

	veh_steering_actual_marker = veh_steering_desired_marker;

	// vehicle wheel markers
	veh_rear_wheel_left_marker.header.frame_id = "map";
	veh_rear_wheel_left_marker.id = 3;
	veh_rear_wheel_left_marker.type = visualization_msgs::Marker::CUBE;
	veh_rear_wheel_left_marker.action = visualization_msgs::Marker::ADD;

	veh_rear_wheel_left_marker.scale.x = vehicle_static_state.wheel_diameter;
	veh_rear_wheel_left_marker.scale.y = vehicle_static_state.wheel_width;
	veh_rear_wheel_left_marker.scale.z = 0.3;

	veh_rear_wheel_left_marker.pose.position.z = 0;
	color_from_argb(veh_rear_wheel_left_marker.color, 1.0, 0.1, 0.1, 0.1);
	veh_rear_wheel_left_marker.lifetime = marker_lifetime;

	veh_rear_wheel_right_marker = veh_rear_wheel_left_marker;
	veh_rear_wheel_right_marker.id = 4;

	veh_front_wheel_right_marker = veh_rear_wheel_left_marker;
	veh_front_wheel_right_marker.id = 5;

	veh_front_wheel_left_marker = veh_rear_wheel_left_marker;
	veh_front_wheel_left_marker.id = 6;

	veh_rearF_wheel_right_marker = veh_rear_wheel_left_marker;
	veh_rearF_wheel_right_marker.id = 7;

	veh_rearF_wheel_left_marker = veh_rear_wheel_left_marker;
	veh_rearF_wheel_left_marker.id = 8;
	

	// control point markers
	closest_path_point_marker.header.frame_id = "map";
	closest_path_point_marker.id = 0;
	closest_path_point_marker.type = visualization_msgs::Marker::CYLINDER;
	closest_path_point_marker.action = visualization_msgs::Marker::ADD;

	closest_path_point_marker.scale.x = 0.5;
	closest_path_point_marker.scale.y = 0.5;
	closest_path_point_marker.scale.z = 2.0;

	color_from_argb(closest_path_point_marker.color, 1.0, 0, 0.2, 1.0);
	closest_path_point_marker.lifetime = marker_lifetime;

	look_ahead_point_marker.header.frame_id = "map";
	look_ahead_point_marker.id = 1;
	look_ahead_point_marker.type = visualization_msgs::Marker::CYLINDER;
	look_ahead_point_marker.action = visualization_msgs::Marker::ADD;

	look_ahead_point_marker.scale.x = 0.5;
	look_ahead_point_marker.scale.y = 0.5;
	look_ahead_point_marker.scale.z = 2.0;

	color_from_argb(look_ahead_point_marker.color, 1.0, 1.0, 0, 0);
	look_ahead_point_marker.lifetime = marker_lifetime;

	look_ahead_obstacle_point_marker.header.frame_id = "map";
	look_ahead_obstacle_point_marker.id = 10;
	look_ahead_obstacle_point_marker.type = visualization_msgs::Marker::CYLINDER;
	look_ahead_obstacle_point_marker.action = visualization_msgs::Marker::ADD;

	look_ahead_obstacle_point_marker.scale.x = 1.5;
	look_ahead_obstacle_point_marker.scale.y = 1.5;
	look_ahead_obstacle_point_marker.scale.z = 2.0;

	color_from_argb(look_ahead_obstacle_point_marker.color, 1.0, 1.0, 0, 0);
	look_ahead_obstacle_point_marker.lifetime = marker_lifetime;

	obstacle_path_point_marker.header.frame_id = "map";
	obstacle_path_point_marker.id = 11;
	obstacle_path_point_marker.type = visualization_msgs::Marker::CYLINDER;
	obstacle_path_point_marker.action = visualization_msgs::Marker::ADD;

	obstacle_path_point_marker.scale.x = 1.5;
	obstacle_path_point_marker.scale.y = 1.5;
	obstacle_path_point_marker.scale.z = 2.0;

	color_from_argb(obstacle_path_point_marker.color, 1.0, 2.0, 0.0, 1.0);
	obstacle_path_point_marker.lifetime = marker_lifetime;
}

}
