#include "VelocityController.h"


void VelocityController::init(DynamicParameters dynamic_params,
		const std::vector<double> p_stat_map_v,
		const std::vector<double> p_stat_map_p)
{
	params = dynamic_params;
	this->p_stat_map_v = p_stat_map_v;
	this->p_stat_map_p = p_stat_map_p;

	this->last_gas_brake_value = 0;
}

void VelocityController::reset(double initial_gas_brake_val)
{
	this->last_gas_brake_value = initial_gas_brake_val;
}

void VelocityController::update_parameters(DynamicParameters dynamic_params)
{
	params = dynamic_params;
}

double VelocityController::get_p_stat(const double target_velocity)
{
	return HelperFunctions::interpolate(p_stat_map_v, p_stat_map_p,
			target_velocity);
}

void VelocityController::export_map()
{
	std::ofstream out_file("velocity_controller_map.csv"); // write json string to file

	for (double current_velocity = 0.0; current_velocity < 15.0; current_velocity += 0.1)
	{
		for (double target_velocity = 0.0; target_velocity < 15.0; target_velocity += 0.1)
		{
			double p = get_gas_brake_value(current_velocity, target_velocity);

			out_file << current_velocity << ", " << target_velocity << ", " << p << std::endl;
		}
	}

	out_file.close();
}

double VelocityController::get_gas_brake_value(const double current_velocity,
		const double target_velocity)
{
	// stationary gas position p_stat, to reach the target_velocity on a level surface
	double p_stat = get_p_stat(target_velocity);

	// velocity difference at which a gas/brake value of 0 is applied
	double delta_v_zero = -p_stat / params.a_gas_neg;

	double delta_v = target_velocity - current_velocity;

	double gas_brake_val = p_stat;

	if (delta_v > 0)
	{
		gas_brake_val += delta_v * params.a_gas_pos;
	}
	else if (delta_v > delta_v_zero)
	{
		gas_brake_val += delta_v * params.a_gas_neg;
	}
	else
	{
		gas_brake_val += delta_v * params.a_brake;
	}

	if (target_velocity < 0.2) // breaking to standstill
	{
		gas_brake_val += params.p_idle; // p_idle is < 0
	}

	// limit max/min allowed gas/brake values
	if (gas_brake_val < params.p_min)
		gas_brake_val = params.p_min;
	if (gas_brake_val > params.p_max)
		gas_brake_val = params.p_max;

	// low pass filtering
	gas_brake_val = params.filter_coeff * last_gas_brake_value + (1 - params.filter_coeff) * gas_brake_val;
	last_gas_brake_value = gas_brake_val;

	return gas_brake_val;
}

double VelocityController::get_target_velocity(const double recorded_velocity,
		const double curvature)
{
	target_velocity = recorded_velocity;

	// calculate maximum safe velocity (based on maximum lateral acceleration)
	v_max_safe = std::sqrt(
			params.acc_lat_max / std::max(0.0001, std::fabs(curvature)));

	if (target_velocity > v_max_safe)
		target_velocity = v_max_safe;
	else
	{
		if (target_velocity > params.v_max) target_velocity = params.v_max;
		if (target_velocity < params.v_min) target_velocity = params.v_min;
	}
	
	return target_velocity;
}


double VelocityController::get_obstacle_velocity(double& velocity, const bool& front_emergency)
{   
	if (front_emergency == 1) velocity = -0.7;
		
	else if(velocity > 0)
	{
		velocity = velocity - velocity*velocity;
		if(velocity < 0.09) velocity = -0.09;
	}
					
	else if (velocity <= 0 && velocity > -0.15) velocity = velocity - velocity*velocity;	

	else 
	{ 
		velocity = 0.0; 
	}	

	return velocity;
}
