/** @file SteeringController.cpp
 @brief Implementation of class Timeout
 @author Tobias Brunner
 */

#include "Timeout.h"

using std::chrono::milliseconds;
using std::chrono::steady_clock;

Timeout::Timeout()
{
	m_last_signal = steady_clock::now() - std::chrono::seconds(1000); // time_point long ago
}

Timeout::Timeout(double timeout_seconds)
{
	m_last_signal = steady_clock::now() - std::chrono::seconds(1000); // time_point long ago
	m_timeout = milliseconds(static_cast<int>(timeout_seconds * 1000));
}

bool Timeout::timed_out()
{
	if ((steady_clock::now() - m_last_signal) > m_timeout)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void Timeout::got_signal()
{
	m_last_signal = steady_clock::now();
}

void Timeout::set_timeout(double timeout_seconds)
{
	m_timeout = milliseconds(static_cast<int>(timeout_seconds * 1000));
}

