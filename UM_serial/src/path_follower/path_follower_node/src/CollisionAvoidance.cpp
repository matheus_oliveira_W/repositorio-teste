/** @file CollisionAvoidance.cpp
 @brief Implementation of class CollisionAvoidance
 @author Luiza Bartels
 */

#include "CollisionAvoidance.h"

#include <iostream>

void CollisionAvoidance::lidar_fusion()
{
	sensor_fused_h.clear();
	sensor_fused_x.clear();
	sensor_fused_y.clear();
	sensor_fused_ang.clear();

	int menor = 50;

	if ((left_lidar_x.size() < menor) && (left_lidar_x.size() > 0))
	{
		k = left_lidar_x.size() - 1;
		menor = left_lidar_x.size();
	}

	if ((right_lidar_x.size() < menor) && (right_lidar_x.size() > 0))
	{
		k = right_lidar_x.size() - 1;
		menor = right_lidar_x.size();
	}

	if ((front_radar_x.size() < menor) && (front_radar_x.size() > 0))
	{
		k = front_radar_x.size() - 1;
		menor = front_radar_x.size();
	}

	if (k > 5) k = 5;
	else k = k;

	for (size_t i = 0; i < k; ++i)
	{
		
		if(left_lidar_x.empty() != true)
		{
			double h1 = std::sqrt(left_lidar_x[i] * left_lidar_x[i] + left_lidar_y[i] * left_lidar_y[i]);
			sensor_fused_h.push_back(h1);
			sensor_fused_x.push_back(left_lidar_x[i]);
			sensor_fused_y.push_back(left_lidar_y[i]);
			sensor_fused_ang.push_back(left_lidar_ang[i]);
		}
		if(right_lidar_x.empty() != true)
		{
			double h2 = std::sqrt(right_lidar_x[i] * right_lidar_x[i] + right_lidar_y[i] * right_lidar_y[i]);
			sensor_fused_h.push_back(h2);
			sensor_fused_x.push_back(right_lidar_x[i]);
			sensor_fused_y.push_back(right_lidar_y[i]);
			sensor_fused_ang.push_back(right_lidar_ang[i]);
		}
		if(front_radar_x.empty() != true)
		{
			double h3 = std::sqrt(front_radar_x[i] * front_radar_x[i] + front_radar_y[i] * front_radar_y[i]);
			sensor_fused_h.push_back(h3);
			sensor_fused_x.push_back(front_radar_x[i]);
			sensor_fused_y.push_back(front_radar_y[i]);
			sensor_fused_ang.push_back(front_radar_ang[i]);
		}

	}

}

void CollisionAvoidance::callback_sick_front_left(sick_ldmrs_msgs::ObjectArray175 msg)
{
	left_lidar_x.clear();
	left_lidar_y.clear();
	left_lidar_ang.clear();

	for (const auto& left_objects : msg.objects_175)
	{
		left_lidar_x.push_back(left_objects.object_box_center.pose.position.x);
		left_lidar_y.push_back(left_objects.object_box_center.pose.position.y + TRUCK_WIDTH * 0.5);
		left_lidar_ang.push_back((180.00 / PI) * atan2((left_objects.object_box_center.pose.position.y + TRUCK_WIDTH * 0.5),
										left_objects.object_box_center.pose.position.x));
		left_lidar_size_x.push_back(left_objects.object_box_size.x);
		left_lidar_size_y.push_back(left_objects.object_box_size.y);
	}
}

void CollisionAvoidance::callback_sick_front_right(sick_ldmrs_msgs::ObjectArray224 msg)
{
	right_lidar_x.clear();
	right_lidar_y.clear();
	right_lidar_ang.clear();

	for (const auto& right_objects : msg.objects_224)
	{
		right_lidar_x.push_back(right_objects.object_box_center.pose.position.x);
		right_lidar_y.push_back(right_objects.object_box_center.pose.position.y - TRUCK_WIDTH * 0.5);
		right_lidar_ang.push_back((180.00 / PI) * atan2((right_objects.object_box_center.pose.position.y - TRUCK_WIDTH * 0.5),
										right_objects.object_box_center.pose.position.x));
		right_lidar_size_x.push_back(right_objects.object_box_size.x); 
		right_lidar_size_y.push_back(right_objects.object_box_size.y); 
	}
}

void CollisionAvoidance::callback_radar_front(derived_object_msgs::ObjectWithCovarianceArray msg)
{
	front_radar_x.clear();
	front_radar_y.clear();
	front_radar_ang.clear();

	for (const auto& front_radar_objects : msg.objects)
	{
		front_radar_x.push_back(front_radar_objects.pose.pose.position.x);
		front_radar_y.push_back(front_radar_objects.pose.pose.position.y);
		front_radar_ang.push_back((180.00 / PI) * atan2(front_radar_objects.pose.pose.position.y, 
									 	front_radar_objects.pose.pose.position.x));
	}
}

double CollisionAvoidance::get_sensor_range_distance(double velocity, double base_distance, double dist_factor)
{
	double dist = (base_distance + velocity) * dist_factor;

	return dist;
}

double CollisionAvoidance::get_sensor_range_curve_distance(double velocity, double base_distance, double dist_factor, double curve_value)
{
	double dist = (base_distance + velocity) * (dist_factor - curve_value);

	return dist;
}

void CollisionAvoidance::nearest_obstacle()
{
	int menor = 50;

	if ((left_lidar_x.size() < menor) && (left_lidar_x.size() > 0))
	{
		k = left_lidar_x.size() - 1;
		menor = left_lidar_x.size();
	}
	if ((right_lidar_x.size() < menor) && (right_lidar_x.size() > 0))
	{
		k = right_lidar_x.size() - 1;
		menor = right_lidar_x.size();
	}
	if ((front_radar_x.size() < menor) && (front_radar_x.size() > 0))
	{
		k = front_radar_x.size() - 1;
		menor = front_radar_x.size();
	}

	if (k > 5) k = 5;
	else k = k;

	obj0_x = 999.99;
	obj0_y = 999.99;
	ang0 = 999.99;
	obj1_y = 999.99;
	ang1 = 999.99;

	for (size_t i = 0; i < k; ++i)
	{
		double min = 70.00;
	
			if(sensor_fused_x[i] < min && sensor_fused_x[i] < obj0_x)
			{
				obj1_x = obj0_x;
				obj1_y = obj0_y;
				ang1 = ang0;

				obj0_x = sensor_fused_x[i];
				obj0_y = sensor_fused_y[i];
				ang0 = sensor_fused_ang[i];
			}
			else if (sensor_fused_x[i] < min && sensor_fused_x[i] > obj0_x)
			{
				if (sensor_fused_x[i] < obj1_x)
				{
					obj1_x = sensor_fused_x[i];
					obj1_y = sensor_fused_y[i];
					ang1 = sensor_fused_ang[i];
				}
				else return;
				
			}
		
		
	}
}


/*SE X FOR MENOR QUE O MINIMO E MENOR QUE O VETOR DE POSICAO 0:
SUBSTITUI O VEOTR DE POSICAO 0 E COLOCA O ANTIGO NA POSICAO 1

SE X FOR MENOR QUE O MINIMO E MAIOR QUE O VETOR DE POSICAO 0:
VETOR D EPOS 0 PERMANECE E COLOCA O NOVO NA POSICAO 1
*/