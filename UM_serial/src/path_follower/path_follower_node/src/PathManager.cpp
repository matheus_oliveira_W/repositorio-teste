#include "PathManager.h"

PathManager::PathManager()
{
	valid_color_pallet = false;
	active_path_index = 0;

	std::string home_dir = boost::filesystem::path(getenv("HOME")).c_str();
	temp_dir = home_dir + "/.tmp";

	boost::filesystem::create_directories(temp_dir);
}
void PathManager::add_path()
{
	add_path(std::string("path_" + path_vector.size()));
}

void PathManager::add_path(const std::string& path_name)
{
	add_path(path_name, reverse, go_zone_radius_default, wait_zone_radius_default);
}

void PathManager::add_path(const std::string& path_name, const int reverse,
		const double go_zone_radius, const double wait_zone_radius)
{
	path_vector.push_back(GpsPath());

	path_vector.back().set_name(path_name);
	path_vector.back().init(go_zone_radius, wait_zone_radius);

	if (valid_color_pallet)
	{
		int color_index = (path_vector.size() - 1) % color_pallet.size();

		path_vector.back().set_color(color_pallet[0][color_index],
				color_pallet[1][color_index], color_pallet[2][color_index],
				color_pallet[3][color_index]);
	}
	else
	{
		path_vector.back().set_color(1.0, 0, 1.0, 0);
	}
}

unsigned int PathManager::active_path_id() const
{
	return active_path_index;
}

void PathManager::post_process_path(const double max_seg_length,
		const double epsilon, const double curvature_smoothing_factor)
{
	active_path().post_process(max_seg_length, epsilon,
			curvature_smoothing_factor);
}

const std::vector<GpsPath>& PathManager::paths() const
{
	return path_vector;
}

double PathManager::x_pos_ref() const
{
	auto const_this = const_cast<PathManager*>(this);

	if (const_this->reference_path().has_points())
	{
		return const_this->reference_path().x().front();
	}
	else
	{
		return 0;
	}
}

double PathManager::y_pos_ref() const
{
	auto const_this = const_cast<PathManager*>(this);

	if (const_this->reference_path().has_points())
	{
		return const_this->reference_path().y().front();
	}
	else
	{
		return 0;
	}
}

GpsPath& PathManager::active_path()
{
	/*if (active_path_index >= path_vector.size())
	 {
	 throw std::range_error("Error: active_path_index is out of range!");
	 }*/

	return path_vector[active_path_index];
}

GpsPath& PathManager::reference_path()
{
	GpsPath& t = path_vector[0];
	return t;
}

void PathManager::set_active_path_index(const unsigned int index)
{
	if (index >= path_vector.size())
	{
		throw std::range_error(
				"Error setting active_path_index: index "
						+ std::to_string(index) + " is out of range!");
	}

	active_path_index = index;
}

void PathManager::rename_path(const unsigned int index,
		const std::string& new_name)
{
	if (index >= path_vector.size())
	{
		throw std::range_error(
				"Error removing path: index " + std::to_string(index)
						+ " is out of range!");
	}

	path_vector[index].set_name(new_name);
}

void PathManager::remove_path(const unsigned int index)
{
	if (index >= path_vector.size())
	{
		throw std::range_error(
				"Error removing path: index " + std::to_string(index)
						+ " is out of range!");
	}

	if (path_vector.size() == 1)
	{
		throw std::runtime_error(
				"Error removing path: path at index 0 cannot be removed!");
	}

	if (active_path_index > index && active_path_index > 0)
	{
		--active_path_index;
	}

	path_vector.erase(path_vector.begin() + index);
}

void PathManager::reset_paths()
{
	path_vector.clear();
	add_path();
	active_path_index = 0;
}

void PathManager::set_color_pallet(
		const std::vector<std::vector<double>>& color_pallet)
{
	if (color_pallet[0].size() > 0
			&& color_pallet[0].size() == color_pallet[1].size()
			&& color_pallet[0].size() == color_pallet[2].size()
			&& color_pallet[0].size() == color_pallet[3].size())
	{
		valid_color_pallet = true;
		this->color_pallet = color_pallet;
	}
}

void PathManager::save_to_file(const std::string& file_name)
{
	nlohmann::json j;
	j = path_vector; // serialize path to json string

	std::ofstream out_file(file_name); // write json string to file
	out_file << std::setw(4) << j;
	out_file << std::endl; // workaround for eclipse
	out_file.close();

	set_last_path_file(file_name);
}

void PathManager::load_from_file(const std::string& file_name)
{
	std::ifstream in_file(file_name);
	nlohmann::json j;
	in_file >> j;
	in_file.close();

	path_vector = j.get<std::vector<GpsPath>>(); // de-serialize path from json string
	active_path_index = 0;

	set_last_path_file(file_name);
}

void PathManager::set_last_path_file(const std::string& file_path)
{
	std::ofstream out_file(temp_dir + "/last_path");
	out_file << file_path;
	out_file.close();
}

std::string PathManager::get_last_path_file()
{
	std::string file_path = "";

	try
	{
		std::ifstream in_file(temp_dir + "/last_path");
		in_file >> file_path;
		in_file.close();
	} catch (const std::exception& e)
	{
	}

	return file_path;
}

bool PathManager::load_last_path()
{
	std::string last_path_file = get_last_path_file();

	if (last_path_file.size() > 0)
	{
		try
		{
			load_from_file(last_path_file);
			return true;
		} catch (const std::exception& e)
		{
		}
	}

	return false;
}

std::string PathManager::save_to_temp_file()
{
	auto t = std::time(nullptr);
	auto tm = *std::localtime(&t);

	std::stringstream file_path;
	file_path << temp_dir << std::put_time(&tm, "/path_%d-%m-%Y_%H-%M-%S");

	save_to_file(file_path.str());

	set_last_path_file(file_path.str());

	return file_path.str();
}

