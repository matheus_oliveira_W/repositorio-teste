/** @file WatchdogNode.cpp
	@brief Watchdog for checking if an alert should be activated for the driver to take over control.
	@author Tobias Brunner
*/

#include <ros/ros.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Empty.h>

#include <string>
#include <thread>

#include "Timeout.h"

std::thread alert_thread;
bool alert_active, got_alive_signal, alert_received;
Timeout path_follower_timeout;


std::string exec_shell_command(const std::string &cmd)
{
	std::array<char, 128> buffer;
	std::string result;
	std::shared_ptr<FILE> pipe(popen(cmd.c_str(), "r"), pclose);
	if (!pipe) throw std::runtime_error("popen() failed!");

	while (!feof(pipe.get()))
		{
			if (fgets(buffer.data(), 128, pipe.get()) != nullptr)
				result += buffer.data();
		}

  	if (result.length() > 0)
	{
		ROS_WARN_STREAM(result);
	}

  return result;
}

void start_alert()
{
	if (!alert_active)
	{
		alert_thread = std::thread([&]()
				{
					alert_active = true;
					exec_shell_command("~/.alert/alert.sh");
				});
	}
}

void stop_alert()
{
	if (alert_active)
	{
		// no thread needed, wont block
		exec_shell_command("~/.alert/stop_alert.sh");

		alert_thread.join();
		alert_active = false;
	}
}

void timer_callback(const ros::TimerEvent& event)
{
	if ((path_follower_timeout.timed_out() && got_alive_signal) || alert_received)
	{
		if (!alert_active)
		{
			start_alert();
		}
	}
	else
	{
		if (alert_active)
		{
			stop_alert();
		}
	}
}

void alert_callback(std_msgs::Bool::ConstPtr msg)
{
	if (msg->data)
	{
		alert_received = true;
	}
	else
	{
		alert_received = false;
	}
}

int main(int argc, char** argv)
{   
	alert_active = false;
	got_alive_signal = false;
	alert_received = false;

	path_follower_timeout.set_timeout(0.5);

    ros::init(argc, argv, "alert_node");
   
    auto node_handle = ros::NodeHandle();
    auto private_node_handle = ros::NodeHandle("~");

    auto sub_alert = node_handle.subscribe<std_msgs::Bool>("/alert", 10, alert_callback);
    auto sub_path_follower_alive = node_handle.subscribe<std_msgs::Empty>("/path_follower_node/alive", 10,
    		[&](std_msgs::Empty::ConstPtr msg){ path_follower_timeout.got_signal(); got_alive_signal = true; });
    
    auto timer = node_handle.createTimer(ros::Duration(0.1), timer_callback);

    ros::spin();

    return 0;
}
