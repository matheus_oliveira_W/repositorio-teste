import os
import rospy
import rospkg
import rosservice
import datetime
import math
import time
from decimal import Decimal

#import faulthandler
# faulthandler.enable()

# Qt imports -----------------------------------
from qt_gui.plugin import Plugin
from python_qt_binding import loadUi
from python_qt_binding.QtWidgets import QWidget
from python_qt_binding.QtWidgets import QTreeWidgetItem, QFileDialog, QInputDialog
from python_qt_binding.QtCore import QObject, pyqtSignal, pyqtSlot, QTimer

# Message imports -----------------------------------
from sensor_msgs.msg import NavSatFix, NavSatStatus
from std_msgs.msg import *

from path_follower.srv import *
from path_follower.msg import ServiceCommand, ServiceCommandReply, StateEvents, \
    VehicleState, ControlTargetState, PathInfoArray, SensorState

from timeout import Timeout

COLOR_STR_GREEN = "rgb(130, 255, 120)"
COLOR_STR_LIGHT_GREEN = "rgb(190, 255, 190)"
COLOR_STR_ORANGE = "rgb(255, 203, 70)"
COLOR_STR_RED = "rgb(255, 96, 96)"
COLOR_STR_WHITE = "rgb(255, 255, 255)"
COLOR_STR_BLUE = "rgb(67, 189, 255)"
COLOR_STR_PINK = "rgb(220,20,60)"


def get_label_style_sheet(color_string, margin=0.3):
    return "QLabel {" \
           "background-color:" + color_string + ";" \
        "border: 1px solid gray; " \
        "border-radius: 0px; " \
        "margin-top: " + str(margin) + "em; " \
        "margin-bottom: " + str(margin) + "em; " \
        " }"


class PathFollowerPlugin(Plugin):

    # declare internal SIGNALs for GUI thread communication
    signal_oxts_alive = pyqtSignal(object)
    signal_oxts_data = pyqtSignal(object)

    signal_paravan_alive = pyqtSignal(object)
    signal_uem_data_signal = pyqtSignal(object)
    signal_paravan_gas_brake_norm_desired = pyqtSignal(object)
    signal_paravan_steering_norm_desired = pyqtSignal(object)

    signal_active_path_id = pyqtSignal(object)
    signal_path_follower_node_alive = pyqtSignal(object)
    signal_path_info = pyqtSignal(object)
    signal_state_event = pyqtSignal(object)
    signal_vehicle_state = pyqtSignal(object)
    signal_control_target_state = pyqtSignal(object)

    signal_sensor_state = pyqtSignal(object)

    def __init__(self, context):
        super(PathFollowerPlugin, self).__init__(context)

        # Give QObjects reasonable names
        self.setObjectName('Path_Follower_GUI')

        # Create QWidget
        self._widget = QWidget()

        # Get path to UI file which should be in the "resource" folder of this package
        ui_file = os.path.join(rospkg.RosPack().get_path(
            'path_follower'), 'resource', 'path_follower_gui.ui')

        # Extend the widget with all attributes and children from UI file
        loadUi(ui_file, self._widget)

        # Give QObjects reasonable names
        self._widget.setObjectName('PathFollower_GUI')

        # sleep for 1 second to avoid crashes during GUI initialization
        time.sleep(1)

        # connect GUI elements to callback functions -------------------------
        self._widget.btn_recording.clicked.connect(self.callback_btn_recording)
        self._widget.btn_following.clicked.connect(self.callback_btn_following)
        self._widget.btn_post_process.clicked.connect(
            self.callback_btn_post_processing)
        self._widget.btn_save_path.clicked.connect(self.callback_btn_save_path)
        self._widget.btn_load_path.clicked.connect(self.callback_btn_load_path)
        self._widget.btn_plot_mapping_data.clicked.connect(
            self.callback_btn_plot_mapping_data)
        self._widget.btn_plot_path_data.clicked.connect(
            self.callback_btn_plot_path_data)
        self._widget.btn_addPath.clicked.connect(self.callback_btn_add_path)
        self._widget.btn_removePath.clicked.connect(
            self.callback_btn_remove_path)
        self._widget.btn_renamePath.clicked.connect(
            self.callback_btn_rename_path)

        self._widget.sb_go_zone_radius.valueChanged.connect(
            self.callback_sb_go_zone_radius)
        self._widget.sb_wait_zone_radius.valueChanged.connect(
            self.callback_sb_wait_zone_radius)
        self._widget.cb_show_go_zone.clicked.connect(
            self.callback_cb_show_go_zone)
        self._widget.cb_show_wait_zone.clicked.connect(
            self.callback_cb_show_wait_zone)
        self._widget.cb_idle_braking.clicked.connect(
            self.callback_cb_idle_braking)
        self._widget.comboBox_paths.currentIndexChanged.connect(
            self.callback_path_selected)
        self._widget.cb_reverse.clicked.connect(
            self.callback_cb_reverse)

        self._widget.sb_go_zone_radius.lineEdit().setReadOnly(True)
        self._widget.sb_wait_zone_radius.lineEdit().setReadOnly(True)

        # Timeouts ------------------------------------------------------------
        self.timeout_path_follower_node_alive = Timeout(0.5)
        self.timeout_oxts_node_alive = Timeout(0.5)
        self.timeout_paravan_node_alive = Timeout(0.5)
        self.timeout_oxts_data = Timeout(0.5)
        self.timeout_paravan_data = Timeout(3.0)
        self.timeout_control_target_state = Timeout(0.5)
        self.timeout_vehicle_state = Timeout(0.5)
        self.timeout_rc_available = Timeout(0.5)

        def make_subscriber(topic_name, msg_type, callback_signal):
            rospy.Subscriber(topic_name, msg_type,
                             lambda msg: callback_signal.emit(msg))

        # subscribers ----------------------------------------------------------
        make_subscriber("oxts/alive", Empty, self.signal_oxts_alive)
        make_subscriber("oxts/nav_sat_fix", NavSatFix, self.signal_oxts_data)

        make_subscriber("paravan/alive", Empty, self.signal_paravan_alive)
        make_subscriber("/uem/data_signal", Empty, self.signal_uem_data_signal)
        
        make_subscriber("/gas_brake_norm_desired", Float32,
                        self.signal_paravan_gas_brake_norm_desired)

        make_subscriber("/steering_norm_desired", Float32, 
                        self.signal_paravan_steering_norm_desired)
        
        make_subscriber("path_follower_node/alive", Empty,
                        self.signal_path_follower_node_alive)
        make_subscriber("path_follower_node/path_info_array",
                        PathInfoArray, self.signal_path_info)
        make_subscriber("path_follower_node/active_path_id",
                        UInt8, self.signal_active_path_id)
        make_subscriber("path_follower_node/state_event",
                        UInt8, self.signal_state_event)
        make_subscriber("path_follower_node/vehicle_state",
                        VehicleState, self.signal_vehicle_state)
        make_subscriber("path_follower_node/control_target_state",
                        ControlTargetState, self.signal_control_target_state)
        make_subscriber("path_follower_node/sensor_state",
                        SensorState, self.signal_sensor_state)

        # SIGNAL connections
        self.signal_oxts_alive.connect(self.callback_oxts_alive)
        self.signal_oxts_data.connect(self.callback_oxts_data)

        self.signal_paravan_alive.connect(self.callback_paravan_alive)
        self.signal_uem_data_signal.connect(self.callback_uem_data_signal)

        self.signal_paravan_gas_brake_norm_desired.connect(
            self.callback_paravan_gas_brake_norm_desired)

        self.signal_paravan_steering_norm_desired.connect(
            self.callback_paravan_steering_norm_desired)

        self.signal_path_follower_node_alive.connect(
            self.callback_path_follower_node_alive)
        self.signal_path_info.connect(self.callback_path_info)
        self.signal_active_path_id.connect(self.callback_active_path_id)
        self.signal_state_event.connect(self.callback_state_event)
        self.signal_vehicle_state.connect(self.callback_vehicle_state)
        self.signal_control_target_state.connect(
            self.callback_control_target_state)
        
        self.signal_sensor_state.connect(
            self.callback_sensor_state)

        # member storage for received message data
        self.active_path_id = None
        self.gui_settings = None
        self.nav_sat_fix_msg = None

        self.notification_color = COLOR_STR_WHITE
        self.notification_text = ""
        self.new_service_notification = False

        self.is_recording = False
        self.is_following = False

        # timer ----------------------------------------------------------
        self.gui_timer = QTimer()
        self.gui_timer.timeout.connect(self.update_gui)
        self.gui_timer.start(200)  # duration in ms

        # Show _widget.windowTitle on left-top of each plugin (when it's set in _widget)
        if context.serial_number() > 1:
            self._widget.setWindowTitle(
                self._widget.windowTitle() + (' (%d)' % context.serial_number()))

        context.add_widget(self._widget)

    """ helper functions """

    """ oxts_node callbacks for ROS messages """

    def callback_oxts_alive(self, msg):
        self.timeout_oxts_node_alive.got_signal()

    def callback_oxts_data(self, nav_sat_fix):
        self.timeout_oxts_data.got_signal()
        self.nav_sat_fix_msg = nav_sat_fix

    """ paravan_node callbacks for ROS messages"""

    def callback_paravan_alive(self, msg):
        self.timeout_paravan_node_alive.got_signal()
        
    def callback_paravan_rc_available(self, msg):
        self.timeout_rc_available.got_signal()

    def callback_uem_data_signal(self, msg):
        self.timeout_paravan_data.got_signal()

    """ path_follower_node callbacks for ROS messages"""

    def callback_paravan_steering_norm_desired(self, float_msg):
        
        steering_norm_desired = -float_msg.data*50+50

        steering_norm_desired = int(steering_norm_desired)

        self._widget.label_steering_desired.setText(str(steering_norm_desired))


    def callback_paravan_gas_brake_norm_desired(self, float_msg):

        gas_brake_norm_desired = float_msg.data

        self._widget.pb_gas_desired.setFormat("%p%")
        self._widget.pb_brake_desired.setFormat("%p%")

        if gas_brake_norm_desired > 0:
            self._widget.pb_gas_desired.setValue(
                int(gas_brake_norm_desired * 100))
            self._widget.pb_brake_desired.setValue(0)
        else:
            self._widget.pb_gas_desired.setValue(0)
            self._widget.pb_brake_desired.setValue(
                -int(gas_brake_norm_desired * 100))

    def callback_path_follower_node_alive(self, msg):
        self.timeout_path_follower_node_alive.got_signal()

    def callback_state_event(self, state_event_msg):

        if state_event_msg.data == StateEvents.RECORDING:
            self.is_recording = True
            self.is_following = False

            # "System State"
            self.set_system_state(COLOR_STR_GREEN, "Gravando ...")

            # "Path Management"
            self._widget.btn_recording.setText("Interromper gravacao")
            self._widget.btn_save_path.setEnabled(False)
            self._widget.btn_load_path.setEnabled(False)
            self._widget.btn_post_process.setEnabled(False)
            self._widget.btn_recording.setEnabled(True)
            self._widget.btn_plot_mapping_data.setEnabled(True)
            self._widget.btn_plot_path_data.setEnabled(True)
            self._widget.cb_show_go_zone.setEnabled(False)
            self._widget.cb_show_wait_zone.setEnabled(False)
            self._widget.cb_idle_braking.setEnabled(False)
            self._widget.sb_go_zone_radius.setEnabled(False)
            self._widget.sb_wait_zone_radius.setEnabled(False)
            self._widget.cb_reverse.setEnabled(False)

            # "Drive Management"
            self._widget.btn_following.setText("COMECAR MISSAO")
            self._widget.btn_following.setEnabled(False)

        elif state_event_msg.data == StateEvents.FOLLOWING:
            self.is_recording = False
            self.is_following = True

            # "System State"
            self.set_system_state(COLOR_STR_GREEN, "Navegando ...")

            # "Path Management"
            self._widget.btn_recording.setText("GRAVAR MISSAO")
            self._widget.btn_save_path.setEnabled(False)
            self._widget.btn_load_path.setEnabled(False)
            self._widget.btn_post_process.setEnabled(False)
            self._widget.btn_recording.setEnabled(False)
            self._widget.btn_plot_mapping_data.setEnabled(True)
            self._widget.btn_plot_path_data.setEnabled(True)
            self._widget.cb_show_go_zone.setEnabled(False)
            self._widget.cb_show_wait_zone.setEnabled(False)
            self._widget.cb_idle_braking.setEnabled(True)
            self._widget.sb_go_zone_radius.setEnabled(False)
            self._widget.sb_wait_zone_radius.setEnabled(False)
            self._widget.comboBox_paths.setEnabled(False)
            self._widget.btn_addPath.setEnabled(False)
            self._widget.btn_removePath.setEnabled(False)
            self._widget.btn_renamePath.setEnabled(False)
            self._widget.cb_reverse.setEnabled(True)

            # "Drive Management"
            self._widget.btn_following.setText("Interromper navegacao ...")
            self._widget.btn_following.setEnabled(True)

        elif state_event_msg.data == StateEvents.IDLE:
            if self.is_following:
                self.set_notification(
                    COLOR_STR_GREEN, "Navegacao interrompida!")

            self.is_recording = False
            self.is_following = False

            # "System State"
            self.set_system_state(COLOR_STR_BLUE, "Online")

            # "Path Management"
            self._widget.btn_recording.setText("GRAVAR MISSAO")
            self._widget.btn_save_path.setEnabled(True)
            self._widget.btn_load_path.setEnabled(True)
            self._widget.btn_post_process.setEnabled(True)
            self._widget.btn_recording.setEnabled(True)
            self._widget.btn_plot_mapping_data.setEnabled(True)
            self._widget.btn_plot_path_data.setEnabled(True)
            self._widget.cb_show_go_zone.setEnabled(True)
            self._widget.cb_show_wait_zone.setEnabled(True)
            self._widget.cb_idle_braking.setEnabled(True)
            self._widget.sb_go_zone_radius.setEnabled(True)
            self._widget.sb_wait_zone_radius.setEnabled(True)
            self._widget.comboBox_paths.setEnabled(True)
            self._widget.btn_addPath.setEnabled(True)
            self._widget.btn_renamePath.setEnabled(True)
            self._widget.cb_reverse.setEnabled(True)

            # "Drive Management"
            self._widget.btn_following.setText("COMECAR MISSAO")
            self._widget.btn_following.setEnabled(True)

        elif state_event_msg.data == StateEvents.HANDOVER:
            self.set_system_state(COLOR_STR_RED, "Emergencia")
            self.set_notification(COLOR_STR_RED, "Motorista necessario (F8 para interromper alarme)")

        elif state_event_msg.data == StateEvents.WAIT_ZONE_REACHED:
            self.set_notification(COLOR_STR_BLUE, "Entrou na Wait Zone")

        # single instance events - happen only once in a while
        elif state_event_msg.data == StateEvents.RECORDING_FAILED_OXTS_NODE_DEAD:
            self.set_notification(COLOR_STR_RED, "Gravacao interrompida - software OxTS caiu!")

        elif state_event_msg.data == StateEvents.RECORDING_FAILED_OXTS_NO_DATA:
            self.set_notification(COLOR_STR_RED, "Gravacao interrompida - sem dados de GPS!")

        elif state_event_msg.data == StateEvents.RECORDING_FAILED_OXTS_NO_FIX:
            self.set_notification(COLOR_STR_RED, "Gravacao interrompida - sem satelites GPS fixos!")

        elif state_event_msg.data == StateEvents.FOLLOWING_FAILED_OXTS_NODE_DEAD:
            self.set_notification(COLOR_STR_RED, "Navegacao interrompida - software OxTS caiu!")

        elif state_event_msg.data == StateEvents.FOLLOWING_FAILED_OXTS_NO_DATA:
            self.set_notification(COLOR_STR_RED, "Navegacao interrompida - sem dados de GPS!")

        elif state_event_msg.data == StateEvents.FOLLOWING_FAILED_OXTS_NO_FIX:
            self.set_notification(COLOR_STR_RED, "Navegacao interrompida - sem satelites GPS fixos!")
        
        elif state_event_msg.data == StateEvents.FOLLOWING_FAILED_PARAVAN_NODE_DEAD:
            self.set_notification(COLOR_STR_RED, "Navegacao interrompida - software U&M caiu!")
            
        elif state_event_msg.data == StateEvents.FOLLOWING_FAILED_PARAVAN_NO_DATA:
            self.set_notification(COLOR_STR_RED, "Navegacao interrompida - sem dados de atuadores!")
        
        elif state_event_msg.data == StateEvents.FOLLOWING_FAILED_PARAVAN_NO_REMOTE_CONTROL:
            self.set_notification(COLOR_STR_RED, "Navegacao interrompida - Controle Remoto nao disponivel!")

    def callback_active_path_id(self, msg):
        self.active_path_id = msg.data

    def callback_control_target_state(self, msg):
        self.timeout_control_target_state.got_signal()

        self._widget.label_drive_controller_steering_desired.setText(
            str(round(Decimal(math.degrees(msg.steering_angle)), 1)))

        self._widget.label_drive_controller_velocity_desired.setText(
            str(round(Decimal(msg.velocity), 1)))

    def callback_sensor_state(self, msg):

        if msg.left_lidar_state:
                self.set_LLidar_label(COLOR_STR_RED, "Offline")
        else:
                self.set_LLidar_label(COLOR_STR_GREEN, "Online")

        if msg.right_lidar_state:
                self.set_RLidar_label(COLOR_STR_RED, "Offline")
        else:
                self.set_RLidar_label(COLOR_STR_GREEN, "Online")

        if msg.front_radar_state:
                self.set_FRadar_label(COLOR_STR_RED, "Offline")
        else:
                self.set_FRadar_label(COLOR_STR_GREEN, "Online")

        if msg.warning_flag:
            self.set_label_obstacle(COLOR_STR_RED, "OBSTACULO")
        else:
            self.set_label_obstacle(COLOR_STR_GREEN, "LIVRE")

        if msg.warning_flag and msg.emergency_flag == False:
            self.set_label_emergency(COLOR_STR_ORANGE, "ATENCAO!")
        elif msg.warning_flag and msg.emergency_flag:
            self.set_label_emergency(COLOR_STR_RED, "EMERGENCIA")
        else:
            self.set_label_emergency(COLOR_STR_GREEN, "LIVRE")

        self._widget.label_x0.setText(str(round(Decimal(msg.dist_x_0), 2)))
        self._widget.label_y0.setText(str(round(Decimal(msg.dist_y_0), 2)))
        self._widget.label_ang0.setText(str(round(Decimal(msg.ang_0), 2)))

        self._widget.label_x1.setText(str(round(Decimal(msg.dist_x_1), 2)))
        self._widget.label_y1.setText(str(round(Decimal(msg.dist_y_1), 2)))
        self._widget.label_ang1.setText(str(round(Decimal(msg.ang_1), 2)))

        self._widget.label_freio.setText(str(round(Decimal(msg.dist_brake), 2)))
        self._widget.label_curva.setText(str(round(Decimal(msg.dist_curve), 2)))
        self._widget.label_ang.setText(str(round(Decimal(msg.lap_deviation), 2)))
        
        if msg.follow_with_sensors == True:
            self.set_label_sensores(COLOR_STR_GREEN, "c/ Sensores")
        else:
            self.set_label_sensores(COLOR_STR_RED, "s/ Sensores")
        

    def callback_path_info(self, path_info_array_msg):
        if self.active_path_id is None:
            return

        # if necessary, add or remove entries in the path comboBox
        num_paths = len(path_info_array_msg.info)
        while num_paths > self._widget.comboBox_paths.count():
            self._widget.comboBox_paths.addItem("")

        while num_paths < self._widget.comboBox_paths.count():
            self._widget.comboBox_paths.removeItem(
                self._widget.comboBox_paths.count() - 1)

        # if necessary, rename entries in the path comboBox
        for index, path_info_msg in enumerate(path_info_array_msg.info):
            if not path_info_msg.name == self._widget.comboBox_paths.itemText(index):
                self._widget.comboBox_paths.setItemText(
                    index, path_info_msg.name)

        # update path info labels
        if self.active_path_id < len(path_info_array_msg.info):
            # if necessary, adapt the current index to the active_path_id
            if self._widget.comboBox_paths.currentIndex() is not self.active_path_id:
                self._widget.comboBox_paths.blockSignals(True)
                self._widget.comboBox_paths.setCurrentIndex(
                    self.active_path_id)
                self._widget.comboBox_paths.blockSignals(False)

            path_info_msg = path_info_array_msg.info[self.active_path_id]

            r = 255 * path_info_msg.visualization_color.r
            g = 255 * path_info_msg.visualization_color.g
            b = 255 * path_info_msg.visualization_color.b

            self._widget.label_pathColor.setStyleSheet("background-color: rgb({}, {}, {}); \
                border: 1px solid black; border-radius: 5px;".format(r, g, b))

            if path_info_msg.recording_time.secs == 0:
                self._widget.label_infoRecDate.setText("-")
                self._widget.label_infoUtmZone.setText("-")
                self._widget.label_infoNumPoints.setText("-")
                self._widget.label_infoPathLength.setText("-")

            else:
                seconds = path_info_msg.recording_time.secs + \
                    path_info_msg.recording_time.nsecs / 1e9
                date_time = datetime.datetime.fromtimestamp(seconds)

                self._widget.label_infoRecDate.setText(
                    date_time.strftime("%Y-%m-%d %H:%M:%S"))
                self._widget.label_infoUtmZone.setText(
                    str(path_info_msg.utm_zone_string))
                self._widget.label_infoNumPoints.setText(
                    str(path_info_msg.num_points))
                self._widget.label_infoPathLength.setText(
                    str(round(Decimal(path_info_msg.length), 2)))

                # avoid triggering the valueChanged callback signal
                self._widget.sb_go_zone_radius.blockSignals(True)
                self._widget.sb_go_zone_radius.setValue(
                    path_info_msg.go_zone_radius)
                self._widget.sb_go_zone_radius.blockSignals(False)

                # avoid triggering the valueChanged callback signal
                self._widget.sb_wait_zone_radius.blockSignals(True)
                self._widget.sb_wait_zone_radius.setValue(
                    path_info_msg.wait_zone_radius)
                self._widget.sb_wait_zone_radius.blockSignals(False)

    def callback_vehicle_state(self, msg):
        self.timeout_vehicle_state.got_signal()

        if msg.gps_data_valid:
            #self._widget.label_veh_state_x.setText("{:.2f}".format(msg.x))
            #self._widget.label_veh_state_y.setText("{:.2f}".format(msg.y))
            #self._widget.label_veh_state_yaw_angle.setText(
            #    "{:.2f}".format(math.degrees(msg.yaw_angle)))
            #self._widget.label_veh_state_yaw_angle.setText(
            #    "{:.4f}".format(msg.yaw_angle))
            #self._widget.label_veh_state_heading.setText(
            #    "{:.4f}".format(msg.heading))
            #self._widget.label_veh_state_velocity.setText(
            #    "{:.2f}".format(msg.velocity))
            '''
            self._widget.label_veh_state_lidar_left.setText(
                "{:.2f}".format(msg.left_lidar_feedback_x))
            self._widget.label_veh_state_lidar_right.setText(
                "{:.2f}".format(msg.right_lidar_feedback_x))
            self._widget.label_veh_state_radar.setText(
                "{:.2f}".format(msg.radar_feedback_x))
            self._widget.label_veh_state_lidar_left_y.setText(
                "{:.2f}".format(msg.left_lidar_feedback_y))
            self._widget.label_veh_state_lidar_right_y.setText(
                "{:.2f}".format(msg.right_lidar_feedback_y))
            self._widget.label_veh_state_radar_y.setText(
                "{:.2f}".format(msg.radar_feedback_y))

            self._widget.label_veh_state_radar_back.setText(
                "{:.2f}".format(msg.radar_back_feedback_x))
            self._widget.label_veh_state_lidar_back.setText(
                "{:.2f}".format(msg.lidar_back_feedback_x))
            self._widget.label_veh_state_radar_back_y.setText(
                "{:.2f}".format(msg.radar_back_feedback_y))
            self._widget.label_veh_state_lidar_back_y.setText(
                "{:.2f}".format(msg.lidar_back_feedback_y))
            '''

        else:
            #self._widget.label_veh_state_x.setText("-")
            #self._widget.label_veh_state_y.setText("-")
            #self._widget.label_veh_state_yaw_angle.setText("-")
            #self._widget.label_veh_state_heading.setText("-")
            #self._widget.label_veh_state_velocity.setText("-")
            '''
            self._widget.label_veh_state_lidar_left.setText(
                "{:.2f}".format(msg.left_lidar_feedback_x))
            self._widget.label_veh_state_lidar_right.setText(
                "{:.2f}".format(msg.right_lidar_feedback_x))
            self._widget.label_veh_state_radar.setText(
                "{:.2f}".format(msg.radar_feedback_x))

            self._widget.label_veh_state_lidar_left_y.setText(
                "{:.2f}".format(msg.left_lidar_feedback_y))
            self._widget.label_veh_state_lidar_right_y.setText(
                "{:.2f}".format(msg.right_lidar_feedback_y))
            self._widget.label_veh_state_radar_y.setText(
                "{:.2f}".format(msg.radar_feedback_y))

            self._widget.label_veh_state_radar_back.setText(
                "{:.2f}".format(msg.radar_back_feedback_x))
            self._widget.label_veh_state_lidar_back.setText(
                "{:.2f}".format(msg.lidar_back_feedback_x))
            self._widget.label_veh_state_radar_back_y.setText(
                "{:.2f}".format(msg.radar_back_feedback_y))
            self._widget.label_veh_state_lidar_back_y.setText(
                "{:.2f}".format(msg.lidar_back_feedback_y))
            '''


        #if msg.rel_data_valid:
            #self._widget.label_veh_state_x_rel.setText("{:.2f}".format(msg.x_rel))
            #self._widget.label_veh_state_y_rel.setText("{:.2f}".format(msg.y_rel))
        #else:
            #self._widget.label_veh_state_x_rel.setText("-")
            #self._widget.label_veh_state_y_rel.setText("-")

        '''
        if msg.reverse_flag == 1:

            self.set_notification_from_obstacles(COLOR_STR_GREEN, "REVERSE")
            self.set_notification_from_emergency(COLOR_STR_GREEN, "REVERSE")

            if msg.back_emergency_brake == True:

                if (msg.lidar_back_detection == 20 and msg.back_radar_detection == 0):

                    self.set_notification_from_reverse(COLOR_STR_RED, "EMERGENCY - LiDAR")

                elif (msg.lidar_back_detection == 20 and msg.back_radar_detection == 20):

                    self.set_notification_from_reverse(COLOR_STR_RED, "EMERGENCY - both")

                elif (msg.lidar_back_detection == 0 and msg.back_radar_detection == 10):

                    self.set_notification_from_reverse(COLOR_STR_RED, "EMERGENCY - lost floor")

                elif (msg.lidar_back_detection == 0 and msg.back_radar_detection == 20):

                    self.set_notification_from_reverse(COLOR_STR_RED, "EMERGENCY - Radar")
            
            elif (msg.lidar_back_detection == 10 and msg.back_radar_detection == 0):

                self.set_notification_from_reverse(COLOR_STR_RED, "OBSTACLE - LiDAR")

            else:

                self.set_notification_from_emergency(COLOR_STR_GREEN, "FREE")

            
        else:

            self.set_notification_from_reverse(COLOR_STR_GREEN, "FORWARD")

            if msg.obstacle_validation == 1:

                if msg.emergency_brake == 1:
                    self.set_notification_from_obstacles(COLOR_STR_PINK, "OBSTACLE")
                    self.set_notification_from_emergency(COLOR_STR_RED, "EMERGENCY")
                
                else:
                    self.set_notification_from_obstacles(COLOR_STR_PINK, "OBSTACLE")
                    self.set_notification_from_emergency(COLOR_STR_ORANGE, "WARNING")

            else:
                self.set_notification_from_obstacles(COLOR_STR_GREEN, "FREE")
                self.set_notification_from_emergency(COLOR_STR_GREEN, "FREE")

        '''    

        ##if msg.right_lidar_segmentation == 1:
        ##    self.set_notification_from_lr_seg_left(COLOR_STR_RED, "      ")
        ##    self.set_notification_from_lr_seg_front(COLOR_STR_RED, "   ^   ")
        ##    self.set_notification_from_lr_seg_right(COLOR_STR_RED, "      ")

        ##elif msg.right_lidar_segmentation == 2:
        ##    self.set_notification_from_lr_seg_left(COLOR_STR_BLUE, "   <   ")
        ##    self.set_notification_from_lr_seg_front(COLOR_STR_BLUE, "      ")
        ##    self.set_notification_from_lr_seg_right(COLOR_STR_BLUE, "      ")

        ##elif msg.right_lidar_segmentation == 3:
        ##    self.set_notification_from_lr_seg_left(COLOR_STR_BLUE, "      ")
        ##    self.set_notification_from_lr_seg_front(COLOR_STR_BLUE, "      ")
        ##    self.set_notification_from_lr_seg_right(COLOR_STR_BLUE, "   >   ")

        ##else:
        ##    self.set_notification_from_lr_seg_left(COLOR_STR_BLUE, "      ")
        ##    self.set_notification_from_lr_seg_front(COLOR_STR_BLUE, "      ")
        ##    self.set_notification_from_lr_seg_right(COLOR_STR_BLUE, "      ")


        ##if msg.left_lidar_segmentation == 1:
        ##    self.set_notification_from_ll_seg_left(COLOR_STR_RED, "      ")
        ##    self.set_notification_from_ll_seg_front(COLOR_STR_RED, "   ^   ")
        ##    self.set_notification_from_ll_seg_right(COLOR_STR_RED, "      ")
        ##elif msg.left_lidar_segmentation == 2:
        ##    self.set_notification_from_ll_seg_left(COLOR_STR_BLUE, "   <   ")
        ##    self.set_notification_from_ll_seg_front(COLOR_STR_BLUE, "      ")
        ##    self.set_notification_from_ll_seg_right(COLOR_STR_BLUE, "      ")

        ##elif msg.left_lidar_segmentation == 3:
        ##    self.set_notification_from_ll_seg_left(COLOR_STR_BLUE, "      ")
        ##    self.set_notification_from_ll_seg_front(COLOR_STR_BLUE, "      ")
        ##    self.set_notification_from_ll_seg_right(COLOR_STR_BLUE, "   >   ")

        ##else:
        ##    self.set_notification_from_ll_seg_left(COLOR_STR_BLUE, "      ")
        ##    self.set_notification_from_ll_seg_front(COLOR_STR_BLUE, "      ")
        ##    self.set_notification_from_ll_seg_right(COLOR_STR_BLUE, "      ")
    
        ##if msg.radar_segmentation == 1:
        ##    self.set_notification_from_radar_seg_left(COLOR_STR_RED, "      ")
        ##    self.set_notification_from_radar_seg_front(COLOR_STR_RED, "   ^   ")
        ##    self.set_notification_from_radar_seg_right(COLOR_STR_RED, "      ")

        ##else:
        ##    self.set_notification_from_radar_seg_left(COLOR_STR_BLUE, "      ")
        ##    self.set_notification_from_radar_seg_front(COLOR_STR_BLUE, "      ")
        ##    self.set_notification_from_radar_seg_right(COLOR_STR_BLUE, "      ")

    """ GUI elements callbacks (buttons, sliders, ...) """

    def callback_path_selected(self, index):
        response = self.send_service_request(
            ServiceCommand.SET_ACTIVE_PATH, int_data=index)

        if response is not None:
            if response.reply == ServiceCommandReply.SUCCESS:
                self.set_notification(
                    COLOR_STR_GREEN, "Set active path to {}".format(index))
                '''
                self.set_notification_from_obstacles(
                    COLOR_STR_GREEN, "FREE")
                '''
            else:
                self.set_notification(
                    COLOR_STR_RED, "Error while setting active path!")

    def callback_btn_add_path(self):
        num_paths = self._widget.comboBox_paths.count()
        default_name = "path_{}".format(num_paths)

        new_name, ok = QInputDialog.getText(
            self._widget, 'Add path', 'Path name:', text=default_name)

        if ok:
            response = self.send_service_request(
                ServiceCommand.ADD_PATH, str_data=new_name)

            if response is not None:
                if response.reply == ServiceCommandReply.SUCCESS:
                    self._widget.comboBox_paths.addItem(new_name)
                    self.set_notification(
                        COLOR_STR_GREEN, "Added path '{}'".format(new_name))
                else:
                    self.set_notification(
                        COLOR_STR_RED, "Error while adding path!")

    def callback_btn_remove_path(self):
        index = self._widget.comboBox_paths.currentIndex()
        path_name = self._widget.comboBox_paths.itemText(index)

        response = self.send_service_request(
            ServiceCommand.REMOVE_PATH, int_data=index)

        if response is not None:
            if response.reply == ServiceCommandReply.SUCCESS:
                self.set_notification(
                    COLOR_STR_GREEN, "Removed path '{}' (index {})".format(path_name, index))
            else:
                self.set_notification(
                    COLOR_STR_RED, "Error while removing path!")

    def callback_btn_rename_path(self):
        index = self._widget.comboBox_paths.currentIndex()
        current_name = self._widget.comboBox_paths.itemText(index)
        new_name, ok = QInputDialog.getText(
            self._widget, 'Rename path', 'Path name:', text=current_name)

        if ok:
            response = self.send_service_request(
                ServiceCommand.RENAME_PATH, str_data=new_name, int_data=index)

            if response is not None:
                if response.reply == ServiceCommandReply.SUCCESS:
                    self.set_notification(
                        COLOR_STR_GREEN, "Renamed path '{}' to '{}'".format(current_name, new_name))
                    self._widget.comboBox_paths.setItemText(index, new_name)
                else:
                    self.set_notification(
                        COLOR_STR_RED, "Error while renaming path!")

    def notify_from_service(self, color_string, notification_text):
        self.notification_color = color_string
        self.notification_text = notification_text
        self.new_service_notification = True

    # ONLY CALLED FROM GUI THREAD
    def set_notification(self, color_string, notification_text):
        self._widget.label_notifications.setStyleSheet(
            get_label_style_sheet(color_string))
        self._widget.label_notifications.setText(notification_text)

    def set_label_obstacle(self, color_string, notification_text):
        self._widget.label_obstacle.setStyleSheet(
            get_label_style_sheet(color_string))
        self._widget.label_obstacle.setText(notification_text)
    
    def set_label_emergency(self, color_string, notification_text):
        self._widget.label_emergency.setStyleSheet(
            get_label_style_sheet(color_string))
        self._widget.label_emergency.setText(notification_text)

    def set_label_sensores(self, color_string, notification_text):
        self._widget.label_sensores.setStyleSheet(
            get_label_style_sheet(color_string))
        self._widget.label_sensores.setText(notification_text)
    

    # SENSORS NOTIFICATION

    '''
    def set_notification_from_obstacles(self, color_string, notification_text):
        self._widget.label_obstacle.setStyleSheet(
            get_label_style_sheet(color_string))
        self._widget.label_obstacle.setText(notification_text)
    
    def set_notification_from_emergency(self, color_string, notification_text):
        self._widget.label_emergency.setStyleSheet(
            get_label_style_sheet(color_string))
        self._widget.label_emergency.setText(notification_text)

    def set_notification_from_reverse(self, color_string, notification_text):
        self._widget.label_reverse.setStyleSheet(
            get_label_style_sheet(color_string))
        self._widget.label_reverse.setText(notification_text)
    '''

    ##def set_notification_from_radar_seg_left(self, color_string, notification_text):
    ##    self._widget.label_radar_seg_left.setStyleSheet(
    ##        get_label_style_sheet(color_string))
    ##    self._widget.label_radar_seg_left.setText(notification_text)
    ##def set_notification_from_radar_seg_front(self, color_string, notification_text):
    ##    self._widget.label_radar_seg_front.setStyleSheet(
    ##        get_label_style_sheet(color_string))
    ##    self._widget.label_radar_seg_front.setText(notification_text)
    ##def set_notification_from_radar_seg_right(self, color_string, notification_text):
    ##    self._widget.label_radar_seg_right.setStyleSheet(
    ##        get_label_style_sheet(color_string))
    ##    self._widget.label_radar_seg_right.setText(notification_text)

    ##def set_notification_from_ll_seg_left(self, color_string, notification_text):
    ##    self._widget.label_ll_seg_left.setStyleSheet(
    ##        get_label_style_sheet(color_string))
    ##    self._widget.label_ll_seg_left.setText(notification_text)
    ##def set_notification_from_ll_seg_front(self, color_string, notification_text):
    ##    self._widget.label_ll_seg_front.setStyleSheet(
    ##        get_label_style_sheet(color_string))
    ##    self._widget.label_ll_seg_front.setText(notification_text)
    ##def set_notification_from_ll_seg_right(self, color_string, notification_text):
    ##    self._widget.label_ll_seg_right.setStyleSheet(
    ##        get_label_style_sheet(color_string))
    ##    self._widget.label_ll_seg_right.setText(notification_text)

    ##def set_notification_from_lr_seg_left(self, color_string, notification_text):
    ##    self._widget.label_lr_seg_left.setStyleSheet(
    ##        get_label_style_sheet(color_string))
    ##    self._widget.label_lr_seg_left.setText(notification_text)
    ##def set_notification_from_lr_seg_front(self, color_string, notification_text):
    ##    self._widget.label_lr_seg_front.setStyleSheet(
    ##        get_label_style_sheet(color_string))
    ##    self._widget.label_lr_seg_front.setText(notification_text)
    ##def set_notification_from_lr_seg_right(self, color_string, notification_text):
    ##    self._widget.label_lr_seg_right.setStyleSheet(
    ##        get_label_style_sheet(color_string))
    ##    self._widget.label_lr_seg_right.setText(notification_text)

    # ONLY CALLED FROM GUI THREAD
    def set_system_state(self, color_string, state_text):
        self._widget.label_system_state.setStyleSheet(
            get_label_style_sheet(color_string))
        self._widget.label_system_state.setText(
            "<font size=3><b>Navegador</b></font><p><font size=1>({})</font>".format(state_text))

    def send_service_request(self, command, str_data="", float_data=0, int_data=0):
        try:
            node_service = rospy.ServiceProxy(
                'path_follower_node/path_follower_service', PathFollowerService)
            response = node_service(
                command=command, str_data=str_data, float_data=float_data, int_data=int_data)
            return response
        except rospy.ServiceException, e:
            self.notify_from_service(
                COLOR_STR_RED, "Navegador offline!")
            rospy.logerr("Service call failed: {}".format(str(e)))
            return None

    def reset_gui_elements(self):
        # "Path Management"
        self._widget.btn_recording.setText("Start path recording")
        self._widget.btn_save_path.setEnabled(False)
        self._widget.btn_load_path.setEnabled(False)
        self._widget.btn_post_process.setEnabled(False)
        self._widget.btn_recording.setEnabled(False)
        self._widget.btn_plot_mapping_data.setEnabled(False)
        self._widget.btn_plot_path_data.setEnabled(False)
        self._widget.cb_show_go_zone.setEnabled(False)
        self._widget.cb_show_wait_zone.setEnabled(False)
        self._widget.cb_idle_braking.setEnabled(False)
        self._widget.sb_go_zone_radius.setEnabled(False)
        self._widget.sb_wait_zone_radius.setEnabled(False)
        self._widget.cb_reverse.setEnabled(False)

        # "Drive Management"
        self._widget.btn_following.setText("Start path following")
        self._widget.btn_following.setEnabled(False)
        self._widget.pb_gas_desired.setValue(0)
        self._widget.pb_brake_desired.setValue(0)
        
        
        # "System State"
        self.set_system_state(COLOR_STR_WHITE, "Offline")

        # "Notifications"
        self.set_notification(COLOR_STR_WHITE, "Navegador offline!")

        self.notification_color = COLOR_STR_WHITE
        self.notification_text = ""
        self.new_service_notification = False

    def set_oxts_label(self, color_string, state_name):
        self._widget.label_oxtsState.setStyleSheet(
            get_label_style_sheet(color_string, 0))
        self._widget.label_oxtsState.setText(
            "<font size=3><b>OxTS</b></font><p><font size=1>({})</font>".format(state_name))

    def set_paravan_label(self, color_string, state_name):
        self._widget.label_paravanState.setStyleSheet(
            get_label_style_sheet(color_string, 0))
        self._widget.label_paravanState.setText(
            "<font size=3><b>U&M</b></font><p><font size=1>({})</font>".format(state_name))

    def set_LLidar_label(self, color_string, state_name):
        self._widget.label_LLidatState.setStyleSheet(
            get_label_style_sheet(color_string, 0))
        self._widget.label_LLidatState.setText(
            "<font size=3><b>Lidar Esq.</b></font><p><font size=1>({})</font>".format(state_name))

    def set_FRadar_label(self, color_string, state_name):
        self._widget.label_FRadarState.setStyleSheet(
            get_label_style_sheet(color_string, 0))
        self._widget.label_FRadarState.setText(
            "<font size=3><b>Radar Front</b></font><p><font size=1>({})</font>".format(state_name))

    def set_RLidar_label(self, color_string, state_name):
        self._widget.label_RLState.setStyleSheet(
            get_label_style_sheet(color_string, 0))
        self._widget.label_RLState.setText(
            "<font size=3><b>Lidar Dir.</b></font><p><font size=1>({})</font>".format(state_name))

    @pyqtSlot()
    def update_gui(self):

        # check path_follower_node
        if self.timeout_path_follower_node_alive.timed_out():  # path follower node is dead
            self.reset_gui_elements()
        else:
            self._widget.cb_show_go_zone.setChecked(
                rospy.get_param('/path_follower_node/display_go_zone', False))
            self._widget.cb_show_wait_zone.setChecked(
                rospy.get_param('/path_follower_node/display_wait_zone', False))
            self._widget.cb_idle_braking.setChecked(
                rospy.get_param('/path_follower_node/idle_braking', False))

        # check "oxts" node and data
        if self.timeout_oxts_node_alive.timed_out():  # oxts node is dead
            self.set_oxts_label(COLOR_STR_WHITE, "offline")
        elif self.timeout_oxts_data.timed_out():  # oxts does not send data
            self.set_oxts_label(COLOR_STR_BLUE, "no data")
        else:  # oxts_node is alive and sending data
            if self.nav_sat_fix_msg is not None and self.nav_sat_fix_msg.status.status is not NavSatStatus.STATUS_NO_FIX:
                self.set_oxts_label(COLOR_STR_GREEN, "online")
            else:
                self.set_oxts_label(COLOR_STR_LIGHT_GREEN, "no fix")

        # check "paravan" node and data
        if self.timeout_paravan_node_alive.timed_out():  # paravan_node is dead
            self.set_paravan_label(COLOR_STR_WHITE, "offline")
            
        elif self.timeout_paravan_data.timed_out():  # paravan_node does not send data
            self.set_paravan_label(COLOR_STR_BLUE, "no data")

        else:  # paravan_node is alive and sending data
            self.set_paravan_label(COLOR_STR_GREEN, "online")  

        # check sensors data
           

        if self.timeout_control_target_state.timed_out():
            self._widget.label_drive_controller_steering_desired.setText("-")
            
            self._widget.label_drive_controller_velocity_desired.setText("-")

        #if self.timeout_vehicle_state.timed_out():
            #self._widget.label_veh_state_x.setText("-")
            #self._widget.label_veh_state_y.setText("-")
            #self._widget.label_veh_state_x_rel.setText("-")
            #self._widget.label_veh_state_y_rel.setText("-")
            #self._widget.label_veh_state_yaw_angle.setText("-")
            #self._widget.label_veh_state_heading.setText("-")
            #self._widget.label_veh_state_velocity.setText("-")

        if self._widget.comboBox_paths.count()==1:
            self._widget.btn_removePath.setEnabled(False)
        elif self._widget.comboBox_paths.count()>1  and not self.is_following:
            self._widget.btn_removePath.setEnabled(True)

        # -------------------------------------------------------------------------------------

        # update notification panel
        if self.new_service_notification:
            self.set_notification(self.notification_color,
                                  self.notification_text)
            self.new_service_notification = False

        self._widget.repaint()

    def callback_cb_idle_braking(self, state):
        rospy.set_param("/path_follower_node/idle_braking", state)

    def callback_cb_show_go_zone(self, state):
        rospy.set_param("/path_follower_node/display_go_zone", state)

    def callback_cb_show_wait_zone(self, state):
        rospy.set_param("/path_follower_node/display_wait_zone", state)

    def callback_cb_reverse(self, state):

        if self._widget.cb_reverse.isChecked():
            response = self.send_service_request(
                ServiceCommand.SET_REVERSE_MISSION, int_data = 1)
        
        else:
            response = self.send_service_request(
                ServiceCommand.SET_REVERSE_MISSION, int_data = 0)


    def callback_sb_go_zone_radius(self, value):
        response = self.send_service_request(
            ServiceCommand.SET_GO_ZONE_RADIUS, float_data=value)

        if response is not None:
            if response.reply == ServiceCommandReply.SUCCESS:
                self.set_notification(
                    COLOR_STR_GREEN, "Radio Go-Zone modificado")
            else:
                self.set_notification(
                    COLOR_STR_RED, "Erro ao mudar raio Go-Zone!")

    def callback_sb_wait_zone_radius(self, value):
        response = self.send_service_request(
            ServiceCommand.SET_WAIT_ZONE_RADIUS, float_data=value)

        if response is not None:
            if response.reply == ServiceCommandReply.SUCCESS:
                self.set_notification(
                    COLOR_STR_GREEN, "Raio Wait-Zone modificado")
            else:
                self.set_notification(
                    COLOR_STR_RED, "Erro ao mudar raio Wait-Zone!")

    def callback_btn_save_path(self):
        file_name = str(QFileDialog.getSaveFileName(
            self._widget, 'Save path to file', '/home/cmore/path.json', filter='json (*.json)')[0])

        if file_name == '':
            return
        else:
            response = self.send_service_request(
                ServiceCommand.SAVE_PATH_TO_FILE, str_data=file_name)

            if response is not None:
                if response.reply == ServiceCommandReply.SUCCESS:
                    self.set_notification(
                        COLOR_STR_GREEN, "Rota salva com sucesso!")
                else:
                    self.set_notification(
                        COLOR_STR_RED, "Erro ao salvar rota!")

    def callback_btn_load_path(self):
        file_name = str(QFileDialog.getOpenFileName(
            self._widget, 'Load path from file', '/home/cmore', "json (*.json)")[0])

        if file_name == '':
            return
        else:

            response = self.send_service_request(
                ServiceCommand.LOAD_PATH_FROM_FILE, str_data=file_name)

            if response is not None:
                if response.reply == ServiceCommandReply.SUCCESS:
                    self.set_notification(
                        COLOR_STR_GREEN, "Rota carregada com sucesso!")
                elif response.reply == ServiceCommandReply.FAIL_LOADING_PATH_FROM_FILE:
                    self.set_notification(
                        COLOR_STR_RED, "Erro ao carregar rota!")
                else:
                    self.set_notification(
                        COLOR_STR_RED, "Erro desconhecido ao carregar rota!")

    def callback_btn_post_processing(self):

        response = self.send_service_request(ServiceCommand.POST_PROCESS_PATH)

        if response is not None:
            if response.reply == ServiceCommandReply.SUCCESS:
                self.set_notification(
                    COLOR_STR_GREEN, "Sucesso no pos processamento!")
            elif response.reply == ServiceCommandReply.FAIL_NO_PATH_AVAILABLE:
                self.set_notification(COLOR_STR_RED, "Sem rotas validas!")
            elif response.reply == ServiceCommandReply.FAIL_PATH_TOO_SHORT:
                self.set_notification(COLOR_STR_RED, "Rota muito pequena!")

    def callback_btn_following(self):

        if self.is_following:
            response = self.send_service_request(ServiceCommand.STOP_FOLLOWING, int_data = 0)

            if response is not None:
                if response.reply == ServiceCommandReply.SUCCESS:
                    # self.is_following = False  # is done from StateEvent::IDLE
                    self.set_notification(
                        COLOR_STR_GREEN, "Navegacao interrompida!")

        else:  # try to start following
            response = self.send_service_request(
                ServiceCommand.START_FOLLOWING, int_data = 1)

            if response is not None:
                if response.reply == ServiceCommandReply.SUCCESS:
                    self.set_notification(COLOR_STR_GREEN, "Navegacao iniciada")
                elif response.reply == ServiceCommandReply.FAIL_VEHCILE_NOT_IN_GO_ZONE:
                    self.set_notification(COLOR_STR_RED, "Caminhao nao esta na Go-Zone!")
                elif response.reply == ServiceCommandReply.FAIL_NO_PATH_AVAILABLE:
                    self.set_notification(COLOR_STR_RED, "Sem rotas validas!")
                elif response.reply == ServiceCommandReply.FAIL_PATH_TOO_SHORT:
                    self.set_notification(COLOR_STR_RED, "Rota muito pequena!")
                elif response.reply == ServiceCommandReply.FAIL_OXTS_OFFLINE:
                    self.set_notification(COLOR_STR_RED, "Software OxTS offline!")
                elif response.reply == ServiceCommandReply.FAIL_OXTS_NO_DATA:
                    self.set_notification(COLOR_STR_RED, "Sem dados de GPS!")
                elif response.reply == ServiceCommandReply.FAIL_OXTS_DATA_BUT_NO_FIX:
                    self.set_notification(COLOR_STR_RED, "Sem satelites GPS fixos!")
                elif response.reply == ServiceCommandReply.WARN_UEM_NO_REMOTE_CONTROL:
                    self.set_notification(COLOR_STR_ORANGE, "Navegacao iniciada sem controle remoto!")
                elif response.reply == ServiceCommandReply.FAIL_UEM_NO_REMOTE_CONTROL:
                    self.set_notification(COLOR_STR_RED, "Navegacao impossivel - Controle Remoto nao conectado!")
                elif response.reply == ServiceCommandReply.WARN_UEM_OFFLINE:
                    self.set_notification(COLOR_STR_ORANGE, "Navegacao inicializada com U&M offline!")
                elif response.reply == ServiceCommandReply.FAIL_UEM_OFFLINE:
                    self.set_notification(COLOR_STR_RED, "Navegacao impossivel - U&M offline!")
                elif response.reply == ServiceCommandReply.WARN_UEM_NO_DATA:
                    self.set_notification(COLOR_STR_ORANGE, "Navegacao iniciada sem dados U&M!")
                elif response.reply == ServiceCommandReply.FAIL_UEM_NO_DATA:
                    self.set_notification(COLOR_STR_RED, "Navegacao impossivel - sem dados U&M!")
                #elif response.reply == ServiceCommandReply.FAIL_BUCKET_IS_OPENED:
                #    self.set_notification(COLOR_STR_RED, "Following not possible - bucket opened!")
    
    def callback_btn_recording(self):
        if self.is_recording:
            response = self.send_service_request(ServiceCommand.STOP_RECORDING)

            if response is not None:
                if response.reply == ServiceCommandReply.SUCCESS:
                    self.is_recording = False
                    self.set_notification(
                        COLOR_STR_GREEN, "Gravacao finalizada!")

        else:  # try to start recording
            response = self.send_service_request(
                ServiceCommand.START_RECORDING)

            if response is not None:
                if response.reply == ServiceCommandReply.SUCCESS:
                    self.set_notification(
                        COLOR_STR_GREEN, "Gravacao iniciada ...")
                elif response.reply == ServiceCommandReply.FAIL_OXTS_OFFLINE:
                    self.set_notification(
                        COLOR_STR_RED, "Software OxTS offline!")
                elif response.reply == ServiceCommandReply.FAIL_OXTS_NO_DATA:
                    self.set_notification(
                        COLOR_STR_RED, "Sem dados de GPS!")
                elif response.reply == ServiceCommandReply.FAIL_OXTS_DATA_BUT_NO_FIX:
                    self.set_notification(COLOR_STR_RED, "Sem satelites GPS fixos!")

    def callback_btn_plot_mapping_data(self):
        response = self.send_service_request(ServiceCommand.PLOT_MAPPING_DATA)

        if response is not None:
            if response.reply == ServiceCommandReply.SUCCESS:
                self.set_notification(COLOR_STR_GREEN, "Graficos gerados com sucesso!")
            elif response.reply == ServiceCommandReply.FAIL_OLD_PLOT_STILL_ACTIVE:
                self.set_notification(
                    COLOR_STR_ORANGE, "Fechar todos os graficos!")

    def callback_btn_plot_path_data(self):
        response = self.send_service_request(ServiceCommand.PLOT_PATH_DATA)

        if response is not None:
            if response.reply == ServiceCommandReply.SUCCESS:
                self.set_notification(COLOR_STR_GREEN, "Graficos gerados com sucesso!")
            elif response.reply == ServiceCommandReply.FAIL_OLD_PLOT_STILL_ACTIVE:
                self.set_notification(
                    COLOR_STR_ORANGE, "Fechar todos os graficos!")
