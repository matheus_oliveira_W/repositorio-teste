; Auto-generated. Do not edit!


(cl:in-package path_follower-srv)


;//! \htmlinclude PathFollowerService-request.msg.html

(cl:defclass <PathFollowerService-request> (roslisp-msg-protocol:ros-message)
  ((command
    :reader command
    :initarg :command
    :type cl:fixnum
    :initform 0)
   (str_data
    :reader str_data
    :initarg :str_data
    :type cl:string
    :initform "")
   (float_data
    :reader float_data
    :initarg :float_data
    :type cl:float
    :initform 0.0)
   (int_data
    :reader int_data
    :initarg :int_data
    :type cl:integer
    :initform 0))
)

(cl:defclass PathFollowerService-request (<PathFollowerService-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <PathFollowerService-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'PathFollowerService-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name path_follower-srv:<PathFollowerService-request> is deprecated: use path_follower-srv:PathFollowerService-request instead.")))

(cl:ensure-generic-function 'command-val :lambda-list '(m))
(cl:defmethod command-val ((m <PathFollowerService-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-srv:command-val is deprecated.  Use path_follower-srv:command instead.")
  (command m))

(cl:ensure-generic-function 'str_data-val :lambda-list '(m))
(cl:defmethod str_data-val ((m <PathFollowerService-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-srv:str_data-val is deprecated.  Use path_follower-srv:str_data instead.")
  (str_data m))

(cl:ensure-generic-function 'float_data-val :lambda-list '(m))
(cl:defmethod float_data-val ((m <PathFollowerService-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-srv:float_data-val is deprecated.  Use path_follower-srv:float_data instead.")
  (float_data m))

(cl:ensure-generic-function 'int_data-val :lambda-list '(m))
(cl:defmethod int_data-val ((m <PathFollowerService-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-srv:int_data-val is deprecated.  Use path_follower-srv:int_data instead.")
  (int_data m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <PathFollowerService-request>) ostream)
  "Serializes a message object of type '<PathFollowerService-request>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'command)) ostream)
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'str_data))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'str_data))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'float_data))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let* ((signed (cl:slot-value msg 'int_data)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <PathFollowerService-request>) istream)
  "Deserializes a message object of type '<PathFollowerService-request>"
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'command)) (cl:read-byte istream))
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'str_data) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'str_data) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'float_data) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'int_data) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<PathFollowerService-request>)))
  "Returns string type for a service object of type '<PathFollowerService-request>"
  "path_follower/PathFollowerServiceRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'PathFollowerService-request)))
  "Returns string type for a service object of type 'PathFollowerService-request"
  "path_follower/PathFollowerServiceRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<PathFollowerService-request>)))
  "Returns md5sum for a message object of type '<PathFollowerService-request>"
  "635ea1697aa662b37b6293cb945f6681")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'PathFollowerService-request)))
  "Returns md5sum for a message object of type 'PathFollowerService-request"
  "635ea1697aa662b37b6293cb945f6681")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<PathFollowerService-request>)))
  "Returns full string definition for message of type '<PathFollowerService-request>"
  (cl:format cl:nil "uint8 	command~%string 	str_data~%float32 float_data~%int32 	int_data~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'PathFollowerService-request)))
  "Returns full string definition for message of type 'PathFollowerService-request"
  (cl:format cl:nil "uint8 	command~%string 	str_data~%float32 float_data~%int32 	int_data~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <PathFollowerService-request>))
  (cl:+ 0
     1
     4 (cl:length (cl:slot-value msg 'str_data))
     4
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <PathFollowerService-request>))
  "Converts a ROS message object to a list"
  (cl:list 'PathFollowerService-request
    (cl:cons ':command (command msg))
    (cl:cons ':str_data (str_data msg))
    (cl:cons ':float_data (float_data msg))
    (cl:cons ':int_data (int_data msg))
))
;//! \htmlinclude PathFollowerService-response.msg.html

(cl:defclass <PathFollowerService-response> (roslisp-msg-protocol:ros-message)
  ((reply
    :reader reply
    :initarg :reply
    :type cl:fixnum
    :initform 0)
   (str_reply
    :reader str_reply
    :initarg :str_reply
    :type cl:string
    :initform "")
   (float_reply
    :reader float_reply
    :initarg :float_reply
    :type cl:float
    :initform 0.0)
   (int_reply
    :reader int_reply
    :initarg :int_reply
    :type cl:integer
    :initform 0))
)

(cl:defclass PathFollowerService-response (<PathFollowerService-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <PathFollowerService-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'PathFollowerService-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name path_follower-srv:<PathFollowerService-response> is deprecated: use path_follower-srv:PathFollowerService-response instead.")))

(cl:ensure-generic-function 'reply-val :lambda-list '(m))
(cl:defmethod reply-val ((m <PathFollowerService-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-srv:reply-val is deprecated.  Use path_follower-srv:reply instead.")
  (reply m))

(cl:ensure-generic-function 'str_reply-val :lambda-list '(m))
(cl:defmethod str_reply-val ((m <PathFollowerService-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-srv:str_reply-val is deprecated.  Use path_follower-srv:str_reply instead.")
  (str_reply m))

(cl:ensure-generic-function 'float_reply-val :lambda-list '(m))
(cl:defmethod float_reply-val ((m <PathFollowerService-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-srv:float_reply-val is deprecated.  Use path_follower-srv:float_reply instead.")
  (float_reply m))

(cl:ensure-generic-function 'int_reply-val :lambda-list '(m))
(cl:defmethod int_reply-val ((m <PathFollowerService-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-srv:int_reply-val is deprecated.  Use path_follower-srv:int_reply instead.")
  (int_reply m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <PathFollowerService-response>) ostream)
  "Serializes a message object of type '<PathFollowerService-response>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'reply)) ostream)
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'str_reply))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'str_reply))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'float_reply))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let* ((signed (cl:slot-value msg 'int_reply)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <PathFollowerService-response>) istream)
  "Deserializes a message object of type '<PathFollowerService-response>"
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'reply)) (cl:read-byte istream))
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'str_reply) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'str_reply) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'float_reply) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'int_reply) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<PathFollowerService-response>)))
  "Returns string type for a service object of type '<PathFollowerService-response>"
  "path_follower/PathFollowerServiceResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'PathFollowerService-response)))
  "Returns string type for a service object of type 'PathFollowerService-response"
  "path_follower/PathFollowerServiceResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<PathFollowerService-response>)))
  "Returns md5sum for a message object of type '<PathFollowerService-response>"
  "635ea1697aa662b37b6293cb945f6681")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'PathFollowerService-response)))
  "Returns md5sum for a message object of type 'PathFollowerService-response"
  "635ea1697aa662b37b6293cb945f6681")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<PathFollowerService-response>)))
  "Returns full string definition for message of type '<PathFollowerService-response>"
  (cl:format cl:nil "uint8 	reply~%string 	str_reply~%float32 float_reply~%int32 	int_reply~%~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'PathFollowerService-response)))
  "Returns full string definition for message of type 'PathFollowerService-response"
  (cl:format cl:nil "uint8 	reply~%string 	str_reply~%float32 float_reply~%int32 	int_reply~%~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <PathFollowerService-response>))
  (cl:+ 0
     1
     4 (cl:length (cl:slot-value msg 'str_reply))
     4
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <PathFollowerService-response>))
  "Converts a ROS message object to a list"
  (cl:list 'PathFollowerService-response
    (cl:cons ':reply (reply msg))
    (cl:cons ':str_reply (str_reply msg))
    (cl:cons ':float_reply (float_reply msg))
    (cl:cons ':int_reply (int_reply msg))
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'PathFollowerService)))
  'PathFollowerService-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'PathFollowerService)))
  'PathFollowerService-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'PathFollowerService)))
  "Returns string type for a service object of type '<PathFollowerService>"
  "path_follower/PathFollowerService")