; Auto-generated. Do not edit!


(cl:in-package path_follower-msg)


;//! \htmlinclude StateEvents.msg.html

(cl:defclass <StateEvents> (roslisp-msg-protocol:ros-message)
  ()
)

(cl:defclass StateEvents (<StateEvents>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <StateEvents>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'StateEvents)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name path_follower-msg:<StateEvents> is deprecated: use path_follower-msg:StateEvents instead.")))
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql '<StateEvents>)))
    "Constants for message type '<StateEvents>"
  '((:RECORDING . 0)
    (:IDLE . 1)
    (:FOLLOWING . 2)
    (:WAIT_ZONE_REACHED . 3)
    (:HANDOVER . 4)
    (:RECORDING_FAILED_OXTS_NODE_DEAD . 10)
    (:RECORDING_FAILED_OXTS_NO_DATA . 11)
    (:RECORDING_FAILED_OXTS_NO_FIX . 12)
    (:FOLLOWING_FAILED_OXTS_NODE_DEAD . 20)
    (:FOLLOWING_FAILED_OXTS_NO_DATA . 21)
    (:FOLLOWING_FAILED_OXTS_NO_FIX . 22)
    (:FOLLOWING_FAILED_PARAVAN_NODE_DEAD . 23)
    (:FOLLOWING_FAILED_PARAVAN_NO_DATA . 24)
    (:FOLLOWING_FAILED_PARAVAN_NO_REMOTE_CONTROL . 25))
)
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql 'StateEvents)))
    "Constants for message type 'StateEvents"
  '((:RECORDING . 0)
    (:IDLE . 1)
    (:FOLLOWING . 2)
    (:WAIT_ZONE_REACHED . 3)
    (:HANDOVER . 4)
    (:RECORDING_FAILED_OXTS_NODE_DEAD . 10)
    (:RECORDING_FAILED_OXTS_NO_DATA . 11)
    (:RECORDING_FAILED_OXTS_NO_FIX . 12)
    (:FOLLOWING_FAILED_OXTS_NODE_DEAD . 20)
    (:FOLLOWING_FAILED_OXTS_NO_DATA . 21)
    (:FOLLOWING_FAILED_OXTS_NO_FIX . 22)
    (:FOLLOWING_FAILED_PARAVAN_NODE_DEAD . 23)
    (:FOLLOWING_FAILED_PARAVAN_NO_DATA . 24)
    (:FOLLOWING_FAILED_PARAVAN_NO_REMOTE_CONTROL . 25))
)
(cl:defmethod roslisp-msg-protocol:serialize ((msg <StateEvents>) ostream)
  "Serializes a message object of type '<StateEvents>"
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <StateEvents>) istream)
  "Deserializes a message object of type '<StateEvents>"
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<StateEvents>)))
  "Returns string type for a message object of type '<StateEvents>"
  "path_follower/StateEvents")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'StateEvents)))
  "Returns string type for a message object of type 'StateEvents"
  "path_follower/StateEvents")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<StateEvents>)))
  "Returns md5sum for a message object of type '<StateEvents>"
  "7f539e5f3e9b2c75ab993b9dff6a200f")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'StateEvents)))
  "Returns md5sum for a message object of type 'StateEvents"
  "7f539e5f3e9b2c75ab993b9dff6a200f")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<StateEvents>)))
  "Returns full string definition for message of type '<StateEvents>"
  (cl:format cl:nil "# states~%uint8 RECORDING     = 0~%uint8 IDLE          = 1~%uint8 FOLLOWING     = 2~%uint8 WAIT_ZONE_REACHED = 3~%uint8 HANDOVER = 4~%~%# fail states~%uint8 RECORDING_FAILED_OXTS_NODE_DEAD   = 10~%uint8 RECORDING_FAILED_OXTS_NO_DATA     = 11~%uint8 RECORDING_FAILED_OXTS_NO_FIX      = 12~%~%uint8 FOLLOWING_FAILED_OXTS_NODE_DEAD    = 20~%uint8 FOLLOWING_FAILED_OXTS_NO_DATA      = 21~%uint8 FOLLOWING_FAILED_OXTS_NO_FIX       = 22~%uint8 FOLLOWING_FAILED_PARAVAN_NODE_DEAD = 23~%uint8 FOLLOWING_FAILED_PARAVAN_NO_DATA   = 24~%uint8 FOLLOWING_FAILED_PARAVAN_NO_REMOTE_CONTROL = 25~%~%~%~%~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'StateEvents)))
  "Returns full string definition for message of type 'StateEvents"
  (cl:format cl:nil "# states~%uint8 RECORDING     = 0~%uint8 IDLE          = 1~%uint8 FOLLOWING     = 2~%uint8 WAIT_ZONE_REACHED = 3~%uint8 HANDOVER = 4~%~%# fail states~%uint8 RECORDING_FAILED_OXTS_NODE_DEAD   = 10~%uint8 RECORDING_FAILED_OXTS_NO_DATA     = 11~%uint8 RECORDING_FAILED_OXTS_NO_FIX      = 12~%~%uint8 FOLLOWING_FAILED_OXTS_NODE_DEAD    = 20~%uint8 FOLLOWING_FAILED_OXTS_NO_DATA      = 21~%uint8 FOLLOWING_FAILED_OXTS_NO_FIX       = 22~%uint8 FOLLOWING_FAILED_PARAVAN_NODE_DEAD = 23~%uint8 FOLLOWING_FAILED_PARAVAN_NO_DATA   = 24~%uint8 FOLLOWING_FAILED_PARAVAN_NO_REMOTE_CONTROL = 25~%~%~%~%~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <StateEvents>))
  (cl:+ 0
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <StateEvents>))
  "Converts a ROS message object to a list"
  (cl:list 'StateEvents
))
