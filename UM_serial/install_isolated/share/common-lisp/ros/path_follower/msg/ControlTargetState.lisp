; Auto-generated. Do not edit!


(cl:in-package path_follower-msg)


;//! \htmlinclude ControlTargetState.msg.html

(cl:defclass <ControlTargetState> (roslisp-msg-protocol:ros-message)
  ((closest_point_x
    :reader closest_point_x
    :initarg :closest_point_x
    :type cl:float
    :initform 0.0)
   (closest_point_y
    :reader closest_point_y
    :initarg :closest_point_y
    :type cl:float
    :initform 0.0)
   (look_ahead_point_x
    :reader look_ahead_point_x
    :initarg :look_ahead_point_x
    :type cl:float
    :initform 0.0)
   (look_ahead_point_y
    :reader look_ahead_point_y
    :initarg :look_ahead_point_y
    :type cl:float
    :initform 0.0)
   (look_ahead_distance
    :reader look_ahead_distance
    :initarg :look_ahead_distance
    :type cl:float
    :initform 0.0)
   (velocity
    :reader velocity
    :initarg :velocity
    :type cl:float
    :initform 0.0)
   (steering_angle
    :reader steering_angle
    :initarg :steering_angle
    :type cl:float
    :initform 0.0)
   (steering_angle_norm
    :reader steering_angle_norm
    :initarg :steering_angle_norm
    :type cl:float
    :initform 0.0)
   (gas_brake_norm
    :reader gas_brake_norm
    :initarg :gas_brake_norm
    :type cl:float
    :initform 0.0))
)

(cl:defclass ControlTargetState (<ControlTargetState>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <ControlTargetState>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'ControlTargetState)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name path_follower-msg:<ControlTargetState> is deprecated: use path_follower-msg:ControlTargetState instead.")))

(cl:ensure-generic-function 'closest_point_x-val :lambda-list '(m))
(cl:defmethod closest_point_x-val ((m <ControlTargetState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:closest_point_x-val is deprecated.  Use path_follower-msg:closest_point_x instead.")
  (closest_point_x m))

(cl:ensure-generic-function 'closest_point_y-val :lambda-list '(m))
(cl:defmethod closest_point_y-val ((m <ControlTargetState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:closest_point_y-val is deprecated.  Use path_follower-msg:closest_point_y instead.")
  (closest_point_y m))

(cl:ensure-generic-function 'look_ahead_point_x-val :lambda-list '(m))
(cl:defmethod look_ahead_point_x-val ((m <ControlTargetState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:look_ahead_point_x-val is deprecated.  Use path_follower-msg:look_ahead_point_x instead.")
  (look_ahead_point_x m))

(cl:ensure-generic-function 'look_ahead_point_y-val :lambda-list '(m))
(cl:defmethod look_ahead_point_y-val ((m <ControlTargetState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:look_ahead_point_y-val is deprecated.  Use path_follower-msg:look_ahead_point_y instead.")
  (look_ahead_point_y m))

(cl:ensure-generic-function 'look_ahead_distance-val :lambda-list '(m))
(cl:defmethod look_ahead_distance-val ((m <ControlTargetState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:look_ahead_distance-val is deprecated.  Use path_follower-msg:look_ahead_distance instead.")
  (look_ahead_distance m))

(cl:ensure-generic-function 'velocity-val :lambda-list '(m))
(cl:defmethod velocity-val ((m <ControlTargetState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:velocity-val is deprecated.  Use path_follower-msg:velocity instead.")
  (velocity m))

(cl:ensure-generic-function 'steering_angle-val :lambda-list '(m))
(cl:defmethod steering_angle-val ((m <ControlTargetState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:steering_angle-val is deprecated.  Use path_follower-msg:steering_angle instead.")
  (steering_angle m))

(cl:ensure-generic-function 'steering_angle_norm-val :lambda-list '(m))
(cl:defmethod steering_angle_norm-val ((m <ControlTargetState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:steering_angle_norm-val is deprecated.  Use path_follower-msg:steering_angle_norm instead.")
  (steering_angle_norm m))

(cl:ensure-generic-function 'gas_brake_norm-val :lambda-list '(m))
(cl:defmethod gas_brake_norm-val ((m <ControlTargetState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:gas_brake_norm-val is deprecated.  Use path_follower-msg:gas_brake_norm instead.")
  (gas_brake_norm m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <ControlTargetState>) ostream)
  "Serializes a message object of type '<ControlTargetState>"
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'closest_point_x))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'closest_point_y))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'look_ahead_point_x))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'look_ahead_point_y))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'look_ahead_distance))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'velocity))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'steering_angle))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'steering_angle_norm))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'gas_brake_norm))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <ControlTargetState>) istream)
  "Deserializes a message object of type '<ControlTargetState>"
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'closest_point_x) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'closest_point_y) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'look_ahead_point_x) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'look_ahead_point_y) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'look_ahead_distance) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'velocity) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'steering_angle) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'steering_angle_norm) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'gas_brake_norm) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<ControlTargetState>)))
  "Returns string type for a message object of type '<ControlTargetState>"
  "path_follower/ControlTargetState")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'ControlTargetState)))
  "Returns string type for a message object of type 'ControlTargetState"
  "path_follower/ControlTargetState")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<ControlTargetState>)))
  "Returns md5sum for a message object of type '<ControlTargetState>"
  "59db037f6601c72a4efd8f7c6d69d33a")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'ControlTargetState)))
  "Returns md5sum for a message object of type 'ControlTargetState"
  "59db037f6601c72a4efd8f7c6d69d33a")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<ControlTargetState>)))
  "Returns full string definition for message of type '<ControlTargetState>"
  (cl:format cl:nil "# This message contains the target values used for guiding the vehicle~%~%float32 closest_point_x     # [m] UTM easting of the point on the path which is currently closest to the vehicle~%float32 closest_point_y     # [m] UTM northing ~%float32 look_ahead_point_x  # [m] UTM easting of the look ahead point (target point for steering controller)~%float32 look_ahead_point_y  # [m] UTM northing ~%float32 look_ahead_distance # [m] distance from closest point to look ahead point, measured along the path~%float32 velocity            # [m/s] target velocity~%float32 steering_angle      # [rad] ~%float32 steering_angle_norm # [-] normalized steering angle (-1, 1)~%float32 gas_brake_norm      # [-] normalized gas/brake position (-1, 1)~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'ControlTargetState)))
  "Returns full string definition for message of type 'ControlTargetState"
  (cl:format cl:nil "# This message contains the target values used for guiding the vehicle~%~%float32 closest_point_x     # [m] UTM easting of the point on the path which is currently closest to the vehicle~%float32 closest_point_y     # [m] UTM northing ~%float32 look_ahead_point_x  # [m] UTM easting of the look ahead point (target point for steering controller)~%float32 look_ahead_point_y  # [m] UTM northing ~%float32 look_ahead_distance # [m] distance from closest point to look ahead point, measured along the path~%float32 velocity            # [m/s] target velocity~%float32 steering_angle      # [rad] ~%float32 steering_angle_norm # [-] normalized steering angle (-1, 1)~%float32 gas_brake_norm      # [-] normalized gas/brake position (-1, 1)~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <ControlTargetState>))
  (cl:+ 0
     4
     4
     4
     4
     4
     4
     4
     4
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <ControlTargetState>))
  "Converts a ROS message object to a list"
  (cl:list 'ControlTargetState
    (cl:cons ':closest_point_x (closest_point_x msg))
    (cl:cons ':closest_point_y (closest_point_y msg))
    (cl:cons ':look_ahead_point_x (look_ahead_point_x msg))
    (cl:cons ':look_ahead_point_y (look_ahead_point_y msg))
    (cl:cons ':look_ahead_distance (look_ahead_distance msg))
    (cl:cons ':velocity (velocity msg))
    (cl:cons ':steering_angle (steering_angle msg))
    (cl:cons ':steering_angle_norm (steering_angle_norm msg))
    (cl:cons ':gas_brake_norm (gas_brake_norm msg))
))
