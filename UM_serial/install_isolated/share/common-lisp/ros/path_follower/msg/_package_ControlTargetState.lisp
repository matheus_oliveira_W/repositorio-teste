(cl:in-package path_follower-msg)
(cl:export '(CLOSEST_POINT_X-VAL
          CLOSEST_POINT_X
          CLOSEST_POINT_Y-VAL
          CLOSEST_POINT_Y
          LOOK_AHEAD_POINT_X-VAL
          LOOK_AHEAD_POINT_X
          LOOK_AHEAD_POINT_Y-VAL
          LOOK_AHEAD_POINT_Y
          LOOK_AHEAD_DISTANCE-VAL
          LOOK_AHEAD_DISTANCE
          VELOCITY-VAL
          VELOCITY
          STEERING_ANGLE-VAL
          STEERING_ANGLE
          STEERING_ANGLE_NORM-VAL
          STEERING_ANGLE_NORM
          GAS_BRAKE_NORM-VAL
          GAS_BRAKE_NORM
))