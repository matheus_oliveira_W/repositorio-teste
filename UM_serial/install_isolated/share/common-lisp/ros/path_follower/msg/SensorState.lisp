; Auto-generated. Do not edit!


(cl:in-package path_follower-msg)


;//! \htmlinclude SensorState.msg.html

(cl:defclass <SensorState> (roslisp-msg-protocol:ros-message)
  ((warning_flag
    :reader warning_flag
    :initarg :warning_flag
    :type cl:boolean
    :initform cl:nil)
   (emergency_flag
    :reader emergency_flag
    :initarg :emergency_flag
    :type cl:boolean
    :initform cl:nil)
   (left_lidar_state
    :reader left_lidar_state
    :initarg :left_lidar_state
    :type cl:boolean
    :initform cl:nil)
   (right_lidar_state
    :reader right_lidar_state
    :initarg :right_lidar_state
    :type cl:boolean
    :initform cl:nil)
   (front_radar_state
    :reader front_radar_state
    :initarg :front_radar_state
    :type cl:boolean
    :initform cl:nil)
   (follow_with_sensors
    :reader follow_with_sensors
    :initarg :follow_with_sensors
    :type cl:boolean
    :initform cl:nil)
   (dist_x_0
    :reader dist_x_0
    :initarg :dist_x_0
    :type cl:float
    :initform 0.0)
   (dist_y_0
    :reader dist_y_0
    :initarg :dist_y_0
    :type cl:float
    :initform 0.0)
   (ang_0
    :reader ang_0
    :initarg :ang_0
    :type cl:float
    :initform 0.0)
   (dist_x_1
    :reader dist_x_1
    :initarg :dist_x_1
    :type cl:float
    :initform 0.0)
   (dist_y_1
    :reader dist_y_1
    :initarg :dist_y_1
    :type cl:float
    :initform 0.0)
   (ang_1
    :reader ang_1
    :initarg :ang_1
    :type cl:float
    :initform 0.0)
   (dist_brake
    :reader dist_brake
    :initarg :dist_brake
    :type cl:float
    :initform 0.0)
   (dist_curve
    :reader dist_curve
    :initarg :dist_curve
    :type cl:float
    :initform 0.0)
   (lap_deviation
    :reader lap_deviation
    :initarg :lap_deviation
    :type cl:float
    :initform 0.0))
)

(cl:defclass SensorState (<SensorState>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <SensorState>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'SensorState)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name path_follower-msg:<SensorState> is deprecated: use path_follower-msg:SensorState instead.")))

(cl:ensure-generic-function 'warning_flag-val :lambda-list '(m))
(cl:defmethod warning_flag-val ((m <SensorState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:warning_flag-val is deprecated.  Use path_follower-msg:warning_flag instead.")
  (warning_flag m))

(cl:ensure-generic-function 'emergency_flag-val :lambda-list '(m))
(cl:defmethod emergency_flag-val ((m <SensorState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:emergency_flag-val is deprecated.  Use path_follower-msg:emergency_flag instead.")
  (emergency_flag m))

(cl:ensure-generic-function 'left_lidar_state-val :lambda-list '(m))
(cl:defmethod left_lidar_state-val ((m <SensorState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:left_lidar_state-val is deprecated.  Use path_follower-msg:left_lidar_state instead.")
  (left_lidar_state m))

(cl:ensure-generic-function 'right_lidar_state-val :lambda-list '(m))
(cl:defmethod right_lidar_state-val ((m <SensorState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:right_lidar_state-val is deprecated.  Use path_follower-msg:right_lidar_state instead.")
  (right_lidar_state m))

(cl:ensure-generic-function 'front_radar_state-val :lambda-list '(m))
(cl:defmethod front_radar_state-val ((m <SensorState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:front_radar_state-val is deprecated.  Use path_follower-msg:front_radar_state instead.")
  (front_radar_state m))

(cl:ensure-generic-function 'follow_with_sensors-val :lambda-list '(m))
(cl:defmethod follow_with_sensors-val ((m <SensorState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:follow_with_sensors-val is deprecated.  Use path_follower-msg:follow_with_sensors instead.")
  (follow_with_sensors m))

(cl:ensure-generic-function 'dist_x_0-val :lambda-list '(m))
(cl:defmethod dist_x_0-val ((m <SensorState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:dist_x_0-val is deprecated.  Use path_follower-msg:dist_x_0 instead.")
  (dist_x_0 m))

(cl:ensure-generic-function 'dist_y_0-val :lambda-list '(m))
(cl:defmethod dist_y_0-val ((m <SensorState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:dist_y_0-val is deprecated.  Use path_follower-msg:dist_y_0 instead.")
  (dist_y_0 m))

(cl:ensure-generic-function 'ang_0-val :lambda-list '(m))
(cl:defmethod ang_0-val ((m <SensorState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:ang_0-val is deprecated.  Use path_follower-msg:ang_0 instead.")
  (ang_0 m))

(cl:ensure-generic-function 'dist_x_1-val :lambda-list '(m))
(cl:defmethod dist_x_1-val ((m <SensorState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:dist_x_1-val is deprecated.  Use path_follower-msg:dist_x_1 instead.")
  (dist_x_1 m))

(cl:ensure-generic-function 'dist_y_1-val :lambda-list '(m))
(cl:defmethod dist_y_1-val ((m <SensorState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:dist_y_1-val is deprecated.  Use path_follower-msg:dist_y_1 instead.")
  (dist_y_1 m))

(cl:ensure-generic-function 'ang_1-val :lambda-list '(m))
(cl:defmethod ang_1-val ((m <SensorState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:ang_1-val is deprecated.  Use path_follower-msg:ang_1 instead.")
  (ang_1 m))

(cl:ensure-generic-function 'dist_brake-val :lambda-list '(m))
(cl:defmethod dist_brake-val ((m <SensorState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:dist_brake-val is deprecated.  Use path_follower-msg:dist_brake instead.")
  (dist_brake m))

(cl:ensure-generic-function 'dist_curve-val :lambda-list '(m))
(cl:defmethod dist_curve-val ((m <SensorState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:dist_curve-val is deprecated.  Use path_follower-msg:dist_curve instead.")
  (dist_curve m))

(cl:ensure-generic-function 'lap_deviation-val :lambda-list '(m))
(cl:defmethod lap_deviation-val ((m <SensorState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:lap_deviation-val is deprecated.  Use path_follower-msg:lap_deviation instead.")
  (lap_deviation m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <SensorState>) ostream)
  "Serializes a message object of type '<SensorState>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'warning_flag) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'emergency_flag) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'left_lidar_state) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'right_lidar_state) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'front_radar_state) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'follow_with_sensors) 1 0)) ostream)
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'dist_x_0))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'dist_y_0))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'ang_0))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'dist_x_1))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'dist_y_1))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'ang_1))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'dist_brake))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'dist_curve))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'lap_deviation))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <SensorState>) istream)
  "Deserializes a message object of type '<SensorState>"
    (cl:setf (cl:slot-value msg 'warning_flag) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'emergency_flag) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'left_lidar_state) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'right_lidar_state) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'front_radar_state) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'follow_with_sensors) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'dist_x_0) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'dist_y_0) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'ang_0) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'dist_x_1) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'dist_y_1) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'ang_1) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'dist_brake) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'dist_curve) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'lap_deviation) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<SensorState>)))
  "Returns string type for a message object of type '<SensorState>"
  "path_follower/SensorState")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'SensorState)))
  "Returns string type for a message object of type 'SensorState"
  "path_follower/SensorState")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<SensorState>)))
  "Returns md5sum for a message object of type '<SensorState>"
  "77399bad35ed57ed01c5dee8095579a7")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'SensorState)))
  "Returns md5sum for a message object of type 'SensorState"
  "77399bad35ed57ed01c5dee8095579a7")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<SensorState>)))
  "Returns full string definition for message of type '<SensorState>"
  (cl:format cl:nil "# This message constains the state of the sensors~%~%bool warning_flag~%bool emergency_flag~%bool left_lidar_state~%bool right_lidar_state~%bool front_radar_state~%bool follow_with_sensors~%~%float32 dist_x_0~%float32 dist_y_0~%float32 ang_0~%float32 dist_x_1~%float32 dist_y_1~%float32 ang_1~%~%float32 dist_brake~%float32 dist_curve~%float32 lap_deviation~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'SensorState)))
  "Returns full string definition for message of type 'SensorState"
  (cl:format cl:nil "# This message constains the state of the sensors~%~%bool warning_flag~%bool emergency_flag~%bool left_lidar_state~%bool right_lidar_state~%bool front_radar_state~%bool follow_with_sensors~%~%float32 dist_x_0~%float32 dist_y_0~%float32 ang_0~%float32 dist_x_1~%float32 dist_y_1~%float32 ang_1~%~%float32 dist_brake~%float32 dist_curve~%float32 lap_deviation~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <SensorState>))
  (cl:+ 0
     1
     1
     1
     1
     1
     1
     4
     4
     4
     4
     4
     4
     4
     4
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <SensorState>))
  "Converts a ROS message object to a list"
  (cl:list 'SensorState
    (cl:cons ':warning_flag (warning_flag msg))
    (cl:cons ':emergency_flag (emergency_flag msg))
    (cl:cons ':left_lidar_state (left_lidar_state msg))
    (cl:cons ':right_lidar_state (right_lidar_state msg))
    (cl:cons ':front_radar_state (front_radar_state msg))
    (cl:cons ':follow_with_sensors (follow_with_sensors msg))
    (cl:cons ':dist_x_0 (dist_x_0 msg))
    (cl:cons ':dist_y_0 (dist_y_0 msg))
    (cl:cons ':ang_0 (ang_0 msg))
    (cl:cons ':dist_x_1 (dist_x_1 msg))
    (cl:cons ':dist_y_1 (dist_y_1 msg))
    (cl:cons ':ang_1 (ang_1 msg))
    (cl:cons ':dist_brake (dist_brake msg))
    (cl:cons ':dist_curve (dist_curve msg))
    (cl:cons ':lap_deviation (lap_deviation msg))
))
