; Auto-generated. Do not edit!


(cl:in-package path_follower-msg)


;//! \htmlinclude PathInfo.msg.html

(cl:defclass <PathInfo> (roslisp-msg-protocol:ros-message)
  ((recording_time
    :reader recording_time
    :initarg :recording_time
    :type cl:real
    :initform 0)
   (name
    :reader name
    :initarg :name
    :type cl:string
    :initform "")
   (visualization_color
    :reader visualization_color
    :initarg :visualization_color
    :type std_msgs-msg:ColorRGBA
    :initform (cl:make-instance 'std_msgs-msg:ColorRGBA))
   (utm_zone_string
    :reader utm_zone_string
    :initarg :utm_zone_string
    :type cl:string
    :initform "")
   (num_points
    :reader num_points
    :initarg :num_points
    :type cl:fixnum
    :initform 0)
   (length
    :reader length
    :initarg :length
    :type cl:float
    :initform 0.0)
   (go_zone_radius
    :reader go_zone_radius
    :initarg :go_zone_radius
    :type cl:float
    :initform 0.0)
   (wait_zone_radius
    :reader wait_zone_radius
    :initarg :wait_zone_radius
    :type cl:float
    :initform 0.0))
)

(cl:defclass PathInfo (<PathInfo>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <PathInfo>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'PathInfo)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name path_follower-msg:<PathInfo> is deprecated: use path_follower-msg:PathInfo instead.")))

(cl:ensure-generic-function 'recording_time-val :lambda-list '(m))
(cl:defmethod recording_time-val ((m <PathInfo>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:recording_time-val is deprecated.  Use path_follower-msg:recording_time instead.")
  (recording_time m))

(cl:ensure-generic-function 'name-val :lambda-list '(m))
(cl:defmethod name-val ((m <PathInfo>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:name-val is deprecated.  Use path_follower-msg:name instead.")
  (name m))

(cl:ensure-generic-function 'visualization_color-val :lambda-list '(m))
(cl:defmethod visualization_color-val ((m <PathInfo>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:visualization_color-val is deprecated.  Use path_follower-msg:visualization_color instead.")
  (visualization_color m))

(cl:ensure-generic-function 'utm_zone_string-val :lambda-list '(m))
(cl:defmethod utm_zone_string-val ((m <PathInfo>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:utm_zone_string-val is deprecated.  Use path_follower-msg:utm_zone_string instead.")
  (utm_zone_string m))

(cl:ensure-generic-function 'num_points-val :lambda-list '(m))
(cl:defmethod num_points-val ((m <PathInfo>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:num_points-val is deprecated.  Use path_follower-msg:num_points instead.")
  (num_points m))

(cl:ensure-generic-function 'length-val :lambda-list '(m))
(cl:defmethod length-val ((m <PathInfo>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:length-val is deprecated.  Use path_follower-msg:length instead.")
  (length m))

(cl:ensure-generic-function 'go_zone_radius-val :lambda-list '(m))
(cl:defmethod go_zone_radius-val ((m <PathInfo>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:go_zone_radius-val is deprecated.  Use path_follower-msg:go_zone_radius instead.")
  (go_zone_radius m))

(cl:ensure-generic-function 'wait_zone_radius-val :lambda-list '(m))
(cl:defmethod wait_zone_radius-val ((m <PathInfo>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:wait_zone_radius-val is deprecated.  Use path_follower-msg:wait_zone_radius instead.")
  (wait_zone_radius m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <PathInfo>) ostream)
  "Serializes a message object of type '<PathInfo>"
  (cl:let ((__sec (cl:floor (cl:slot-value msg 'recording_time)))
        (__nsec (cl:round (cl:* 1e9 (cl:- (cl:slot-value msg 'recording_time) (cl:floor (cl:slot-value msg 'recording_time)))))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __sec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __sec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __sec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __sec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 0) __nsec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __nsec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __nsec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __nsec) ostream))
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'name))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'name))
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'visualization_color) ostream)
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'utm_zone_string))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'utm_zone_string))
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'num_points)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'num_points)) ostream)
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'length))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'go_zone_radius))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'wait_zone_radius))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <PathInfo>) istream)
  "Deserializes a message object of type '<PathInfo>"
    (cl:let ((__sec 0) (__nsec 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __sec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __sec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __sec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __sec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 0) __nsec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __nsec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __nsec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __nsec) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'recording_time) (cl:+ (cl:coerce __sec 'cl:double-float) (cl:/ __nsec 1e9))))
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'name) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'name) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'visualization_color) istream)
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'utm_zone_string) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'utm_zone_string) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'num_points)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'num_points)) (cl:read-byte istream))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'length) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'go_zone_radius) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'wait_zone_radius) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<PathInfo>)))
  "Returns string type for a message object of type '<PathInfo>"
  "path_follower/PathInfo")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'PathInfo)))
  "Returns string type for a message object of type 'PathInfo"
  "path_follower/PathInfo")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<PathInfo>)))
  "Returns md5sum for a message object of type '<PathInfo>"
  "33a4f61a6c31b8cdccfa3cc9ec623b2d")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'PathInfo)))
  "Returns md5sum for a message object of type 'PathInfo"
  "33a4f61a6c31b8cdccfa3cc9ec623b2d")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<PathInfo>)))
  "Returns full string definition for message of type '<PathInfo>"
  (cl:format cl:nil "time 	  recording_time        # time when path recording has started~%string    name                  # name of the path~%std_msgs/ColorRGBA visualization_color   # color of path visualization in rviz~%string 	  utm_zone_string       # UTM zone of the path (string representation)~%uint16    num_points            # number of points in the path~%float32   length                # path length (sum of all segment lengths)~%float32   go_zone_radius        # [m] radius of go-zone~%float32   wait_zone_radius      # [m] radius of wait-zone~%~%================================================================================~%MSG: std_msgs/ColorRGBA~%float32 r~%float32 g~%float32 b~%float32 a~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'PathInfo)))
  "Returns full string definition for message of type 'PathInfo"
  (cl:format cl:nil "time 	  recording_time        # time when path recording has started~%string    name                  # name of the path~%std_msgs/ColorRGBA visualization_color   # color of path visualization in rviz~%string 	  utm_zone_string       # UTM zone of the path (string representation)~%uint16    num_points            # number of points in the path~%float32   length                # path length (sum of all segment lengths)~%float32   go_zone_radius        # [m] radius of go-zone~%float32   wait_zone_radius      # [m] radius of wait-zone~%~%================================================================================~%MSG: std_msgs/ColorRGBA~%float32 r~%float32 g~%float32 b~%float32 a~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <PathInfo>))
  (cl:+ 0
     8
     4 (cl:length (cl:slot-value msg 'name))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'visualization_color))
     4 (cl:length (cl:slot-value msg 'utm_zone_string))
     2
     4
     4
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <PathInfo>))
  "Converts a ROS message object to a list"
  (cl:list 'PathInfo
    (cl:cons ':recording_time (recording_time msg))
    (cl:cons ':name (name msg))
    (cl:cons ':visualization_color (visualization_color msg))
    (cl:cons ':utm_zone_string (utm_zone_string msg))
    (cl:cons ':num_points (num_points msg))
    (cl:cons ':length (length msg))
    (cl:cons ':go_zone_radius (go_zone_radius msg))
    (cl:cons ':wait_zone_radius (wait_zone_radius msg))
))
