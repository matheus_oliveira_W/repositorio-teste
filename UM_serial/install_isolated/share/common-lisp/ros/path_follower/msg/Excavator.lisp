; Auto-generated. Do not edit!


(cl:in-package path_follower-msg)


;//! \htmlinclude Excavator.msg.html

(cl:defclass <Excavator> (roslisp-msg-protocol:ros-message)
  ((ex_latitude
    :reader ex_latitude
    :initarg :ex_latitude
    :type cl:float
    :initform 0.0)
   (ex_longitude
    :reader ex_longitude
    :initarg :ex_longitude
    :type cl:float
    :initform 0.0))
)

(cl:defclass Excavator (<Excavator>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <Excavator>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'Excavator)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name path_follower-msg:<Excavator> is deprecated: use path_follower-msg:Excavator instead.")))

(cl:ensure-generic-function 'ex_latitude-val :lambda-list '(m))
(cl:defmethod ex_latitude-val ((m <Excavator>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:ex_latitude-val is deprecated.  Use path_follower-msg:ex_latitude instead.")
  (ex_latitude m))

(cl:ensure-generic-function 'ex_longitude-val :lambda-list '(m))
(cl:defmethod ex_longitude-val ((m <Excavator>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader path_follower-msg:ex_longitude-val is deprecated.  Use path_follower-msg:ex_longitude instead.")
  (ex_longitude m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <Excavator>) ostream)
  "Serializes a message object of type '<Excavator>"
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'ex_latitude))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'ex_longitude))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <Excavator>) istream)
  "Deserializes a message object of type '<Excavator>"
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'ex_latitude) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'ex_longitude) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<Excavator>)))
  "Returns string type for a message object of type '<Excavator>"
  "path_follower/Excavator")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'Excavator)))
  "Returns string type for a message object of type 'Excavator"
  "path_follower/Excavator")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<Excavator>)))
  "Returns md5sum for a message object of type '<Excavator>"
  "d4836f677d2197f5049d039e568a5f7c")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'Excavator)))
  "Returns md5sum for a message object of type 'Excavator"
  "d4836f677d2197f5049d039e568a5f7c")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<Excavator>)))
  "Returns full string definition for message of type '<Excavator>"
  (cl:format cl:nil "float32 ex_latitude~%float32 ex_longitude~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'Excavator)))
  "Returns full string definition for message of type 'Excavator"
  (cl:format cl:nil "float32 ex_latitude~%float32 ex_longitude~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <Excavator>))
  (cl:+ 0
     4
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <Excavator>))
  "Converts a ROS message object to a list"
  (cl:list 'Excavator
    (cl:cons ':ex_latitude (ex_latitude msg))
    (cl:cons ':ex_longitude (ex_longitude msg))
))
