(cl:in-package path_follower-msg)
(cl:export '(X-VAL
          X
          Y-VAL
          Y
          ALT-VAL
          ALT
          VELOCITY-VAL
          VELOCITY
          YAW_ANGLE-VAL
          YAW_ANGLE
          HEADING-VAL
          HEADING
          GEAR-VAL
          GEAR
          GPS_DATA_VALID-VAL
          GPS_DATA_VALID
          X_REL-VAL
          X_REL
          Y_REL-VAL
          Y_REL
          REL_DATA_VALID-VAL
          REL_DATA_VALID
          STEERING_ANGLE-VAL
          STEERING_ANGLE
          STEERING_DATA_VALID-VAL
          STEERING_DATA_VALID
          OBSTACLE_VALIDATION-VAL
          OBSTACLE_VALIDATION
          RIGHT_LIDAR_SEGMENTATION-VAL
          RIGHT_LIDAR_SEGMENTATION
          LEFT_LIDAR_SEGMENTATION-VAL
          LEFT_LIDAR_SEGMENTATION
          RADAR_SEGMENTATION-VAL
          RADAR_SEGMENTATION
          RIGHT_LIDAR_FEEDBACK_X-VAL
          RIGHT_LIDAR_FEEDBACK_X
          LEFT_LIDAR_FEEDBACK_X-VAL
          LEFT_LIDAR_FEEDBACK_X
          RADAR_FEEDBACK_X-VAL
          RADAR_FEEDBACK_X
          RIGHT_LIDAR_FEEDBACK_Y-VAL
          RIGHT_LIDAR_FEEDBACK_Y
          LEFT_LIDAR_FEEDBACK_Y-VAL
          LEFT_LIDAR_FEEDBACK_Y
          RADAR_FEEDBACK_Y-VAL
          RADAR_FEEDBACK_Y
          EMERGENCY_BRAKE-VAL
          EMERGENCY_BRAKE
          RADAR_BACK_FEEDBACK_X-VAL
          RADAR_BACK_FEEDBACK_X
          LIDAR_BACK_FEEDBACK_X-VAL
          LIDAR_BACK_FEEDBACK_X
          RADAR_BACK_FEEDBACK_Y-VAL
          RADAR_BACK_FEEDBACK_Y
          LIDAR_BACK_FEEDBACK_Y-VAL
          LIDAR_BACK_FEEDBACK_Y
          BACK_EMERGENCY_BRAKE-VAL
          BACK_EMERGENCY_BRAKE
          LIDAR_BACK_DETECTION-VAL
          LIDAR_BACK_DETECTION
          BACK_RADAR_DETECTION-VAL
          BACK_RADAR_DETECTION
          REVERSE_FLAG-VAL
          REVERSE_FLAG
))