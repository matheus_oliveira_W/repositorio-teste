(cl:in-package path_follower-msg)
(cl:export '(WARNING_FLAG-VAL
          WARNING_FLAG
          EMERGENCY_FLAG-VAL
          EMERGENCY_FLAG
          LEFT_LIDAR_STATE-VAL
          LEFT_LIDAR_STATE
          RIGHT_LIDAR_STATE-VAL
          RIGHT_LIDAR_STATE
          FRONT_RADAR_STATE-VAL
          FRONT_RADAR_STATE
          FOLLOW_WITH_SENSORS-VAL
          FOLLOW_WITH_SENSORS
          DIST_X_0-VAL
          DIST_X_0
          DIST_Y_0-VAL
          DIST_Y_0
          ANG_0-VAL
          ANG_0
          DIST_X_1-VAL
          DIST_X_1
          DIST_Y_1-VAL
          DIST_Y_1
          ANG_1-VAL
          ANG_1
          DIST_BRAKE-VAL
          DIST_BRAKE
          DIST_CURVE-VAL
          DIST_CURVE
          LAP_DEVIATION-VAL
          LAP_DEVIATION
))