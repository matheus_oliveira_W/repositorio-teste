; Auto-generated. Do not edit!


(cl:in-package path_follower-msg)


;//! \htmlinclude ServiceCommandReply.msg.html

(cl:defclass <ServiceCommandReply> (roslisp-msg-protocol:ros-message)
  ()
)

(cl:defclass ServiceCommandReply (<ServiceCommandReply>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <ServiceCommandReply>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'ServiceCommandReply)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name path_follower-msg:<ServiceCommandReply> is deprecated: use path_follower-msg:ServiceCommandReply instead.")))
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql '<ServiceCommandReply>)))
    "Constants for message type '<ServiceCommandReply>"
  '((:SUCCESS . 0)
    (:FAIL_OXTS_OFFLINE . 10)
    (:FAIL_OXTS_NO_DATA . 11)
    (:FAIL_OXTS_DATA_BUT_NO_FIX . 12)
    (:FAIL_UEM_OFFLINE . 13)
    (:FAIL_UEM_NO_DATA . 14)
    (:FAIL_UEM_NO_REMOTE_CONTROL . 15)
    (:FAIL_STATE_LOGIC . 16)
    (:FAIL_NO_PATH_AVAILABLE . 17)
    (:FAIL_PATH_TOO_SHORT . 18)
    (:FAIL_LOADING_PATH_FROM_FILE . 19)
    (:FAIL_VEHCILE_NOT_IN_GO_ZONE . 20)
    (:FAIL_OLD_PLOT_STILL_ACTIVE . 21)
    (:FAIL_BUCKET_IS_OPENED . 22)
    (:WARN_UEM_OFFLINE . 30)
    (:WARN_UEM_NO_DATA . 31)
    (:WARN_UEM_NO_REMOTE_CONTROL . 32)
    (:SERVICE_ERROR . 66))
)
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql 'ServiceCommandReply)))
    "Constants for message type 'ServiceCommandReply"
  '((:SUCCESS . 0)
    (:FAIL_OXTS_OFFLINE . 10)
    (:FAIL_OXTS_NO_DATA . 11)
    (:FAIL_OXTS_DATA_BUT_NO_FIX . 12)
    (:FAIL_UEM_OFFLINE . 13)
    (:FAIL_UEM_NO_DATA . 14)
    (:FAIL_UEM_NO_REMOTE_CONTROL . 15)
    (:FAIL_STATE_LOGIC . 16)
    (:FAIL_NO_PATH_AVAILABLE . 17)
    (:FAIL_PATH_TOO_SHORT . 18)
    (:FAIL_LOADING_PATH_FROM_FILE . 19)
    (:FAIL_VEHCILE_NOT_IN_GO_ZONE . 20)
    (:FAIL_OLD_PLOT_STILL_ACTIVE . 21)
    (:FAIL_BUCKET_IS_OPENED . 22)
    (:WARN_UEM_OFFLINE . 30)
    (:WARN_UEM_NO_DATA . 31)
    (:WARN_UEM_NO_REMOTE_CONTROL . 32)
    (:SERVICE_ERROR . 66))
)
(cl:defmethod roslisp-msg-protocol:serialize ((msg <ServiceCommandReply>) ostream)
  "Serializes a message object of type '<ServiceCommandReply>"
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <ServiceCommandReply>) istream)
  "Deserializes a message object of type '<ServiceCommandReply>"
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<ServiceCommandReply>)))
  "Returns string type for a message object of type '<ServiceCommandReply>"
  "path_follower/ServiceCommandReply")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'ServiceCommandReply)))
  "Returns string type for a message object of type 'ServiceCommandReply"
  "path_follower/ServiceCommandReply")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<ServiceCommandReply>)))
  "Returns md5sum for a message object of type '<ServiceCommandReply>"
  "f05070682928bf756746c7dc8b2dcb1e")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'ServiceCommandReply)))
  "Returns md5sum for a message object of type 'ServiceCommandReply"
  "f05070682928bf756746c7dc8b2dcb1e")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<ServiceCommandReply>)))
  "Returns full string definition for message of type '<ServiceCommandReply>"
  (cl:format cl:nil "# success reply~%uint8 	SUCCESS = 0~%~%# fail replies~%uint8 	FAIL_OXTS_OFFLINE              = 10~%uint8 	FAIL_OXTS_NO_DATA              = 11~%uint8  	FAIL_OXTS_DATA_BUT_NO_FIX      = 12~%uint8 	FAIL_UEM_OFFLINE               = 13~%uint8 	FAIL_UEM_NO_DATA               = 14~%uint8   FAIL_UEM_NO_REMOTE_CONTROL     = 15~%uint8   FAIL_STATE_LOGIC               = 16~%uint8   FAIL_NO_PATH_AVAILABLE         = 17~%uint8   FAIL_PATH_TOO_SHORT            = 18~%uint8   FAIL_LOADING_PATH_FROM_FILE    = 19~%uint8   FAIL_VEHCILE_NOT_IN_GO_ZONE    = 20~%uint8   FAIL_OLD_PLOT_STILL_ACTIVE     = 21~%uint8   FAIL_BUCKET_IS_OPENED          = 22~%~%# warn replies~%uint8   WARN_UEM_OFFLINE               = 30~%uint8   WARN_UEM_NO_DATA               = 31~%uint8   WARN_UEM_NO_REMOTE_CONTROL     = 32~%~%# service error~%uint8 SERVICE_ERROR = 66~%~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'ServiceCommandReply)))
  "Returns full string definition for message of type 'ServiceCommandReply"
  (cl:format cl:nil "# success reply~%uint8 	SUCCESS = 0~%~%# fail replies~%uint8 	FAIL_OXTS_OFFLINE              = 10~%uint8 	FAIL_OXTS_NO_DATA              = 11~%uint8  	FAIL_OXTS_DATA_BUT_NO_FIX      = 12~%uint8 	FAIL_UEM_OFFLINE               = 13~%uint8 	FAIL_UEM_NO_DATA               = 14~%uint8   FAIL_UEM_NO_REMOTE_CONTROL     = 15~%uint8   FAIL_STATE_LOGIC               = 16~%uint8   FAIL_NO_PATH_AVAILABLE         = 17~%uint8   FAIL_PATH_TOO_SHORT            = 18~%uint8   FAIL_LOADING_PATH_FROM_FILE    = 19~%uint8   FAIL_VEHCILE_NOT_IN_GO_ZONE    = 20~%uint8   FAIL_OLD_PLOT_STILL_ACTIVE     = 21~%uint8   FAIL_BUCKET_IS_OPENED          = 22~%~%# warn replies~%uint8   WARN_UEM_OFFLINE               = 30~%uint8   WARN_UEM_NO_DATA               = 31~%uint8   WARN_UEM_NO_REMOTE_CONTROL     = 32~%~%# service error~%uint8 SERVICE_ERROR = 66~%~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <ServiceCommandReply>))
  (cl:+ 0
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <ServiceCommandReply>))
  "Converts a ROS message object to a list"
  (cl:list 'ServiceCommandReply
))
