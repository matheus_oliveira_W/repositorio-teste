; Auto-generated. Do not edit!


(cl:in-package remote_control-msg)


;//! \htmlinclude RemoteControlStatesArray.msg.html

(cl:defclass <RemoteControlStatesArray> (roslisp-msg-protocol:ros-message)
  ((info
    :reader info
    :initarg :info
    :type (cl:vector remote_control-msg:RemoteControlStates)
   :initform (cl:make-array 0 :element-type 'remote_control-msg:RemoteControlStates :initial-element (cl:make-instance 'remote_control-msg:RemoteControlStates))))
)

(cl:defclass RemoteControlStatesArray (<RemoteControlStatesArray>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <RemoteControlStatesArray>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'RemoteControlStatesArray)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name remote_control-msg:<RemoteControlStatesArray> is deprecated: use remote_control-msg:RemoteControlStatesArray instead.")))

(cl:ensure-generic-function 'info-val :lambda-list '(m))
(cl:defmethod info-val ((m <RemoteControlStatesArray>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader remote_control-msg:info-val is deprecated.  Use remote_control-msg:info instead.")
  (info m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <RemoteControlStatesArray>) ostream)
  "Serializes a message object of type '<RemoteControlStatesArray>"
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'info))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'info))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <RemoteControlStatesArray>) istream)
  "Deserializes a message object of type '<RemoteControlStatesArray>"
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'info) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'info)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'remote_control-msg:RemoteControlStates))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<RemoteControlStatesArray>)))
  "Returns string type for a message object of type '<RemoteControlStatesArray>"
  "remote_control/RemoteControlStatesArray")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'RemoteControlStatesArray)))
  "Returns string type for a message object of type 'RemoteControlStatesArray"
  "remote_control/RemoteControlStatesArray")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<RemoteControlStatesArray>)))
  "Returns md5sum for a message object of type '<RemoteControlStatesArray>"
  "0547a26e3f37504504fac7b1153afa62")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'RemoteControlStatesArray)))
  "Returns md5sum for a message object of type 'RemoteControlStatesArray"
  "0547a26e3f37504504fac7b1153afa62")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<RemoteControlStatesArray>)))
  "Returns full string definition for message of type '<RemoteControlStatesArray>"
  (cl:format cl:nil "RemoteControlStates[] info #array of Remote Control messages~%~%================================================================================~%MSG: remote_control/RemoteControlStates~%float32 brake_throttle 		# left joystick~%float32 steering		# right joystick~%~%#### byte 2 ####~%~%bool horn			# B3~%bool open_bucket		# B4~%bool close_bucket		# B5~%uint16 beams			# CH1 (OP2 = 0 switched-off; OFF = 1 low beam; OP1 = 2 high beam)~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'RemoteControlStatesArray)))
  "Returns full string definition for message of type 'RemoteControlStatesArray"
  (cl:format cl:nil "RemoteControlStates[] info #array of Remote Control messages~%~%================================================================================~%MSG: remote_control/RemoteControlStates~%float32 brake_throttle 		# left joystick~%float32 steering		# right joystick~%~%#### byte 2 ####~%~%bool horn			# B3~%bool open_bucket		# B4~%bool close_bucket		# B5~%uint16 beams			# CH1 (OP2 = 0 switched-off; OFF = 1 low beam; OP1 = 2 high beam)~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <RemoteControlStatesArray>))
  (cl:+ 0
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'info) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <RemoteControlStatesArray>))
  "Converts a ROS message object to a list"
  (cl:list 'RemoteControlStatesArray
    (cl:cons ':info (info msg))
))
