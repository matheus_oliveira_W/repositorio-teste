
(cl:in-package :asdf)

(defsystem "remote_control-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "RemoteControlStates" :depends-on ("_package_RemoteControlStates"))
    (:file "_package_RemoteControlStates" :depends-on ("_package"))
    (:file "RemoteControlStatesArray" :depends-on ("_package_RemoteControlStatesArray"))
    (:file "_package_RemoteControlStatesArray" :depends-on ("_package"))
  ))