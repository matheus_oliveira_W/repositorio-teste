;; Auto-generated. Do not edit!


(when (boundp 'path_follower::PathFollowerService)
  (if (not (find-package "PATH_FOLLOWER"))
    (make-package "PATH_FOLLOWER"))
  (shadow 'PathFollowerService (find-package "PATH_FOLLOWER")))
(unless (find-package "PATH_FOLLOWER::PATHFOLLOWERSERVICE")
  (make-package "PATH_FOLLOWER::PATHFOLLOWERSERVICE"))
(unless (find-package "PATH_FOLLOWER::PATHFOLLOWERSERVICEREQUEST")
  (make-package "PATH_FOLLOWER::PATHFOLLOWERSERVICEREQUEST"))
(unless (find-package "PATH_FOLLOWER::PATHFOLLOWERSERVICERESPONSE")
  (make-package "PATH_FOLLOWER::PATHFOLLOWERSERVICERESPONSE"))

(in-package "ROS")





(defclass path_follower::PathFollowerServiceRequest
  :super ros::object
  :slots (_command _str_data _float_data _int_data ))

(defmethod path_follower::PathFollowerServiceRequest
  (:init
   (&key
    ((:command __command) 0)
    ((:str_data __str_data) "")
    ((:float_data __float_data) 0.0)
    ((:int_data __int_data) 0)
    )
   (send-super :init)
   (setq _command (round __command))
   (setq _str_data (string __str_data))
   (setq _float_data (float __float_data))
   (setq _int_data (round __int_data))
   self)
  (:command
   (&optional __command)
   (if __command (setq _command __command)) _command)
  (:str_data
   (&optional __str_data)
   (if __str_data (setq _str_data __str_data)) _str_data)
  (:float_data
   (&optional __float_data)
   (if __float_data (setq _float_data __float_data)) _float_data)
  (:int_data
   (&optional __int_data)
   (if __int_data (setq _int_data __int_data)) _int_data)
  (:serialization-length
   ()
   (+
    ;; uint8 _command
    1
    ;; string _str_data
    4 (length _str_data)
    ;; float32 _float_data
    4
    ;; int32 _int_data
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; uint8 _command
       (write-byte _command s)
     ;; string _str_data
       (write-long (length _str_data) s) (princ _str_data s)
     ;; float32 _float_data
       (sys::poke _float_data (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; int32 _int_data
       (write-long _int_data s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; uint8 _command
     (setq _command (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; string _str_data
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _str_data (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; float32 _float_data
     (setq _float_data (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; int32 _int_data
     (setq _int_data (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;;
   self)
  )

(defclass path_follower::PathFollowerServiceResponse
  :super ros::object
  :slots (_reply _str_reply _float_reply _int_reply ))

(defmethod path_follower::PathFollowerServiceResponse
  (:init
   (&key
    ((:reply __reply) 0)
    ((:str_reply __str_reply) "")
    ((:float_reply __float_reply) 0.0)
    ((:int_reply __int_reply) 0)
    )
   (send-super :init)
   (setq _reply (round __reply))
   (setq _str_reply (string __str_reply))
   (setq _float_reply (float __float_reply))
   (setq _int_reply (round __int_reply))
   self)
  (:reply
   (&optional __reply)
   (if __reply (setq _reply __reply)) _reply)
  (:str_reply
   (&optional __str_reply)
   (if __str_reply (setq _str_reply __str_reply)) _str_reply)
  (:float_reply
   (&optional __float_reply)
   (if __float_reply (setq _float_reply __float_reply)) _float_reply)
  (:int_reply
   (&optional __int_reply)
   (if __int_reply (setq _int_reply __int_reply)) _int_reply)
  (:serialization-length
   ()
   (+
    ;; uint8 _reply
    1
    ;; string _str_reply
    4 (length _str_reply)
    ;; float32 _float_reply
    4
    ;; int32 _int_reply
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; uint8 _reply
       (write-byte _reply s)
     ;; string _str_reply
       (write-long (length _str_reply) s) (princ _str_reply s)
     ;; float32 _float_reply
       (sys::poke _float_reply (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; int32 _int_reply
       (write-long _int_reply s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; uint8 _reply
     (setq _reply (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; string _str_reply
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _str_reply (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; float32 _float_reply
     (setq _float_reply (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; int32 _int_reply
     (setq _int_reply (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;;
   self)
  )

(defclass path_follower::PathFollowerService
  :super ros::object
  :slots ())

(setf (get path_follower::PathFollowerService :md5sum-) "635ea1697aa662b37b6293cb945f6681")
(setf (get path_follower::PathFollowerService :datatype-) "path_follower/PathFollowerService")
(setf (get path_follower::PathFollowerService :request) path_follower::PathFollowerServiceRequest)
(setf (get path_follower::PathFollowerService :response) path_follower::PathFollowerServiceResponse)

(defmethod path_follower::PathFollowerServiceRequest
  (:response () (instance path_follower::PathFollowerServiceResponse :init)))

(setf (get path_follower::PathFollowerServiceRequest :md5sum-) "635ea1697aa662b37b6293cb945f6681")
(setf (get path_follower::PathFollowerServiceRequest :datatype-) "path_follower/PathFollowerServiceRequest")
(setf (get path_follower::PathFollowerServiceRequest :definition-)
      "uint8 	command
string 	str_data
float32 float_data
int32 	int_data
---
uint8 	reply
string 	str_reply
float32 float_reply
int32 	int_reply


")

(setf (get path_follower::PathFollowerServiceResponse :md5sum-) "635ea1697aa662b37b6293cb945f6681")
(setf (get path_follower::PathFollowerServiceResponse :datatype-) "path_follower/PathFollowerServiceResponse")
(setf (get path_follower::PathFollowerServiceResponse :definition-)
      "uint8 	command
string 	str_data
float32 float_data
int32 	int_data
---
uint8 	reply
string 	str_reply
float32 float_reply
int32 	int_reply


")



(provide :path_follower/PathFollowerService "635ea1697aa662b37b6293cb945f6681")


