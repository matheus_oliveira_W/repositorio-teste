;; Auto-generated. Do not edit!


(when (boundp 'path_follower::ControlTargetState)
  (if (not (find-package "PATH_FOLLOWER"))
    (make-package "PATH_FOLLOWER"))
  (shadow 'ControlTargetState (find-package "PATH_FOLLOWER")))
(unless (find-package "PATH_FOLLOWER::CONTROLTARGETSTATE")
  (make-package "PATH_FOLLOWER::CONTROLTARGETSTATE"))

(in-package "ROS")
;;//! \htmlinclude ControlTargetState.msg.html


(defclass path_follower::ControlTargetState
  :super ros::object
  :slots (_closest_point_x _closest_point_y _look_ahead_point_x _look_ahead_point_y _look_ahead_distance _velocity _steering_angle _steering_angle_norm _gas_brake_norm ))

(defmethod path_follower::ControlTargetState
  (:init
   (&key
    ((:closest_point_x __closest_point_x) 0.0)
    ((:closest_point_y __closest_point_y) 0.0)
    ((:look_ahead_point_x __look_ahead_point_x) 0.0)
    ((:look_ahead_point_y __look_ahead_point_y) 0.0)
    ((:look_ahead_distance __look_ahead_distance) 0.0)
    ((:velocity __velocity) 0.0)
    ((:steering_angle __steering_angle) 0.0)
    ((:steering_angle_norm __steering_angle_norm) 0.0)
    ((:gas_brake_norm __gas_brake_norm) 0.0)
    )
   (send-super :init)
   (setq _closest_point_x (float __closest_point_x))
   (setq _closest_point_y (float __closest_point_y))
   (setq _look_ahead_point_x (float __look_ahead_point_x))
   (setq _look_ahead_point_y (float __look_ahead_point_y))
   (setq _look_ahead_distance (float __look_ahead_distance))
   (setq _velocity (float __velocity))
   (setq _steering_angle (float __steering_angle))
   (setq _steering_angle_norm (float __steering_angle_norm))
   (setq _gas_brake_norm (float __gas_brake_norm))
   self)
  (:closest_point_x
   (&optional __closest_point_x)
   (if __closest_point_x (setq _closest_point_x __closest_point_x)) _closest_point_x)
  (:closest_point_y
   (&optional __closest_point_y)
   (if __closest_point_y (setq _closest_point_y __closest_point_y)) _closest_point_y)
  (:look_ahead_point_x
   (&optional __look_ahead_point_x)
   (if __look_ahead_point_x (setq _look_ahead_point_x __look_ahead_point_x)) _look_ahead_point_x)
  (:look_ahead_point_y
   (&optional __look_ahead_point_y)
   (if __look_ahead_point_y (setq _look_ahead_point_y __look_ahead_point_y)) _look_ahead_point_y)
  (:look_ahead_distance
   (&optional __look_ahead_distance)
   (if __look_ahead_distance (setq _look_ahead_distance __look_ahead_distance)) _look_ahead_distance)
  (:velocity
   (&optional __velocity)
   (if __velocity (setq _velocity __velocity)) _velocity)
  (:steering_angle
   (&optional __steering_angle)
   (if __steering_angle (setq _steering_angle __steering_angle)) _steering_angle)
  (:steering_angle_norm
   (&optional __steering_angle_norm)
   (if __steering_angle_norm (setq _steering_angle_norm __steering_angle_norm)) _steering_angle_norm)
  (:gas_brake_norm
   (&optional __gas_brake_norm)
   (if __gas_brake_norm (setq _gas_brake_norm __gas_brake_norm)) _gas_brake_norm)
  (:serialization-length
   ()
   (+
    ;; float32 _closest_point_x
    4
    ;; float32 _closest_point_y
    4
    ;; float32 _look_ahead_point_x
    4
    ;; float32 _look_ahead_point_y
    4
    ;; float32 _look_ahead_distance
    4
    ;; float32 _velocity
    4
    ;; float32 _steering_angle
    4
    ;; float32 _steering_angle_norm
    4
    ;; float32 _gas_brake_norm
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float32 _closest_point_x
       (sys::poke _closest_point_x (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _closest_point_y
       (sys::poke _closest_point_y (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _look_ahead_point_x
       (sys::poke _look_ahead_point_x (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _look_ahead_point_y
       (sys::poke _look_ahead_point_y (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _look_ahead_distance
       (sys::poke _look_ahead_distance (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _velocity
       (sys::poke _velocity (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _steering_angle
       (sys::poke _steering_angle (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _steering_angle_norm
       (sys::poke _steering_angle_norm (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _gas_brake_norm
       (sys::poke _gas_brake_norm (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float32 _closest_point_x
     (setq _closest_point_x (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _closest_point_y
     (setq _closest_point_y (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _look_ahead_point_x
     (setq _look_ahead_point_x (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _look_ahead_point_y
     (setq _look_ahead_point_y (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _look_ahead_distance
     (setq _look_ahead_distance (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _velocity
     (setq _velocity (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _steering_angle
     (setq _steering_angle (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _steering_angle_norm
     (setq _steering_angle_norm (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _gas_brake_norm
     (setq _gas_brake_norm (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get path_follower::ControlTargetState :md5sum-) "59db037f6601c72a4efd8f7c6d69d33a")
(setf (get path_follower::ControlTargetState :datatype-) "path_follower/ControlTargetState")
(setf (get path_follower::ControlTargetState :definition-)
      "# This message contains the target values used for guiding the vehicle

float32 closest_point_x     # [m] UTM easting of the point on the path which is currently closest to the vehicle
float32 closest_point_y     # [m] UTM northing 
float32 look_ahead_point_x  # [m] UTM easting of the look ahead point (target point for steering controller)
float32 look_ahead_point_y  # [m] UTM northing 
float32 look_ahead_distance # [m] distance from closest point to look ahead point, measured along the path
float32 velocity            # [m/s] target velocity
float32 steering_angle      # [rad] 
float32 steering_angle_norm # [-] normalized steering angle (-1, 1)
float32 gas_brake_norm      # [-] normalized gas/brake position (-1, 1)
")



(provide :path_follower/ControlTargetState "59db037f6601c72a4efd8f7c6d69d33a")


