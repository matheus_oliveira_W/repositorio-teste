;; Auto-generated. Do not edit!


(when (boundp 'path_follower::StateEvents)
  (if (not (find-package "PATH_FOLLOWER"))
    (make-package "PATH_FOLLOWER"))
  (shadow 'StateEvents (find-package "PATH_FOLLOWER")))
(unless (find-package "PATH_FOLLOWER::STATEEVENTS")
  (make-package "PATH_FOLLOWER::STATEEVENTS"))

(in-package "ROS")
;;//! \htmlinclude StateEvents.msg.html


(intern "*RECORDING*" (find-package "PATH_FOLLOWER::STATEEVENTS"))
(shadow '*RECORDING* (find-package "PATH_FOLLOWER::STATEEVENTS"))
(defconstant path_follower::StateEvents::*RECORDING* 0)
(intern "*IDLE*" (find-package "PATH_FOLLOWER::STATEEVENTS"))
(shadow '*IDLE* (find-package "PATH_FOLLOWER::STATEEVENTS"))
(defconstant path_follower::StateEvents::*IDLE* 1)
(intern "*FOLLOWING*" (find-package "PATH_FOLLOWER::STATEEVENTS"))
(shadow '*FOLLOWING* (find-package "PATH_FOLLOWER::STATEEVENTS"))
(defconstant path_follower::StateEvents::*FOLLOWING* 2)
(intern "*WAIT_ZONE_REACHED*" (find-package "PATH_FOLLOWER::STATEEVENTS"))
(shadow '*WAIT_ZONE_REACHED* (find-package "PATH_FOLLOWER::STATEEVENTS"))
(defconstant path_follower::StateEvents::*WAIT_ZONE_REACHED* 3)
(intern "*HANDOVER*" (find-package "PATH_FOLLOWER::STATEEVENTS"))
(shadow '*HANDOVER* (find-package "PATH_FOLLOWER::STATEEVENTS"))
(defconstant path_follower::StateEvents::*HANDOVER* 4)
(intern "*RECORDING_FAILED_OXTS_NODE_DEAD*" (find-package "PATH_FOLLOWER::STATEEVENTS"))
(shadow '*RECORDING_FAILED_OXTS_NODE_DEAD* (find-package "PATH_FOLLOWER::STATEEVENTS"))
(defconstant path_follower::StateEvents::*RECORDING_FAILED_OXTS_NODE_DEAD* 10)
(intern "*RECORDING_FAILED_OXTS_NO_DATA*" (find-package "PATH_FOLLOWER::STATEEVENTS"))
(shadow '*RECORDING_FAILED_OXTS_NO_DATA* (find-package "PATH_FOLLOWER::STATEEVENTS"))
(defconstant path_follower::StateEvents::*RECORDING_FAILED_OXTS_NO_DATA* 11)
(intern "*RECORDING_FAILED_OXTS_NO_FIX*" (find-package "PATH_FOLLOWER::STATEEVENTS"))
(shadow '*RECORDING_FAILED_OXTS_NO_FIX* (find-package "PATH_FOLLOWER::STATEEVENTS"))
(defconstant path_follower::StateEvents::*RECORDING_FAILED_OXTS_NO_FIX* 12)
(intern "*FOLLOWING_FAILED_OXTS_NODE_DEAD*" (find-package "PATH_FOLLOWER::STATEEVENTS"))
(shadow '*FOLLOWING_FAILED_OXTS_NODE_DEAD* (find-package "PATH_FOLLOWER::STATEEVENTS"))
(defconstant path_follower::StateEvents::*FOLLOWING_FAILED_OXTS_NODE_DEAD* 20)
(intern "*FOLLOWING_FAILED_OXTS_NO_DATA*" (find-package "PATH_FOLLOWER::STATEEVENTS"))
(shadow '*FOLLOWING_FAILED_OXTS_NO_DATA* (find-package "PATH_FOLLOWER::STATEEVENTS"))
(defconstant path_follower::StateEvents::*FOLLOWING_FAILED_OXTS_NO_DATA* 21)
(intern "*FOLLOWING_FAILED_OXTS_NO_FIX*" (find-package "PATH_FOLLOWER::STATEEVENTS"))
(shadow '*FOLLOWING_FAILED_OXTS_NO_FIX* (find-package "PATH_FOLLOWER::STATEEVENTS"))
(defconstant path_follower::StateEvents::*FOLLOWING_FAILED_OXTS_NO_FIX* 22)
(intern "*FOLLOWING_FAILED_PARAVAN_NODE_DEAD*" (find-package "PATH_FOLLOWER::STATEEVENTS"))
(shadow '*FOLLOWING_FAILED_PARAVAN_NODE_DEAD* (find-package "PATH_FOLLOWER::STATEEVENTS"))
(defconstant path_follower::StateEvents::*FOLLOWING_FAILED_PARAVAN_NODE_DEAD* 23)
(intern "*FOLLOWING_FAILED_PARAVAN_NO_DATA*" (find-package "PATH_FOLLOWER::STATEEVENTS"))
(shadow '*FOLLOWING_FAILED_PARAVAN_NO_DATA* (find-package "PATH_FOLLOWER::STATEEVENTS"))
(defconstant path_follower::StateEvents::*FOLLOWING_FAILED_PARAVAN_NO_DATA* 24)
(intern "*FOLLOWING_FAILED_PARAVAN_NO_REMOTE_CONTROL*" (find-package "PATH_FOLLOWER::STATEEVENTS"))
(shadow '*FOLLOWING_FAILED_PARAVAN_NO_REMOTE_CONTROL* (find-package "PATH_FOLLOWER::STATEEVENTS"))
(defconstant path_follower::StateEvents::*FOLLOWING_FAILED_PARAVAN_NO_REMOTE_CONTROL* 25)
(defclass path_follower::StateEvents
  :super ros::object
  :slots ())

(defmethod path_follower::StateEvents
  (:init
   (&key
    )
   (send-super :init)
   self)
  (:serialization-length
   ()
   (+
    0
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;;
   self)
  )

(setf (get path_follower::StateEvents :md5sum-) "7f539e5f3e9b2c75ab993b9dff6a200f")
(setf (get path_follower::StateEvents :datatype-) "path_follower/StateEvents")
(setf (get path_follower::StateEvents :definition-)
      "# states
uint8 RECORDING     = 0
uint8 IDLE          = 1
uint8 FOLLOWING     = 2
uint8 WAIT_ZONE_REACHED = 3
uint8 HANDOVER = 4

# fail states
uint8 RECORDING_FAILED_OXTS_NODE_DEAD   = 10
uint8 RECORDING_FAILED_OXTS_NO_DATA     = 11
uint8 RECORDING_FAILED_OXTS_NO_FIX      = 12

uint8 FOLLOWING_FAILED_OXTS_NODE_DEAD    = 20
uint8 FOLLOWING_FAILED_OXTS_NO_DATA      = 21
uint8 FOLLOWING_FAILED_OXTS_NO_FIX       = 22
uint8 FOLLOWING_FAILED_PARAVAN_NODE_DEAD = 23
uint8 FOLLOWING_FAILED_PARAVAN_NO_DATA   = 24
uint8 FOLLOWING_FAILED_PARAVAN_NO_REMOTE_CONTROL = 25






")



(provide :path_follower/StateEvents "7f539e5f3e9b2c75ab993b9dff6a200f")


