;; Auto-generated. Do not edit!


(when (boundp 'path_follower::PathInfo)
  (if (not (find-package "PATH_FOLLOWER"))
    (make-package "PATH_FOLLOWER"))
  (shadow 'PathInfo (find-package "PATH_FOLLOWER")))
(unless (find-package "PATH_FOLLOWER::PATHINFO")
  (make-package "PATH_FOLLOWER::PATHINFO"))

(in-package "ROS")
;;//! \htmlinclude PathInfo.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass path_follower::PathInfo
  :super ros::object
  :slots (_recording_time _name _visualization_color _utm_zone_string _num_points _length _go_zone_radius _wait_zone_radius ))

(defmethod path_follower::PathInfo
  (:init
   (&key
    ((:recording_time __recording_time) (instance ros::time :init))
    ((:name __name) "")
    ((:visualization_color __visualization_color) (instance std_msgs::ColorRGBA :init))
    ((:utm_zone_string __utm_zone_string) "")
    ((:num_points __num_points) 0)
    ((:length __length) 0.0)
    ((:go_zone_radius __go_zone_radius) 0.0)
    ((:wait_zone_radius __wait_zone_radius) 0.0)
    )
   (send-super :init)
   (setq _recording_time __recording_time)
   (setq _name (string __name))
   (setq _visualization_color __visualization_color)
   (setq _utm_zone_string (string __utm_zone_string))
   (setq _num_points (round __num_points))
   (setq _length (float __length))
   (setq _go_zone_radius (float __go_zone_radius))
   (setq _wait_zone_radius (float __wait_zone_radius))
   self)
  (:recording_time
   (&optional __recording_time)
   (if __recording_time (setq _recording_time __recording_time)) _recording_time)
  (:name
   (&optional __name)
   (if __name (setq _name __name)) _name)
  (:visualization_color
   (&rest __visualization_color)
   (if (keywordp (car __visualization_color))
       (send* _visualization_color __visualization_color)
     (progn
       (if __visualization_color (setq _visualization_color (car __visualization_color)))
       _visualization_color)))
  (:utm_zone_string
   (&optional __utm_zone_string)
   (if __utm_zone_string (setq _utm_zone_string __utm_zone_string)) _utm_zone_string)
  (:num_points
   (&optional __num_points)
   (if __num_points (setq _num_points __num_points)) _num_points)
  (:length
   (&optional __length)
   (if __length (setq _length __length)) _length)
  (:go_zone_radius
   (&optional __go_zone_radius)
   (if __go_zone_radius (setq _go_zone_radius __go_zone_radius)) _go_zone_radius)
  (:wait_zone_radius
   (&optional __wait_zone_radius)
   (if __wait_zone_radius (setq _wait_zone_radius __wait_zone_radius)) _wait_zone_radius)
  (:serialization-length
   ()
   (+
    ;; time _recording_time
    8
    ;; string _name
    4 (length _name)
    ;; std_msgs/ColorRGBA _visualization_color
    (send _visualization_color :serialization-length)
    ;; string _utm_zone_string
    4 (length _utm_zone_string)
    ;; uint16 _num_points
    2
    ;; float32 _length
    4
    ;; float32 _go_zone_radius
    4
    ;; float32 _wait_zone_radius
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; time _recording_time
       (write-long (send _recording_time :sec) s) (write-long (send _recording_time :nsec) s)
     ;; string _name
       (write-long (length _name) s) (princ _name s)
     ;; std_msgs/ColorRGBA _visualization_color
       (send _visualization_color :serialize s)
     ;; string _utm_zone_string
       (write-long (length _utm_zone_string) s) (princ _utm_zone_string s)
     ;; uint16 _num_points
       (write-word _num_points s)
     ;; float32 _length
       (sys::poke _length (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _go_zone_radius
       (sys::poke _go_zone_radius (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _wait_zone_radius
       (sys::poke _wait_zone_radius (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; time _recording_time
     (send _recording_time :sec (sys::peek buf ptr- :integer)) (incf ptr- 4)  (send _recording_time :nsec (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; string _name
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _name (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; std_msgs/ColorRGBA _visualization_color
     (send _visualization_color :deserialize buf ptr-) (incf ptr- (send _visualization_color :serialization-length))
   ;; string _utm_zone_string
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _utm_zone_string (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; uint16 _num_points
     (setq _num_points (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; float32 _length
     (setq _length (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _go_zone_radius
     (setq _go_zone_radius (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _wait_zone_radius
     (setq _wait_zone_radius (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get path_follower::PathInfo :md5sum-) "33a4f61a6c31b8cdccfa3cc9ec623b2d")
(setf (get path_follower::PathInfo :datatype-) "path_follower/PathInfo")
(setf (get path_follower::PathInfo :definition-)
      "time 	  recording_time        # time when path recording has started
string    name                  # name of the path
std_msgs/ColorRGBA visualization_color   # color of path visualization in rviz
string 	  utm_zone_string       # UTM zone of the path (string representation)
uint16    num_points            # number of points in the path
float32   length                # path length (sum of all segment lengths)
float32   go_zone_radius        # [m] radius of go-zone
float32   wait_zone_radius      # [m] radius of wait-zone

================================================================================
MSG: std_msgs/ColorRGBA
float32 r
float32 g
float32 b
float32 a

")



(provide :path_follower/PathInfo "33a4f61a6c31b8cdccfa3cc9ec623b2d")


