// Auto-generated. Do not edit!

// (in-package path_follower.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class ControlTargetState {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.closest_point_x = null;
      this.closest_point_y = null;
      this.look_ahead_point_x = null;
      this.look_ahead_point_y = null;
      this.look_ahead_distance = null;
      this.velocity = null;
      this.steering_angle = null;
      this.steering_angle_norm = null;
      this.gas_brake_norm = null;
    }
    else {
      if (initObj.hasOwnProperty('closest_point_x')) {
        this.closest_point_x = initObj.closest_point_x
      }
      else {
        this.closest_point_x = 0.0;
      }
      if (initObj.hasOwnProperty('closest_point_y')) {
        this.closest_point_y = initObj.closest_point_y
      }
      else {
        this.closest_point_y = 0.0;
      }
      if (initObj.hasOwnProperty('look_ahead_point_x')) {
        this.look_ahead_point_x = initObj.look_ahead_point_x
      }
      else {
        this.look_ahead_point_x = 0.0;
      }
      if (initObj.hasOwnProperty('look_ahead_point_y')) {
        this.look_ahead_point_y = initObj.look_ahead_point_y
      }
      else {
        this.look_ahead_point_y = 0.0;
      }
      if (initObj.hasOwnProperty('look_ahead_distance')) {
        this.look_ahead_distance = initObj.look_ahead_distance
      }
      else {
        this.look_ahead_distance = 0.0;
      }
      if (initObj.hasOwnProperty('velocity')) {
        this.velocity = initObj.velocity
      }
      else {
        this.velocity = 0.0;
      }
      if (initObj.hasOwnProperty('steering_angle')) {
        this.steering_angle = initObj.steering_angle
      }
      else {
        this.steering_angle = 0.0;
      }
      if (initObj.hasOwnProperty('steering_angle_norm')) {
        this.steering_angle_norm = initObj.steering_angle_norm
      }
      else {
        this.steering_angle_norm = 0.0;
      }
      if (initObj.hasOwnProperty('gas_brake_norm')) {
        this.gas_brake_norm = initObj.gas_brake_norm
      }
      else {
        this.gas_brake_norm = 0.0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type ControlTargetState
    // Serialize message field [closest_point_x]
    bufferOffset = _serializer.float32(obj.closest_point_x, buffer, bufferOffset);
    // Serialize message field [closest_point_y]
    bufferOffset = _serializer.float32(obj.closest_point_y, buffer, bufferOffset);
    // Serialize message field [look_ahead_point_x]
    bufferOffset = _serializer.float32(obj.look_ahead_point_x, buffer, bufferOffset);
    // Serialize message field [look_ahead_point_y]
    bufferOffset = _serializer.float32(obj.look_ahead_point_y, buffer, bufferOffset);
    // Serialize message field [look_ahead_distance]
    bufferOffset = _serializer.float32(obj.look_ahead_distance, buffer, bufferOffset);
    // Serialize message field [velocity]
    bufferOffset = _serializer.float32(obj.velocity, buffer, bufferOffset);
    // Serialize message field [steering_angle]
    bufferOffset = _serializer.float32(obj.steering_angle, buffer, bufferOffset);
    // Serialize message field [steering_angle_norm]
    bufferOffset = _serializer.float32(obj.steering_angle_norm, buffer, bufferOffset);
    // Serialize message field [gas_brake_norm]
    bufferOffset = _serializer.float32(obj.gas_brake_norm, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type ControlTargetState
    let len;
    let data = new ControlTargetState(null);
    // Deserialize message field [closest_point_x]
    data.closest_point_x = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [closest_point_y]
    data.closest_point_y = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [look_ahead_point_x]
    data.look_ahead_point_x = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [look_ahead_point_y]
    data.look_ahead_point_y = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [look_ahead_distance]
    data.look_ahead_distance = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [velocity]
    data.velocity = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [steering_angle]
    data.steering_angle = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [steering_angle_norm]
    data.steering_angle_norm = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [gas_brake_norm]
    data.gas_brake_norm = _deserializer.float32(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 36;
  }

  static datatype() {
    // Returns string type for a message object
    return 'path_follower/ControlTargetState';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '59db037f6601c72a4efd8f7c6d69d33a';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    # This message contains the target values used for guiding the vehicle
    
    float32 closest_point_x     # [m] UTM easting of the point on the path which is currently closest to the vehicle
    float32 closest_point_y     # [m] UTM northing 
    float32 look_ahead_point_x  # [m] UTM easting of the look ahead point (target point for steering controller)
    float32 look_ahead_point_y  # [m] UTM northing 
    float32 look_ahead_distance # [m] distance from closest point to look ahead point, measured along the path
    float32 velocity            # [m/s] target velocity
    float32 steering_angle      # [rad] 
    float32 steering_angle_norm # [-] normalized steering angle (-1, 1)
    float32 gas_brake_norm      # [-] normalized gas/brake position (-1, 1)
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new ControlTargetState(null);
    if (msg.closest_point_x !== undefined) {
      resolved.closest_point_x = msg.closest_point_x;
    }
    else {
      resolved.closest_point_x = 0.0
    }

    if (msg.closest_point_y !== undefined) {
      resolved.closest_point_y = msg.closest_point_y;
    }
    else {
      resolved.closest_point_y = 0.0
    }

    if (msg.look_ahead_point_x !== undefined) {
      resolved.look_ahead_point_x = msg.look_ahead_point_x;
    }
    else {
      resolved.look_ahead_point_x = 0.0
    }

    if (msg.look_ahead_point_y !== undefined) {
      resolved.look_ahead_point_y = msg.look_ahead_point_y;
    }
    else {
      resolved.look_ahead_point_y = 0.0
    }

    if (msg.look_ahead_distance !== undefined) {
      resolved.look_ahead_distance = msg.look_ahead_distance;
    }
    else {
      resolved.look_ahead_distance = 0.0
    }

    if (msg.velocity !== undefined) {
      resolved.velocity = msg.velocity;
    }
    else {
      resolved.velocity = 0.0
    }

    if (msg.steering_angle !== undefined) {
      resolved.steering_angle = msg.steering_angle;
    }
    else {
      resolved.steering_angle = 0.0
    }

    if (msg.steering_angle_norm !== undefined) {
      resolved.steering_angle_norm = msg.steering_angle_norm;
    }
    else {
      resolved.steering_angle_norm = 0.0
    }

    if (msg.gas_brake_norm !== undefined) {
      resolved.gas_brake_norm = msg.gas_brake_norm;
    }
    else {
      resolved.gas_brake_norm = 0.0
    }

    return resolved;
    }
};

module.exports = ControlTargetState;
