// Auto-generated. Do not edit!

// (in-package path_follower.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class SensorState {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.warning_flag = null;
      this.emergency_flag = null;
      this.left_lidar_state = null;
      this.right_lidar_state = null;
      this.front_radar_state = null;
      this.follow_with_sensors = null;
      this.dist_x_0 = null;
      this.dist_y_0 = null;
      this.ang_0 = null;
      this.dist_x_1 = null;
      this.dist_y_1 = null;
      this.ang_1 = null;
      this.dist_brake = null;
      this.dist_curve = null;
      this.lap_deviation = null;
    }
    else {
      if (initObj.hasOwnProperty('warning_flag')) {
        this.warning_flag = initObj.warning_flag
      }
      else {
        this.warning_flag = false;
      }
      if (initObj.hasOwnProperty('emergency_flag')) {
        this.emergency_flag = initObj.emergency_flag
      }
      else {
        this.emergency_flag = false;
      }
      if (initObj.hasOwnProperty('left_lidar_state')) {
        this.left_lidar_state = initObj.left_lidar_state
      }
      else {
        this.left_lidar_state = false;
      }
      if (initObj.hasOwnProperty('right_lidar_state')) {
        this.right_lidar_state = initObj.right_lidar_state
      }
      else {
        this.right_lidar_state = false;
      }
      if (initObj.hasOwnProperty('front_radar_state')) {
        this.front_radar_state = initObj.front_radar_state
      }
      else {
        this.front_radar_state = false;
      }
      if (initObj.hasOwnProperty('follow_with_sensors')) {
        this.follow_with_sensors = initObj.follow_with_sensors
      }
      else {
        this.follow_with_sensors = false;
      }
      if (initObj.hasOwnProperty('dist_x_0')) {
        this.dist_x_0 = initObj.dist_x_0
      }
      else {
        this.dist_x_0 = 0.0;
      }
      if (initObj.hasOwnProperty('dist_y_0')) {
        this.dist_y_0 = initObj.dist_y_0
      }
      else {
        this.dist_y_0 = 0.0;
      }
      if (initObj.hasOwnProperty('ang_0')) {
        this.ang_0 = initObj.ang_0
      }
      else {
        this.ang_0 = 0.0;
      }
      if (initObj.hasOwnProperty('dist_x_1')) {
        this.dist_x_1 = initObj.dist_x_1
      }
      else {
        this.dist_x_1 = 0.0;
      }
      if (initObj.hasOwnProperty('dist_y_1')) {
        this.dist_y_1 = initObj.dist_y_1
      }
      else {
        this.dist_y_1 = 0.0;
      }
      if (initObj.hasOwnProperty('ang_1')) {
        this.ang_1 = initObj.ang_1
      }
      else {
        this.ang_1 = 0.0;
      }
      if (initObj.hasOwnProperty('dist_brake')) {
        this.dist_brake = initObj.dist_brake
      }
      else {
        this.dist_brake = 0.0;
      }
      if (initObj.hasOwnProperty('dist_curve')) {
        this.dist_curve = initObj.dist_curve
      }
      else {
        this.dist_curve = 0.0;
      }
      if (initObj.hasOwnProperty('lap_deviation')) {
        this.lap_deviation = initObj.lap_deviation
      }
      else {
        this.lap_deviation = 0.0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type SensorState
    // Serialize message field [warning_flag]
    bufferOffset = _serializer.bool(obj.warning_flag, buffer, bufferOffset);
    // Serialize message field [emergency_flag]
    bufferOffset = _serializer.bool(obj.emergency_flag, buffer, bufferOffset);
    // Serialize message field [left_lidar_state]
    bufferOffset = _serializer.bool(obj.left_lidar_state, buffer, bufferOffset);
    // Serialize message field [right_lidar_state]
    bufferOffset = _serializer.bool(obj.right_lidar_state, buffer, bufferOffset);
    // Serialize message field [front_radar_state]
    bufferOffset = _serializer.bool(obj.front_radar_state, buffer, bufferOffset);
    // Serialize message field [follow_with_sensors]
    bufferOffset = _serializer.bool(obj.follow_with_sensors, buffer, bufferOffset);
    // Serialize message field [dist_x_0]
    bufferOffset = _serializer.float32(obj.dist_x_0, buffer, bufferOffset);
    // Serialize message field [dist_y_0]
    bufferOffset = _serializer.float32(obj.dist_y_0, buffer, bufferOffset);
    // Serialize message field [ang_0]
    bufferOffset = _serializer.float32(obj.ang_0, buffer, bufferOffset);
    // Serialize message field [dist_x_1]
    bufferOffset = _serializer.float32(obj.dist_x_1, buffer, bufferOffset);
    // Serialize message field [dist_y_1]
    bufferOffset = _serializer.float32(obj.dist_y_1, buffer, bufferOffset);
    // Serialize message field [ang_1]
    bufferOffset = _serializer.float32(obj.ang_1, buffer, bufferOffset);
    // Serialize message field [dist_brake]
    bufferOffset = _serializer.float32(obj.dist_brake, buffer, bufferOffset);
    // Serialize message field [dist_curve]
    bufferOffset = _serializer.float32(obj.dist_curve, buffer, bufferOffset);
    // Serialize message field [lap_deviation]
    bufferOffset = _serializer.float32(obj.lap_deviation, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type SensorState
    let len;
    let data = new SensorState(null);
    // Deserialize message field [warning_flag]
    data.warning_flag = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [emergency_flag]
    data.emergency_flag = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [left_lidar_state]
    data.left_lidar_state = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [right_lidar_state]
    data.right_lidar_state = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [front_radar_state]
    data.front_radar_state = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [follow_with_sensors]
    data.follow_with_sensors = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [dist_x_0]
    data.dist_x_0 = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [dist_y_0]
    data.dist_y_0 = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [ang_0]
    data.ang_0 = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [dist_x_1]
    data.dist_x_1 = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [dist_y_1]
    data.dist_y_1 = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [ang_1]
    data.ang_1 = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [dist_brake]
    data.dist_brake = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [dist_curve]
    data.dist_curve = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [lap_deviation]
    data.lap_deviation = _deserializer.float32(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 42;
  }

  static datatype() {
    // Returns string type for a message object
    return 'path_follower/SensorState';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '77399bad35ed57ed01c5dee8095579a7';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    # This message constains the state of the sensors
    
    bool warning_flag
    bool emergency_flag
    bool left_lidar_state
    bool right_lidar_state
    bool front_radar_state
    bool follow_with_sensors
    
    float32 dist_x_0
    float32 dist_y_0
    float32 ang_0
    float32 dist_x_1
    float32 dist_y_1
    float32 ang_1
    
    float32 dist_brake
    float32 dist_curve
    float32 lap_deviation
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new SensorState(null);
    if (msg.warning_flag !== undefined) {
      resolved.warning_flag = msg.warning_flag;
    }
    else {
      resolved.warning_flag = false
    }

    if (msg.emergency_flag !== undefined) {
      resolved.emergency_flag = msg.emergency_flag;
    }
    else {
      resolved.emergency_flag = false
    }

    if (msg.left_lidar_state !== undefined) {
      resolved.left_lidar_state = msg.left_lidar_state;
    }
    else {
      resolved.left_lidar_state = false
    }

    if (msg.right_lidar_state !== undefined) {
      resolved.right_lidar_state = msg.right_lidar_state;
    }
    else {
      resolved.right_lidar_state = false
    }

    if (msg.front_radar_state !== undefined) {
      resolved.front_radar_state = msg.front_radar_state;
    }
    else {
      resolved.front_radar_state = false
    }

    if (msg.follow_with_sensors !== undefined) {
      resolved.follow_with_sensors = msg.follow_with_sensors;
    }
    else {
      resolved.follow_with_sensors = false
    }

    if (msg.dist_x_0 !== undefined) {
      resolved.dist_x_0 = msg.dist_x_0;
    }
    else {
      resolved.dist_x_0 = 0.0
    }

    if (msg.dist_y_0 !== undefined) {
      resolved.dist_y_0 = msg.dist_y_0;
    }
    else {
      resolved.dist_y_0 = 0.0
    }

    if (msg.ang_0 !== undefined) {
      resolved.ang_0 = msg.ang_0;
    }
    else {
      resolved.ang_0 = 0.0
    }

    if (msg.dist_x_1 !== undefined) {
      resolved.dist_x_1 = msg.dist_x_1;
    }
    else {
      resolved.dist_x_1 = 0.0
    }

    if (msg.dist_y_1 !== undefined) {
      resolved.dist_y_1 = msg.dist_y_1;
    }
    else {
      resolved.dist_y_1 = 0.0
    }

    if (msg.ang_1 !== undefined) {
      resolved.ang_1 = msg.ang_1;
    }
    else {
      resolved.ang_1 = 0.0
    }

    if (msg.dist_brake !== undefined) {
      resolved.dist_brake = msg.dist_brake;
    }
    else {
      resolved.dist_brake = 0.0
    }

    if (msg.dist_curve !== undefined) {
      resolved.dist_curve = msg.dist_curve;
    }
    else {
      resolved.dist_curve = 0.0
    }

    if (msg.lap_deviation !== undefined) {
      resolved.lap_deviation = msg.lap_deviation;
    }
    else {
      resolved.lap_deviation = 0.0
    }

    return resolved;
    }
};

module.exports = SensorState;
