// Auto-generated. Do not edit!

// (in-package path_follower.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class ServiceCommand {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
    }
    else {
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type ServiceCommand
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type ServiceCommand
    let len;
    let data = new ServiceCommand(null);
    return data;
  }

  static getMessageSize(object) {
    return 0;
  }

  static datatype() {
    // Returns string type for a message object
    return 'path_follower/ServiceCommand';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '97156beaade098dab21e61954a51527c';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    # path recording
    uint8 	START_RECORDING  = 0	
    uint8 	STOP_RECORDING 	 = 1
    
    # path following
    uint8 	START_FOLLOWING  = 10
    uint8 	STOP_FOLLOWING   = 11
    
    # idle braking
    uint8   BRAKE_IN_IDLE    = 12
    
    # file handling
    uint8 	SAVE_PATH_TO_FILE    = 20
    uint8 	LOAD_PATH_FROM_FILE  = 21
    
    # zone definition
    uint8 	SET_GO_ZONE_RADIUS   = 30
    uint8 	SET_WAIT_ZONE_RADIUS = 31
    
    # path processing
    uint8 	POST_PROCESS_PATH = 40
    
    # visualization
    uint8   SHOW_GO_ZONE      = 50
    uint8   SHOW_WAIT_ZONE    = 51
    uint8   PLOT_MAPPING_DATA = 52
    uint8   PLOT_PATH_DATA    = 53
    
    # path selection
    uint8   ADD_PATH        = 60
    uint8   REMOVE_PATH     = 61
    uint8   RENAME_PATH     = 62
    uint8   SET_ACTIVE_PATH = 63
    
    # bucket
    uint8 	BUCKET_CLOSED	= 70
    uint8 	BUCKET_OPENED	= 71
    
    #excavator
    uint8 	EX_LOCATION 	= 80
    uint8 	DP_LOCATION 	= 81
    
    #reverse mission
    uint8   SET_REVERSE_MISSION = 90
    
    
    
    
    
    
    
    
    
    
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new ServiceCommand(null);
    return resolved;
    }
};

// Constants for message
ServiceCommand.Constants = {
  START_RECORDING: 0,
  STOP_RECORDING: 1,
  START_FOLLOWING: 10,
  STOP_FOLLOWING: 11,
  BRAKE_IN_IDLE: 12,
  SAVE_PATH_TO_FILE: 20,
  LOAD_PATH_FROM_FILE: 21,
  SET_GO_ZONE_RADIUS: 30,
  SET_WAIT_ZONE_RADIUS: 31,
  POST_PROCESS_PATH: 40,
  SHOW_GO_ZONE: 50,
  SHOW_WAIT_ZONE: 51,
  PLOT_MAPPING_DATA: 52,
  PLOT_PATH_DATA: 53,
  ADD_PATH: 60,
  REMOVE_PATH: 61,
  RENAME_PATH: 62,
  SET_ACTIVE_PATH: 63,
  BUCKET_CLOSED: 70,
  BUCKET_OPENED: 71,
  EX_LOCATION: 80,
  DP_LOCATION: 81,
  SET_REVERSE_MISSION: 90,
}

module.exports = ServiceCommand;
