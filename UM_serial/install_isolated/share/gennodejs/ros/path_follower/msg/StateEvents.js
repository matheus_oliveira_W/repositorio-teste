// Auto-generated. Do not edit!

// (in-package path_follower.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class StateEvents {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
    }
    else {
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type StateEvents
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type StateEvents
    let len;
    let data = new StateEvents(null);
    return data;
  }

  static getMessageSize(object) {
    return 0;
  }

  static datatype() {
    // Returns string type for a message object
    return 'path_follower/StateEvents';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '7f539e5f3e9b2c75ab993b9dff6a200f';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    # states
    uint8 RECORDING     = 0
    uint8 IDLE          = 1
    uint8 FOLLOWING     = 2
    uint8 WAIT_ZONE_REACHED = 3
    uint8 HANDOVER = 4
    
    # fail states
    uint8 RECORDING_FAILED_OXTS_NODE_DEAD   = 10
    uint8 RECORDING_FAILED_OXTS_NO_DATA     = 11
    uint8 RECORDING_FAILED_OXTS_NO_FIX      = 12
    
    uint8 FOLLOWING_FAILED_OXTS_NODE_DEAD    = 20
    uint8 FOLLOWING_FAILED_OXTS_NO_DATA      = 21
    uint8 FOLLOWING_FAILED_OXTS_NO_FIX       = 22
    uint8 FOLLOWING_FAILED_PARAVAN_NODE_DEAD = 23
    uint8 FOLLOWING_FAILED_PARAVAN_NO_DATA   = 24
    uint8 FOLLOWING_FAILED_PARAVAN_NO_REMOTE_CONTROL = 25
    
    
    
    
    
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new StateEvents(null);
    return resolved;
    }
};

// Constants for message
StateEvents.Constants = {
  RECORDING: 0,
  IDLE: 1,
  FOLLOWING: 2,
  WAIT_ZONE_REACHED: 3,
  HANDOVER: 4,
  RECORDING_FAILED_OXTS_NODE_DEAD: 10,
  RECORDING_FAILED_OXTS_NO_DATA: 11,
  RECORDING_FAILED_OXTS_NO_FIX: 12,
  FOLLOWING_FAILED_OXTS_NODE_DEAD: 20,
  FOLLOWING_FAILED_OXTS_NO_DATA: 21,
  FOLLOWING_FAILED_OXTS_NO_FIX: 22,
  FOLLOWING_FAILED_PARAVAN_NODE_DEAD: 23,
  FOLLOWING_FAILED_PARAVAN_NO_DATA: 24,
  FOLLOWING_FAILED_PARAVAN_NO_REMOTE_CONTROL: 25,
}

module.exports = StateEvents;
