// Auto-generated. Do not edit!

// (in-package path_follower.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let PathInfo = require('./PathInfo.js');

//-----------------------------------------------------------

class PathInfoArray {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.info = null;
    }
    else {
      if (initObj.hasOwnProperty('info')) {
        this.info = initObj.info
      }
      else {
        this.info = [];
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type PathInfoArray
    // Serialize message field [info]
    // Serialize the length for message field [info]
    bufferOffset = _serializer.uint32(obj.info.length, buffer, bufferOffset);
    obj.info.forEach((val) => {
      bufferOffset = PathInfo.serialize(val, buffer, bufferOffset);
    });
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type PathInfoArray
    let len;
    let data = new PathInfoArray(null);
    // Deserialize message field [info]
    // Deserialize array length for message field [info]
    len = _deserializer.uint32(buffer, bufferOffset);
    data.info = new Array(len);
    for (let i = 0; i < len; ++i) {
      data.info[i] = PathInfo.deserialize(buffer, bufferOffset)
    }
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    object.info.forEach((val) => {
      length += PathInfo.getMessageSize(val);
    });
    return length + 4;
  }

  static datatype() {
    // Returns string type for a message object
    return 'path_follower/PathInfoArray';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'e0025102d88911d653ee6a41cb463a7c';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    PathInfo[] info # array of PathInfo messages
    ================================================================================
    MSG: path_follower/PathInfo
    time 	  recording_time        # time when path recording has started
    string    name                  # name of the path
    std_msgs/ColorRGBA visualization_color   # color of path visualization in rviz
    string 	  utm_zone_string       # UTM zone of the path (string representation)
    uint16    num_points            # number of points in the path
    float32   length                # path length (sum of all segment lengths)
    float32   go_zone_radius        # [m] radius of go-zone
    float32   wait_zone_radius      # [m] radius of wait-zone
    
    ================================================================================
    MSG: std_msgs/ColorRGBA
    float32 r
    float32 g
    float32 b
    float32 a
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new PathInfoArray(null);
    if (msg.info !== undefined) {
      resolved.info = new Array(msg.info.length);
      for (let i = 0; i < resolved.info.length; ++i) {
        resolved.info[i] = PathInfo.Resolve(msg.info[i]);
      }
    }
    else {
      resolved.info = []
    }

    return resolved;
    }
};

module.exports = PathInfoArray;
