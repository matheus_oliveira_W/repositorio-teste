// Auto-generated. Do not edit!

// (in-package path_follower.srv)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------


//-----------------------------------------------------------

class PathFollowerServiceRequest {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.command = null;
      this.str_data = null;
      this.float_data = null;
      this.int_data = null;
    }
    else {
      if (initObj.hasOwnProperty('command')) {
        this.command = initObj.command
      }
      else {
        this.command = 0;
      }
      if (initObj.hasOwnProperty('str_data')) {
        this.str_data = initObj.str_data
      }
      else {
        this.str_data = '';
      }
      if (initObj.hasOwnProperty('float_data')) {
        this.float_data = initObj.float_data
      }
      else {
        this.float_data = 0.0;
      }
      if (initObj.hasOwnProperty('int_data')) {
        this.int_data = initObj.int_data
      }
      else {
        this.int_data = 0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type PathFollowerServiceRequest
    // Serialize message field [command]
    bufferOffset = _serializer.uint8(obj.command, buffer, bufferOffset);
    // Serialize message field [str_data]
    bufferOffset = _serializer.string(obj.str_data, buffer, bufferOffset);
    // Serialize message field [float_data]
    bufferOffset = _serializer.float32(obj.float_data, buffer, bufferOffset);
    // Serialize message field [int_data]
    bufferOffset = _serializer.int32(obj.int_data, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type PathFollowerServiceRequest
    let len;
    let data = new PathFollowerServiceRequest(null);
    // Deserialize message field [command]
    data.command = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [str_data]
    data.str_data = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [float_data]
    data.float_data = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [int_data]
    data.int_data = _deserializer.int32(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += object.str_data.length;
    return length + 13;
  }

  static datatype() {
    // Returns string type for a service object
    return 'path_follower/PathFollowerServiceRequest';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'c9c05eb4ed61206553000411970d3652';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    uint8 	command
    string 	str_data
    float32 float_data
    int32 	int_data
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new PathFollowerServiceRequest(null);
    if (msg.command !== undefined) {
      resolved.command = msg.command;
    }
    else {
      resolved.command = 0
    }

    if (msg.str_data !== undefined) {
      resolved.str_data = msg.str_data;
    }
    else {
      resolved.str_data = ''
    }

    if (msg.float_data !== undefined) {
      resolved.float_data = msg.float_data;
    }
    else {
      resolved.float_data = 0.0
    }

    if (msg.int_data !== undefined) {
      resolved.int_data = msg.int_data;
    }
    else {
      resolved.int_data = 0
    }

    return resolved;
    }
};

class PathFollowerServiceResponse {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.reply = null;
      this.str_reply = null;
      this.float_reply = null;
      this.int_reply = null;
    }
    else {
      if (initObj.hasOwnProperty('reply')) {
        this.reply = initObj.reply
      }
      else {
        this.reply = 0;
      }
      if (initObj.hasOwnProperty('str_reply')) {
        this.str_reply = initObj.str_reply
      }
      else {
        this.str_reply = '';
      }
      if (initObj.hasOwnProperty('float_reply')) {
        this.float_reply = initObj.float_reply
      }
      else {
        this.float_reply = 0.0;
      }
      if (initObj.hasOwnProperty('int_reply')) {
        this.int_reply = initObj.int_reply
      }
      else {
        this.int_reply = 0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type PathFollowerServiceResponse
    // Serialize message field [reply]
    bufferOffset = _serializer.uint8(obj.reply, buffer, bufferOffset);
    // Serialize message field [str_reply]
    bufferOffset = _serializer.string(obj.str_reply, buffer, bufferOffset);
    // Serialize message field [float_reply]
    bufferOffset = _serializer.float32(obj.float_reply, buffer, bufferOffset);
    // Serialize message field [int_reply]
    bufferOffset = _serializer.int32(obj.int_reply, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type PathFollowerServiceResponse
    let len;
    let data = new PathFollowerServiceResponse(null);
    // Deserialize message field [reply]
    data.reply = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [str_reply]
    data.str_reply = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [float_reply]
    data.float_reply = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [int_reply]
    data.int_reply = _deserializer.int32(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += object.str_reply.length;
    return length + 13;
  }

  static datatype() {
    // Returns string type for a service object
    return 'path_follower/PathFollowerServiceResponse';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '630ef74c676b7c37e4807cf0ca4b53a9';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    uint8 	reply
    string 	str_reply
    float32 float_reply
    int32 	int_reply
    
    
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new PathFollowerServiceResponse(null);
    if (msg.reply !== undefined) {
      resolved.reply = msg.reply;
    }
    else {
      resolved.reply = 0
    }

    if (msg.str_reply !== undefined) {
      resolved.str_reply = msg.str_reply;
    }
    else {
      resolved.str_reply = ''
    }

    if (msg.float_reply !== undefined) {
      resolved.float_reply = msg.float_reply;
    }
    else {
      resolved.float_reply = 0.0
    }

    if (msg.int_reply !== undefined) {
      resolved.int_reply = msg.int_reply;
    }
    else {
      resolved.int_reply = 0
    }

    return resolved;
    }
};

module.exports = {
  Request: PathFollowerServiceRequest,
  Response: PathFollowerServiceResponse,
  md5sum() { return '635ea1697aa662b37b6293cb945f6681'; },
  datatype() { return 'path_follower/PathFollowerService'; }
};
