// Auto-generated. Do not edit!

// (in-package remote_control.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let RemoteControlStates = require('./RemoteControlStates.js');

//-----------------------------------------------------------

class RemoteControlStatesArray {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.info = null;
    }
    else {
      if (initObj.hasOwnProperty('info')) {
        this.info = initObj.info
      }
      else {
        this.info = [];
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type RemoteControlStatesArray
    // Serialize message field [info]
    // Serialize the length for message field [info]
    bufferOffset = _serializer.uint32(obj.info.length, buffer, bufferOffset);
    obj.info.forEach((val) => {
      bufferOffset = RemoteControlStates.serialize(val, buffer, bufferOffset);
    });
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type RemoteControlStatesArray
    let len;
    let data = new RemoteControlStatesArray(null);
    // Deserialize message field [info]
    // Deserialize array length for message field [info]
    len = _deserializer.uint32(buffer, bufferOffset);
    data.info = new Array(len);
    for (let i = 0; i < len; ++i) {
      data.info[i] = RemoteControlStates.deserialize(buffer, bufferOffset)
    }
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += 13 * object.info.length;
    return length + 4;
  }

  static datatype() {
    // Returns string type for a message object
    return 'remote_control/RemoteControlStatesArray';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '0547a26e3f37504504fac7b1153afa62';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    RemoteControlStates[] info #array of Remote Control messages
    
    ================================================================================
    MSG: remote_control/RemoteControlStates
    float32 brake_throttle 		# left joystick
    float32 steering		# right joystick
    
    #### byte 2 ####
    
    bool horn			# B3
    bool open_bucket		# B4
    bool close_bucket		# B5
    uint16 beams			# CH1 (OP2 = 0 switched-off; OFF = 1 low beam; OP1 = 2 high beam)
    
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new RemoteControlStatesArray(null);
    if (msg.info !== undefined) {
      resolved.info = new Array(msg.info.length);
      for (let i = 0; i < resolved.info.length; ++i) {
        resolved.info[i] = RemoteControlStates.Resolve(msg.info[i]);
      }
    }
    else {
      resolved.info = []
    }

    return resolved;
    }
};

module.exports = RemoteControlStatesArray;
