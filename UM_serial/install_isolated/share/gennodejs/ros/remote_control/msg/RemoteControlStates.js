// Auto-generated. Do not edit!

// (in-package remote_control.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class RemoteControlStates {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.brake_throttle = null;
      this.steering = null;
      this.horn = null;
      this.open_bucket = null;
      this.close_bucket = null;
      this.beams = null;
    }
    else {
      if (initObj.hasOwnProperty('brake_throttle')) {
        this.brake_throttle = initObj.brake_throttle
      }
      else {
        this.brake_throttle = 0.0;
      }
      if (initObj.hasOwnProperty('steering')) {
        this.steering = initObj.steering
      }
      else {
        this.steering = 0.0;
      }
      if (initObj.hasOwnProperty('horn')) {
        this.horn = initObj.horn
      }
      else {
        this.horn = false;
      }
      if (initObj.hasOwnProperty('open_bucket')) {
        this.open_bucket = initObj.open_bucket
      }
      else {
        this.open_bucket = false;
      }
      if (initObj.hasOwnProperty('close_bucket')) {
        this.close_bucket = initObj.close_bucket
      }
      else {
        this.close_bucket = false;
      }
      if (initObj.hasOwnProperty('beams')) {
        this.beams = initObj.beams
      }
      else {
        this.beams = 0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type RemoteControlStates
    // Serialize message field [brake_throttle]
    bufferOffset = _serializer.float32(obj.brake_throttle, buffer, bufferOffset);
    // Serialize message field [steering]
    bufferOffset = _serializer.float32(obj.steering, buffer, bufferOffset);
    // Serialize message field [horn]
    bufferOffset = _serializer.bool(obj.horn, buffer, bufferOffset);
    // Serialize message field [open_bucket]
    bufferOffset = _serializer.bool(obj.open_bucket, buffer, bufferOffset);
    // Serialize message field [close_bucket]
    bufferOffset = _serializer.bool(obj.close_bucket, buffer, bufferOffset);
    // Serialize message field [beams]
    bufferOffset = _serializer.uint16(obj.beams, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type RemoteControlStates
    let len;
    let data = new RemoteControlStates(null);
    // Deserialize message field [brake_throttle]
    data.brake_throttle = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [steering]
    data.steering = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [horn]
    data.horn = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [open_bucket]
    data.open_bucket = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [close_bucket]
    data.close_bucket = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [beams]
    data.beams = _deserializer.uint16(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 13;
  }

  static datatype() {
    // Returns string type for a message object
    return 'remote_control/RemoteControlStates';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'efd37443cf846b04d46650fcb60a01f2';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    float32 brake_throttle 		# left joystick
    float32 steering		# right joystick
    
    #### byte 2 ####
    
    bool horn			# B3
    bool open_bucket		# B4
    bool close_bucket		# B5
    uint16 beams			# CH1 (OP2 = 0 switched-off; OFF = 1 low beam; OP1 = 2 high beam)
    
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new RemoteControlStates(null);
    if (msg.brake_throttle !== undefined) {
      resolved.brake_throttle = msg.brake_throttle;
    }
    else {
      resolved.brake_throttle = 0.0
    }

    if (msg.steering !== undefined) {
      resolved.steering = msg.steering;
    }
    else {
      resolved.steering = 0.0
    }

    if (msg.horn !== undefined) {
      resolved.horn = msg.horn;
    }
    else {
      resolved.horn = false
    }

    if (msg.open_bucket !== undefined) {
      resolved.open_bucket = msg.open_bucket;
    }
    else {
      resolved.open_bucket = false
    }

    if (msg.close_bucket !== undefined) {
      resolved.close_bucket = msg.close_bucket;
    }
    else {
      resolved.close_bucket = false
    }

    if (msg.beams !== undefined) {
      resolved.beams = msg.beams;
    }
    else {
      resolved.beams = 0
    }

    return resolved;
    }
};

module.exports = RemoteControlStates;
