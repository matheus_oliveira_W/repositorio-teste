
"use strict";

let RemoteControlStatesArray = require('./RemoteControlStatesArray.js');
let RemoteControlStates = require('./RemoteControlStates.js');

module.exports = {
  RemoteControlStatesArray: RemoteControlStatesArray,
  RemoteControlStates: RemoteControlStates,
};
