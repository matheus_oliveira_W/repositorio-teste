// Auto-generated. Do not edit!

// (in-package sick_ldmrs_msgs.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let Object = require('./Object.js');
let std_msgs = _finder('std_msgs');

//-----------------------------------------------------------

class ObjectArray175 {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.header_175 = null;
      this.objects_175 = null;
    }
    else {
      if (initObj.hasOwnProperty('header_175')) {
        this.header_175 = initObj.header_175
      }
      else {
        this.header_175 = new std_msgs.msg.Header();
      }
      if (initObj.hasOwnProperty('objects_175')) {
        this.objects_175 = initObj.objects_175
      }
      else {
        this.objects_175 = [];
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type ObjectArray175
    // Serialize message field [header_175]
    bufferOffset = std_msgs.msg.Header.serialize(obj.header_175, buffer, bufferOffset);
    // Serialize message field [objects_175]
    // Serialize the length for message field [objects_175]
    bufferOffset = _serializer.uint32(obj.objects_175.length, buffer, bufferOffset);
    obj.objects_175.forEach((val) => {
      bufferOffset = Object.serialize(val, buffer, bufferOffset);
    });
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type ObjectArray175
    let len;
    let data = new ObjectArray175(null);
    // Deserialize message field [header_175]
    data.header_175 = std_msgs.msg.Header.deserialize(buffer, bufferOffset);
    // Deserialize message field [objects_175]
    // Deserialize array length for message field [objects_175]
    len = _deserializer.uint32(buffer, bufferOffset);
    data.objects_175 = new Array(len);
    for (let i = 0; i < len; ++i) {
      data.objects_175[i] = Object.deserialize(buffer, bufferOffset)
    }
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += std_msgs.msg.Header.getMessageSize(object.header_175);
    object.objects_175.forEach((val) => {
      length += Object.getMessageSize(val);
    });
    return length + 4;
  }

  static datatype() {
    // Returns string type for a message object
    return 'sick_ldmrs_msgs/ObjectArray175';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'a4365c8fdb36f56b8361098662ce741d';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    std_msgs/Header header_175
    sick_ldmrs_msgs/Object[] objects_175
    
    
    
    ================================================================================
    MSG: std_msgs/Header
    # Standard metadata for higher-level stamped data types.
    # This is generally used to communicate timestamped data 
    # in a particular coordinate frame.
    # 
    # sequence ID: consecutively increasing ID 
    uint32 seq
    #Two-integer timestamp that is expressed as:
    # * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
    # * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
    # time-handling sugar is provided by the client library
    time stamp
    #Frame this data is associated with
    # 0: no frame
    # 1: global frame
    string frame_id
    
    ================================================================================
    MSG: sick_ldmrs_msgs/Object
    int32 id
    
    time tracking_time                          # since when the object is tracked
    time last_seen
    
    geometry_msgs/TwistWithCovariance velocity
    
    geometry_msgs/Pose bounding_box_center
    geometry_msgs/Vector3 bounding_box_size
    
    geometry_msgs/PoseWithCovariance object_box_center
    geometry_msgs/Vector3 object_box_size
    
    geometry_msgs/Point[] contour_points
    
    ================================================================================
    MSG: geometry_msgs/TwistWithCovariance
    # This expresses velocity in free space with uncertainty.
    
    Twist twist
    
    # Row-major representation of the 6x6 covariance matrix
    # The orientation parameters use a fixed-axis representation.
    # In order, the parameters are:
    # (x, y, z, rotation about X axis, rotation about Y axis, rotation about Z axis)
    float64[36] covariance
    
    ================================================================================
    MSG: geometry_msgs/Twist
    # This expresses velocity in free space broken into its linear and angular parts.
    Vector3  linear
    Vector3  angular
    
    ================================================================================
    MSG: geometry_msgs/Vector3
    # This represents a vector in free space. 
    # It is only meant to represent a direction. Therefore, it does not
    # make sense to apply a translation to it (e.g., when applying a 
    # generic rigid transformation to a Vector3, tf2 will only apply the
    # rotation). If you want your data to be translatable too, use the
    # geometry_msgs/Point message instead.
    
    float64 x
    float64 y
    float64 z
    ================================================================================
    MSG: geometry_msgs/Pose
    # A representation of pose in free space, composed of position and orientation. 
    Point position
    Quaternion orientation
    
    ================================================================================
    MSG: geometry_msgs/Point
    # This contains the position of a point in free space
    float64 x
    float64 y
    float64 z
    
    ================================================================================
    MSG: geometry_msgs/Quaternion
    # This represents an orientation in free space in quaternion form.
    
    float64 x
    float64 y
    float64 z
    float64 w
    
    ================================================================================
    MSG: geometry_msgs/PoseWithCovariance
    # This represents a pose in free space with uncertainty.
    
    Pose pose
    
    # Row-major representation of the 6x6 covariance matrix
    # The orientation parameters use a fixed-axis representation.
    # In order, the parameters are:
    # (x, y, z, rotation about X axis, rotation about Y axis, rotation about Z axis)
    float64[36] covariance
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new ObjectArray175(null);
    if (msg.header_175 !== undefined) {
      resolved.header_175 = std_msgs.msg.Header.Resolve(msg.header_175)
    }
    else {
      resolved.header_175 = new std_msgs.msg.Header()
    }

    if (msg.objects_175 !== undefined) {
      resolved.objects_175 = new Array(msg.objects_175.length);
      for (let i = 0; i < resolved.objects_175.length; ++i) {
        resolved.objects_175[i] = Object.Resolve(msg.objects_175[i]);
      }
    }
    else {
      resolved.objects_175 = []
    }

    return resolved;
    }
};

module.exports = ObjectArray175;
