import time


class Timeout:
    def __init__(self, timeout_seconds):
        self.millis_timeout = 1000 * timeout_seconds
        self.millis_last_signal = 0  # int(round(time.time() * 1000))

    def got_signal(self):
        self.millis_last_signal = int(round(time.time() * 1000))

    def timed_out(self):
        millis_now = int(round(time.time() * 1000))

        if millis_now - self.millis_last_signal > self.millis_timeout:
            return True
        else:
            return False
