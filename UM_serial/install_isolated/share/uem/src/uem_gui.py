import os
import rospy
import rospkg
import rosservice
from enum import Enum

# Qt imports -----------------------------------
from qt_gui.plugin import Plugin
from python_qt_binding import loadUi
from python_qt_binding.QtWidgets import QWidget, QGraphicsOpacityEffect
from python_qt_binding.QtCore import QObject, pyqtSignal, pyqtSlot, QTimer

import python_qt_binding.QtCore as QtCore

# Message imports -----------------------------------
from std_msgs.msg import Float32, UInt16, Empty, UInt64, Bool
from sensor_msgs.msg import Joy
#from path_follower.msg import ServiceCommand, ServiceCommandReply, StateEvents

from timeout import Timeout

COLOR_STR_GREEN = "rgb(130, 255, 120)"
COLOR_STR_RED = "rgb(255, 96, 96)"

def get_label_style_sheet(color_string, margin=0.3):
    return "QLabel {" \
           "background-color:" + color_string + ";" \
        "border: 1px solid gray; " \
        "border-radius: 0px; " \
        "margin-top: " + str(margin) + "em; " \
        "margin-bottom: " + str(margin) + "em; " \
        " }"

class RelayBox1(Enum):
    Ignition = 0x001  # bit position 0
    Start = 0x002  # 1
    Left_Turn_Signal = 0x004  # 2
    Right_Turn_Signal = 0x008  # 3
    Alert_flashes = 0x010  # 4
    Right_Turn_Signal_Rear = 0x020  # 5
    BucketO = 0x040  # 6
    BucketC = 0x1000  # 12
    Low_Beam = 0x080  # 7
    High_Beam = 0x100  # 8
    Parking_Brake = 0x200  # 9
    Horn = 0x400  # 10   

class RelayBox3(Enum):
    Emergency_Brake = 0x001  # 0

class RelayBox2(Enum):
    Gear_R = 0x001  # bit position 0
    Gear_N = 0x002  # 1
    Gear_1 = 0x004  # 2
    Gear_2 = 0x008  # 3
    Gear_3 = 0x010  # 4
    Gear_4 = 0x020  # 5
    Gear_5 = 0x040  # 6
    Gear_6 = 0x080  # 7
    Reverse_Sound = 0x100  # 8

class ActuatorControl(Enum):
    Autonomous_Mode = 0x001 #bit position 0
    Manual_Mode = 0x002 #1
    RC_GUI_Mode = 0x004 #2
    RC_RC_Mode = 0x008 #3
    Default = 0x0010 #4

class StatusSteering (Enum):
    StartIni = 0x001  # bit position 0
    Ignition = 0x002  # 1
    SignalValid = 0x004  # 2
    RemoteControl = 0x010  # 4
    MagClutch = 0x020  # 5
    Controller = 0x040  # 6
    ErrorLED = 0x100  # 8
    WarningTone = 0x200  # 9
    GBNeutral = 0x800  # 11
    PathFollow = 0x1000  # 12
    RPM = 0x2000  # 13
    StatusMsgTimeout = 0x8000  # 15



class LowPassFilter:
    def __init__(self, filter_coeff=0.9, ini_val=0):
        self.old_val = ini_val
        self.filter_coeff = filter_coeff

    def filter(self, current_val):
        new_val = self.filter_coeff * self.old_val + \
            (1.0 - self.filter_coeff) * current_val
        self.old_val = new_val
        return new_val


class UeMPlugin(Plugin):       

    def __init__(self, context):
        super(UeMPlugin, self).__init__(context)

        # Give QObjects reasonable names
        self.setObjectName('UeM_rqt_GUI')

        # Create QWidget
        self._widget = QWidget()
        # Get path to UI file which should be in the "resource" folder of this package
        ui_file = os.path.join(rospkg.RosPack().get_path(
            'uem'), 'resource', 'uem_gui.ui')
        # Extend the widget with all attributes and children from UI file
        loadUi(ui_file, self._widget)
        # Give QObjects reasonable names
        self._widget.setObjectName('UeM_GUI')

        self._widget.btnResetSteering.clicked.connect(
            self.handle_btn_reset_steering)
        self._widget.btnResetGasBrake.clicked.connect(
            self.handle_btn_reset_gas_brake)
        self._widget.btnResetRB1.clicked.connect(self.handle_btn_reset_rb1)
        self._widget.btnResetRB2.clicked.connect(self.handle_btn_reset_rb2)

        self._widget.sliderSteering.valueChanged.connect(
            self.steering_val_changed)
        self._widget.sliderGasBrake.valueChanged.connect(
            self.gas_brake_val_changed)

        self._widget.sb_limit_steering.valueChanged.connect(
            self.cb_limit_steering)
        self._widget.sb_limit_gas.valueChanged.connect(self.cb_limit_gas)
        self._widget.sb_limit_brake.valueChanged.connect(self.cb_limit_brake)

        self._widget.checkBox_SendSteering.clicked.connect(
            self.cb_send_steering)
        self._widget.checkBox_SendGasBrake.clicked.connect(
            self.cb_send_gas_brake)
        self._widget.checkBox_SendRelayBox1.clicked.connect(self.cb_send_rb_1)
        self._widget.checkBox_SendRelayBox2.clicked.connect(self.cb_send_rb_2)
        self._widget.checkBox_SendRelayBox3.clicked.connect(self.cb_send_rb_3)
        self._widget.checkBox_SendTruckMode.clicked.connect(self.cb_send_tm)

        self._widget.rb_control_external.toggled.connect(self.callback_actuator_control)
        self._widget.rb_control_GUI.toggled.connect(self.callback_actuator_control)
        self._widget.rb_control_joystick.toggled.connect(self.callback_actuator_control)
        self._widget.rb_control_autonomous.toggled.connect(self.callback_actuator_control)
        self._widget.rb_default.toggled.connect(self.callback_actuator_control)
        
        self._widget.checkBox_Ignition.stateChanged.connect(self.relay_box_1_state_changed)
        self._widget.checkBox_Start.stateChanged.connect(self.relay_box_1_state_changed)
        self._widget.checkBox_LeftTurnSignal.toggled.connect(self.relay_box_1_state_changed)
        self._widget.checkBox_RightTurnSignal.toggled.connect(self.relay_box_1_state_changed)
        self._widget.checkBox_LowBeam.toggled.connect(self.relay_box_1_state_changed)
        self._widget.checkBox_HighBeam.toggled.connect(self.relay_box_1_state_changed)
        self._widget.checkBox_Horn.toggled.connect(self.relay_box_1_state_changed)
        self._widget.checkBox_EmergencyBrake.toggled.connect(self.relay_box_3_state_changed)
        self._widget.checkBox_AlertFlashes.toggled.connect(self.relay_box_1_state_changed)
        self._widget.checkBox_BucketO.toggled.connect(self.relay_box_1_state_changed)
        self._widget.checkBox_BucketC.toggled.connect(self.relay_box_1_state_changed)
        

        self._widget.radioButton_Gear_R.toggled.connect(self.relay_box_2_state_changed)
        self._widget.radioButton_Gear_N.toggled.connect(self.relay_box_2_state_changed)
        self._widget.radioButton_Gear_1.toggled.connect(self.relay_box_2_state_changed)
        self._widget.radioButton_Gear_2.toggled.connect(self.relay_box_2_state_changed)
        self._widget.radioButton_Gear_3.toggled.connect(self.relay_box_2_state_changed)
        self._widget.radioButton_Gear_4.toggled.connect(self.relay_box_2_state_changed)
        self._widget.radioButton_Gear_5.toggled.connect(self.relay_box_2_state_changed)
        self._widget.radioButton_Gear_6.toggled.connect(self.relay_box_2_state_changed)
        
        
        def make_read_only(item):
            faded_effect = QGraphicsOpacityEffect()
            faded_effect.setOpacity(0.7)
            item.setGraphicsEffect(faded_effect)
            item.setAttribute(QtCore.Qt.WA_TransparentForMouseEvents) 

        make_read_only(self._widget.rb_control_joystick)
        make_read_only(self._widget.rb_default)
        
        def set_checked_status_from_param(checkbox, param_name):
            param_checked = rospy.get_param(param_name, False)
            checkbox.blockSignals(True) 
            checkbox.setChecked(param_checked)
            checkbox.blockSignals(False)  
        
        set_checked_status_from_param(self._widget.checkBox_Ignition, "/uem_node/ini_set_relay_Ignition")
        set_checked_status_from_param(self._widget.checkBox_Start, "/uem_node/ini_set_relay_Start")
        set_checked_status_from_param(self._widget.checkBox_LeftTurnSignal, "/uem_node/ini_set_relay_LeftTurnSignal")
        set_checked_status_from_param(self._widget.checkBox_RightTurnSignal, "/uem_node/ini_set_relay_RightTurnSignal")
        set_checked_status_from_param(self._widget.checkBox_Horn, "/uem_node/ini_set_relay_Horn")
        set_checked_status_from_param(self._widget.checkBox_LowBeam, "/uem_node/ini_set_relay_Low_Beam")
        set_checked_status_from_param(self._widget.checkBox_HighBeam, "/uem_node/ini_set_relay_High_Beam")
        
        set_checked_status_from_param(self._widget.checkBox_EmergencyBrake, "/uem_node/ini_set_Emergency_Brake")
        
        
        def set_checked_no_signal(checkbox, state):
            checkbox.blockSignals(True) 
            checkbox.setChecked(state)
            checkbox.blockSignals(False) 
          
        gear = rospy.get_param("/uem_node/ini_set_relay_Gear", "N")
        
        if gear == 'R':
            set_checked_no_signal(self._widget.radioButton_Gear_R, True)
        elif gear == 'N':
            set_checked_no_signal(self._widget.radioButton_Gear_N, True)
        elif gear == 1:
            set_checked_no_signal(self._widget.radioButton_Gear_1, True)
        elif gear == 2:
            set_checked_no_signal(self._widget.radioButton_Gear_2, True)
        elif gear == 3:
            set_checked_no_signal(self._widget.radioButton_Gear_3, True)
        elif gear == 4:
            set_checked_no_signal(self._widget.radioButton_Gear_4, True)
        elif gear == 5:
            set_checked_no_signal(self._widget.radioButton_Gear_5, True)
        elif gear == 6:
            set_checked_no_signal(self._widget.radioButton_Gear_6, True)
                

        # subscribers ----------------------------------------------------------
        rospy.Subscriber("/paravan/steering_norm_actual", Float32, self.current_steering_callback)
        rospy.Subscriber("/paravan/gas_brake_norm_actual", Float32, self.current_gas_brake_callback)
        #rospy.Subscriber("/paravan/relay_box_1_status", UInt16, self.relay_box_1_status_callback)
        #rospy.Subscriber("/paravan/relay_box_2_status", UInt16, self.relay_box_2_status_callback)
        #rospy.Subscriber("/paravan/relay_box_3_status", UInt16, self.relay_box_3_status_callback)
        rospy.Subscriber("/steering_norm_desired", Float32, self.desired_steering_callback)
        rospy.Subscriber("/gas_brake_norm_desired", Float32, self.desired_gas_brake_callback)
        rospy.Subscriber("/paravan/steering_init_status", UInt64, self.steering_stat_callback)
        rospy.Subscriber("/paravan/gas_brake_init_status", UInt64, self.gas_brake_stat_callback)
        rospy.Subscriber("/paravan/remote_control_available", Empty, self.rc_available_callback)
        rospy.Subscriber("/uem/rc_on", Bool, self.remote_control_callback)

        # publishers ----------------------------------------------------------
        self.pub_steering = rospy.Publisher(
            '/steering_norm_desired', Float32, queue_size=1)
        self.pub_gas_brake = rospy.Publisher(
            '/gas_brake_norm_desired', Float32, queue_size=1)
        self.pub_relay_box_1 = rospy.Publisher(
            '/paravan/relay_box_1_desired', UInt16, queue_size=1)
        self.pub_relay_box_2 = rospy.Publisher(
            '/uem/relay_box_2_from_uem', UInt16, queue_size=1)
        self.pub_relay_box_3 = rospy.Publisher(
            '/uem/relay_box_3_from_uem', UInt16, queue_size=1)
        self.pub_status = rospy.Publisher(
            '/truck_status', UInt16, queue_size=1)
        self.pub_rc_following = rospy.Publisher(
            '/uem_gui/following_from_rc', UInt16, queue_size=1)

        self.low_pass_steering = LowPassFilter(0.9)
        self.low_pass_gas_brake = LowPassFilter(0.8)

        self.first_param_update = True

        # timers --------------------------------------------------------------
        self.parameter_timer = rospy.Timer(rospy.Duration(0.5), self.cb_parameter_timer)
        self.init_relay_box_1_functions = rospy.Timer(rospy.Duration(1.0), 
                                                      lambda t: self.relay_box_1_state_changed(0), oneshot=True)
        self.init_relay_box_2_functions = rospy.Timer(rospy.Duration(1.0), 
                                                      lambda t: self.relay_box_2_state_changed(0), oneshot=True)
        self.init_actuators_control_functions = rospy.Timer(rospy.Duration(1.0), 
                                                      lambda t: self.callback_actuator_control(0), oneshot=True)
        
        self.gui_timer = QTimer()
        self.gui_timer.timeout.connect(self.update_gui)
        # duration in ms 
        self.gui_timer.start(100)

        # member storage for received message data
        self.request_remote_control = False
        self.request_remote_control_flag = False

        self.sliderSteering = 0.0
        self.sliderGasBrake = 0.0

        self.steering_value = 0.0
        self.gas_brake_value = 0.0

        self.steering_value_actual = 0.0
        self.gas_brake_value_actual = 0.0
        
        self.steering_value_desired = 0.0
        self.gas_brake_value_desired = 0.0

        #self.truck_status_value_desired = 0

        self.sb_limit_steering = 0.0
        self.sb_limit_gas = 0.0
        self.sb_limit_brake = 0.0

        self.checkBox_SendGasBrake = False
        self.checkBox_SendSteering = False
        self.checkBox_SendRelayBox1 = False
        self.checkBox_SendRelayBox2 = False
        self.checkBox_SendRelayBox3 = False
        self.checkBox_SendTruckMode = False

        self.rb_control_GUI = False
        self.rb_control_external = True
        self.rb_control_joystick = False
        self.rb_autonomo = False
        self.rb_default = False

        self.rb1_reset_stat = False
        self.rb2_reset_stat = False

        self.btn_reset_steering = False
        self.btn_reset_gas_brake = False

        self.limit_changed = False
        
        self.timeout_rc_available = Timeout(1.0)
        self.timeout_actuator_init_status = Timeout(0.5)
        
        self.relay_box_1_state_changed(0)
        self.relay_box_2_state_changed(0)
        self.callback_actuator_control(0)
        
        # Show _widget.windowTitle on left-top of each plugin (when it's set in _widget).
        if context.serial_number() > 1:
            self._widget.setWindowTitle(
                self._widget.windowTitle() + (' (%d)' % context.serial_number()))
        # Add widget to the user interface
        context.add_widget(self._widget)

    def cb_limit_gas(self, val):
        rospy.set_param("/uem_node/limit_gas",
                        self._widget.sb_limit_gas.value())

    def cb_limit_brake(self, val):
        rospy.set_param("/uem_node/limit_brake",
                        self._widget.sb_limit_brake.value())

    def cb_limit_steering(self, val):
        rospy.set_param("/uem_node/limit_steering",
                        self._widget.sb_limit_steering.value())

    def cb_send_steering(self, is_checked):
        rospy.set_param("/uem_node/send_steering", is_checked)

    def cb_send_gas_brake(self, is_checked):
        rospy.set_param("/uem_node/send_gas_brake", is_checked)

    def cb_send_rb_1(self, is_checked):
        rospy.set_param("/uem_node/send_relay_box_1_functions", is_checked)

    def cb_send_rb_2(self, is_checked):
        rospy.set_param("/uem_node/send_relay_box_2_functions", is_checked)

    def cb_send_rb_3(self, is_checked):
        rospy.set_param("/uem_node/send_relay_box_3_functions", is_checked)

    def cb_send_tm(self, is_checked):
        rospy.set_param("/uem_node/send_truck_mode", is_checked)

    def cb_send_keep_alive(self, is_checked):
        rospy.set_param(
            "/uem_node/send_relay_box_keep_alive_signal", is_checked)
        
    def cb_keep_steering_neutral(self, is_checked):
        rospy.set_param(
            "/uem_node/keep_steering_neutral", is_checked)

    def cb_send_shutdown_sequence(self, is_checked):
        rospy.set_param("/uem_node/send_shutdown_sequence", is_checked)

    def different(self, a, b):
        return abs(a - b) > 0.0001

    def set_checked_status_from_param(self, checkbox, param_name):
        param_checked = rospy.get_param(param_name, False)
        checkbox.blockSignals(True) 
        checkbox.setChecked(param_checked)
        checkbox.blockSignals(False)       

    def cb_parameter_timer(self, t):
        if self.first_param_update:
            self.sb_limit_steering = rospy.get_param(
                '/uem_node/limit_steering', 1.0)
            self.sb_limit_gas = rospy.get_param(
                '/uem_node/limit_steering', 1.0)
            self.sb_limit_brake = rospy.get_param(
                '/uem_node/limit_brake', -1.0)

            self.checkBox_SendGasBrake = rospy.get_param(
                '/uem_node/send_gas_brake', False)
            self.checkBox_SendSteering = rospy.get_param(
                '/uem_node/send_steering', False)
            self.checkBox_SendRelayBox1 = rospy.get_param(
                '/uem_node/send_relay_box_1_functions', False)
            self.checkBox_SendRelayBox2 = rospy.get_param(
                '/uem_node/send_relay_box_2_functions', False)
            self.checkBox_SendRelayBox3 = rospy.get_param(
                '/uem_node/send_relay_box_3_functions', False)
            self.checkBox_SendTruckMode = rospy.get_param(
                '/uem_node/send_truck_mode', False)

            self.first_param_update = False
            return

        self.set_checked_status_from_param(
            self._widget.checkBox_SendGasBrake, '/uem_node/send_gas_brake')
        self.set_checked_status_from_param(
            self._widget.checkBox_SendSteering, '/uem_node/send_steering')
        self.set_checked_status_from_param(
            self._widget.checkBox_SendRelayBox1, '/uem_node/send_relay_box_1_functions')
        self.set_checked_status_from_param(
            self._widget.checkBox_SendRelayBox2, '/uem_node/send_relay_box_2_functions')
        self.set_checked_status_from_param(
            self._widget.checkBox_SendRelayBox3, '/uem_node/send_relay_box_3_functions')
        self.set_checked_status_from_param(
            self._widget.checkBox_SendTruckMode, '/uem_node/send_truck_mode')

        value = rospy.get_param('/uem_node/limit_steering', 1.0)
        if self.different(value, self._widget.sb_limit_steering.value()):
            self.limit_changed = True
            self.sb_limit_steering = value

        value = rospy.get_param('/uem_node/limit_gas', 1.0)
        if self.different(value, self._widget.sb_limit_gas.value()):
            self.limit_changed = True
            self.sb_limit_gas = value

        value = rospy.get_param('/uem_node/limit_brake', -1.0)
        if self.different(value, self._widget.sb_limit_brake.value()):
            self.limit_changed = True
            self.sb_limit_brake = value

    def remote_control_callback(self, msg):
        self.request_remote_control = msg.data

    def handle_btn_reset_steering(self):
        self.btn_reset_steering = True
        self.sliderSteering = 0

    def handle_btn_reset_gas_brake(self):
        self.btn_reset_gas_brake = True
        self.sliderGasBrake = 0

    def handle_btn_reset_rb1(self):
        self.rb1_reset_stat = True

    def handle_btn_reset_rb2(self):
        self.rb2_reset_stat = True

    def current_steering_callback(self, msg):
        self.steering_value_actual = msg.data*1000            

    def current_gas_brake_callback(self, msg):
        self.gas_brake_value_actual = msg.data*1000
        
    def desired_steering_callback(self, msg):
        self.steering_value_desired = msg.data*1000 
    
    def desired_gas_brake_callback(self, msg):
        self.gas_brake_value_desired = msg.data*1000   

    def steering_stat_callback(self, msg):
        self.timeout_actuator_init_status.got_signal()

    #def truck_status(self, msg):
    #    self.truck_status_value_desired = msg.data

    def gas_brake_stat_callback(self, msg):
        self.timeout_actuator_init_status.got_signal()

    def relay_box_1_status_callback(self, msg):        
        self.stat_RB1Ignition = RelayBox1['Ignition'].value & msg.data
        self.stat_RB1Start = RelayBox1['Start'].value & msg.data
        self.stat_RB1Left_Turn_Signal = RelayBox1['Left_Turn_Signal'].value & msg.data
        self.stat_RB1Right_Turn_Signal = RelayBox1['Right_Turn_Signal'].value & msg.data
        self.stat_RB1Low_Beam = RelayBox1['Low_Beam'].value & msg.data
        self.stat_RB1High_Beam = RelayBox1['High_Beam'].value & msg.data
        self.stat_RB1Parking_Brake = RelayBox1['Parking_Brake'].value & msg.data
        self.stat_RB1Horn = RelayBox1['Horn'].value & msg.data

    def relay_box_2_status_callback(self, msg):
        self.stat_RB2Gear_R = RelayBox2['Gear_R'].value & msg.data
        self.stat_RB2Gear_N = RelayBox2['Gear_N'].value & msg.data
        self.stat_RB2Gear_1 = RelayBox2['Gear_1'].value & msg.data
        self.stat_RB2Gear_2 = RelayBox2['Gear_2'].value & msg.data
        self.stat_RB2Gear_3 = RelayBox2['Gear_3'].value & msg.data
        self.stat_RB2Gear_4 = RelayBox2['Gear_4'].value & msg.data
        self.stat_RB2Gear_5 = RelayBox2['Gear_5'].value & msg.data
        self.stat_RB2Gear_7 = RelayBox2['Gear_7'].value & msg.data

    def relay_box_3_status_callback(self, msg):
        self.stat_RB3Emergency = RelayBox3['Emergency_Brake'].value & msg.data
        

    def steering_val_changed(self, slider_value):
        if self.rb_control_GUI:
            self.steering_value = slider_value / 1000.0

            #print('steer_slider_value: ', slider_value)
    
            msg = Float32()
            msg.data = self.steering_value
            self.pub_steering.publish(msg)

    def gas_brake_val_changed(self, slider_value):
        if self.rb_control_GUI:
            self.gas_brake_value = slider_value / 1000.0

            #print('gas_slider_value: ', slider_value)
    
            msg = Float32()
            msg.data = self.gas_brake_value
            self.pub_gas_brake.publish(msg)

    def relay_box_1_state_changed(self, int):
                
        box1_flags = 0

        if self._widget.checkBox_Ignition.isChecked():
            box1_flags |= RelayBox1['Ignition'].value
        if self._widget.checkBox_Start.isChecked():
            box1_flags |= RelayBox1['Start'].value
        if self._widget.checkBox_LeftTurnSignal.isChecked():
            box1_flags |= RelayBox1['Left_Turn_Signal'].value
        if self._widget.checkBox_RightTurnSignal.isChecked():
            box1_flags |= RelayBox1['Right_Turn_Signal'].value
        if self._widget.checkBox_LowBeam.isChecked():
            box1_flags |= RelayBox1['Low_Beam'].value
        if self._widget.checkBox_HighBeam.isChecked():
            box1_flags |= RelayBox1['High_Beam'].value        
        if self._widget.checkBox_Horn.isChecked():
            box1_flags |= RelayBox1['Horn'].value
        if self._widget.checkBox_AlertFlashes.isChecked():
            box1_flags |= RelayBox1['Alert_flashes'].value
        if self._widget.checkBox_BucketO.isChecked():
            box1_flags |= RelayBox1['BucketO'].value
        if self._widget.checkBox_BucketC.isChecked():
            box1_flags |= RelayBox1['BucketC'].value

        msg_box_1 = UInt16()
        msg_box_1.data = box1_flags
        self.pub_relay_box_1.publish(msg_box_1)  

    def relay_box_3_state_changed(self, int):
                
        box3_flags = 0

        if self._widget.checkBox_EmergencyBrake.isChecked():
            box3_flags |= RelayBox3['Emergency_Brake'].value
        
        msg_box_3 = UInt16()
        msg_box_3.data = box3_flags
        self.pub_relay_box_3.publish(msg_box_3)     
        
    def relay_box_2_state_changed(self, int):
                
        box2_flags = 0

        if self._widget.radioButton_Gear_R.isChecked():
            box2_flags |= RelayBox2['Gear_R'].value
        if self._widget.radioButton_Gear_N.isChecked():
            box2_flags |= RelayBox2['Gear_N'].value
        if self._widget.radioButton_Gear_1.isChecked():
            box2_flags |= RelayBox2['Gear_1'].value
        if self._widget.radioButton_Gear_2.isChecked():
            box2_flags |= RelayBox2['Gear_2'].value
        if self._widget.radioButton_Gear_3.isChecked():
            box2_flags |= RelayBox2['Gear_3'].value
        if self._widget.radioButton_Gear_4.isChecked():
            box2_flags |= RelayBox2['Gear_4'].value
        if self._widget.radioButton_Gear_5.isChecked():
            box2_flags |= RelayBox2['Gear_5'].value
        if self._widget.radioButton_Gear_6.isChecked():
            box2_flags |= RelayBox2['Gear_6'].value       
        
        msg_box_2 = UInt16()
        msg_box_2.data = box2_flags
        self.pub_relay_box_2.publish(msg_box_2)

    def callback_actuator_control(self, int):

        actuators_flags = 0

        if self._widget.rb_control_GUI.isChecked():
            self.rb_control_GUI = True
            self.rb_control_external = False
            self.rb_control_joystick = False
            actuators_flags |= ActuatorControl['RC_GUI_Mode'].value
            
            
        if self._widget.rb_control_external.isChecked():
            self.rb_control_external = True
            self.rb_control_GUI = False
            self.rb_control_joystick = False
            actuators_flags |= ActuatorControl['Manual_Mode'].value
            
        '''
        if self._widget.rb_control_joystick.isChecked():
            self.rb_control_joystick = True
            self.rb_control_GUI = False
            self.rb_control_external = False
            actuators_flags |= ActuatorControl['RC_RC_Mode'].value
        '''

        if self._widget.rb_control_autonomous.isChecked():
            self.rb_control_joystick = False
            self.rb_control_GUI = False
            self.rb_control_external = False
            self.rb_control_autonomous = True
            actuators_flags |= ActuatorControl['Autonomous_Mode'].value
        '''
        if self._widget.rb_default.isChecked():
            self.rb_control_joystick = False
            self.rb_control_GUI = False
            self.rb_control_external = False
            self.rb_control_autonomous = False
            #self.rb_default = True
            #actuators_flags |= ActuatorControl['Default'].value
        '''   

        if self._widget.checkBox_SendTruckMode.isChecked():
            msg_actuator = UInt16()
            msg_actuator.data = actuators_flags
            self.pub_status.publish(msg_actuator)

    def rc_available_callback(self, msg):
        self.timeout_rc_available.got_signal()
               
    def set_checked_no_signal(self, gui_item, checked_state):         
        gui_item.blockSignals(True) 
        gui_item.setChecked(checked_state)
        gui_item.blockSignals(False)
        
    def set_slider_value_no_signal(self, slider, value):         
        slider.blockSignals(True) 
        slider.setValue(value)
        slider.blockSignals(False)

    def set_rc_notification(self, color_string, notification_text):
        self._widget.label_remote_control.setStyleSheet(
                get_label_style_sheet(color_string))
        self._widget.label_remote_control.setText(notification_text)
        
    def enable_secondary_functions(self, state):
        self._widget.checkBox_Ignition.setEnabled(state)
        self._widget.checkBox_Start.setEnabled(state)
        self._widget.checkBox_LeftTurnSignal.setEnabled(state)
        self._widget.checkBox_RightTurnSignal.setEnabled(state)
        self._widget.checkBox_AlertFlashes.setEnabled(state)
        self._widget.checkBox_LowBeam.setEnabled(state)
        self._widget.checkBox_HighBeam.setEnabled(state)
        self._widget.checkBox_Horn.setEnabled(state)
        self._widget.checkBox_EmergencyBrake.setEnabled(state)
        self._widget.checkBox_BucketO.setEnabled(state)
        self._widget.checkBox_BucketC.setEnabled(state)

        self._widget.radioButton_Gear_R.setEnabled(state)
        self._widget.radioButton_Gear_N.setEnabled(state)
        self._widget.radioButton_Gear_1.setEnabled(state)
        self._widget.radioButton_Gear_2.setEnabled(state)
        self._widget.radioButton_Gear_3.setEnabled(state)
        self._widget.radioButton_Gear_4.setEnabled(state)
        self._widget.radioButton_Gear_5.setEnabled(state)
        self._widget.radioButton_Gear_6.setEnabled(state)

    # GUI update function
    @pyqtSlot()
    def update_gui(self):
        
        #if self.request_remote_control and self.request_remote_control_flag == False:
        #    self._widget.rb_control_joystick.setChecked(self.request_remote_control)
        #    self.request_remote_control_flag = True

        #elif self.request_remote_control == False and self.request_remote_control_flag == True:
            #self._widget.rb_control_external.setChecked(True)
        #    self.request_remote_control_flag = False
        
        self._widget.labelSteeringValueDesired.setText(
            "{0:.4f}".format(self.steering_value))
        self._widget.labelGasBrakeValueDesired.setText(
            "{0:.4f}".format(self.gas_brake_value))

        if self.timeout_rc_available.timed_out(): 
            self.set_rc_notification(COLOR_STR_RED, "REMOTE CONTROL: OFF")

        else:
            self.set_rc_notification(COLOR_STR_GREEN, "REMOTE CONTROL: ON")            

        if self.limit_changed:
            self._widget.sb_limit_steering.setValue(self.sb_limit_steering)
            self._widget.sb_limit_gas.setValue(self.sb_limit_gas)
            self._widget.sb_limit_brake.setValue(self.sb_limit_brake)
            self.limit_changed = False

        if self.btn_reset_steering:
            self._widget.sliderSteering.setValue(self.sliderSteering)
            self.btn_reset_steering = False

        if self.btn_reset_gas_brake:
            self._widget.sliderGasBrake.setValue(self.sliderGasBrake)
            self.btn_reset_gas_brake = False

        if self._widget.rb_control_joystick.isChecked():
            self._widget.sliderSteering.setValue(self.sliderSteering * 1000)
            self._widget.sliderGasBrake.setValue(self.sliderGasBrake * 1000)

        if self.rb_control_GUI:
            self._widget.sliderSteering.setEnabled(True)
            self._widget.sliderGasBrake.setEnabled(True)
            self._widget.btnResetGasBrake.setEnabled(True)
            self._widget.btnResetSteering.setEnabled(True)

            self.enable_secondary_functions(True)            

        if self.rb_control_external:
            self.handle_btn_reset_steering()
            self.handle_btn_reset_gas_brake()
            self._widget.sliderSteering.setEnabled(False)
            self._widget.sliderGasBrake.setEnabled(False)
            self._widget.btnResetGasBrake.setEnabled(False)
            self._widget.btnResetSteering.setEnabled(False)

            self.enable_secondary_functions(True)
            
            self.set_slider_value_no_signal(self._widget.sliderSteering, self.steering_value_desired)
            self.set_slider_value_no_signal(self._widget.sliderGasBrake, self.gas_brake_value_desired)

        if self.rb_control_joystick:
            self.handle_btn_reset_steering()
            self.handle_btn_reset_gas_brake()
            self._widget.sliderSteering.setEnabled(False)
            self._widget.sliderGasBrake.setEnabled(False)
            self._widget.btnResetGasBrake.setEnabled(False)
            self._widget.btnResetSteering.setEnabled(False)

            self.enable_secondary_functions(False)
                
            self.set_slider_value_no_signal(self._widget.sliderSteering, self.steering_value_desired)
            self.set_slider_value_no_signal(self._widget.sliderGasBrake, self.gas_brake_value_desired)

        if self.rb1_reset_stat:            
            self._widget.checkBox_Ignition.setChecked(False)
            self._widget.checkBox_Start.setChecked(False)
            self._widget.checkBox_LeftTurnSignal.setChecked(False)
            self._widget.checkBox_RightTurnSignal.setChecked(False)
            self._widget.checkBox_LowBeam.setChecked(False)
            self._widget.checkBox_HighBeam.setChecked(False)
            self._widget.checkBox_Horn.setChecked(False)
            self._widget.checkBox_EmergencyBrake.setChecked(False)
            self._widget.checkBox_AlertFlashes.setChecked(False)
            self._widget.checkBox_BucketO.setChecked(False)
            self._widget.checkBox_BucketC.setChecked(False)
            self.rb1_reset_stat = False

        if self.rb2_reset_stat:
            self._widget.radioButton_Gear_R.setChecked(False)
            self._widget.radioButton_Gear_N.setChecked(False)
            self._widget.radioButton_Gear_1.setChecked(False)
            self._widget.radioButton_Gear_2.setChecked(False)
            self._widget.radioButton_Gear_3.setChecked(False)
            self._widget.radioButton_Gear_4.setChecked(False)
            self._widget.radioButton_Gear_5.setChecked(False)
            self._widget.radioButton_Gear_6.setChecked(False)
            self.rb2_reset_stat = False
