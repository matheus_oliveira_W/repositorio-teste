// Generated by gencpp from file path_follower/ControlTargetState.msg
// DO NOT EDIT!


#ifndef PATH_FOLLOWER_MESSAGE_CONTROLTARGETSTATE_H
#define PATH_FOLLOWER_MESSAGE_CONTROLTARGETSTATE_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>


namespace path_follower
{
template <class ContainerAllocator>
struct ControlTargetState_
{
  typedef ControlTargetState_<ContainerAllocator> Type;

  ControlTargetState_()
    : closest_point_x(0.0)
    , closest_point_y(0.0)
    , look_ahead_point_x(0.0)
    , look_ahead_point_y(0.0)
    , look_ahead_distance(0.0)
    , velocity(0.0)
    , steering_angle(0.0)
    , steering_angle_norm(0.0)
    , gas_brake_norm(0.0)  {
    }
  ControlTargetState_(const ContainerAllocator& _alloc)
    : closest_point_x(0.0)
    , closest_point_y(0.0)
    , look_ahead_point_x(0.0)
    , look_ahead_point_y(0.0)
    , look_ahead_distance(0.0)
    , velocity(0.0)
    , steering_angle(0.0)
    , steering_angle_norm(0.0)
    , gas_brake_norm(0.0)  {
  (void)_alloc;
    }



   typedef float _closest_point_x_type;
  _closest_point_x_type closest_point_x;

   typedef float _closest_point_y_type;
  _closest_point_y_type closest_point_y;

   typedef float _look_ahead_point_x_type;
  _look_ahead_point_x_type look_ahead_point_x;

   typedef float _look_ahead_point_y_type;
  _look_ahead_point_y_type look_ahead_point_y;

   typedef float _look_ahead_distance_type;
  _look_ahead_distance_type look_ahead_distance;

   typedef float _velocity_type;
  _velocity_type velocity;

   typedef float _steering_angle_type;
  _steering_angle_type steering_angle;

   typedef float _steering_angle_norm_type;
  _steering_angle_norm_type steering_angle_norm;

   typedef float _gas_brake_norm_type;
  _gas_brake_norm_type gas_brake_norm;





  typedef boost::shared_ptr< ::path_follower::ControlTargetState_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::path_follower::ControlTargetState_<ContainerAllocator> const> ConstPtr;

}; // struct ControlTargetState_

typedef ::path_follower::ControlTargetState_<std::allocator<void> > ControlTargetState;

typedef boost::shared_ptr< ::path_follower::ControlTargetState > ControlTargetStatePtr;
typedef boost::shared_ptr< ::path_follower::ControlTargetState const> ControlTargetStateConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::path_follower::ControlTargetState_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::path_follower::ControlTargetState_<ContainerAllocator> >::stream(s, "", v);
return s;
}

} // namespace path_follower

namespace ros
{
namespace message_traits
{



// BOOLTRAITS {'IsFixedSize': True, 'IsMessage': True, 'HasHeader': False}
// {'std_msgs': ['/opt/ros/kinetic/share/std_msgs/cmake/../msg'], 'path_follower': ['/home/bplus/UM_serial/src/path_follower/path_follower_node/msg']}

// !!!!!!!!!!! ['__class__', '__delattr__', '__dict__', '__doc__', '__eq__', '__format__', '__getattribute__', '__hash__', '__init__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', '_parsed_fields', 'constants', 'fields', 'full_name', 'has_header', 'header_present', 'names', 'package', 'parsed_fields', 'short_name', 'text', 'types']




template <class ContainerAllocator>
struct IsFixedSize< ::path_follower::ControlTargetState_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::path_follower::ControlTargetState_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::path_follower::ControlTargetState_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::path_follower::ControlTargetState_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::path_follower::ControlTargetState_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::path_follower::ControlTargetState_<ContainerAllocator> const>
  : FalseType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::path_follower::ControlTargetState_<ContainerAllocator> >
{
  static const char* value()
  {
    return "59db037f6601c72a4efd8f7c6d69d33a";
  }

  static const char* value(const ::path_follower::ControlTargetState_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0x59db037f6601c72aULL;
  static const uint64_t static_value2 = 0x4efd8f7c6d69d33aULL;
};

template<class ContainerAllocator>
struct DataType< ::path_follower::ControlTargetState_<ContainerAllocator> >
{
  static const char* value()
  {
    return "path_follower/ControlTargetState";
  }

  static const char* value(const ::path_follower::ControlTargetState_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::path_follower::ControlTargetState_<ContainerAllocator> >
{
  static const char* value()
  {
    return "# This message contains the target values used for guiding the vehicle\n\
\n\
float32 closest_point_x     # [m] UTM easting of the point on the path which is currently closest to the vehicle\n\
float32 closest_point_y     # [m] UTM northing \n\
float32 look_ahead_point_x  # [m] UTM easting of the look ahead point (target point for steering controller)\n\
float32 look_ahead_point_y  # [m] UTM northing \n\
float32 look_ahead_distance # [m] distance from closest point to look ahead point, measured along the path\n\
float32 velocity            # [m/s] target velocity\n\
float32 steering_angle      # [rad] \n\
float32 steering_angle_norm # [-] normalized steering angle (-1, 1)\n\
float32 gas_brake_norm      # [-] normalized gas/brake position (-1, 1)\n\
";
  }

  static const char* value(const ::path_follower::ControlTargetState_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::path_follower::ControlTargetState_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.closest_point_x);
      stream.next(m.closest_point_y);
      stream.next(m.look_ahead_point_x);
      stream.next(m.look_ahead_point_y);
      stream.next(m.look_ahead_distance);
      stream.next(m.velocity);
      stream.next(m.steering_angle);
      stream.next(m.steering_angle_norm);
      stream.next(m.gas_brake_norm);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct ControlTargetState_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::path_follower::ControlTargetState_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::path_follower::ControlTargetState_<ContainerAllocator>& v)
  {
    s << indent << "closest_point_x: ";
    Printer<float>::stream(s, indent + "  ", v.closest_point_x);
    s << indent << "closest_point_y: ";
    Printer<float>::stream(s, indent + "  ", v.closest_point_y);
    s << indent << "look_ahead_point_x: ";
    Printer<float>::stream(s, indent + "  ", v.look_ahead_point_x);
    s << indent << "look_ahead_point_y: ";
    Printer<float>::stream(s, indent + "  ", v.look_ahead_point_y);
    s << indent << "look_ahead_distance: ";
    Printer<float>::stream(s, indent + "  ", v.look_ahead_distance);
    s << indent << "velocity: ";
    Printer<float>::stream(s, indent + "  ", v.velocity);
    s << indent << "steering_angle: ";
    Printer<float>::stream(s, indent + "  ", v.steering_angle);
    s << indent << "steering_angle_norm: ";
    Printer<float>::stream(s, indent + "  ", v.steering_angle_norm);
    s << indent << "gas_brake_norm: ";
    Printer<float>::stream(s, indent + "  ", v.gas_brake_norm);
  }
};

} // namespace message_operations
} // namespace ros

#endif // PATH_FOLLOWER_MESSAGE_CONTROLTARGETSTATE_H
