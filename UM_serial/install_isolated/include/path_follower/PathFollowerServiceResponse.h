// Generated by gencpp from file path_follower/PathFollowerServiceResponse.msg
// DO NOT EDIT!


#ifndef PATH_FOLLOWER_MESSAGE_PATHFOLLOWERSERVICERESPONSE_H
#define PATH_FOLLOWER_MESSAGE_PATHFOLLOWERSERVICERESPONSE_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>


namespace path_follower
{
template <class ContainerAllocator>
struct PathFollowerServiceResponse_
{
  typedef PathFollowerServiceResponse_<ContainerAllocator> Type;

  PathFollowerServiceResponse_()
    : reply(0)
    , str_reply()
    , float_reply(0.0)
    , int_reply(0)  {
    }
  PathFollowerServiceResponse_(const ContainerAllocator& _alloc)
    : reply(0)
    , str_reply(_alloc)
    , float_reply(0.0)
    , int_reply(0)  {
  (void)_alloc;
    }



   typedef uint8_t _reply_type;
  _reply_type reply;

   typedef std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other >  _str_reply_type;
  _str_reply_type str_reply;

   typedef float _float_reply_type;
  _float_reply_type float_reply;

   typedef int32_t _int_reply_type;
  _int_reply_type int_reply;





  typedef boost::shared_ptr< ::path_follower::PathFollowerServiceResponse_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::path_follower::PathFollowerServiceResponse_<ContainerAllocator> const> ConstPtr;

}; // struct PathFollowerServiceResponse_

typedef ::path_follower::PathFollowerServiceResponse_<std::allocator<void> > PathFollowerServiceResponse;

typedef boost::shared_ptr< ::path_follower::PathFollowerServiceResponse > PathFollowerServiceResponsePtr;
typedef boost::shared_ptr< ::path_follower::PathFollowerServiceResponse const> PathFollowerServiceResponseConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::path_follower::PathFollowerServiceResponse_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::path_follower::PathFollowerServiceResponse_<ContainerAllocator> >::stream(s, "", v);
return s;
}

} // namespace path_follower

namespace ros
{
namespace message_traits
{



// BOOLTRAITS {'IsFixedSize': False, 'IsMessage': True, 'HasHeader': False}
// {'std_msgs': ['/opt/ros/kinetic/share/std_msgs/cmake/../msg'], 'path_follower': ['/home/bplus/UM_serial/src/path_follower/path_follower_node/msg']}

// !!!!!!!!!!! ['__class__', '__delattr__', '__dict__', '__doc__', '__eq__', '__format__', '__getattribute__', '__hash__', '__init__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', '_parsed_fields', 'constants', 'fields', 'full_name', 'has_header', 'header_present', 'names', 'package', 'parsed_fields', 'short_name', 'text', 'types']




template <class ContainerAllocator>
struct IsFixedSize< ::path_follower::PathFollowerServiceResponse_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::path_follower::PathFollowerServiceResponse_<ContainerAllocator> const>
  : FalseType
  { };

template <class ContainerAllocator>
struct IsMessage< ::path_follower::PathFollowerServiceResponse_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::path_follower::PathFollowerServiceResponse_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::path_follower::PathFollowerServiceResponse_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::path_follower::PathFollowerServiceResponse_<ContainerAllocator> const>
  : FalseType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::path_follower::PathFollowerServiceResponse_<ContainerAllocator> >
{
  static const char* value()
  {
    return "630ef74c676b7c37e4807cf0ca4b53a9";
  }

  static const char* value(const ::path_follower::PathFollowerServiceResponse_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0x630ef74c676b7c37ULL;
  static const uint64_t static_value2 = 0xe4807cf0ca4b53a9ULL;
};

template<class ContainerAllocator>
struct DataType< ::path_follower::PathFollowerServiceResponse_<ContainerAllocator> >
{
  static const char* value()
  {
    return "path_follower/PathFollowerServiceResponse";
  }

  static const char* value(const ::path_follower::PathFollowerServiceResponse_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::path_follower::PathFollowerServiceResponse_<ContainerAllocator> >
{
  static const char* value()
  {
    return "uint8 	reply\n\
string 	str_reply\n\
float32 float_reply\n\
int32 	int_reply\n\
\n\
\n\
";
  }

  static const char* value(const ::path_follower::PathFollowerServiceResponse_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::path_follower::PathFollowerServiceResponse_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.reply);
      stream.next(m.str_reply);
      stream.next(m.float_reply);
      stream.next(m.int_reply);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct PathFollowerServiceResponse_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::path_follower::PathFollowerServiceResponse_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::path_follower::PathFollowerServiceResponse_<ContainerAllocator>& v)
  {
    s << indent << "reply: ";
    Printer<uint8_t>::stream(s, indent + "  ", v.reply);
    s << indent << "str_reply: ";
    Printer<std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other > >::stream(s, indent + "  ", v.str_reply);
    s << indent << "float_reply: ";
    Printer<float>::stream(s, indent + "  ", v.float_reply);
    s << indent << "int_reply: ";
    Printer<int32_t>::stream(s, indent + "  ", v.int_reply);
  }
};

} // namespace message_operations
} // namespace ros

#endif // PATH_FOLLOWER_MESSAGE_PATHFOLLOWERSERVICERESPONSE_H
