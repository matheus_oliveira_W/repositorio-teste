#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/bplus/UM_serial/devel_isolated/oxts:/opt/ros/kinetic:/home/bplus/UM_serial/install_isolated"
export LD_LIBRARY_PATH="/opt/ros/kinetic/lib:/opt/ros/kinetic/lib/x86_64-linux-gnu:/home/bplus/UM_serial/install_isolated/lib:/home/bplus/UM_serial/install_isolated/lib/x86_64-linux-gnu"
export PATH="/opt/ros/kinetic/bin:/home/bplus/UM_serial/install_isolated/bin:/home/bplus/bin:/home/bplus/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin"
export PKG_CONFIG_PATH="/opt/ros/kinetic/lib/pkgconfig:/opt/ros/kinetic/lib/x86_64-linux-gnu/pkgconfig:/home/bplus/UM_serial/install_isolated/lib/pkgconfig:/home/bplus/UM_serial/install_isolated/lib/x86_64-linux-gnu/pkgconfig"
export PYTHONPATH="/opt/ros/kinetic/lib/python2.7/dist-packages:/home/bplus/UM_serial/install_isolated/lib/python2.7/dist-packages"
export ROSLISP_PACKAGE_DIRECTORIES="/home/bplus/UM_serial/devel_isolated/oxts/share/common-lisp"
export ROS_PACKAGE_PATH="/home/bplus/UM_serial/src/oxts:$ROS_PACKAGE_PATH"