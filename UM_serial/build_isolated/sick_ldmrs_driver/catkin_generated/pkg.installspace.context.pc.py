# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/bplus/UM_serial/install_isolated/include;/usr/include".split(';') if "/home/bplus/UM_serial/install_isolated/include;/usr/include" != "" else []
PROJECT_CATKIN_DEPENDS = "roscpp;sensor_msgs;diagnostic_updater;dynamic_reconfigure;pcl_conversions;sick_ldmrs_msgs".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "/usr/lib/x86_64-linux-gnu/libboost_system.so".split(';') if "/usr/lib/x86_64-linux-gnu/libboost_system.so" != "" else []
PROJECT_NAME = "sick_ldmrs_driver"
PROJECT_SPACE_DIR = "/home/bplus/UM_serial/install_isolated"
PROJECT_VERSION = "0.0.0"
