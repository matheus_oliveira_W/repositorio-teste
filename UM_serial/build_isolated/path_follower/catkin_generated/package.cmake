set(_CATKIN_CURRENT_PACKAGE "path_follower")
set(path_follower_VERSION "0.0.1")
set(path_follower_MAINTAINER "Luiza Bartels <luiza.bartels@uem.com.br>")
set(path_follower_PACKAGE_FORMAT "2")
set(path_follower_BUILD_DEPENDS "rospy" "rqt_gui" "rqt_gui_py" "roscpp" "roslib" "message_runtime" "message_generation" "std_msgs" "sensor_msgs" "geometry_msgs" "sick_ldmrs_msgs")
set(path_follower_BUILD_EXPORT_DEPENDS "Geographic" "rospy" "rqt_gui" "rqt_gui_py" "roscpp" "roslib" "message_runtime" "message_generation" "std_msgs" "sensor_msgs" "geometry_msgs" "sick_ldmrs_msgs")
set(path_follower_BUILDTOOL_DEPENDS "catkin")
set(path_follower_BUILDTOOL_EXPORT_DEPENDS )
set(path_follower_EXEC_DEPENDS "rospy" "rqt_gui" "rqt_gui_py" "roscpp" "roslib" "message_runtime" "message_generation" "std_msgs" "sensor_msgs" "geometry_msgs" "sick_ldmrs_msgs")
set(path_follower_RUN_DEPENDS "rospy" "rqt_gui" "rqt_gui_py" "roscpp" "roslib" "message_runtime" "message_generation" "std_msgs" "sensor_msgs" "geometry_msgs" "sick_ldmrs_msgs" "Geographic")
set(path_follower_TEST_DEPENDS )
set(path_follower_DOC_DEPENDS )
set(path_follower_URL_WEBSITE "")
set(path_follower_URL_BUGTRACKER "")
set(path_follower_URL_REPOSITORY "")
set(path_follower_DEPRECATED "")