#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "$0" )" && pwd )"

PATH_FOLLOWER_CMD="$SCRIPT_DIR/shortcuts/run_path_follower.sh"
SENSORS_CMD="$SCRIPT_DIR/shortcuts/run_sensors.sh"

cd $SCRIPT_DIR

sudo apt-get install terminator
sudo apt-get install sox # play audio files
sudo apt-get install redshift # color the screen red
sudo apt-get install xbindkeys # keyboard shortcuts

# make alert scripts executable
chmod +x alert/*.sh

# install alert scripts
rm -rf ~/.alert
mkdir ~/.alert
cp -a alert/. ~/.alert/

# make path follower run-script executable
chmod +x shortcuts/run_path_follower.sh

# install shortcuts for xbindkeys
cp shortcuts/.xbindkeysrc ~/.xbindkeysrc

# add shortcut to start the path follower
echo \"$PATH_FOLLOWER_CMD\" >> ~/.xbindkeysrc 
echo "  F12" >> ~/.xbindkeysrc 
cp shortcuts/.gnomerc ~/.gnomerc

# restart xbindkeys
killall xbindkeys
xbindkeys

# install dock shortcut
cp shortcuts/path_follower.desktop ~/.local/share/applications/path_follower.desktop
echo "Icon=$SCRIPT_DIR/../../src/path_follower/path_follower_gui/resource/icon.png" >> ~/.local/share/applications/path_follower.desktop
echo "Exec=$PATH_FOLLOWER_CMD" >> ~/.local/share/applications/path_follower.desktop
chmod +x ~/.local/share/applications/path_follower.desktop

# install dock shortcut
cp shortcuts/sensors.desktop ~/.local/share/applications/sensors.desktop
echo "Icon=$SCRIPT_DIR/../../src/path_follower/path_follower_gui/resource/visibility.png" >> ~/.local/share/applications/sensors.desktop
echo "Exec=$SENSORS_CMD" >> ~/.local/share/applications/sensors.desktop
chmod +x ~/.local/share/applications/sensors.desktop


#sudo apt-get install ros-kinetic-joy # ps4 controller










