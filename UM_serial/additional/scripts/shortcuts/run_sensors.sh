#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "$0" )" && pwd )"

START_LIDAR_CMD="cd $SCRIPT_DIR ; cd ..; cd ..; cd ..; source ~/.bashrc; source install_isolated/setup.bash; roslaunch sick_ldmrs_driver sick_ldmrs_node.launch; exec bash"
START_RADAR_CMD="cd $SCRIPT_DIR ; cd ..; cd ..; cd ..; source ~/.bashrc; source install_isolated/setup.bash; roslaunch delphi_esr delphi_esr.launch; exec bash"

# ------------------------------------------------------------------------------

#ROS_RUNNING=false
LIDAR_RUNNING=false
RADAR_RUNNING=false

NODE_LIST="$(cd $SCRIPT_DIR ; cd ..; cd ..; cd ..; source ~/.bashrc; source install/setup.bash; yes | rosnode cleanup; rosnode list)"

if [[ ${NODE_LIST} == *"lidar_node"* ]]; then
  LIDAR_RUNNING=true 
fi

if [[ ${NODE_LIST} == *"radar_node"* ]]; then
  RADAR_RUNNING=true 
fi

# ------------------------------------------------------------------------------

if [ "$LIDAR_RUNNING" = false ]; then
  terminator --geometry=850x700+100+150 -x bash -c "$START_LIDAR_CMD" &
  START_RADAR_CMD="sleep 2; $START_RADAR_CMD"
fi

if [ "$RADAR_RUNNING" = false ]; then
  terminator --geometry=850x700+1000+150 -x bash -c "$START_RADAR_CMD" &
fi














