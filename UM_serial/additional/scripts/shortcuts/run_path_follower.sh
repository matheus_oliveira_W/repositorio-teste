#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "$0" )" && pwd )"

START_PARAVAN_CMD="cd $SCRIPT_DIR ; cd ..; cd ..; cd ..; source ~/.bashrc; source install_isolated/setup.bash; roslaunch uem uem.launch; exec bash"
START_PATH_FOLLOWER_CMD="cd $SCRIPT_DIR ; cd ..; cd ..; cd ..; source ~/.bashrc; source install_isolated/setup.bash; roslaunch path_follower path_follower.launch; exec bash"

START_RC_CMD="cd $SCRIPT_DIR ; cd ..; cd ..; cd ..; source ~/.bashrc; source install_isolated/setup.bash; roslaunch remote_control remote_control.launch; exec bash"

# ------------------------------------------------------------------------------

#ROS_RUNNING=false
PARAVAN_RUNNING=false
PATH_FOLLOWER_RUNNING=false
RC_RUNNING=false

NODE_LIST="$(cd $SCRIPT_DIR ; cd ..; cd ..; cd ..; source ~/.bashrc; source install_isolated/setup.bash; yes | rosnode cleanup; rosnode list)"

if [[ ${NODE_LIST} == *"paravan_node"* ]]; then
  PARAVAN_RUNNING=true 
fi

if [[ ${NODE_LIST} == *"path_follower_node"* ]]; then
  PATH_FOLLOWER_RUNNING=true 
fi

if [[ ${NODE_LIST} == *"remote_control"* ]]; then
  RC_RUNNING=true 
fi

# ------------------------------------------------------------------------------

if [ "$PARAVAN_RUNNING" = false ]; then
  terminator --geometry=850x700+100+150 -x bash -c "sleep 5; $START_PARAVAN_CMD" &
  START_PATH_FOLLOWER_CMD="sleep 5; $START_PATH_FOLLOWER_CMD" &
  START_RC_CMD="sleep 5; $START_RC_CMD" 
fi

if [ "$PATH_FOLLOWER_RUNNING" = false ]; then
  terminator --geometry=850x700+1000+150 -x bash -c "$START_PATH_FOLLOWER_CMD" &
fi

if [ "$RC_RUNNING" = false ]; then
  terminator --geometry=850x700+1000+150 -x bash -c "$START_RC_CMD"
fi














