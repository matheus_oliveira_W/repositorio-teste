# ELECTRONICS ACTUATORS SERIAL INTERFACE ##

Original navigation code with a serial library to control electronics actuators.

This project uses the Serial library develop by WJWWood.
[Check the documentation here.](https://github.com/wjwwood/serial)

- Run **sudo apt-get install ros-kinetic-serial** to get this library installed into your ROS distro.

## GETTING STARTED

1. If you have the original device attached, or if you have some physical devices to develop and/or test this project, run the following command to list the USB devices.
````
ls -l /dev/ttyUSB*
````
2. If everything is correct, it will list every USB port, but probably you won't be able to read or write on them. To make it read and writable, run the following command. **X is the listed port's number**
````
sudo chmod a+rw /dev/ttyUSBX 
````
3. If you're not attached to the original Emanuel's electronic device or if you have any physical device to develop or test this project, run the following command.
````
socat -d -d pty,raw,echo=0 pty,raw,echo=0
````
It will create two virtual ports. Use one to read and the other to write. Check what is being write by running the folowing on a new terminal. **XX are the numbers of one of your virtual ports**
````
cat -v < /dev/pts/XX 
````
4. Run the following command if you want to write messages on a specific port.
````
echo -ne 'ERROR,0\r\n' > /dev/pts/XX
````

## INSTALLATION
This is already a ROS kinetic workspace.

1. Clone this repository on your home location. cd into the folder and run the following commands line:
````
catkin_make install
````
````
source install/setup.bash
````